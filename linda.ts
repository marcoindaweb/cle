/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import {set} from '@linda/core/configuration'
import {load, start} from '@linda/core'
import {BASE_DIRECTORY} from '@linda/core/fs/const'
import {LOG_LEVEL} from '@linda/core/log/const'
import {SILLY} from '@linda/core/log'
import {DB_URI} from '@linda/database'

//i16RwMOv7hAjpdIm
set(BASE_DIRECTORY, __dirname)
set(LOG_LEVEL, SILLY)
set(DB_URI, 'mongodb://root:password@localhost')
load('@linda/frontend')
load('@linda/database')
load('@linda/ui')
load('@linda/config')
load('@linda/admin')
load('@linda/config-ui')
load('@linda/fs')
load('@linda/i18n')
load('@linda/content-manager')
load('@linda/content-manager-ui')
load('@linda/modules-ui')
load('@linda/email')
load('@linda/auth')
load('cle')

start()
