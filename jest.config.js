/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

module.exports = () => {
    return {
        // collectCoverageFrom: ['src/**/*.{ts,tsx}', 'server/**/*.{ts,tsx}'],
        // collectCoverage: true,
        moduleFileExtensions: ['ts', 'js', 'tsx', 'jsx', 'graphql'],
        transform: {
            '^.+\\.(ts|tsx)$': 'ts-jest',
            '^.+\\.graphql$': 'jest-transform-graphql',
        },
        // testMatch: ['**/__tests__/**/*.(ts|js|tsx|jsx)', '**/?(*.)(spec|test).ts?(x)'],
        moduleNameMapper: {
            '\\.(css|less)$': 'identity-obj-proxy',
        },
        testMatch: ['**/?(*.)(spec|test).ts?(x)'],
        // testEnvironment: 'node',
        rootDir: __dirname,
    }
}
