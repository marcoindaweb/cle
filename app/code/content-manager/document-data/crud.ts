/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import {
  AggregationCursor,
  Collection,
  Cursor,
  DeleteWriteOpResultObject,
  FilterQuery,
  OptionalId,
  WithId
} from '@linda/database'
import { DocumentSchema } from '../types'
import { Validation } from '@linda/core/validation'
import { transaction } from '@linda/database/utils/session'
import { buildAggregationPipeline } from './query'
export type CollVal<C extends Collection> = C extends Collection<infer V> ? V : never

export type AnyDocument<T extends Record<string, any> = Record<string, any>> = { _id: any } & T

export const createOne =
  async <C extends Collection<AnyDocument>>(
    validate: Validation,
    coll: C,
    data: OptionalId<CollVal<C>>
  ): Promise<WithId<CollVal<C>>> => {
    validate(data)
    const res = await coll.insertOne(data)
    return {
      _id: res.insertedId,
      ...data
    }
  }

export const createMany =
  async <C extends Collection<AnyDocument>>(
    validate: Validation,
    coll: C,
    data: Array<OptionalId<CollVal<C>>>
  ): Promise<Array<WithId<CollVal<C>>>> => {
    validate(data)
    const res = await coll.insertMany(data)
    return data.map((val, index) => ({
      ...val,
      _id: res.insertedIds[index]
    }))
  }

export const updateOne =
  async <C extends Collection<AnyDocument>>(
    validate: Validation,
    coll: C,
    data: WithId<CollVal<C>>
  ): Promise<WithId<CollVal<C>>> => {
    validate(data)
    await coll.updateOne({ _id: data._id }, { $set: data })
    return data
  }

export const updateMany =
  async <C extends Collection<AnyDocument>>(
    validate: Validation,
    coll: C,
    data: Array<WithId<CollVal<C>>>
  ): Promise<Array<WithId<CollVal<C>>>> => {
    validate(data)
    await coll.bulkWrite([
      ...data.map(v => ({
        updateOne: {
          filter: { _id: v._id },
          update: { $set: v }
        }
      }))
    ])
    return data
  }

export const findOne =
  async <C extends Collection<AnyDocument>>(
    coll: C,
    doc: DocumentSchema,
    filter: FilterQuery<CollVal<C>>
  ): Promise<CollVal<C> | null> => {
    const res = await coll.aggregate<CollVal<C>>(buildAggregationPipeline(doc, filter)).toArray()
    return res[0] ?? null
  }

export const findMany =
  <C extends Collection<AnyDocument>>(
    coll: C,
    doc: DocumentSchema,
    filter: FilterQuery<CollVal<C>>
  ): AggregationCursor<CollVal<C>> => {
    return coll.aggregate(buildAggregationPipeline(doc, filter))
  }

export const deleteOne =
  async <C extends Collection<AnyDocument>>(
    coll: C,
    filter: FilterQuery<CollVal<C>>
  ): Promise<DeleteWriteOpResultObject> => {
    return await coll.deleteOne(filter)
  }

export const deleteMany =
  async <C extends Collection<AnyDocument>>(
    coll: C,
    filter: FilterQuery<CollVal<C>>
  ): Promise<DeleteWriteOpResultObject> => {
    return await coll.deleteMany(filter)
  }

export const count =
  async <C extends Collection<AnyDocument>>(
    coll: C,
    doc: DocumentSchema,
    filter: FilterQuery<CollVal<C>>
  ): Promise<number> => {
    const res = await coll.aggregate<{ count: number }>([
      ...buildAggregationPipeline(doc, filter),
      { $count: 'count' }
    ]).toArray()
    return res[0].count
  }
