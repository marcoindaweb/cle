/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { AggregationCursor, Collection, Cursor, FilterQuery } from '@linda/database'

type CollectionType<T extends Collection> = T extends Collection<infer R> ? R : never

export type createOne<V extends Collection> =
  (coll: V, data: Omit<CollectionType<V>, '_id'>) => Promise<CollectionType<V>>
export type createMany<V extends Collection> =
  (coll: V, data: Array<Omit<CollectionType<V>, '_id'>>) => Promise<Array<CollectionType<V>>>
export type updateMany<V extends Collection> =
  (coll: V, data: Array<CollectionType<V>>) => Promise<Array<CollectionType<V>>>
export type updateOne<V extends Collection> =
  (coll: V, data: CollectionType<V>) => Promise<CollectionType<V>>
export type findOne<V extends Collection> =
  (coll: V, filter: FilterQuery<CollectionType<V>>) => Promise<CollectionType<V> | null>
export type findMany<V extends Collection> =
  (coll: V, filter: FilterQuery<CollectionType<V>>) => Promise<Cursor<CollectionType<V>>>
export type deleteOne<V extends Collection> =
  (coll: V, filter: FilterQuery<CollectionType<V>>) => Promise<void>
export type deleteMany<V extends Collection> =
  (coll: V, filter: FilterQuery<CollectionType<V>>) => Promise<void>
export type count<V extends Collection> =
  (coll: V, filter: FilterQuery<CollectionType<V>>) => Promise<number>
export interface Repository<V extends Collection> {
  createOne: (data: Omit<CollectionType<V>, '_id'>) => Promise<CollectionType<V>>
  createMany: (data: Array<Omit<CollectionType<V>, '_id'>>) => Promise<Array<CollectionType<V>>>
  updateOne: (data: CollectionType<V>) => Promise<CollectionType<V>>
  updateMany: (data: Array<CollectionType<V>>) => Promise<Array<CollectionType<V>>>
  deleteOne: (filter: FilterQuery<CollectionType<V>>) => Promise<void>
  deleteMany: (filter: FilterQuery<CollectionType<V>>) => Promise<void>
  count: (filter: FilterQuery<CollectionType<V>>) => Promise<number>
  findOne: (filter: FilterQuery<CollectionType<V>>) => Promise<CollectionType<V> | null>
  findMany: (filter: FilterQuery<CollectionType<V>>) => AggregationCursor<CollectionType<V>>
}
