/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { FilterQuery } from '@linda/database'
import {
  DocumentSchema,
  isArrayProperty,
  isObjectProperty,
  isRelProperty,
  PropertiesObject,
  Property,
  PropertyType,
  RelationProperty
} from '../types'
import { decodeRef, getNestedPropertyOfType, getOnlyPropertiesWithRelation } from '../documents-schema/data/calculations'
import { getDocCollectionName } from '../documents-schema/code-generation/calculations'
import empty from 'lodash/isEmpty'

export const getUsedRelationPropertiesInQuery =
  (doc: DocumentSchema, query: FilterQuery<any>): PropertiesObject => {
    const used: PropertiesObject = {}
    const relProps = getOnlyPropertiesWithRelation(doc.properties)
    for (const key of Object.keys(query)) {
      const prop = getCorrespondingPropByKey(relProps, key)
      if (prop !== undefined) {
        used[key.split('.')[0]] = prop
      }
    }
    return used
  }

export const getCorrespondingPropByKey =
  (relProps: PropertiesObject, key: string): Property | undefined => {
    const parts = key.split('.')
    const prop = relProps[parts[0]]
    if (prop !== undefined && isRelProperty(prop)) {
      return prop
    } else if (prop !== undefined && (isArrayProperty(prop) || isObjectProperty(prop))) {
      parts.splice(0, 1)
      const res = getCorrespondingPropByKey(prop.document, parts.join('.'))
      if (res !== undefined) {
        return prop
      }
    }
  }

export const buildLookupAggregation = (doc: DocumentSchema, query: FilterQuery<any>): object[] => {
  const lookups: object[] = []
  const relProps = getUsedRelationPropertiesInQuery(doc, query)
  for (const [name, prop] of Object.entries(relProps)) {
    const res = getNestedPropertyOfType<RelationProperty>(prop, PropertyType.relation, name)
    if (res === undefined) {
      throw new Error('Property don\'t have a nested relation property')
    }
    const [relProp, key] = res
    const isInverse = relProp.refProperty !== undefined // Is trying to get from the inverse side of the relation
    lookups.push({
      $lookup: {
        from: getDocCollectionName(decodeRef(relProp.ref)),
        localField: isInverse ? '_id' : key,
        foreignField: relProp.refProperty ?? '_id',
        as: key
      }
    })
    // If it isn't an owning many-relation then unwind it
    if (relProp.many !== true && !isInverse) {
      lookups.push({ $unwind: `$${key}` })
    }
  }
  return lookups
}

export const buildAggregationPipeline = (doc: DocumentSchema, query: FilterQuery<any>): object[] => {
  const lookups = buildLookupAggregation(doc, query)
  return [
    ...lookups,
    { $match: query }
  ]
}
