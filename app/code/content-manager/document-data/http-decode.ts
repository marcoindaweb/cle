/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { DocOverHttp, OriginalDoc } from '../types'
import { ObjectID } from '@linda/database'

export const decodeDocumentOverHttp = <T extends DocOverHttp<any>>(data: T): OriginalDoc<T> => {
  // eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
  // @ts-ignore
  const o: OriginalDoc<T> = {}
  for (const [key, val] of Object.entries(data)) {
    if (ObjectID.isValid(val)) {
      o[key] = ObjectID.createFromHexString(val)
    } else if (Array.isArray(val)) {
      o[key] = val.map(v => decodeDocumentOverHttp(v))
    } else if (typeof val === 'object') {
      o[key] = decodeDocumentOverHttp(val)
    } else {
      o[key] = val
    }
  }
  return o
}
