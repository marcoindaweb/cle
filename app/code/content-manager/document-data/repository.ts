/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import {
  collection,
  Collection,
  Cursor,
  DeleteWriteOpResultObject,
  FilterQuery,
  OptionalId,
  WithId
} from '@linda/database'
import {
  AnyDocument,
  CollVal,
  count,
  createMany,
  createOne,
  deleteMany,
  deleteOne,
  findMany,
  findOne,
  updateMany,
  updateOne
} from './crud'
import { DocumentSchema, ReducedDocument } from '../types'
import { makeValidatorForDocument } from '../documents-schema/validation'
import { getDocuments } from '../documents-schema/data/state'
import { eachItem } from '@linda/core/validation'
import { getDocCollectionName } from '../documents-schema/code-generation/calculations'
import { findDocument } from '../documents-schema'

interface Repository<C extends Collection<AnyDocument>> {
  createOne: (data: OptionalId<CollVal<C>>) => Promise<WithId<CollVal<C>>>
  createMany: (data: Array<OptionalId<CollVal<C>>>) => Promise<Array<WithId<CollVal<C>>>>
  updateOne: (data: WithId<CollVal<C>>) => Promise<WithId<CollVal<C>>>
  updateMany: (data: Array<WithId<CollVal<C>>>) => Promise<Array<WithId<CollVal<C>>>>
  findMany: (filter: FilterQuery<CollVal<C>>) => Cursor<CollVal<C>>
  findOne: (data: FilterQuery<CollVal<C>>) => Promise<CollVal<C> | null>
  count: (filter: FilterQuery<CollVal<C>>) => Promise<number>
  deleteMany: (filter: FilterQuery<CollVal<C>>) => Promise<DeleteWriteOpResultObject>
  deleteOne: (data: FilterQuery<CollVal<C>>) => Promise<DeleteWriteOpResultObject>
}

export const makeRepository =
  <D extends DocumentSchema, C extends Collection>(doc: D, coll: C): Repository<C> => {
    const docs = getDocuments()
    const createOneValidation = makeValidatorForDocument(doc, { optionalId: true }, docs)
    const createManyValidation = eachItem(createOneValidation)
    const updateOneValidation = makeValidatorForDocument(doc, { optionalId: false }, docs)
    const updateManyValidation = eachItem(updateOneValidation)
    return {
      createOne: data => createOne(createOneValidation, coll, data),
      createMany: data => createMany(createManyValidation, coll, data),
      updateOne: data => updateOne(updateOneValidation, coll, data),
      updateMany: data => updateMany(updateManyValidation, coll, data),
      findMany: filter => findMany(coll, doc, filter),
      findOne: filter => findOne(coll, doc, filter),
      count: filter => count(coll, doc, filter),
      deleteMany: filter => deleteMany(coll, filter),
      deleteOne: filter => deleteOne(coll, filter)
    }
  }

export const makeRepositoryFromReducedDocument = (doc: ReducedDocument): Repository<any> => {
  const collName = getDocCollectionName(doc)
  const document = findDocument(doc)
  if (document === undefined) {
    throw new Error(`Unable to find document from ${JSON.stringify(doc)}`)
  }
  return makeRepository(document, collection(collName))
}
