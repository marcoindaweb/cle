/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { DocumentSchema, PropertyType } from '../../types'
import { makeRef } from '../../documents-schema/data/calculations'
import {
  buildLookupAggregation,
  getUsedRelationPropertiesInQuery
} from '../query'
import { getDocCollectionName } from '../../documents-schema/code-generation/calculations'

describe('query', () => {
  it('return used relations from the owning side', () => {
    const doc2: DocumentSchema = {
      name: 'doc2',
      module: 'doc2',
      properties: {
        _id: { type: PropertyType.objectId },
        name: { type: PropertyType.string },
        nested: { type: PropertyType.object, document: { value: { type: PropertyType.string } } }
      }
    }
    const doc1: DocumentSchema = {
      name: 'doc1',
      module: 'doc1',
      properties: {
        _id: { type: PropertyType.objectId },
        simple: { type: PropertyType.relation, ref: makeRef(doc2) },
        nested: {
          type: PropertyType.object,
          document: { relation: { type: PropertyType.relation, ref: makeRef(doc2) } }
        }
      }
    }
    expect(getUsedRelationPropertiesInQuery(doc1, {
      'simple.name': 'test'
    })).toEqual({ simple: doc1.properties.simple })
    expect(getUsedRelationPropertiesInQuery(doc1, {
      'simple.name': /test/
    })).toEqual({ simple: doc1.properties.simple })
    expect(getUsedRelationPropertiesInQuery(doc1, {
      'simple.name': { $in: ['test', 'other'] }
    })).toEqual({ simple: doc1.properties.simple })
    expect(getUsedRelationPropertiesInQuery(doc1, {
      'nested.relation.name': { $in: ['test', 'other'] }
    })).toEqual({ nested: doc1.properties.nested })
    expect(getUsedRelationPropertiesInQuery(doc1, {
      'simple.name': { $in: ['test', 'other'] },
      'nested.relation.name': { $in: ['test', 'other'] }
    })).toEqual({ simple: doc1.properties.simple, nested: doc1.properties.nested })
    expect(getUsedRelationPropertiesInQuery(doc1, {
      'simple.nested.value': { $in: ['test', 'other'] }
    })).toEqual({ simple: doc1.properties.simple })
  })

  it('return used relations from the inverse side', () => {
    const doc2: DocumentSchema = {
      name: 'doc2',
      module: 'doc2',
      properties: {
        _id: { type: PropertyType.objectId },
        name: { type: PropertyType.string },
        nested: { type: PropertyType.object, document: { value: { type: PropertyType.string } } },
        simpleInvRel: { type: PropertyType.relation, refProperty: 'simple', ref: 'doc1~~doc1' },
        nestedInvRel: {
          type: PropertyType.relation,
          refProperty: 'nested.relation',
          ref: 'doc1~~doc1'
        }
      }
    }
    expect(getUsedRelationPropertiesInQuery(doc2, {
      'simpleInvRel._id': 'test'
    })).toEqual({ simpleInvRel: doc2.properties.simpleInvRel })
    expect(getUsedRelationPropertiesInQuery(doc2, {
      'simpleInvRel._id': /test/
    })).toEqual({ simpleInvRel: doc2.properties.simpleInvRel })
    expect(getUsedRelationPropertiesInQuery(doc2, {
      'simpleInvRel._id': { $in: ['test', 'other'] }
    })).toEqual({ simpleInvRel: doc2.properties.simpleInvRel })
    expect(getUsedRelationPropertiesInQuery(doc2, {
      'nestedInvRel._id': { $in: ['test', 'other'] }
    })).toEqual({ nestedInvRel: doc2.properties.nestedInvRel })
    expect(getUsedRelationPropertiesInQuery(doc2, {
      'simpleInvRel._id': { $in: ['test', 'other'] },
      'nestedInvRel._id': { $in: ['test', 'other'] }
    })).toEqual({
      simpleInvRel: doc2.properties.simpleInvRel,
      nestedInvRel: doc2.properties.nestedInvRel
    })
    expect(getUsedRelationPropertiesInQuery(doc2, {
      'simpleInvRel._id': { $in: ['test', 'other'] }
    })).toEqual({ simpleInvRel: doc2.properties.simpleInvRel })
  })

  it('build $lookup aggregation from the owning side', () => {
    const doc2: DocumentSchema = {
      name: 'doc2',
      module: 'doc2',
      properties: {
        _id: { type: PropertyType.objectId },
        name: { type: PropertyType.string },
        nested: { type: PropertyType.object, document: { value: { type: PropertyType.string } } },
        simpleInvRel: { type: PropertyType.relation, refProperty: 'simple', ref: 'doc1~~doc1' },
        nestedInvRel: {
          type: PropertyType.relation,
          refProperty: 'nested.relation',
          ref: 'doc1~~doc1'
        }
      }
    }
    const doc1: DocumentSchema = {
      name: 'doc1',
      module: 'doc1',
      properties: {
        _id: { type: PropertyType.objectId },
        simple: { type: PropertyType.relation, ref: makeRef(doc2) },
        nested: {
          type: PropertyType.object,
          document: { relation: { type: PropertyType.relation, ref: makeRef(doc2), many: true } }
        }
      }
    }
    expect(buildLookupAggregation(doc1, { _id: 'a', 'simple.name': 'test' }))
      .toEqual([
        {
          $lookup: {
            from: getDocCollectionName(doc2),
            localField: 'simple',
            foreignField: '_id',
            as: 'simple'
          }
        },
        { $unwind: '$simple' }
      ])
    expect(buildLookupAggregation(doc1, { 'nested.relation.name': 'test' }))
      .toEqual([
        {
          $lookup: {
            from: getDocCollectionName(doc2),
            localField: 'nested.relation',
            foreignField: '_id',
            as: 'nested.relation'
          }
        }
      ])
  })

  it('build $lookup aggregation from the inverse side', () => {
    const doc2: DocumentSchema = {
      name: 'doc2',
      module: 'doc2',
      properties: {
        _id: { type: PropertyType.objectId },
        name: { type: PropertyType.string },
        nested: { type: PropertyType.object, document: { value: { type: PropertyType.string } } },
        simpleInvRel: { type: PropertyType.relation, refProperty: 'simple', ref: 'doc1~~doc1' },
        nestedInvRel: {
          type: PropertyType.relation,
          refProperty: 'nested.relation',
          ref: 'doc1~~doc1'
        }
      }
    }
    const doc1: DocumentSchema = {
      name: 'doc1',
      module: 'doc1',
      properties: {
        _id: { type: PropertyType.objectId },
        simple: { type: PropertyType.relation, ref: makeRef(doc2) },
        nested: {
          type: PropertyType.object,
          document: { relation: { type: PropertyType.relation, ref: makeRef(doc2), many: true } }
        }
      }
    }
    expect(buildLookupAggregation(doc2, { _id: 'a', 'simpleInvRel._id': 'test' }))
      .toEqual([
        {
          $lookup: {
            from: getDocCollectionName(doc1),
            localField: '_id',
            foreignField: 'simple',
            as: 'simpleInvRel'
          }
        }
      ])
    expect(buildLookupAggregation(doc2, { 'nestedInvRel._id': 'test' }))
      .toEqual([
        {
          $lookup: {
            from: getDocCollectionName(doc1),
            localField: '_id',
            foreignField: 'nested.relation',
            as: 'nestedInvRel'
          }
        }
      ])
  })
})
