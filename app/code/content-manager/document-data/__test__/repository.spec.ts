/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

/* eslint-disable @typescript-eslint/prefer-ts-expect-error */
import { DocumentSchema, PropertyType } from '../../types'
import { makeRepository } from '../index'
import { Collection, collection, ObjectID } from '@linda/database'
import { getDocCollectionName } from '../../documents-schema/code-generation/calculations'
import { clearTestingDB, useTestingDB } from '@linda/database/test'
import { start } from '@linda/core'
import { Repository } from '../types'
import { registerDocument } from '../../index'

const user: DocumentSchema = {
  name: 'user',
  module: 'auth',
  properties: {
    _id: { type: PropertyType.objectId },
    name: { type: PropertyType.string },
    roles: { type: PropertyType.relation, ref: 'auth~~role', many: true }
  }
}

interface User {
  _id: ObjectID
  name: string
  roles: Role[] | ObjectID[]
}

const role: DocumentSchema = {
  name: 'role',
  module: 'auth',
  properties: {
    _id: { type: PropertyType.objectId },
    name: { type: PropertyType.string },
    users: { type: PropertyType.relation, ref: 'auth~~user', refProperty: 'roles.role' }
  }
}

interface Role {
  _id: ObjectID
  name: string
  users?: User[] | ObjectID[]
}

let roleColl: Collection<Role>
let roleRepo: Repository<Collection<Role>>
let userColl: Collection<User>
let userRepo: Repository<Collection<User>>

describe('repository', () => {
  beforeAll(async () => {
    useTestingDB()
    await start()
    registerDocument(role)
    registerDocument(user)
    roleColl = collection<Role>(getDocCollectionName(role))
    // @ts-ignore
    roleRepo = makeRepository(role, roleColl)
    userColl = collection<User>(getDocCollectionName(user))
    // @ts-ignore
    userRepo = makeRepository(user, userColl)
  })

  afterEach(async () => {
    await clearTestingDB()
  })

  it('create one', async () => {
    const res = await userRepo.createOne({ name: 'ciao', roles: [] })
    const mRes = await userColl.findOne({ _id: res._id })
    expect(mRes?.name).toBeTruthy()
    expect(mRes?.name).toEqual(res.name)
  })

  it('create many', async () => {
    const res = await userRepo.createMany([
      { name: 'ciao', roles: [] },
      { name: 'ciao2', roles: [] }
    ])
    const mRes = await userColl.find({ _id: { $in: res.map(v => v._id) } }).toArray()
    expect(mRes[0]?.name).toBeTruthy()
    expect(mRes[1]?.name).toBeTruthy()
    expect(mRes[0]?.name).toEqual(res[0].name)
    expect(mRes[1]?.name).toEqual(res[1].name)
  })

  it('update one', async () => {
    const res = await userRepo.createOne({ name: 'ciao', roles: [] })
    const uRes = await userRepo.updateOne({ ...res, name: 'new-ciao' })
    const mRes = await userColl.findOne({ _id: res._id })
    expect(mRes?.name).toBeTruthy()
    expect(mRes?.name).toEqual(uRes.name)
  })

  it('update many', async () => {
    const res = await userRepo.createMany([
      { name: 'ciao', roles: [] },
      { name: 'ciao2', roles: [] }
    ])
    const uRes = await userRepo.updateMany([
      { ...res[0], name: 'ciao-new' },
      { ...res[1], name: 'ciao2-new' }
    ])
    const mRes = await userColl.find({ _id: { $in: res.map(v => v._id) } }).toArray()
    expect(mRes[0]?.name).toBeTruthy()
    expect(mRes[1]?.name).toBeTruthy()
    expect(mRes[0]?.name).toEqual(uRes[0].name)
    expect(mRes[1]?.name).toEqual(uRes[1].name)
  })

  it('delete one', async () => {
    const res = await userRepo.createOne({ name: 'ciao', roles: [] })
    await userRepo.deleteOne({ _id: res._id })
    const mRes = await userColl.findOne({ _id: res._id })
    expect(mRes).toBeNull()
  })

  it('delete many', async () => {
    const res = await userRepo.createMany([
      { name: 'ciao', roles: [] },
      { name: 'ciao2', roles: [] }
    ])
    const ids = res.map(v => v._id)
    await userRepo.deleteMany({ _id: { $in: ids } })
    const mRes = await userColl.find({ _id: { $in: ids } }).toArray()
    expect(mRes.length).toBe(0)
  })

  it('count', async () => {
    await userRepo.createMany([
      { name: 'ciao', roles: [] },
      { name: 'ciao2', roles: [] }
    ])
    const count1 = await userRepo.count({ name: /^c.+/ })
    const count2 = await userRepo.count({ name: 'ciao' })
    expect(count1).toBe(2)
    expect(count2).toBe(1)
  })

  it('find one', async () => {
    const created = await userRepo.createMany([
      { name: 'ciao', roles: [] },
      { name: 'ciao2', roles: [] }
    ])
    const found1 = await userRepo.findOne({ name: /2$/ })
    const found2 = await userRepo.findOne({ name: 'ciao' })
    expect(found1?._id).toEqual(created[1]._id)
    expect(found2?._id).toEqual(created[0]._id)
  })

  it('find many', async () => {
    const created = await userRepo.createMany([
      { name: 'ciao', roles: [] },
      { name: 'ciao2', roles: [] }
    ])
    const found = await userRepo.findMany({ name: /c.+/ }).toArray()
    expect(found.map(v => v._id)).toEqual(created.map(v => v._id))
  })

  it('find one by relation', async () => {
    const roles = await roleRepo.createMany([
      { name: 'role1' },
      { name: 'role2' }
    ])
    const created = await userRepo.createMany([
      { name: 'ciao', roles: [roles[0]._id] },
      { name: 'ciao2', roles: [roles[1]._id] }
    ])
    const found1 = await userRepo.findOne({ 'roles.name': 'role1' })
    const found2 = await userRepo.findOne({ 'roles.name': 'role2' })
    expect(found1?._id).toEqual(created[0]._id)
    expect(found2?._id).toEqual(created[1]._id)
  })

  it('find many by relation', async () => {
    const roles = await roleRepo.createMany([
      { name: 'role1' },
      { name: 'role2' }
    ])
    const created = await userRepo.createMany([
      { name: 'ciao1', roles: [roles[0]._id] },
      { name: 'ciao2', roles: roles.map(v => v._id) },
      { name: 'ciao3', roles: [roles[1]._id] },
      { name: 'ciao4', roles: [roles[0]._id] },
      { name: 'ciao5', roles: [roles[0]._id] }
    ])
    const found1 = await userRepo.findMany({ 'roles.name': 'role1' }).toArray()
    const found2 = await userRepo.findMany({ 'roles.name': 'role2' }).toArray()
    expect(found1.map(v => v._id))
      .toEqual([created[0]._id, created[1]._id, created[3]._id, created[4]._id])
    expect(found2.map(v => v._id))
      .toEqual([created[1]._id, created[2]._id])
  })

  it('count by relation', async () => {
    const roles = await roleRepo.createMany([
      { name: 'role1' },
      { name: 'role2' }
    ])
    await userRepo.createMany([
      { name: 'ciao1', roles: [roles[0]._id] },
      { name: 'ciao2', roles: roles.map(v => v._id) },
      { name: 'ciao3', roles: [roles[1]._id] },
      { name: 'ciao4', roles: [roles[0]._id] },
      { name: 'ciao5', roles: [roles[0]._id] }
    ])
    const count1 = await userRepo.count({ 'roles.name': 'role1' })
    const count2 = await userRepo.count({ 'roles.name': 'role2' })
    expect(count1).toEqual(4)
    expect(count2).toEqual(2)
  })
})
