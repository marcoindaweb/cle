/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { ajaxSelectorFamily } from '@linda/frontend/frontend/recoil'
import { AnyDocument } from '@linda/content-manager/document-data/crud'
import { ReducedDocument } from '@linda/content-manager/types'
import { ajax, AjaxError, fetchJson, RecoilState } from '@linda/frontend/frontend'
import { FilterQuery } from '@linda/database'
import { useCallback } from 'react'

const buildDocDataUri = (doc: ReducedDocument, ext: string): string =>
  `/content-manager/v1/documents/data/${doc.module}/${doc.name}/${ext}`

export const findOneDocsSelectorFamily = <T extends AnyDocument>(doc: ReducedDocument):
(query: FilterQuery<T>) => RecoilState<T | null | AjaxError> =>
  ajaxSelectorFamily<T | null | AjaxError, FilterQuery<T>>(
    `content-manager/find-one/${doc.module}/${doc.name}`,
    (query) => async () => {
      const res =
        await ajax<T>(buildDocDataUri(doc, `/find-one?query=${encodeURI(JSON.stringify(query))}`))
      if (res instanceof AjaxError) {
        if (res.response.status === 404) {
          return res
        } else {
          return null
        }
      } else {
        return await res.json()
      }
    }
  )

export const findManyDocsSelectorFamily = <T extends AnyDocument>(doc: ReducedDocument):
(query: FilterQuery<T>) => RecoilState<T[] | AjaxError> =>
  ajaxSelectorFamily<T[] | AjaxError, FilterQuery<T>>(
    `content-manager/find-many/${doc.module}/${doc.name}`,
    (query) => async () => await fetchJson(
      buildDocDataUri(doc, `/find-many?query=${encodeURI(JSON.stringify(query))}`)
    )
  )

export const countDocsSelectorFamily = <T extends AnyDocument>(doc: ReducedDocument):
(query: FilterQuery<T>) => RecoilState<T[] | AjaxError> =>
  ajaxSelectorFamily<T[] | AjaxError, FilterQuery<T>>(
    `content-manager/count/${doc.module}/${doc.name}`,
    (query) => async () => await fetchJson(
      buildDocDataUri(doc, `/count?query=${encodeURI(JSON.stringify(query))}`)
    )
  )

export const useUpdateManyDocs = <T extends AnyDocument>(doc: ReducedDocument):
(data: T[]) => Promise<T[] | AjaxError> =>
  useCallback(async (data: T[]) => {
    return await fetchJson<T[]>(
      buildDocDataUri(doc, '/update-many'),
      {
        method: 'PUT',
        body: JSON.stringify({ data })
      }
    )
  }, [doc])

export const useUpdateOneDoc = <T extends AnyDocument>(doc: ReducedDocument):
(data: T) => Promise<T | AjaxError> =>
  useCallback(async (data: T) => {
    return await fetchJson<T>(
      buildDocDataUri(doc, '/update-one'),
      {
        method: 'PUT',
        body: JSON.stringify({ data })
      }
    )
  }, [doc])

export const useCreateOneDoc = <T extends AnyDocument>(doc: ReducedDocument):
(data: Omit<T, '_id'>) => Promise<T | AjaxError> =>
  useCallback(async (data: T) => {
    return await fetchJson<T>(
      buildDocDataUri(doc, '/create-one'),
      {
        method: 'POST',
        body: JSON.stringify({ data })
      }
    )
  }, [doc])

export const useCreateManyDocs = <T extends AnyDocument>(doc: ReducedDocument):
(data: Array<Omit<T, '_id'>>) => Promise<T[] | AjaxError> =>
  useCallback(async (data: T[]) => {
    return await fetchJson<T[]>(
      buildDocDataUri(doc, '/create-many'),
      {
        method: 'POST',
        body: JSON.stringify({ data })
      }
    )
  }, [doc])

export const useDeleteOneDoc = <T extends AnyDocument>(doc: ReducedDocument):
(query: FilterQuery<T>) => Promise<true | AjaxError> =>
  useCallback(async (query: FilterQuery<T>) => {
    const res = await ajax<undefined>(buildDocDataUri(doc, '/delete-one'),
      {
        method: 'DELETE',
        body: JSON.stringify({ query })
      })
    if (res instanceof AjaxError) {
      return res
    } else {
      return true
    }
  }, [doc])

export const useDeleteManyDocs = <T extends AnyDocument>(doc: ReducedDocument):
(query: FilterQuery<T>) => Promise<true | AjaxError> =>
  useCallback(async (query: FilterQuery<T>) => {
    const res = await ajax<undefined>(buildDocDataUri(doc, '/delete-many'),
      {
        method: 'DELETE',
        body: JSON.stringify({ query })
      })
    if (res instanceof AjaxError) {
      return res
    } else {
      return true
    }
  }, [doc])
