/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import {hasKey, isEqualTo, merge, oneOf, Validation, whereAllKeys, whereKey, withMessage} from '@linda/core/validation'
import { PropertyType } from '../types'

// const hasType: Validation = value => {
//   return hasKey('type')(value)
// }
// const types = [
//   PropertyType.boolean,
//   PropertyType.number,
//   PropertyType.array,
//   PropertyType.relation,
//   PropertyType.string,
//   PropertyType.object,
//   PropertyType.file,
//   PropertyType.date,
//   PropertyType.objectId
// ]
const isPropertyOfType = (type: PropertyType): Validation => withMessage(
  whereKey('type', isEqualTo(type)),
  () => `Expected property of type ${type}`
)

export const isStringProperty = isPropertyOfType(PropertyType.string)
export const isNumberProperty = isPropertyOfType(PropertyType.number)
export const isBooleanProperty = isPropertyOfType(PropertyType.boolean)
export const isDateProperty = isPropertyOfType(PropertyType.date)
export const isFileProperty = isPropertyOfType(PropertyType.file)
export const isObjectIdProperty = isPropertyOfType(PropertyType.objectId)
export const isRelationProperty = merge(isPropertyOfType(PropertyType.relation), hasKey('ref'))
export const isProperty = (): Validation => withMessage(
  oneOf(
    isStringProperty,
    isNumberProperty,
    isBooleanProperty,
    isDateProperty,
    isFileProperty,
    isObjectIdProperty,
    isObjectProperty,
    isRelationProperty,
    isArrayProperty
  ), (v) => `Wrong property: ${v}`)
export const isObjectProperty = merge(
  isPropertyOfType(PropertyType.object),
  whereKey('document', whereAllKeys(isProperty()))
)
export const isArrayProperty = merge(
  isPropertyOfType(PropertyType.array),
  whereKey('document', whereAllKeys(isProperty()))
)

export const validateProperty = (v: any) => isProperty()(v)
