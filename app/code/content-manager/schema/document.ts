/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */
import {
  isString,
  match,
  merge,
  minLength,
  schema,
  whereAllKeys,
  withMessage
} from '@linda/core/validation'
import { validateProperty } from './property'

export const validateDocument = schema({
  name: merge(
    withMessage(isString, 'Name must be a string'),
    withMessage(
      match(/^[A-Z]{1}[a-zA-z_]+$/),
      'Name must start with a capital letter and can contain only letters and _'
    ),
    withMessage(
      minLength(4),
      'Name must be at least 4 characters long'
    )
  ),
  module: withMessage(isString, 'Module must be a string'),
  properties: whereAllKeys(validateProperty)
})
