/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { PropertyType } from '../../types'
import { validateDocument } from '../document'

describe('document validation', () => {
  it('validate a document', () => {
    const withoutName = {
      module: 'module',
      properties: {
        a: { type: PropertyType.string }
      }
    }
    const withoutModule = {
      name: 'Name',
      properties: {
        a: { type: PropertyType.string }
      }
    }
    const withInvalidName = {
      name: 'New Document',
      module: 'module',
      properties: {
        a: { type: PropertyType.string }
      }
    }
    const withoutProperties = {
      name: 'Name',
      module: 'module'
    }
    const withInvalidProperties = {
      name: 'Name',
      module: 'module',
      properties: {
        a: {}
      }
    }
    const validDocument = {
      name: 'Name',
      module: 'module',
      properties: {
        test: { type: PropertyType.string }
      }
    }
    expect(() => validateDocument(withoutName)).toThrow()
    expect(() => validateDocument(withoutModule)).toThrow()
    expect(() => validateDocument(withInvalidName)).toThrow()
    expect(() => validateDocument(withoutProperties)).toThrow()
    expect(() => validateDocument(withInvalidProperties)).toThrow()
    expect(() => validateDocument(validDocument)).not.toThrow()
  })
})
