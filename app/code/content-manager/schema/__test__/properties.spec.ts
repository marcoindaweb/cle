/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { property } from '../properties.old'
import { PropertyType } from '../../types'
import { isProperty, validateProperty } from '../property'

describe('properties validation', () => {
  it('must have a type', () => {
    const withoutType = {
      label: 'test'
    }
    const withType = {
      label: 'test',
      type: PropertyType.string
    }
    expect(() => validateProperty(withoutType)).toThrowError()
    expect(() => validateProperty(withType)).not.toThrowError()
  })

  it('validate relation property', () => {
    const withRef = {
      type: PropertyType.relation,
      ref: 'ref'
    }
    const withoutRef = {
      type: PropertyType.relation
    }
    expect(() => validateProperty(withoutRef)).toThrow()
    expect(() => validateProperty(withRef)).not.toThrow()
  })

  it('validate array property', () => {
    const withDoc = {
      type: PropertyType.array,
      document: { id: { type: PropertyType.string } }
    }
    const withInvalidDoc = {
      type: PropertyType.array,
      document: { id: {} }
    }
    const withoutDoc = {
      type: PropertyType.array
    }
    expect(() => validateProperty(withoutDoc)).toThrow()
    expect(() => validateProperty(withInvalidDoc)).toThrow()
    expect(() => validateProperty(withDoc)).not.toThrow()
  })
})
