/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Project } from 'ts-morph'
import { registerDocument } from '../index'
import { DocumentSchema } from '../types'
import { getDocumentPath } from './code-generation/calculations/fs'
import { remove } from '@linda/core/fs'
import { getDocuments, removeDocument } from './data/state'
import { generate } from './code-generation/actions'

export const createDocument = async (doc: DocumentSchema): Promise<void> => {
  const proj = new Project()
  await generate(proj, doc)
  registerDocument(doc)
}

export const updateDocument = async (doc: DocumentSchema): Promise<void> => {
  await createDocument(doc)
}

export const deleteDocument = async (doc: Omit<DocumentSchema, 'properties'>): Promise<void> => {
  const path = getDocumentPath(doc)
  await remove(path)
  removeDocument(doc)
}

export const findDocument = (doc: Omit<DocumentSchema, 'properties'>): DocumentSchema | undefined => {
  const docs = getDocuments()
  return docs.find(v => v.module === doc.module && v.name === doc.name)
}
