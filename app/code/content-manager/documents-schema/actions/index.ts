/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { DocumentSchema } from '../../types'
import { Project, SourceFile } from 'ts-morph'
import { remove } from '@linda/core/fs'
import { getDocumentPath } from '../code-generation/calculations/fs'
import { registerDocument } from '../../index'
import { generate } from '../code-generation/actions'

export const create = async (doc: DocumentSchema): Promise<void> => {
  const proj = new Project()
  await generate(proj, doc)
  registerDocument(doc)
}

export const deleteDocument = async (doc: DocumentSchema): Promise<void> => {
  await remove(getDocumentPath(doc))
}
