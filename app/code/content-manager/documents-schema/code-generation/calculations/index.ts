/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import {
  ArrayProperty,
  DocumentSchema, isArrayProperty, isObjectProperty, isRelProperty,
  ObjectProperty, PropertiesObject,
  Property,
  PropertyType,
  ReducedDocument,
  RelationProperty
} from '../../../types'
import { OptionalKind, PropertySignatureStructure, SourceFile, VariableDeclarationKind, WriterFunction } from 'ts-morph'
import { getRelationProperties, hasFileProperty } from './search'
import { decodeRef } from '../../data/calculations'
import { dirname, isModulePath, relative, relativeToFilePath, removeExt } from '@linda/core/fs/utils'

export type SchemaPathMap = Record<string, DocumentSchema>

export const writeSchemaDocumentTo = (
  doc: DocumentSchema, // Document schema
  schemaFile: SourceFile, // Source file where to write the schema to
  indexFile: SourceFile, // Source file where to import the written schema
  schemaMap: SchemaPathMap // A map of documents and their paths schema path
): [SourceFile, SourceFile, DocumentSchema] => {
  importDefinitionIntoModuleIndex(indexFile, schemaFile, doc)
  addImportDeclarations(schemaFile, doc, schemaMap)
  writeDocumentInterface(schemaFile, doc, schemaMap)
  writeDocumentDefinitionVariable(schemaFile, doc)
  writeDocumentCollectionVariable(schemaFile, doc)
  writeDocumentRepository(schemaFile, doc)
  writeDocumentRegistration(schemaFile, doc)
  return [schemaFile, indexFile, doc]
}

const hasImportWithModuleSpecifier = (source: SourceFile, specifier: string): boolean => {
  return source
    .getImportDeclaration(v => v.getModuleSpecifier().getLiteralValue() === specifier) !== undefined
}

export const importDefinitionIntoModuleIndex =
  (source: SourceFile, docSource: SourceFile, doc: DocumentSchema): void => {
    // Import the source into the module index
    const moduleSpecifier = './' + removeExt(
      relative(dirname(source.getFilePath()), docSource.getFilePath())
    )
    if (!hasImportWithModuleSpecifier(source, moduleSpecifier)) {
      source.addImportDeclaration({ moduleSpecifier })
    }
  }

export const addImportDeclarations =
  (source: SourceFile, doc: DocumentSchema, schemaMap: SchemaPathMap): SourceFile => {
    source.addImportDeclaration({
      namedImports: ['collection', 'ObjectID', 'Collection'],
      moduleSpecifier: '@linda/database'
    })
    source.addImportDeclaration({
      namedImports: ['registerDocument', 'makeRepository'],
      moduleSpecifier: '@linda/content-manager'
    })
    source.addImportDeclaration({
      namedImports: ['PropertyType', 'DocumentSchema'],
      moduleSpecifier: '@linda/content-manager/types'
    })
    if (hasFileProperty(doc)) {
      source.addImportDeclaration({ namedImports: ['IFile'], moduleSpecifier: '@linda/fs' })
    }
    getRelationProperties(doc).forEach(prop => addRelationImport(source, prop, schemaMap))
    return source
  }

export const getPathFromSchemaMap =
  (doc: ReducedDocument, schemaMap: SchemaPathMap): string | undefined => {
    for (const [path, { name, module }] of Object.entries(schemaMap)) {
      if (name === doc.name && module === doc.module) {
        return path
      }
    }
  }

export const getDocByRefFromSchemaMap =
  (ref: string, schemaMap: SchemaPathMap): DocumentSchema | undefined => {
    const val = getPathFromSchemaMap(decodeRef(ref), schemaMap)
    if (val !== undefined) {
      return schemaMap[val]
    }
  }

export const addRelationImport =
  (source: SourceFile, prop: RelationProperty, schemaMap: SchemaPathMap): void => {
    const relPath = getPathFromSchemaMap(decodeRef(prop.ref), schemaMap)
    if (relPath === undefined) {
      throw new Error(`Unable to find path for relation ${prop.ref}`)
    }
    const moduleSpecifier = isModulePath(relPath)
      ? relPath
      : relativeToFilePath(source.getFilePath(), relPath)
    const doc = schemaMap[relPath]
    const shouldWrite = !hasImportWithModuleSpecifier(source, moduleSpecifier)
    if (shouldWrite) {
      source.addImportDeclaration({ namedImports: [getDocInterfaceName(doc)], moduleSpecifier })
    }
  }

const writeDocumentInterface =
  (source: SourceFile, doc: DocumentSchema, schemaMap: SchemaPathMap): SourceFile => {
    source.addInterface({
      name: getDocInterfaceName(doc),
      properties: Object.entries(doc.properties)
        .map(([name, prop]) => generateProperty(name, prop, schemaMap)),
      isExported: true
    })
    return source
  }

const writeDocumentCollectionVariable = (source: SourceFile, doc: DocumentSchema): SourceFile => {
  source.addVariableStatement({
    declarationKind: VariableDeclarationKind.Const,
    isExported: true,
    declarations: [
      {
        name: getDocCollectionVariableName(doc),
        // eslint-disable-next-line max-len
        initializer: `(): Collection<${getDocInterfaceName(doc)}> => collection<${getDocInterfaceName(doc)}>("${getDocCollectionName(doc)}")`
      }
    ]
  })
  return source
}

const writeDocumentDefinitionVariable = (source: SourceFile, doc: DocumentSchema): SourceFile => {
  source.addVariableStatement({
    declarationKind: VariableDeclarationKind.Const,
    isExported: true,
    declarations: [{
      name: getDocSchemaName(doc),
      type: 'DocumentSchema',
      initializer: (writer) => {
        writer.write('{')
        writer.write(`name: '${doc.name}',`).newLine()
        writer.write(`module: '${doc.module}',`).newLine()
        writer.write('properties: {').newLine()
        for (const [name, p] of Object.entries(doc.properties)) {
          writer.write(`${name}: `)
          writeProperty(p)(writer)
          writer.write(',').newLine()
        }
        writer.write('}').newLine() // Close properties
        writer.write('}').newLine() // Close document
      }
    }]
  })
  return source
}
const writeProperty = (prop: Property): WriterFunction => (writer) => {
  writer
    .write('{')
    .newLine()
    .write(`type: ${getPropertyTypeName(prop.type)},`)
    .newLine()
  if (prop.optional === true) {
    writer.write('optional: true,').newLine()
  }
  if (isRelProperty(prop)) {
    writer.write(`ref: '${prop.ref}',`).newLine()
    if (prop.refProperty !== undefined) writer.write(`refProperty: '${prop.refProperty}',`).newLine()
    if (prop.many === true) writer.write('many: true,').newLine()
  }
  if (isArrayProperty(prop) || isObjectProperty(prop)) {
    writer.write('document: {').newLine()
    for (const [name, p] of Object.entries(prop.document)) {
      writer.write(`${name}: `)
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      writeProperty(p)(writer)
      writer.write(', ').newLine()
    }
    writer.write('},')
  }
  writer.write('}')
}

const writePropertyTypeAsStringOnPropertiesObject = (props: PropertiesObject): object => {
  const o = {}
  for (const [key, val] of Object.entries(props)) {
    if (isObjectProperty(val) || isArrayProperty(val)) {
      o[key] = {
        ...val,
        type: getPropertyTypeName(val.type),
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        document: writePropertyTypeAsStringOnPropertiesObject(val.document)
      }
    } else {
      o[key] = { ...val, type: getPropertyTypeName(val.type) }
    }
  }
  return o
}

const getPropertyTypeName = (p: PropertyType): string => {
  switch (p) {
    case PropertyType.boolean:
      return 'PropertyType.boolean'
    case PropertyType.relation:
      return 'PropertyType.relation'
    case PropertyType.objectId:
      return 'PropertyType.objectId'
    case PropertyType.object:
      return 'PropertyType.object'
    case PropertyType.file:
      return 'PropertyType.file'
    case PropertyType.array:
      return 'PropertyType.array'
    case PropertyType.string:
      return 'PropertyType.string'
    case PropertyType.number:
      return 'PropertyType.number'
    case PropertyType.date:
      return 'PropertyType.date'
  }
}

const writeDocumentRegistration = (source: SourceFile, doc: DocumentSchema): SourceFile => {
  source.addStatements(`registerDocument(${getDocSchemaName(doc)})`)
  return source
}

const writeDocumentRepository = (source: SourceFile, doc: DocumentSchema): SourceFile => {
  source.addVariableStatement({
    declarationKind: VariableDeclarationKind.Const,
    isExported: true,
    declarations: [{
      name: getDocRepositoryName(doc),
      initializer: `makeRepository(${getDocSchemaName(doc)},${getDocCollectionVariableName(doc)}())`
    }]
  })
  return source
}

export const getDocInterfaceName =
  (doc: Pick<DocumentSchema, 'name'>): string => doc.name
export const getDocSchemaName =
  (doc: Pick<DocumentSchema, 'name'>): string => `${getDocInterfaceName(doc)}Schema`
export const getDocCollectionName =
  // eslint-disable-next-line max-len
  (doc: Pick<DocumentSchema, 'name' | 'module'>): string => `${doc.module}__${getDocInterfaceName(doc)}`
export const getDocCollectionVariableName =
  (doc: Pick<DocumentSchema, 'name'>): string => `${getDocInterfaceName(doc)}Collection`
export const getDocRepositoryName =
  (doc: Pick<DocumentSchema, 'name'>): string => `${getDocInterfaceName(doc)}Repository`

export const generateProperty =
  (
    name: string,
    property: Property,
    schemaMap: SchemaPathMap
  ): OptionalKind<PropertySignatureStructure> => {
    const hasQuestionToken = isRelProperty(property) && property.refProperty !== undefined
      ? true
      : property.optional === true
    return {
      name,
      type: getPropertyType(property, schemaMap),
      hasQuestionToken
    }
  }

const getArrayPropertyType = (p: ArrayProperty, schemaMap: SchemaPathMap): string => {
  return `Array<{ ${getPropertiesAsString(p.document, schemaMap)} }>`
}

const getObjectPropertyType = (p: ObjectProperty, schemaMap: SchemaPathMap): string => {
  return `{ ${getPropertiesAsString(p.document, schemaMap)} }`
}

const getPropertiesAsString = (props: PropertiesObject, schemaMap: SchemaPathMap): string => {
  let o = ''
  Object.entries(props).forEach(([name, val]) => {
    o += `${name}${val.optional === true ? '?' : ''}: ${getPropertyType(val, schemaMap)}; `
  })
  return o
}

const getRelPropertyType = (p: RelationProperty, schemaMap: SchemaPathMap): string => {
  const doc = getDocByRefFromSchemaMap(p.ref, schemaMap)
  if (doc === undefined) {
    throw new Error(`Unable to find document for ref ${p.ref}`)
  }
  if (p.many === true || p.refProperty !== undefined) {
    return `${getDocInterfaceName(doc)}[] | ObjectID[]`
  } else {
    return `${getDocInterfaceName(doc)} | ObjectID`
  }
}

export const getPropertyType = (property: Property, schemaMap: SchemaPathMap): string => {
  switch (property.type) {
    case PropertyType.date:
      return 'Date'
    case PropertyType.number:
      return 'number'
    case PropertyType.string:
      return 'string'
    case PropertyType.file:
      return 'IFile | ObjectID'
    case PropertyType.objectId:
      return 'ObjectID'
    case PropertyType.boolean:
      return 'boolean'
    case PropertyType.array:
      return getArrayPropertyType(property, schemaMap)
    case PropertyType.object:
      return getObjectPropertyType(property, schemaMap)
    case PropertyType.relation:
      return getRelPropertyType(property, schemaMap)
  }
}
