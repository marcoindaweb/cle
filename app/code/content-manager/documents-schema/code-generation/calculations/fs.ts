/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */
import { DocumentSchema } from '../../../types'
import { join } from '@linda/core/fs/utils'

export const getDocumentPath = (doc: Omit<DocumentSchema, 'properties'>): string => {
  return join(doc.module, `documents/${doc.name}.ts`)
}
