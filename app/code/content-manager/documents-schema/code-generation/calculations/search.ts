/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { DocumentSchema, PropertyType, RelationProperty } from '../../../types'

export const hasFileProperty = (doc: DocumentSchema): boolean => {
  return Object.entries(doc.properties).some(([name, definition]) => definition.type === PropertyType.file)
}

export const getRelationProperties = (doc: DocumentSchema): RelationProperty[] => Object.entries(doc.properties)
  .reduce<RelationProperty[]>((a, [name, def]) => {
  if (def.type === PropertyType.relation) {
    a.push(def)
  }
  return a
}, [])
