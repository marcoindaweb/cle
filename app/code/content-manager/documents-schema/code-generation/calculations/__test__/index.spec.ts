/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

/* eslint-disable max-len */
import { Project } from 'ts-morph'
import { join } from '@linda/core/fs/utils'
import { writeSchemaDocumentTo } from '../index'
import { DocumentSchema, PropertyType } from '../../../../types'
import prettier from 'prettier'

describe('code generation', () => {
  it('generate document code', () => {
    const project = new Project()
    const indexSource = project.createSourceFile(join(__dirname, 'index.ts'))
    const docSource = project.createSourceFile(join(__dirname, 'documents/Doc.ts'))
    const relDoc: DocumentSchema = {
      name: 'RelDoc',
      module: '@test/module',
      properties: {
        _id: { type: PropertyType.objectId }
      }
    }
    const nearDoc: DocumentSchema = {
      name: 'NearDoc',
      module: '@module/new',
      properties: {
        _id: { type: PropertyType.objectId }
      }
    }
    const doc: DocumentSchema = {
      name: 'Doc',
      module: '@module/new',
      properties: {
        _id: { type: PropertyType.objectId },
        name: { type: PropertyType.string, optional: true },
        surname: { type: PropertyType.string },
        age: { type: PropertyType.number },
        oneRel: { type: PropertyType.relation, ref: '@test/module~~RelDoc' },
        maybeOneRel: { type: PropertyType.relation, ref: '@test/module~~RelDoc', optional: true },
        manyRel: { type: PropertyType.relation, ref: '@test/module~~RelDoc', many: true },
        maybeManyRel: {
          type: PropertyType.relation,
          ref: '@test/module~~RelDoc',
          many: true,
          optional: true
        },
        inverseOneRel: {
          type: PropertyType.relation,
          ref: '@test/module~~RelDoc',
          refProperty: 'rel'
        },
        object: {
          type: PropertyType.object,
          document: {
            key: { type: PropertyType.string },
            value: { type: PropertyType.string, optional: true }
          }
        },
        array: {
          type: PropertyType.array,
          document: {
            key: { type: PropertyType.string },
            value: { type: PropertyType.string }
          }
        }
      }
    }
    const [resultingSource, resultingIndex] = writeSchemaDocumentTo(
      doc,
      docSource,
      indexSource,
      { '@test/module/documents/relDoc': relDoc }
    )

    expect(prettier.format(resultingSource.getFullText(), { parser: 'typescript' }))
      .toEqual(prettier.format(`
      import { collection, ObjectID, Collection } from "@linda/database";
      import { registerDocument, makeRepository } from "@linda/content-manager";
      import { PropertyType, DocumentSchema } from "@linda/content-manager/types";
      import { RelDoc } from "@test/module/documents/relDoc";
      
      export interface Doc {
        _id: ObjectID
        name?: string
        surname: string
        age: number
        oneRel: RelDoc | ObjectID
        maybeOneRel?: RelDoc | ObjectID
        manyRel: RelDoc[] | ObjectID[]
        maybeManyRel?: RelDoc[] | ObjectID[]
        inverseOneRel?: RelDoc[] | ObjectID[]
        object: { key: string; value?: string; }
        array: Array<{ key: string, value: string }>
      }
      
      export const DocSchema: DocumentSchema = {
        name: 'Doc',
        module: '@module/new',
        properties: {
          _id: { 
            type: PropertyType.objectId 
          },
          name: { 
            type: PropertyType.string, 
            optional: true 
          },
          surname: { 
            type: PropertyType.string 
          },
          age: { 
            type: PropertyType.number 
          },
          oneRel: { 
            type: PropertyType.relation, 
            ref: '@test/module~~RelDoc' 
          },
          maybeOneRel: { 
            type: PropertyType.relation, 
            optional: true,
            ref: '@test/module~~RelDoc', 
          },
          manyRel: { 
            type: PropertyType.relation, 
            ref: '@test/module~~RelDoc', 
            many: true 
          },
          maybeManyRel: { 
            type: PropertyType.relation, 
            optional: true ,
            ref: '@test/module~~RelDoc', 
            many: true, 
          },
          inverseOneRel: {
            type: PropertyType.relation,
            ref: '@test/module~~RelDoc',
            refProperty: 'rel'
          },
          object: {
            type: PropertyType.object,
            document: {
              key: { 
                type: PropertyType.string 
              },
              value: { 
                type: PropertyType.string, 
                optional: true 
              }
            }
          },
          array: {
            type: PropertyType.array,
            document: {
              key: { 
                type: PropertyType.string 
              },
              value: { 
                type: PropertyType.string 
              }
            }
          }
        }
      }
      export const DocCollection = (): Collection<Doc> => collection<Doc>('@module/new__Doc')
      export const DocRepository = makeRepository(DocSchema, DocCollection())
      registerDocument(DocSchema)
    `, { parser: 'typescript' }))
    expect(prettier.format(resultingIndex.getFullText(), { parser: 'typescript' }))
      .toEqual(prettier.format(`
      import './documents/Doc'
    `, { parser: 'typescript' }))
  })
})
