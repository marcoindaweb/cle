/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Project, SourceFile } from 'ts-morph'
import { DocumentSchema } from '../../../types'
import { getDocumentPath } from '../calculations/fs'
import { SchemaPathMap, writeSchemaDocumentTo } from '../calculations'
import { getDocuments } from '../../data/state'
import { dirname, join } from '@linda/core/fs/utils'

export const buildSchemaPathMap = (): SchemaPathMap => {
  const docs = getDocuments()
  const o: SchemaPathMap = {}
  docs.forEach(doc => {
    o[getDocumentPath(doc)] = doc
  })
  return o
}

export const generate = async (project: Project, doc: DocumentSchema): Promise<void> => {
  const path = join(dirname(require.resolve(doc.module)), `documents/${doc.name}.ts`)

  // Get a new empty source file
  const sourceFile = project.createSourceFile(path, '', { overwrite: true })
  const indexPath = require.resolve(doc.module)
  const moduleIndex = project.addSourceFileAtPath(indexPath)

  if (moduleIndex === undefined) {
    throw new Error('Unable to find module index file')
  }
  const [source, index] = writeSchemaDocumentTo(doc, sourceFile, moduleIndex, buildSchemaPathMap())
  await Promise.all([
    source.save(),
    index.save()
  ])
}

