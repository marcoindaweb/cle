/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { DocumentSchema, PropertyType } from '../../../types'
import { makeValidatorForDocument } from '../index'
import { ObjectID } from '@linda/database'
import { makeRef } from '../../data/calculations'

describe('validation', () => {
  it('generate a validation for a document', () => {
    const doc: DocumentSchema = {
      name: 'test',
      module: 'test',
      properties: {
        _id: { type: PropertyType.objectId },
        name: { type: PropertyType.string }
      }
    }
    const validatorWithOptional = makeValidatorForDocument(doc, { optionalId: true }, [doc])
    const validatorWithoutOptional = makeValidatorForDocument(doc, { optionalId: false }, [doc])
    expect(() => validatorWithOptional({ name: 1 })).toThrow()
    expect(() => validatorWithOptional({ name: 'name' })).not.toThrow()
    expect(() => validatorWithOptional({ name: 'name', _id: 'id' })).toThrow()
    expect(() => validatorWithoutOptional({ name: 'name' })).toThrow()
    expect(() => validatorWithoutOptional({
      name: 'name',
      _id: ObjectID.createFromTime(0)
    })).not.toThrow()
    expect(() => validatorWithoutOptional({ name: 'name', _id: ObjectID.createFromTime(0).toHexString() })).not.toThrow()
    expect(() => validatorWithoutOptional({ name: 'name', _id: 'id' })).toThrow()
  })

  it('validate with optional id', () => {
    const doc: DocumentSchema = {
      name: 'test',
      module: 'test',
      properties: {
        _id: { type: PropertyType.objectId },
        name: { type: PropertyType.string }
      }
    }
    const validate = makeValidatorForDocument(doc, { optionalId: true }, [doc])
    expect(() => validate({ name: 'a' })).not.toThrow()
    expect(() => validate({ _id: 'a', name: 'a' })).toThrow()
    expect(() => validate({ _id: ObjectID.createFromTime(0), name: 'a' })).not.toThrow()
    expect(() => validate({ _id: ObjectID.createFromTime(0).toHexString(), name: 'a' })).not.toThrow()
  })

  it('validate date property', () => {
    const doc: DocumentSchema = {
      name: 'test',
      module: 'test',
      properties: {
        _id: { type: PropertyType.objectId },
        date: { type: PropertyType.date }
      }
    }
    const validator = makeValidatorForDocument(doc, { optionalId: true }, [doc])
    expect(() => validator({ date: new Date() })).not.toThrow()
    expect(() => validator({ date: new Date().toISOString() })).not.toThrow()
    expect(() => validator({ date: '00-01-2020' })).toThrow()
  })

  it('validate object property', () => {
    const doc: DocumentSchema = {
      name: 'test',
      module: 'test',
      properties: {
        _id: { type: PropertyType.objectId },
        obj: {
          type: PropertyType.object,
          document: {
            key: { type: PropertyType.objectId },
            value: { type: PropertyType.string }
          }
        }
      }
    }
    const validator = makeValidatorForDocument(doc, { optionalId: true }, [doc])
    expect(() => validator({ obj: { key: 'aa', value: 'value' } })).toThrow()
    const validObj = { key: ObjectID.createFromTime(new Date().getTime()), value: 'value' }
    expect(() => validator({ obj: validObj })).not.toThrow()
  })

  it('validate an array', () => {
    const doc: DocumentSchema = {
      name: 'test',
      module: 'test',
      properties: {
        _id: { type: PropertyType.objectId },
        array: {
          type: PropertyType.array,
          document: {
            key: { type: PropertyType.objectId },
            value: { type: PropertyType.string }
          }
        }
      }
    }
    const validator = makeValidatorForDocument(doc, { optionalId: true }, [doc])
    const validObj = { key: ObjectID.createFromTime(new Date().getTime()), value: 'value' }
    expect(() => validator({ array: validObj })).toThrow()
    expect(() => validator({ array: [validObj, validObj] })).not.toThrow()
    expect(() => validator({ array: [validObj, { key: 'a', name: 'aa' }] })).toThrow()
  })

  it('validate a relation', () => {
    const relDoc: DocumentSchema = {
      name: 'related',
      module: 'test',
      properties: {
        _id: { type: PropertyType.objectId },
        revRel: { type: PropertyType.relation, refProperty: 'rel', ref: 'test~~test' }
      }
    }
    const doc: DocumentSchema = {
      name: 'test',
      module: 'test',
      properties: {
        _id: { type: PropertyType.objectId },
        rel: {
          type: PropertyType.relation,
          ref: makeRef(relDoc)
        },
        manyRel: {
          type: PropertyType.relation,
          ref: makeRef(relDoc),
          many: true
        }
      }
    }
    const documents = [doc, relDoc]
    const validator = makeValidatorForDocument(doc, { optionalId: true }, documents)
    const oid = ObjectID.createFromTime(0)
    expect(() => validator({ rel: oid, manyRel: [] })).not.toThrow()
    expect(() => validator({ rel: oid.toHexString(), manyRel: [] })).not.toThrow()
    expect(() => validator({ rel: 'aaa', manyRel: [] })).toThrow()
    expect(() => validator({ rel: { _id: oid }, manyRel: [] })).not.toThrow()
    expect(() => validator({ rel: { _id: oid.toHexString() }, manyRel: [] })).not.toThrow()
    expect(() => validator({ rel: { _id: 'aaa' }, manyRel: [] })).toThrow()
    expect(() => validator({ rel: { _id: oid }, manyRel: ['a'] })).toThrow()
    expect(() => validator({ rel: { _id: oid }, manyRel: [oid] })).not.toThrow()
    expect(() => validator({ rel: { _id: oid }, manyRel: [oid.toHexString()] })).not.toThrow()
    expect(() => validator({ rel: { _id: oid }, manyRel: [{ _id: 'a' }] })).toThrow()
    expect(() => validator({ rel: { _id: oid }, manyRel: [{ _id: oid }] })).not.toThrow()
    expect(() => validator({ rel: { _id: oid }, manyRel: [{ _id: oid.toHexString() }] })).not.toThrow()

    const revValidator = makeValidatorForDocument(relDoc, { optionalId: true }, documents)
    expect(() => revValidator({ revRel: undefined })).not.toThrow()
    expect(() => revValidator({ revRel: null })).toThrow()
    expect(() => revValidator({ revRel: oid })).toThrow()
    expect(() => revValidator({ revRel: oid.toHexString() })).toThrow()
    expect(() => revValidator({ revRel: 'aa' })).toThrow()
    expect(() => revValidator({ revRel: ['aa'] })).toThrow()
    expect(() => revValidator({ revRel: [oid, 'aa'] })).toThrow()
    expect(() => revValidator({ revRel: [oid] })).not.toThrow()
    expect(() => revValidator({ revRel: [oid.toHexString()] })).not.toThrow()
  })
})
