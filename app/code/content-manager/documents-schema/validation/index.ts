/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import {
  ArrayProperty,
  BooleanProperty,
  DateProperty,
  DocumentSchema,
  FileProperty,
  NumberProperty,
  ObjectIdProperty,
  ObjectProperty,
  Property,
  PropertyType,
  RelationProperty,
  StringProperty
} from '../../types'
import {
  eachItem,
  iif,
  isArray,
  isBoolean,
  isNull,
  isNumber,
  isObject,
  isString,
  isUndefined,
  merge,
  oneOf,
  Validation,
  whereKey
} from '@linda/core/validation'
import {ObjectID} from '@linda/database'
import {findDocumentByRef} from '../data/calculations'
import {isDateOrIsoDateString} from '@linda/core/validation/date'

export const makeValidatorForDocument =
  (document: Pick<DocumentSchema, 'properties'>, config: { optionalId: boolean }, docs: DocumentSchema[]): Validation => {
    return makePropertiesValidator(document.properties, config, docs)
  }

const makePropertyValidator = (prop: Property, docs: DocumentSchema[]): Validation => {
  switch (prop.type) {
    case PropertyType.objectId:
      return makeObjectIdValidator(prop)
    case PropertyType.string:
      return makeStringValidator(prop)
    case PropertyType.number:
      return makeNumberValidator(prop)
    case PropertyType.date:
      return makeDateValidator(prop)
    case PropertyType.boolean:
      return makeBooleanValidator(prop)
    case PropertyType.file:
      return makeFileValidator(prop)
    case PropertyType.array:
      return makeArrayValidator(prop, docs)
    case PropertyType.object:
      return makeObjectValidator(prop, docs)
    case PropertyType.relation:
      return makeRelationValidator(prop, docs)
  }
}

const makeObjectIdValidator = (prop: ObjectIdProperty): Validation =>
  applyOptionalValidation(prop, isValidObjectId)

const isValidObjectId: Validation = (v: any) => {
  if (!ObjectID.isValid(v)) {
    throw new Error('Expected object id value')
  }
  return true
}

const makeStringValidator = (prop: StringProperty): Validation =>
  applyOptionalValidation(prop, isString)

const makeNumberValidator = (prop: NumberProperty): Validation =>
  applyOptionalValidation(prop, isNumber)

const makeDateValidator = (prop: DateProperty): Validation =>
  applyOptionalValidation(prop, isDateOrIsoDateString)

const makeBooleanValidator = (prop: BooleanProperty): Validation =>
  applyOptionalValidation(prop, isBoolean)

const makeFileValidator = (prop: FileProperty): Validation =>
  applyOptionalValidation(prop, isValidObjectId)

const makePropertiesValidator = (
  props: {[k: string]: Property},
  config: { optionalId: boolean },
  docs: DocumentSchema[]
): Validation => merge(
  ...Object.entries(props).map(([name, def]) => {
    if (config.optionalId && name === '_id') {
      return whereKey('_id', oneOf(makePropertyValidator(def, docs), isNull, isUndefined))
    } else {
      return whereKey(name, makePropertyValidator(def, docs))
    }
  })
)

const makeArrayValidator = (prop: ArrayProperty, docs: DocumentSchema[]): Validation =>
  applyOptionalValidation(prop, merge(
    isArray,
    eachItem(makePropertiesValidator(prop.document, { optionalId: true }, docs))
  ))

const makeObjectValidator = (prop: ObjectProperty, docs: DocumentSchema[]): Validation =>
  applyOptionalValidation(prop, merge(
    isObject,
    makePropertiesValidator(prop.document, { optionalId: true }, docs)
  ))

const makeRelationValidator = (prop: RelationProperty, docs: DocumentSchema[]): Validation => {
  const doc = findDocumentByRef(docs, prop.ref)
  if (doc === undefined) {
    throw new Error(`Invalid ref ${prop.ref} (unable to find referenced document)`)
  }
  const relPropValidation = oneOf(
    makePropertiesValidator({ _id: doc.properties._id }, { optionalId: false }, docs),
    isValidObjectId
  )
  const isInverseRel = prop.refProperty !== undefined
  return applyOptionalValidation(prop, merge(
    iif(() => prop?.many === true || isInverseRel,
      iif(() => isInverseRel,
        oneOf(
          eachItem(relPropValidation),
          isUndefined
        ),
        eachItem(relPropValidation)
      ),
      relPropValidation
    )
  ))
}

const applyOptionalValidation = (prop: Property, validator: Validation): Validation =>
  prop.optional === true ? oneOf(
    validator,
    isUndefined,
    isNull
  ) : validator
