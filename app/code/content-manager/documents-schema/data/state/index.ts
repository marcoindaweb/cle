/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { DocumentSchema } from '../../../types'
import { addDocument as _addDocument, removeDocument as _removeDocument } from '../calculations'

let documents: DocumentSchema[] = []
export const getDocuments = (): DocumentSchema[] => documents
export const addDocument = (doc: DocumentSchema): DocumentSchema[] => {
  documents = _addDocument(documents, doc)
  return getDocuments()
}
export const removeDocument = (doc: Omit<DocumentSchema, 'properties'>): DocumentSchema[] => {
  documents = _removeDocument(documents, doc)
  return getDocuments()
}
