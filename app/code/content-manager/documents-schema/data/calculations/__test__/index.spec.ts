/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { DocumentSchema, PropertyType } from '../../../../types'
import { getOnlyPropertiesWithRelation } from '../index'

describe('data/calculations', () => {
  it('get all the properties with a relation', () => {
    const doc: DocumentSchema = {
      name: 'doc1',
      module: 'doc1',
      properties: {
        _id: { type: PropertyType.objectId },
        simple: { type: PropertyType.relation, ref: 'ref' },
        nested: {
          type: PropertyType.object,
          document: { relation: { type: PropertyType.relation, ref: 'ref' } }
        }
      }
    }
    expect(getOnlyPropertiesWithRelation(doc.properties))
      .toEqual({ simple: doc.properties.simple, nested: doc.properties.nested })
  })
})
