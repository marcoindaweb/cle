/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import {
  DocumentSchema,
  isArrayProperty, isObjectProperty,
  isRelProperty,
  PropertiesObject, Property,
  PropertyType,
  RelationProperty
} from '../../../types'
import { isObject } from '@linda/core/validation'

export const addDocument = (docs: DocumentSchema[], doc: DocumentSchema): DocumentSchema[] => {
  const documents = [...docs]
  if (!documents.some(v => v.name === doc.name)) {
    documents.push(doc)
  } else {
    documents.map(v => v.name === doc.name ? doc : v)
  }
  return documents
}

export const makeRef = (doc: Pick<DocumentSchema, 'name' | 'module'>): string => `${doc.module}~~${doc.name}`
export const decodeRef = (ref: string): Pick<DocumentSchema, 'name' | 'module'> => {
  const parts = ref.split('~~')
  return { name: parts[1], module: parts[0] }
}

export const findDocumentByRef = (docs: DocumentSchema[], ref: string): DocumentSchema | undefined => {
  return findDocument(docs, decodeRef(ref))
}

export const findDocument = (docs: DocumentSchema[], { name, module }: Pick<DocumentSchema, 'name' | 'module'>): DocumentSchema | undefined => {
  return docs.find(v => v.name === name && v.module === module)
}

export const removeDocument = (documents: DocumentSchema[], doc: Omit<DocumentSchema, 'properties'>): DocumentSchema[] => {
  return documents.filter(v => v.name !== doc.name && v.module !== doc.module)
}

export const getOnlyPropertiesWithRelation = (props: PropertiesObject): PropertiesObject => {
  return Object.entries(props).reduce<PropertiesObject>((acc, [key, def]) => {
    if (isRelProperty(def)) {
      acc[key] = def
    } else if (isArrayProperty(def) || isObjectProperty(def)) {
      const res = getOnlyPropertiesWithRelation(def.document)
      if (Object.keys(res).length > 0) {
        acc[key] = def
      }
    }
    return acc
  }, {})
}

/**
 * Return the property if its of the specified type, or looks for it inside the passed prop if its an array/object property
 */
export const getNestedPropertyOfType =
  <T extends Property>(
    prop: Property,
    type: PropertyType,
    key: string
  ): [T, string] | undefined => {
    if (prop.type === type) return [prop as T, key]
    else if (isObjectProperty(prop) || isArrayProperty(prop)) {
      for (const [k, p] of Object.entries(prop.document)) {
        const currKey = [key, k].join('.')
        const res = getNestedPropertyOfType(p, type, currKey)
        if (res !== undefined) {
          return res as [T, string]
        }
      }
    }
  }
