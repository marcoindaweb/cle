/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { AnyDocument } from '../document-data/crud'
import { ObjectID } from '@linda/database'

export interface DocumentSchema {
  name: string
  module: string
  properties: {
    _id: Property
  } & PropertiesObject
}

export interface PropertiesObject {[K: string]: Property}

type SubTypeWithValue<Base, Condition> = Pick<
Base,
{
  [Key in keyof Base]: Base[Key] extends Condition ? Key : never
}[keyof Base]
>

export const isRelProperty = (p: Property): p is RelationProperty => p.type === PropertyType.relation
export const isArrayProperty = (p: Property): p is ArrayProperty => p.type === PropertyType.array
export const isObjectProperty = (p: Property): p is ObjectProperty => p.type === PropertyType.object
export type Property = StringProperty | ObjectIdProperty | NumberProperty | DateProperty | ObjectProperty | BooleanProperty | RelationProperty | ArrayProperty | FileProperty

export enum PropertyType {
  string,
  objectId,
  number,
  date,
  array,
  object,
  boolean,
  relation,
  file,
}

interface BaseProperty {
  optional?: boolean
}

export interface StringProperty extends BaseProperty {
  type: PropertyType.string
}

export interface ObjectIdProperty extends BaseProperty {
  type: PropertyType.objectId
}

export interface NumberProperty extends BaseProperty {
  type: PropertyType.number
}

export interface DateProperty extends BaseProperty {
  type: PropertyType.date
}

export interface BooleanProperty extends BaseProperty {
  type: PropertyType.boolean
}

export interface RelationProperty extends BaseProperty {
  type: PropertyType.relation
  ref: string
  many?: boolean
  refProperty?: string // To specify when we want to do an inverse lookup of the relation
}

export interface ObjectProperty extends BaseProperty {
  type: PropertyType.object
  document: PropertiesObject
}

export interface ArrayProperty extends BaseProperty {
  type: PropertyType.array
  document: PropertiesObject
}

export interface FileProperty extends BaseProperty {
  type: PropertyType.file
}

export type ReducedDocument = Omit<DocumentSchema, 'properties'>
type Replacement<M extends [any, any], T> =
  M extends any ? [T] extends [M[0]] ? M[1] : never : never
type DeepReplace<T, M extends [any, any]> = {
  [P in keyof T]: T[P] extends M[0]
    ? Replacement<M, T[P]>
    : T[P] extends object
      ? DeepReplace<T[P], M>
      : T[P];
}
export type DocOverHttp<T extends AnyDocument> = DeepReplace<T, [ObjectID, string]>
export type OriginalDoc<T extends DocOverHttp<any>> = T extends DocOverHttp<infer R> ? R : never
