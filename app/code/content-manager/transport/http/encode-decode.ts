/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

/**
 * Since modules can have / and inside the name this could cause some problems with urls
 * So we encode-decode the module names by replacing / with --
 */
import { DocumentSchema } from '../../types'

export const encodeModule = (val: string): string => val.replace(/\//g, '--')
export const decodeModule = (val: string): string => val.replace(/--/g, '/')
export const encodeDoc = <T extends Omit<DocumentSchema, 'name' | 'properties'>>(doc: T): T => {
  return {
    ...doc,
    module: encodeModule(doc.module)
  }
}
export const decodeDoc = <T extends Omit<DocumentSchema, 'name' | 'properties'>>(doc: T): T => {
  return {
    ...doc,
    module: decodeModule(doc.module)
  }
}
