/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import {
  app,
  FastifyRequest,
  FastifyReply,
  FastifyPluginCallback
} from '@linda/core/transport/http'
import { DocumentSchema } from '../../types'
import { getDocuments } from '../../documents-schema/data/state'
import {
  createDocument,
  deleteDocument,
  findDocument,
  updateDocument
} from '../../documents-schema'
import { validateDocument } from '../../schema/document'
import { decodeDoc, decodeModule, encodeDoc, encodeModule } from './encode-decode'
import { FilterQuery } from '@linda/database'
import { makeRepositoryFromReducedDocument } from '../../document-data/repository'

type GetDocumentsReq = FastifyRequest
type CreateDocumentReq = FastifyRequest<{
  Body: DocumentSchema
}>
type GetDocumentReq = FastifyRequest<{
  Params: {
    name: string
    module: string
  }
}>
type UpdateDocumentReq = FastifyRequest<{
  Params: {
    name: string
    module: string
  }
  Body: DocumentSchema
}>
type DeleteDocumentReq = FastifyRequest<{
  Params: {
    name: string
    module: string
  }
}>

const findOr404 =
  async (request: GetDocumentReq, reply: FastifyReply): Promise<DocumentSchema | undefined> => {
    const doc = findDocument({
      name: request.params.name,
      module: decodeModule(request.params.module)
    })
    if (doc === undefined) {
      await reply.code(404).send()
      return
    }
    return encodeDoc(doc)
  }

const contentManager: FastifyPluginCallback = (app, opts, next) => {
  app.get('/documents/schema', async (request: GetDocumentsReq, reply) => {
    await reply.send(getDocuments().map(v => ({ name: v.name, module: encodeModule(v.module) })))
  })
  app.post('/documents/schema', async (request: CreateDocumentReq, reply) => {
    const doc = decodeDoc(request.body)
    validateDocument(doc)
    await createDocument(doc)
    await reply.send(request.body)
  })
  app.get('/documents/schema/:module/:name', async (request: GetDocumentReq, reply) => {
    const doc = await findOr404(request, reply)
    if (doc !== undefined) {
      await reply.send(doc)
    }
  })
  app.put('/documents/schema/:module/:name', async (request: UpdateDocumentReq, reply) => {
    validateDocument(request.body)
    const doc = await findOr404(request, reply)
    if (doc === undefined) return
    await updateDocument({ ...decodeDoc(request.body) })
    await reply.send(request.body)
  })
  app.delete('/documents/schema/:module/:name', async (request: DeleteDocumentReq, reply) => {
    await deleteDocument({ name: request.params.name, module: decodeModule(request.params.module) })
    await reply.send()
  })
  type FindOneRequest = FastifyRequest<{
    Querystring: { query: FilterQuery<any> }
    Params: { name: string, module: string }
  }>
  app.get('/documents/data/:module/:name/find-one', async (request: FindOneRequest, reply) => {
    const repository = makeRepositoryFromReducedDocument({
      name: request.params.name,
      module: decodeModule(request.params.module)
    })
    const res = await repository.findOne(request.query.query)
    res === null ? await reply.code(404).send() : await reply.send(res)
  })
  type CountRequest = FastifyRequest<{
    Querystring: { query: FilterQuery<any> }
    Params: { name: string, module: string }
  }>
  app.get('/documents/data/:module/:name/count', async (request: CountRequest, reply) => {
    const repository = makeRepositoryFromReducedDocument({
      name: request.params.name,
      module: decodeModule(request.params.module)
    })
    const res = await repository.count(request.query.query)
    await reply.send(res)
  })
  type FindManyRequest = FastifyRequest<{
    Querystring: { query: FilterQuery<any>, limit?: number, skip?: number }
    Params: { name: string, module: string }
  }>
  app.get('/documents/data/:module/:name/find-many', async (request: FindManyRequest, reply) => {
    const repository = makeRepositoryFromReducedDocument({
      name: request.params.name,
      module: decodeModule(request.params.module)
    })
    let cursor = repository.findMany(request.query.query)
    if (request.query.limit !== undefined) {
      cursor = cursor.limit(request.query.limit)
    }
    if (request.query.skip !== undefined) {
      cursor = cursor.skip(request.query.skip)
    }
    const res = await cursor.toArray()
    await reply.send(res)
  })
  type CreateOneRequest = FastifyRequest<{
    Params: { name: string, module: string }
    Body: {
      data: any
    }
  }>
  app.post('/documents/data/:module/:name/create-one', async (request: CreateOneRequest, reply) => {
    const repository = makeRepositoryFromReducedDocument({
      name: request.params.name,
      module: decodeModule(request.params.module)
    })
    const res = await repository.createOne(request.body.data)
    await reply.send(res)
  })
  type CreateManyRequest = FastifyRequest<{
    Params: { name: string, module: string }
    Body: {
      data: any[]
    }
  }>
  app.post('/documents/data/:module/:name/create-many',
    async (request: CreateManyRequest, reply) => {
      const repository = makeRepositoryFromReducedDocument({
        name: request.params.name,
        module: decodeModule(request.params.module)
      })
      const res = await repository.createMany(request.body.data)
      await reply.send(res)
    })
  type UpdateOneRequest = FastifyRequest<{
    Params: { name: string, module: string }
    Body: {
      data: any
    }
  }>
  app.put('/documents/data/:module/:name/update-one',
    async (request: UpdateOneRequest, reply) => {
      const repository = makeRepositoryFromReducedDocument({
        name: request.params.name,
        module: decodeModule(request.params.module)
      })
      const res = await repository.updateOne(request.body.data)
      await reply.send(res)
    })
  type UpdateManyRequest = FastifyRequest<{
    Params: { name: string, module: string }
    Body: {
      data: any[]
    }
  }>
  app.put('/documents/data/:module/:name/update-many',
    async (request: UpdateManyRequest, reply) => {
      const repository = makeRepositoryFromReducedDocument({
        name: request.params.name,
        module: decodeModule(request.params.module)
      })
      const res = await repository.updateMany(request.body.data)
      await reply.send(res)
    })

  type DeleteOneRequest = FastifyRequest<{
    Body: { query: FilterQuery<any> }
    Params: { name: string, module: string }
  }>
  app.delete('/documents/data/:module/:name/delete-one',
    async (request: DeleteOneRequest, reply) => {
      const repository = makeRepositoryFromReducedDocument({
        name: request.params.name,
        module: decodeModule(request.params.module)
      })
      await repository.deleteOne(request.body.query)
      await reply.status(204).send()
    })
  type DeleteManyRequest = FastifyRequest<{
    Body: { query: FilterQuery<any> }
    Params: { name: string, module: string }
  }>
  app.delete('/documents/data/:module/:name/delete-many',
    async (request: DeleteManyRequest, reply) => {
      const repository = makeRepositoryFromReducedDocument({
        name: request.params.name,
        module: decodeModule(request.params.module)
      })
      await repository.deleteMany(request.body.query)
      await reply.status(204).send()
    })
  next()
}

// eslint-disable-next-line @typescript-eslint/no-floating-promises
app.register(contentManager, { prefix: '/content-manager/v1' })
