/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import { Classes } from '@linda/ui/frontend'

const AdminSuspense: React.FC = ({ children }) => <React.Suspense fallback={<div className={Classes.SKELETON} style={{ height: '100%', maxHeight: '300px' }}></div>}>{ children }</React.Suspense>

export default AdminSuspense
