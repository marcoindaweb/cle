/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRoute } from '@linda/frontend/frontend'
import React, { useEffect } from 'react'
import Sidebar from './Sidebar'
import styles from './style.scss'
import Header from './Header'
import Content from './Content'

export const ADMIN_BG_IMAGE = 'admin/style/background-image'
export const ADMIN_CONTENT_OPACITY = 'admin/style/content-opacity'

export const admin = (route: string): string => `/admin${route}`

const Admin: React.FC = ({ children }) => {
  const [match] = useRoute(admin('/:path*'))

  useEffect(() => {
    document.documentElement.style.setProperty('--admin-background-image', window.localStorage.getItem(ADMIN_BG_IMAGE))
    document.documentElement.style.setProperty('--admin-content-opacity', window.localStorage.getItem(ADMIN_CONTENT_OPACITY))
  }, [])

  return match ? <div className={styles.admin}>
    <div className={styles['left-side']}>
      <Sidebar>
      </Sidebar>
    </div>
    <div className={styles['right-side']}>
      <Header className={styles.header} />
      <Content className={styles.content} />
    </div>
  </div> : null
}

export default Admin
