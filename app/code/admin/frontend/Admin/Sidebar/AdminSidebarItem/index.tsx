/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Icon, IconName } from '@linda/ui/frontend'
import React, { ReactNode } from 'react'
import { useSetOpenSidebarItem } from '../index'
import classes from './style.sass'

export interface SidebarItem {
  icon: IconName
  name: string
  groups: Array<{ key: string, title: ReactNode, routes?: Array<{name: string, path: string}>}>
  key: string
}

const AdminSidebarItem: React.FC<{ item: SidebarItem }> = ({ item: { icon, name, key } }) => {
  const setOpen = useSetOpenSidebarItem()
  return <div className={classes.item} onClick={() => setOpen(key)}>
    <Icon icon={icon} iconSize={22} />
    <span>{name}</span>
  </div>
}

export default AdminSidebarItem
