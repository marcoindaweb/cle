/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useCallback } from 'react'
import style from './style.sass'
import {
  atom,
  atomFamily,
  DefaultValue, Link,
  selector,
  useRecoilState,
  useRecoilValue,
  useSetRecoilState
} from '@linda/frontend/frontend'
import AdminSidebarItem, { SidebarItem } from './AdminSidebarItem'
import { Drawer, Position } from '@linda/ui/frontend'
import isDefined from '@linda/core/utils/primitives'
import Translated from '@linda/i18n/frontend/components/Translated'
import unionBy from 'lodash/unionBy'

const items = selector<SidebarItem[]>({
  key: 'admin/sidebar/items',
  get: ({ get }) => {
    return get(itemsKeys).map(v => get(item(v))).filter(isDefined)
  },
  set: ({ get, set, reset }, val) => {
    if (val instanceof DefaultValue) {
      get(itemsKeys).forEach((key) => {
        reset(item(key))
      })
      reset(itemsKeys)
      return
    }
    set(itemsKeys, val.map(v => v.key))
    val.forEach(v => {
      set(item(v.key), v)
    })
  }
})
const itemsKeys = atom<string[]>({
  key: 'admin/sidebar/itemsKeys',
  default: []
})
const item = atomFamily<SidebarItem | null, string>({
  key: 'admin/sidebar/sidebarItem',
  default: null
})

const openItem = atom<string | null>({
  key: 'admin/sidebar/openItem',
  default: null
})

export const useAddSidebarItem = (): (v: SidebarItem) => void => {
  const update = useSetRecoilState(items)
  return useCallback((item: SidebarItem) => {
    update(v => mergeItems(v, item))
  }, [])
}

const mergeItems = (items: SidebarItem[], item: SidebarItem): SidebarItem[] => {
  const newVal = unionBy([...items, item], 'key').map(item => ({
    ...item,
    groups: unionBy(item.groups, 'key').map(group => ({
      ...group,
      routes: unionBy(group?.routes, 'path')
    }))
  }))
  return newVal
}

export const useSetOpenSidebarItem = (): (v: string | null) => void => useSetRecoilState(openItem)

const AdminSidebarDrawer: React.FC = () => {
  const [key, setItem] = useRecoilState(openItem)
  const current = useRecoilValue(item(key ?? ''))

  return <Drawer usePortal={false} className={style.drawer} position={Position.LEFT} size={'35vw'} isOpen={current !== null} onClose={() => setItem(null)}>
    {current?.groups.map((group) => {
      return <div key={group.key}>
        {typeof group.title === 'string' ? <h2><Translated message={group.title} /></h2> : group.title}
        {group.routes !== undefined && <div className={style['drawer-paths']}>
          {group?.routes.map(({ name, path }) => <Translated key={path} message={name} >{msg =>
            <Link onClick={() => setItem(null)} to={path}>{msg}</Link>
          }</Translated>)}
        </div>}
      </div>
    })}
  </Drawer>
}

const Sidebar: React.FC = () => {
  const sidebarItems = useRecoilValue(items)
  return <div className={style.sidebar}>
    {sidebarItems.map(item => <AdminSidebarItem key={item.key} item={item}/>)}
    <AdminSidebarDrawer/>
  </div>
}
export default Sidebar
