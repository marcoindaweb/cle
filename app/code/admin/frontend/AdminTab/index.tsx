/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { ReactNode } from 'react'
import classes from './style.sass'
import { Divider, Icon } from '@linda/ui/frontend'

const AdminTab: React.FC<{ isOpen: boolean, onToggle: () => void, name: ReactNode, noPadding?: boolean }> = ({ isOpen, onToggle, name, children, noPadding }) => {
  return <div className={classes.tab}>
    <div onClick={onToggle} className={classes['tab-name']}>
      {name}
      <Icon icon={isOpen ? 'caret-up' : 'caret-down' } />
    </div>
    {isOpen && <Divider className={noPadding === true ? classes['no-padding'] : undefined} />}
    {isOpen && <div className={classes['tab-content']}>
      {children}
    </div>}
    <Divider className={noPadding === true ? classes['no-padding'] : undefined} />
  </div>
}

export default AdminTab
