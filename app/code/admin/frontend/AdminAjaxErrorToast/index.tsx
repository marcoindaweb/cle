/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { AjaxError } from '@linda/frontend/frontend'
import AdminToast from '../AdminToast'

export const AdminAjaxErrorToast = async (error: AjaxError | string): Promise<void> => {
  AdminToast.show({
    intent: 'danger',
    message: typeof error === 'string' ? error : await getMessageFromAjaxError(error)
  })
}

export const getMessageFromAjaxError = async (error: AjaxError): Promise<string> => {
  let maybeError = ''
  try {
    maybeError = await error.response.clone().json().then(v => v.message)
    if (maybeError.length > 0) return maybeError
  } catch {
    maybeError = await error.response.clone().text()
    if (maybeError.length > 0) return maybeError
  }
  return 'Unknown error'
}
