/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import { NonIdealState } from '@linda/ui/frontend'
import Translated from '@linda/i18n/frontend/components/Translated'

const Admin404: React.FC = () => <NonIdealState
  icon='clean'
  title={<Translated message="Looks like this page don't exists" />}
/>
export default Admin404
