/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { ReactNode, useEffect, useRef, useState } from 'react'
import ReactDOM from 'react-dom'
import { Button, Divider, IButtonProps } from '@linda/ui/frontend'
import classes from './style.sass'
import { useLocation } from '@linda/frontend/frontend'

const AdminPage: React.FC<{ name: ReactNode, template?: '2-cols' | '1-cols', buttons?: IButtonProps[] }> = ({ name, children, template, buttons }) => {
  const [header, setHeader] = useState<HTMLDivElement | null>(null)

  useEffect(() => {
    setHeader(document.querySelector<HTMLDivElement>('#admin-header'))
  }, [])

  return <div className={classes.page}>
    {header !== null && ReactDOM.createPortal(
      <div>
        <div className={classes.title}>
          <Button onClick={() => history.back()} minimal icon='arrow-left' />
          <h2>{name}</h2>
          <div className={classes.buttons}>
            {buttons?.map((props, i) => <Button key={i} {...props} minimal small={false} large />)}
          </div>
        </div>
        <Divider />
      </div>
      , header)}
    <div className={template === '2-cols' ? classes['content--two-cols'] : undefined}>
      {children}
    </div>
  </div>
}

export default AdminPage
