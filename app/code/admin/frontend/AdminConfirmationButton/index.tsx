/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useState } from 'react'
import { Button, ButtonGroup, IButtonProps } from '@linda/ui/frontend'
import Translated from '@linda/i18n/frontend/components/Translated'

const AdminConfirmationButton: React.FC<IButtonProps & { onConfirm: () => void }> = (props) => {
  const [asking, setAsking] = useState(false)
  return asking ? <ButtonGroup>
    <Button disabled minimal={props.minimal} text={<Translated message='Are you sure?' />} />
    <Button minimal={props.minimal} text={<Translated message='No' />} onClick={() => setAsking(false)} />
    <Button minimal={props.minimal} text={<Translated message='Yes' />} onClick={() => {
      props.onConfirm()
      setAsking(false)
    }} />
  </ButtonGroup> : <Button {...props} onClick={() => setAsking(true)} />
}

export default AdminConfirmationButton
