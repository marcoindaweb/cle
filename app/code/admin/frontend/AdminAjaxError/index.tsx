/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */
import React from 'react'
import { Button, NonIdealState } from '@linda/ui/frontend'
import Translated from '@linda/i18n/frontend/components/Translated'
import { AjaxError } from '@linda/frontend/frontend'
import { useConfig } from '@linda/config-ui/frontend/store/configuration'
import AdminSuspense from '../AdminSuspense'
import { ERROR_EMAIL } from '@linda/email/const'

const ErrorMessage: React.FC<{ err: AjaxError }> = ({ err }) => {
  const email = useConfig<string>(ERROR_EMAIL)
  switch (err.response.status) {
    case 500:
      return <span>
        {/* eslint-disable-next-line max-len */}
        <Translated message={'Error 500: There was an error on the server, please try again.'} />{' '}
        {email !== undefined && <>
          <Translated message={'If the problem persist you can find us at '} />
          <a href={`mailto:${email}`}>{email}</a>
        </> }
      </span>
    case 404:
      return <span>
        <Translated message={'Error 404: We\'re unable to find the requested resource.'} />{' '}
        { email !== undefined && <>
          <Translated message={'If you think this is a problem you can find us at'} />
          <a href={`mailto:${email}`}>{email}</a>
        </> }
      </span>
  }
  return <span>
    <Translated message={'Error {code}, please try again'} />
    {email !== undefined && <>
      <Translated message={'If the problem persist you can find us at'} />
      <a href={`mailto:${email}`}>{email}</a>
    </>}
  </span>
}

export const AdminAjaxError: React.FC<{ err: AjaxError, retry?: () => void}> = ({ err, retry }) => {
  return <NonIdealState icon='error' title={<Translated message='Network error' />} description={
    <div>
      <AdminSuspense><ErrorMessage err={err}/></AdminSuspense>
      {retry !== undefined && <Button text={<Translated message='Retry' />} onClick={retry} />}
    </div>
  } />
}
