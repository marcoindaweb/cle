/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { AjaxError, atom } from '@linda/frontend/frontend'
import { action } from '@linda/frontend/frontend/recoil'

export const adminAjaxErrors = atom<AjaxError[]>({
  key: 'admin/ajax/ajax-errors',
  default: []
})

export const handleAjaxError = action<AjaxError>(
  'admin/ajax/handle-ajax-error',
  ({ set }, err) => set(adminAjaxErrors, val => [...val, err])
)
