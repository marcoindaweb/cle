/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { MouseEvent } from 'react'
import { Button, IButtonProps, Icon, IconName } from '@linda/ui/frontend'
import { Link } from '@linda/frontend/frontend'
import classes from '../AdminList/style.sass'

const Wrapper: React.FC<{ to?: string, button?: boolean, onClick?: (e: MouseEvent) => void }> =
  ({ children, to, button, onClick }) => {
    const className: string[] = []
    if (button === true || to !== undefined) {
      className.push(classes.button)
    }
    if (to !== undefined) {
      className.push(classes['with-link'])
    }
    return to === undefined
      ? <li onClick={onClick} className={className.join(' ')}>{children}</li>
      : <li onClick={onClick} className={className.join(' ')}><Link to={to}>{children}</Link></li>
  }

const AdminListItem: React.FC<{
  to?: string
  icon?: IconName
  button?: boolean
  onClick?: (e: MouseEvent) => void
}> = ({ children, to, icon, button, onClick }) => <Wrapper button={button} to={to}>
  {icon !== undefined && <div>
    <Icon icon={icon} />
  </div>}
  <span>
    {children}
  </span>
</Wrapper>

export default AdminListItem
