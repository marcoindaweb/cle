/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { addFrontend, addOperation, createAddOperation, ROOT } from '@linda/frontend'
import { join } from '@linda/core/fs/utils'
import { AddOperationPosition } from '@linda/frontend/operations/types'
import { component } from '@linda/frontend/selectors'

addFrontend(join(__dirname, 'frontend'))
addOperation(
  createAddOperation({
    component: ROOT,
    reference: join(__dirname, 'frontend/index.tsx'),
    position: AddOperationPosition.INSIDE,
    selector: component('div')
  })
)

export const ADMIN_CONTENT = join(__dirname, 'frontend/Admin/Content/index.tsx')
export const addInsideAdminContent = (path: string): void => addOperation(createAddOperation({
  component: ADMIN_CONTENT,
  reference: path,
  position: AddOperationPosition.INSIDE,
  selector: component('div')
}))
