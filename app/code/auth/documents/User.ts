/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { collection, ObjectID, Collection } from '@linda/database'
import { registerDocument, makeRepository } from '@linda/content-manager'
import { Role } from '@linda/auth/documents/Role'
import { DocumentSchema, PropertyType } from '@linda/content-manager/types'

export interface User {
  _id: ObjectID
  username: string
  password: string
  roles: Role[] | ObjectID[]
}

export const UserSchema: DocumentSchema = {
  name: 'User',
  module: '@linda/auth',
  properties: {
    _id: {
      type: PropertyType.objectId
    },
    username: {
      type: PropertyType.string
    },
    password: {
      type: PropertyType.string
    },
    roles: {
      type: PropertyType.relation,
      ref: '@linda/auth~~Role',
      many: true
    }
  }
}

export const UserCollection = (): Collection<User> => collection<User>('@linda/auth__User')
export const UserRepository = makeRepository(UserSchema, UserCollection())
registerDocument(UserSchema)

UserCollection().watch()

