import { collection, ObjectID, Collection } from '@linda/database'
import { registerDocument, makeRepository } from '@linda/content-manager'
import { PropertyType, DocumentSchema } from '@linda/content-manager/types'

export interface Role {
  _id: ObjectID
  name: string
  permissions: Array<{ name: string }>
}

export const RoleSchema: DocumentSchema = {
  name: 'Role',
  module: '@linda/auth',
  properties: {
    _id: {
      type: PropertyType.objectId
    },
    name: {
      type: PropertyType.string
    },
    permissions: {
      type: PropertyType.array,
      document: {
        name: {
          type: PropertyType.string
        }
      }
    }
  }
}

export const RoleCollection = (): Collection<Role> => collection<Role>('@linda/auth__Role')
export const RoleRepository = makeRepository(RoleSchema, RoleCollection())
registerDocument(RoleSchema)
