/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { ReducedDocument } from '@linda/content-manager/types'
import { findDocument } from '@linda/content-manager/documents-schema'
import { DocumentListLayout, DocumentListLayoutRepository } from '../../documents/DocumentListLayout'
import { makeRef } from '@linda/content-manager/documents-schema/data/calculations'
import { getDefaultListLayout } from '../calculations'

export const getDocumentListLayout =
  async (doc: ReducedDocument): Promise<DocumentListLayout | undefined> => {
    const res = await DocumentListLayoutRepository.findOne({ _id: makeRef(doc) })
    if (res !== null) {
      return res
    } else {
      const fullDoc = findDocument(doc)
      if (fullDoc === undefined) {
        return
      }
      const layout = getDefaultListLayout(fullDoc)
      await DocumentListLayoutRepository.createOne(layout)
      return layout
    }
  }

export const updateDocumentListLayout = async (listLayout: DocumentListLayout): Promise<void> => {
  await DocumentListLayoutRepository.updateOne(listLayout)
}
