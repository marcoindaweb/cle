/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { DocumentSchema, isArrayProperty, isObjectProperty, isRelProperty, ReducedDocument } from '@linda/content-manager/types'
import { DocumentListLayout } from '../../documents/DocumentListLayout'
import { decodeRef, makeRef } from '@linda/content-manager/documents-schema/data/calculations'
import { decodeDoc, encodeDoc } from '@linda/content-manager/transport/http/encode-decode'

export const getDefaultListLayout = (doc: DocumentSchema): DocumentListLayout => {
  const list: DocumentListLayout = {
    _id: makeRef(doc),
    columns: []
  }
  for (const [name, prop] of Object.entries(doc.properties)) {
    if (!isRelProperty(prop) && !isObjectProperty(prop) && !isArrayProperty(prop)) {
      list.columns.push({ property: name, order: list.columns.length })
    }
  }
  return list
}
export const getListID = (doc: ReducedDocument): string => makeRef(doc)
export const encodeListID = (id: string): string => makeRef(encodeDoc(decodeRef(id)))
export const decodeListID = (id: string): string => makeRef(decodeDoc(decodeRef(id)))
export const encodeListLayout = (doc: DocumentListLayout): DocumentListLayout => ({ ...doc, _id: encodeListID(doc._id) })
export const decodeListLayout = (doc: DocumentListLayout): DocumentListLayout => ({ ...doc, _id: decodeListID(doc._id) })
