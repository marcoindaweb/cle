/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { ajaxSelectorFamily } from '@linda/frontend/frontend/recoil'
import { DocListLayout } from '../../types'
import { ajax, AjaxError, fetchJson, useRecoilCallback } from '@linda/frontend/frontend'
import { ReducedDocument } from '@linda/content-manager/types'
import { AdminAjaxErrorToast } from '@linda/admin/frontend/AdminAjaxErrorToast'
import AdminToast from '@linda/admin/frontend/AdminToast'
import useTranslate from '@linda/i18n/frontend/hooks/useTranslate'
import { getListID } from '@linda/content-manager-ui/list-layout/calculations'

export const fetchDocumentListLayout = ajaxSelectorFamily<DocListLayout | AjaxError, ReducedDocument>(
  'content-manager/document-list-layout/fetch-document-list-layout',
  (doc) => () => fetchJson(`/content-manager-ui/v1/list-layout/${getListID(doc)}`)
)

export const useUpdateDocumentListLayout = (): (list: DocListLayout) => Promise<void> => {
  const translate = useTranslate()
  return useRecoilCallback(({ reset }) => async (list: DocListLayout) => {
    const res = await ajax(
      `/content-manager-ui/v1/list-layout/${list._id}`,
      { method: 'POST', body: JSON.stringify(list) }
    )
    if (res instanceof AjaxError) {
      await AdminAjaxErrorToast(res)
    } else {
      AdminToast.show({ intent: 'success', message: translate('Layout updated') })
    }
  }, [])
}
