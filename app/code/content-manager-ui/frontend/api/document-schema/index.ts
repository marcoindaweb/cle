/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { ajax, AjaxError, fetchJson, useResetRecoilState } from '@linda/frontend/frontend'
import { DocumentSchema, ReducedDocument } from '@linda/content-manager/types'
import { ajaxSelector, ajaxSelectorFamily } from '@linda/frontend/frontend/recoil'
import { AdminAjaxErrorToast } from '@linda/admin/frontend/AdminAjaxErrorToast'
import { useCallback } from 'react'
import AdminToast from '@linda/admin/frontend/AdminToast'
import useTranslate from '@linda/i18n/frontend/hooks/useTranslate'

const documentsSchemaRoute = '/content-manager/v1/documents/schema'
const documentSchemaRoute = (module: string, name: string): string =>
  `/content-manager/v1/documents/schema/${module}/${name}`
export const fetchReducedDocumentsList = ajaxSelector<ReducedDocument[] | AjaxError>(
  'content-manager/documents/fetchDocuments',
  async () => await fetchJson<ReducedDocument[]>(documentsSchemaRoute)
)

export const fetchDocumentSchema = ajaxSelectorFamily<DocumentSchema | AjaxError, ReducedDocument>(
  'content-manager/documents/fetchDocumentSchema',
  doc => async () => await fetchJson<DocumentSchema>(documentSchemaRoute(doc.module, doc.name))
)

export const useUpdateDocument = (info: ReducedDocument): (doc: DocumentSchema) => Promise<void> => {
  const resetList = useResetRecoilState(fetchReducedDocumentsList)
  const resetSingle = useResetRecoilState(fetchDocumentSchema(info))
  const translate = useTranslate()
  return useCallback(async (doc: DocumentSchema) => {
    const res = await fetchJson<DocumentSchema>(
      documentSchemaRoute(info.module, info.name),
      { method: 'PUT', body: JSON.stringify(doc) }
    )
    if (res instanceof AjaxError) {
      await AdminAjaxErrorToast(res)
    } else {
      resetList()
      resetSingle()
      AdminToast.show({ intent: 'success', message: translate('Document updated') })
    }
  }, [info])
}

export const useCreateDocument = (): (doc: DocumentSchema) => Promise<void> => {
  const resetList = useResetRecoilState(fetchReducedDocumentsList)
  const translate = useTranslate()
  return useCallback(async (doc: DocumentSchema) => {
    const res = await fetchJson<DocumentSchema>(
      documentsSchemaRoute,
      { method: 'POST', body: JSON.stringify(doc) }
    )
    if (res instanceof AjaxError) {
      await AdminAjaxErrorToast(res)
    } else {
      resetList()
      AdminToast.show({ intent: 'success', message: translate('Document created') })
    }
  }, [])
}

export const useDeleteDocument = (): (info: ReducedDocument) => Promise<void> => {
  const resetList = useResetRecoilState(fetchReducedDocumentsList)
  const translate = useTranslate()
  return useCallback(async (info: ReducedDocument) => {
    const res = await ajax<DocumentSchema>(
      documentSchemaRoute(info.module, info.name),
      { method: 'DELETE' }
    )
    if (res instanceof AjaxError) {
      await AdminAjaxErrorToast(res)
    } else {
      resetList()
      AdminToast.show({ intent: 'success', message: translate('Document deleted') })
    }
  }, [])
}
