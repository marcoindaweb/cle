/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import Translated from '@linda/i18n/frontend/components/Translated'
import React, { useCallback, useEffect } from 'react'
import { AjaxError, Link, useLocation, useRecoilValue, useSetRecoilState } from '@linda/frontend/frontend'
import { fetchDocumentSchema, useDeleteDocument } from '../../../api/document-schema'
import { AdminAjaxError } from '@linda/admin/frontend/AdminAjaxError'
import { Button } from '@linda/ui/frontend'
import useTranslate from '@linda/i18n/frontend/hooks/useTranslate'
import classes from './style.sass'
import AdminConfirmationButton from '@linda/admin/frontend/AdminConfirmationButton'
import { pageButtons } from '../../index'
import { documentRoot, editDocumentSchemaRoute, editListLayoutRoute } from '../../../index'

const List: React.FC<{ name: string, module: string }> = ({ name, module }) => {
  const document = useRecoilValue(fetchDocumentSchema({ name, module }))
  const setButtons = useSetRecoilState(pageButtons)
  const deleteDoc = useDeleteDocument()
  const [,setLocation] = useLocation()
  const onDelete = useCallback(async () => {
    if (document instanceof AjaxError) {
      return
    }
    await deleteDoc(document)
    setLocation(documentRoot)
  }, [document])
  useEffect(() => {
    if (document instanceof AjaxError) {
      setButtons([])
      return
    }
    setButtons([
      {
        intent: 'success',
        text: <Translated message='Add new {name}' values={{ name: document.name }} />
      }
    ])
    return () => setButtons([])
  }, [document])
  return document instanceof AjaxError ? <AdminAjaxError err={document} /> : <div>
    <div className={classes.top}>
      <h2><Translated message={document.name} /></h2>
      <div>
        <Link to={editListLayoutRoute(document)}>
          <Button minimal icon='list' text={<Translated message='Configure list view' />} />
        </Link>
        <Link to={editDocumentSchemaRoute(document)}>
          <Button minimal icon='edit' text={<Translated message='Edit Schema' />} />
        </Link>
        <AdminConfirmationButton
          icon='delete'
          onConfirm={() => onDelete()}
          minimal
          text={<Translated message='Delete Schema' />}
        />
      </div>
    </div>
  </div>
}

export default List
