/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useEffect } from 'react'
import { decodeRef } from '@linda/content-manager/documents-schema/data/calculations'
import { AjaxError, useRecoilValue, useSetRecoilState } from '@linda/frontend/frontend'
import { findManyDocsSelectorFamily } from '@linda/content-manager/frontend/api'
import { AdminAjaxError } from '@linda/admin/frontend/AdminAjaxError'
import { fetchDocumentSchema } from '../../../../api/document-schema'
import AdminSuspense from '@linda/admin/frontend/AdminSuspense'
import { currentSchema } from '../../../document-editor/state'

const ListLayoutRenderer: React.FC<{ id: string }> = ({ id, children }) => {
  return <AdminSuspense>
    <FetchData id={id}>

    </FetchData>
  </AdminSuspense>
}

const FetchData: React.FC<{ id: string }> = ({ id, children }) => {
  const doc = decodeRef(id)
  const query = {}
  const data = useRecoilValue(findManyDocsSelectorFamily(doc)(query))
  const docSchema = useRecoilValue(fetchDocumentSchema(doc))
  const setCurrentSchema = useSetRecoilState(currentSchema)
  useEffect(() => {
    if (docSchema instanceof AjaxError) {
      return
    }
    setCurrentSchema(docSchema)
  }, [docSchema])
  return data instanceof AjaxError
    ? <AdminAjaxError err={data} />
    : currentSchema instanceof AjaxError
      ? <AdminAjaxError err={currentSchema} />
      : <>{children}</>
}

export default ListLayoutRenderer
