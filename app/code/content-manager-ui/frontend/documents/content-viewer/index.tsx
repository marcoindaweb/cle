/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import {useLocation, useRecoilValue, useRoute} from '@linda/frontend/frontend'
import { createDocumentRoute, listDocumentsRoute, updateDocumentRoute } from '../../index'
import List from './list'

const ContentViewer: React.FC = () => {
  const [isList, listParams] = useRoute<{ name: string, module: string }>(listDocumentsRoute({ name: ':name', module: ':module' }))
  const [location] = useLocation()
  const [isCreating, createParams] = useRoute(createDocumentRoute({ name: ':name', module: ':module' }))
  const [isUpdating, updateParams] = useRoute(updateDocumentRoute({ name: ':name', module: ':module' }, ':id'))

  return <>
    {isList && listParams && <List name={listParams.name} module={listParams.module} />}
  </>
}
export default ContentViewer
