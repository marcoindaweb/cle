/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

// import { atom, atomFamily, DefaultValue, selectorFamily } from '@linda/frontend/frontend'
// import { AnyDocument } from '@linda/content-manager/document-data/crud'
// import { PropertiesObject, PropertyType, ReducedDocument } from '@linda/content-manager/types'
// import { makeRef } from '@linda/content-manager/documents-schema/data/calculations'
//
// type DocId = Readonly<{ docId: string }>
// type InstanceId = Readonly<DocId & { instanceId: string }>
// type ValueId = Readonly<InstanceId & { valueId: string[] }>
//
// const makeDocId = (doc: ReducedDocument): DocId => ({ docId: makeRef(doc) })
// const makeInstanceId = (docId: DocId, id: string | number): InstanceId => ({ ...docId, instanceId: id.toString() })
// const makeValueId = (valueId: InstanceId, singleValueKey: string): ValueId => ({ ...valueId, valueId: [singleValueKey] })
// const makeNestedValueId = (singleValueId: ValueId, newId: string): ValueId => ({ ...singleValueId, valueId: [...singleValueId.valueId, newId] })
//
// export const docPropertiesObject = atomFamily<PropertiesObject, DocId>({
//   key: 'content-manager-ui/content-viewer/docPropertiesObject',
//   default: {}
// })
//
// export const docInstances = selectorFamily<AnyDocument[], DocId>({
//   key: 'content-manager-ui/content-viewer/docInstances',
//   get: (id) => ({ get }) => {
//     return get(docInstancesIds(id)).map(i => get(docInstance(i)))
//   },
//   set: (id) => ({ get, set, reset }, val) => {
//     if (val instanceof DefaultValue) {
//       for (const i of get(docInstancesIds(id))) {
//         reset(docInstance(i))
//       }
//       reset(docInstancesIds(id))
//     } else {
//       const ids = val.map(i => i._id)
//       for (const v of val) {
//         set(docInstance(makeInstanceId(id, v._id)), v)
//       }
//       set(docInstancesIds(id), ids)
//     }
//   }
// })
//
// const docInstancesIds = atomFamily<InstanceId[], DocId>({
//   key: 'content-manager-ui/content-viewer/docInstancesIds',
//   default: []
// })
//
// const docInstance = selectorFamily<AnyDocument, InstanceId>({
//   key: 'content-manager-ui/content-viewer/docInstance',
//   get: (id) => ({ get }) => {
//     const propsSchema = get(docPropertiesObject(id))
//     const o = {}
//     for (const [key, def] of Object.entries(propsSchema)) {
//       switch (def.type) {
//         case PropertyType.date:
//         case PropertyType.number:
//         case PropertyType.string:
//         case PropertyType.objectId:
//         case PropertyType.boolean:
//         case PropertyType.relation:
//           o[key] = get(singleDocValue(makeValueId(id, key)))
//         case PropertyType.object:
//           o[key] = get(docInstance())
//         case PropertyType.array:
//           o[key] = get(arrayDocValue(makeValueId(id, key)))
//         case PropertyType.file:
//           throw new Error('File type isn\'t currently handled')
//       }
//     }
//   }
// })
//
// const arrayDocValueIndexes = atomFamily<number[], ValueId>({
//   key: 'content-manager-ui/content-viewer/arrayDocValueIndexes'
// })
//
// const arrayDocValue = selectorFamily<AnyDocument[], ValueId>({
//   key: 'content-manager-ui/content-viewer/arrayDocValue'
// })
//
// const objectDocValue = selectorFamily<AnyDocument, ValueId>({
//   key: 'content-manager-ui/content-viewer/objectDocValue'
// })
//
// const objectDocValueKeys = atom<string[], ValueId>({
//   key: 'content-manager-ui/content-viewer/objectDocValueKeys'
// })
//
// const singleDocValue = atomFamily<any, ValueId>({
//   key: 'content-manager-ui/content-viewer/singleDocValue'
// })
