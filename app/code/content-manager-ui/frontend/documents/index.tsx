/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import AdminPage from '@linda/admin/frontend/AdminPage'
import Translated from '@linda/i18n/frontend/components/Translated'
import React from 'react'
import AdminSuspense from '@linda/admin/frontend/AdminSuspense'
import DocumentsList from './documents-list'
import { atom, useRecoilValue, useRoute } from '@linda/frontend/frontend'
import { documentsRoute, editListLayoutRoute } from '../index'
import { IButtonProps } from '@linda/ui/frontend'
const Content = React.lazy(() => import('./content-viewer/index'))
const Editor = React.lazy(() => import('./document-editor/index'))
const DocumentListLayoutEditor = React.lazy(() => import('./document-list-layout-editor/index'))

export const pageButtons = atom<IButtonProps[]>({
  key: 'content-manager/documents/pageButtons',
  default: []
})

const Documents: React.FC = () => {
  const [showEditor] = useRoute(documentsRoute('/schema/:any*'))
  const [showList] = useRoute(documentsRoute('/content/list/:any*'))
  const [showSingle] = useRoute(documentsRoute('/content/list/:any*'))
  const [showListEditor, params] = useRoute<{ module: string, name: string }>(editListLayoutRoute({ module: ':module', name: ':name' }))
  const buttons = useRecoilValue(pageButtons)
  return <AdminPage template='2-cols' name={<Translated message="Documents" />} buttons={buttons}>
    <AdminSuspense>
      <DocumentsList/>
    </AdminSuspense>
    {showEditor && <AdminSuspense>
      <Editor />
    </AdminSuspense>}
    {(showSingle || showSingle) && <AdminSuspense>
      <Content />
    </AdminSuspense>}
    {showListEditor && params && <AdminSuspense>
      <DocumentListLayoutEditor doc={params} />
    </AdminSuspense>}
  </AdminPage>
}

export default Documents
