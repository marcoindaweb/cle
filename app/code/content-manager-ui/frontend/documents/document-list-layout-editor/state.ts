/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { atomFamily, DefaultValue, selectorFamily, useRecoilCallback } from '@linda/frontend/frontend'
import { DocListLayout, DocListLayoutCol } from '../../types'

export const listLayout = selectorFamily<DocListLayout, string>({
  key: 'content-manager/list-layout',
  get: (id) => ({ get }) => ({
    _id: id,
    columns: get(listLayoutCols(id))
  }),
  set: (id) => ({ set, reset }, val) => {
    if (val instanceof DefaultValue) {
      reset(listLayoutCols(id))
    } else {
      set(listLayoutCols(id), val.columns)
    }
  }
})

export const listLayoutCols = selectorFamily<DocListLayoutCol[], string>({
  key: 'content-manager/list-layout/cols',
  get: (id) => ({ get }) => {
    const o: DocListLayoutCol[] = []
    const ids = get(listLayoutColsIds(id))
    for (const i of ids) {
      o.push(get(listLayoutCol(i)))
    }
    return o
  },
  set: (id) => ({ set, get, reset }, val) => {
    if (val instanceof DefaultValue) {
      const ids = get(listLayoutColsIds(id))
      for (const i of ids) {
        reset(listLayoutCol(i))
      }
      reset(listLayoutColsIds(id))
    } else {
      for (const [index, col] of val.entries()) {
        set(listLayoutCol(index.toString()), col)
      }
      set(listLayoutColsIds(id), Object.keys(val))
    }
  }
})

export const listLayoutColsIds = atomFamily<string[], string>({
  key: 'content-manager/list-layout/cols-ids',
  default: []
})

export const listLayoutCol = selectorFamily<DocListLayoutCol, string>({
  key: 'content-manager/list-layout/col',
  get: (id) => ({ get }) => {
    return {
      property: get(listLayoutColProperty(id)),
      order: get(listLayoutColOrder(id))
    }
  },
  set: (id) => ({ set, get, reset }, val) => {
    if (val instanceof DefaultValue) {
      reset(listLayoutColProperty(id))
      reset(listLayoutColProperty(id))
    } else {
      set(listLayoutColProperty(id), val.property)
      set(listLayoutColOrder(id), val.order)
    }
  }
})

export const listLayoutColProperty = atomFamily<string, string>({
  key: 'content-manager/list-layout/col-property',
  default: ''
})

export const listLayoutColOrder = atomFamily<number, string>({
  key: 'content-manager/list-layout/col-order',
  default: 0
})

export const useSetPropertyOrder = (): (id: string, order: number) => void =>
  useRecoilCallback(({ set }) => (id: string, order: number) => {
    set(listLayoutColOrder(id), order)
  }, [])
