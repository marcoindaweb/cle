/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { ReducedDocument } from '@linda/content-manager/types'
import { AjaxError, useRecoilValue, useSetRecoilState } from '@linda/frontend/frontend'
import { fetchDocumentListLayout } from '../../api/document-list-layout'
import { AdminAjaxError } from '@linda/admin/frontend/AdminAjaxError'
import React, { useEffect } from 'react'
import { listLayout } from './state'
import AdminSuspense from '@linda/admin/frontend/AdminSuspense'
import { getListID } from '@linda/content-manager-ui/list-layout/calculations'
import Cols from './cols'

const DocumentListLayoutEditor: React.FC<{ doc: ReducedDocument }> = ({ doc }) => {
  return <AdminSuspense>
    <ListLayoutFetcher doc={doc}>
      <div>
        <Cols id={getListID(doc)} />
      </div>
    </ListLayoutFetcher>
  </AdminSuspense>
}

const ListLayoutFetcher: React.FC<{ doc: ReducedDocument }> = ({ doc, children }) => {
  const list = useRecoilValue(fetchDocumentListLayout(doc))
  const id = getListID(doc)
  const setList = useSetRecoilState(listLayout(id))
  useEffect(() => {
    if (list instanceof AjaxError) {
      return
    }
    setList(list)
  }, [list, id])
  return list instanceof AjaxError ? <AdminAjaxError err={list} /> : <>{children}</>
}

export default DocumentListLayoutEditor
