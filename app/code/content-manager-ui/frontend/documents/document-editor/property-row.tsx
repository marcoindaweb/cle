/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Property, PropertyType } from '@linda/content-manager/types'
import React from 'react'
import { Button, EditableText } from '@linda/ui/frontend'
import style from './style.sass'
import { useRecoilState, useRecoilValue } from '@linda/frontend/frontend'
import {
  propertiesIds,
  schemaProperty,
  schemaPropertyName,
  schemaPropertyType,
  useAddNewPropertyToDocument, useRemovePropertyIdFromIdList
} from './state'
import { PropertyKindButton } from './property-row/type'
import Options from './property-row/options'

const hasDocument = (type: PropertyType): boolean => [PropertyType.object, PropertyType.array].includes(type)

const AddButton: React.FC<{ id: string }> = ({ id }) => {
  const type = useRecoilValue(schemaPropertyType(id))
  const show = hasDocument(type)
  const onAdd = useAddNewPropertyToDocument(id)

  return show ? <Button onClick={onAdd} minimal small icon='plus' /> : null
}

const Name: React.FC<{ id: string }> = ({ id }) => {
  const [name, setName] = useRecoilState(schemaPropertyName(id))
  return <EditableText value={name} onChange={setName} />
}

const PropertyRow: React.FC<{ id: string, onRemove: () => void, nestLvl?: number }> = ({ id, onRemove, nestLvl }) => {
  const type = useRecoilValue(schemaPropertyType(id))
  const docIds = useRecoilValue(propertiesIds(id))
  const name = useRecoilValue(schemaPropertyName(id))
  const removeFromList = useRemovePropertyIdFromIdList(id)
  return <>
    <div style={{ paddingLeft: 10 * (nestLvl ?? 0) }} className={style['property-row']}>
      <div><AddButton id={id} /></div>
      <div><Name id={id} /></div>
      <div><PropertyKindButton id={id} /></div>
      <div>
        <Options id={id}/>
        <Button small minimal icon='delete' disabled={name === '_id'} onClick={onRemove} />
      </div>
    </div>
    {hasDocument(type) && docIds.map(i => <PropertyRow
      key={i}
      nestLvl={typeof nestLvl === 'number' ? nestLvl + 1 : 1}
      id={i}
      onRemove={() => removeFromList(i)}
    />)}
  </>
}

export default PropertyRow
