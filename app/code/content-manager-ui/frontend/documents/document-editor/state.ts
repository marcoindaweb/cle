/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import {
  atom,
  atomFamily,
  DefaultValue,
  RecoilState,
  selector,
  selectorFamily,
  useRecoilCallback,
  useRecoilValue
} from '@linda/frontend/frontend'
import { DocumentSchema, Property, PropertyType } from '@linda/content-manager/types'
import { useCreateDocument, useUpdateDocument } from '../../api/document-schema'
import { v4 } from 'uuid'

export const schema = selectorFamily<DocumentSchema, string>({
  key: 'content-manager/documents/schema/schema',
  get: id => ({ get }) => ({
    name: get(schemaName(id)),
    module: get(schemaModule(id)),
    properties: { _id: { type: PropertyType.objectId }, ...get(schemaProperties(id)) }
  }),
  set: id => ({ set, reset }, val) => {
    if (val instanceof DefaultValue) {
      reset(schemaName(id))
      reset(schemaModule(id))
      reset(schemaProperties(id))
    } else {
      set(schemaName(id), val.name)
      set(schemaModule(id), val.module)
      set(schemaProperties(id), val.properties)
    }
  }
})

export const schemaName = atomFamily<string, string>({
  key: 'content-manager/documents/schema/schema-name',
  default: 'New Document'
})

export const schemaModule = atomFamily<string, string>({
  key: 'content-manager/documents/schema/schema-module',
  default: 'Module'
})

export const schemaProperties = selectorFamily<Omit<DocumentSchema['properties'], '_id'>, string>({
  key: 'content-manager/documents/schema/current-document-properties',
  get: id => ({ get }) => {
    const o: Omit<DocumentSchema['properties'], '_id'> = {}
    const ids = get(propertiesIds(id))
    for (const id of ids) {
      const prop = get(schemaProperty(id))
      o[prop.name] = prop
    }
    return o
  },
  set: id => ({ set, reset, get }, val) => {
    if (val instanceof DefaultValue) {
      const ids = get(propertiesIds(id))
      for (const id of ids) {
        reset(schemaProperty(id))
      }
      reset(propertiesIds(id))
    } else {
      const keys = Object.keys(val)
      const idsToAdd: string[] = []
      for (const key of keys) {
        const prop = get(propertyByName([id, key]))
        if (prop !== undefined) {
          set(propertyByName([id, key]), { ...val[key], name: key })
        } else {
          const id = v4()
          idsToAdd.push(id)
          set(schemaProperty(id), { ...val[key], name: key })
        }
      }
      set(propertiesIds(id), v => [...v, ...idsToAdd])
    }
  }
})
export const propertiesIds = atomFamily<string[], string>({
  key: 'content-manager/documents/schema/current-document-properties-keys',
  default: []
})

export const schemaProperty = selectorFamily<Property & { name: string }, string>({
  key: 'content-manager/documents/schema/current-document-property',
  get: (id) => ({ get }) => {
    const type = get(schemaPropertyType(id))
    switch (type) {
      case PropertyType.boolean:
      case PropertyType.date:
      case PropertyType.number:
      case PropertyType.string:
      case PropertyType.file:
      case PropertyType.objectId:
        return {
          name: get(schemaPropertyName(id)),
          type,
          optional: get(schemaPropertyOptional(id))
        }
      case PropertyType.relation:
        return {
          name: get(schemaPropertyName(id)),
          type,
          ref: get(schemaPropertyRef(id)),
          optional: get(schemaPropertyOptional(id)),
          many: get(schemaPropertyMany(id)),
          refProperty: get(schemaPropertyRefProperty(id))
        }
      case PropertyType.object:
        return {
          name: get(schemaPropertyName(id)),
          type,
          document: get(schemaProperties(id)),
          optional: get(schemaPropertyOptional(id))
        }
      case PropertyType.array:
        return {
          name: get(schemaPropertyName(id)),
          type,
          document: get(schemaProperties(id)),
          optional: get(schemaPropertyOptional(id))
        }
      default:
        throw new Error(`Unable to find property ${id}`)
    }
  },
  set: (id) => ({ get, reset, set }, val) => {
    if (val instanceof DefaultValue) {
      reset(schemaPropertyName(id))
      reset(schemaPropertyType(id))
      reset(schemaPropertyRef(id))
      reset(schemaProperties(id))
    } else {
      set(schemaPropertyName(id), val.name)
      set(schemaPropertyType(id), val.type)
      set(schemaPropertyOptional(id), val.optional)
      if (val.type === PropertyType.relation) {
        set(schemaPropertyRef(id), val.ref)
        set(schemaPropertyMany(id), val.many)
        set(schemaPropertyRefProperty(id), val.refProperty)
      }
      if (val.type === PropertyType.object || val.type === PropertyType.array) {
        set(schemaProperties(id), val.document)
      }
    }
  }
})

export const schemaPropertyName = atomFamily<string, string>({
  key: 'content-manager/documents/schema/current-document-property-name',
  default: 'new_property'
})
export const schemaPropertyType = atomFamily<PropertyType, string>({
  key: 'content-manager/documents/schema/current-document-property-type',
  default: PropertyType.string
})
export const schemaPropertyRef = atomFamily<string, string>({
  key: 'content-manager/documents/schema/current-document-property-ref',
  default: ''
})
export const schemaPropertyMany = atomFamily<boolean | undefined, string>({
  key: 'content-manager/documents/schema/current-document-property-many',
  default: undefined
})
export const schemaPropertyRefProperty = atomFamily<string | undefined, string>({
  key: 'content-manager/documents/schema/current-document-property-ref-property',
  default: undefined
})
export const schemaPropertyOptional = atomFamily<boolean, string>({
  key: 'content-manager/documents/schema/current-document-property-optional',
  default: false
})

export const propertyByName =
  selectorFamily<Property & { name: string } | undefined, [string, string]>({
    key: 'content-manager/documents/schema/current-document-property-by-name',
    get: ([id, name]) => ({ get }) => {
      for (const i of get(propertiesIds(id))) {
        const prop = get(schemaProperty(i))
        if (prop.name === name) {
          return prop
        }
      }
    },
    set: ([id, name]) => ({ set, get, reset }, val) => {
      if (val instanceof DefaultValue) {
        for (const i of get(propertiesIds(id))) {
          const prop = get(schemaProperty(i))
          if (prop.name === name) {
            reset(schemaProperty(i))
            set(propertiesIds(i), v => v.filter(cId => cId !== i))
            break
          }
        }
      } else {
        for (const i of get(propertiesIds(id))) {
          const prop = get(schemaProperty(i))
          if (prop.name === name) {
            set(schemaProperty(i), val)
            break
          }
        }
      }
    }
  })

export const currentSchemaId = atom<string>({
  key: 'content-manager/schema/form/current-document-id',
  default: ''
})

const currentSchemaSelector = <T>(
  key: string,
  atom: (id: string) => RecoilState<T>
): RecoilState<T> => {
  return selector({
    key,
    get: ({ get }) => {
      const id = get(currentSchemaId)
      return get(atom(id))
    },
    set: ({ set, get, reset }, val) => {
      const id = get(currentSchemaId)
      if (val instanceof DefaultValue) {
        reset(atom(id))
      } else {
        set(atom(id), val)
      }
    }
  })
}

export const currentSchema = currentSchemaSelector(
  'content-manager/schema/form/current-schema',
  schema
)
export const currentSchemaName = currentSchemaSelector(
  'content-manager/schema/form/current-schema-name',
  schemaName
)
export const currentSchemaModule = currentSchemaSelector(
  'content-manager/schema/form/current-schema-module',
  schemaModule
)
export const currentSchemaPropertyIds = currentSchemaSelector(
  'content-manager/schema/form/current-schema-property-ids',
  propertiesIds
)

export const useUpdateCurrentDocument = (): () => Promise<void> => {
  const name = useRecoilValue(currentSchemaName)
  const module = useRecoilValue(currentSchemaModule)
  const update = useUpdateDocument({ name, module })
  return useRecoilCallback(({ snapshot }) => async () => {
    const doc = await snapshot.getPromise(currentSchema)
    await update(doc)
  }, [name])
}

export const useCreateCurrentDocument = (): () => Promise<void> => {
  const create = useCreateDocument()
  return useRecoilCallback(({ snapshot }) => async () => {
    const doc = await snapshot.getPromise(currentSchema)
    await create(doc)
  }, [create])
}

export const useAddNewProperty = (): () => Promise<void> =>
  useRecoilCallback(({ set }) => async () => {
    set(currentSchemaPropertyIds, v => [...v, v4()])
  }, [])

export const useAddNewPropertyToDocument = (id: string): () => Promise<void> =>
  useRecoilCallback(({ set }) => async () => {
    set(propertiesIds(id), v => [...v, v4()])
  }, [id])

export const useRemovePropertyIdFromIdList = (listId: string): (id: string) => void =>
  useRecoilCallback(({ set }) => (id: string) => {
    set(propertiesIds(listId), v => v.filter(i => i !== id))
  }, [listId])
