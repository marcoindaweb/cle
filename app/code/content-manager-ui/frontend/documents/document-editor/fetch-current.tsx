/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { AjaxError, useRecoilCallback } from '@linda/frontend/frontend'
import { fetchDocumentSchema } from '../../api/document-schema'
import React, { useEffect, useState } from 'react'
import AdminToast from '@linda/admin/frontend/AdminToast'
import useTranslate from '@linda/i18n/frontend/hooks/useTranslate'
import { currentSchema, currentSchemaId } from './state'

const FetchCurrent: React.FC<{ name: string, module: string }> = ({ name, module, children }) => {
  const translate = useTranslate()
  const writeDocument = useRecoilCallback(({ snapshot, set }) => async () => {
    const doc = await snapshot.getPromise(fetchDocumentSchema({ name, module }))
    if (doc instanceof AjaxError) {
      AdminToast.show({
        message: translate('There was a problem fetching the current document'),
        intent: 'danger'
      })
      return
    }
    const id = `${doc.module}__${doc.name}`
    set(currentSchemaId, id)
    set(currentSchema, doc)
  }, [name, module])
  const [ready, setReady] = useState(false)
  useEffect(() => {
    setReady(false)
    writeDocument()
      .then(() => setReady(true))
      .finally(() => {})
  }, [name, module])
  return ready ? <>{children}</> : null
}

export default FetchCurrent
