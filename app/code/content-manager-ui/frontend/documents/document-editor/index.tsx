/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useCallback, useEffect, useState } from 'react'
import { Button, EditableText } from '@linda/ui/frontend'
import {
  useRecoilState,
  useRecoilValue,
  useResetRecoilState,
  useRoute,
} from '@linda/frontend/frontend'
import { createDocumentSchemaRoute, editDocumentSchemaRoute } from '../../index'
import FetchCurrent from './fetch-current'
import AdminSuspense from '@linda/admin/frontend/AdminSuspense'
import Translated from '@linda/i18n/frontend/components/Translated'
import Admin404 from '@linda/admin/frontend/Admin404'
import style from './style.sass'
import PropertyRow from './property-row'
import { ModulesSelect } from '@linda/modules-ui/frontend/modules-select'
import { decodeModule, encodeModule } from '@linda/content-manager/transport/http/encode-decode'
import {
  currentSchemaName,
  currentSchemaModule,
  currentSchemaPropertyIds,
  currentSchema,
  useAddNewProperty, useRemovePropertyIdFromIdList, currentSchemaId
} from './state'
import { FormButtons } from './form-buttons'

const EditorForm: React.FC = () => {
  const [name, setName] = useRecoilState(currentSchemaName)
  const [module, setModule] = useRecoilState(currentSchemaModule)
  const [isCreating] = useRoute(createDocumentSchemaRoute())
  const ids = useRecoilValue(currentSchemaPropertyIds)
  const schemaId = useRecoilValue(currentSchemaId)
  const removeFromIdsList = useRemovePropertyIdFromIdList(schemaId)

  const addNewProp = useAddNewProperty()

  return <>
    <FormButtons/>
    <h2 className={style['document-name']}>
      <EditableText disabled={!isCreating} value={name} onChange={setName} />
    </h2>
    <ModulesSelect
      disabled={!isCreating}
      minimal
      onChange={v => setModule(encodeModule(v))}
      value={decodeModule(module)}
    />
    <h3><Translated message="Properties" /></h3>
    <div className={style['property-list']}>
      <div className={style['property-row']}>
        <div></div>
        <div><Translated message='name' /></div>
        <div><Translated message='type' /></div>
        <div></div>
      </div>
      {ids.map(v => <PropertyRow key={v} id={v} onRemove={() => {
        removeFromIdsList(v)
      }} />)}
      <div className={style['property-row']}>
        <Button onClick={() => addNewProp()} icon='plus' small minimal />
      </div>
    </div>
  </>
}

const DocumentsEditor: React.FC = () => {
  const resetDoc = useResetRecoilState(currentSchema)
  const [resetted, setResetted] = useState(false)
  const [isUpdating, props] = useRoute<{ name: string, module: string }>(
    editDocumentSchemaRoute({ name: ':name', module: ':module' })
  )
  const [isCreating] = useRoute(createDocumentSchemaRoute())
  useEffect(() => {
    if (isCreating && !resetted) {
      resetDoc()
      setResetted(true)
    } else if (!isCreating) {
      setResetted(false)
    }
  }, [isCreating, resetted])

  return <div>
    {props !== null && isUpdating
      ? <AdminSuspense>
        <FetchCurrent name={props.name} module={props.module}>
          <EditorForm />
        </FetchCurrent>
      </AdminSuspense>
      : isCreating ? <EditorForm/> : <Admin404 />}
  </div>
}
export default DocumentsEditor
