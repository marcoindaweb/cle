/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useCallback, useEffect, useState } from 'react'
import Translated from '@linda/i18n/frontend/components/Translated'
import { useLocation, useRecoilCallback, useRoute, useSetRecoilState } from '@linda/frontend/frontend'
import { pageButtons } from '../index'
import { currentSchemaModule, currentSchemaName, useCreateCurrentDocument, useUpdateCurrentDocument } from './state'
import { createDocumentSchemaRoute, editDocumentSchemaRoute } from '../../index'

export const FormButtons: React.FC = () => {
  const setPageButtons = useSetRecoilState(pageButtons)
  const createCurrent = useCreateCurrentDocument()
  const updateCurrent = useUpdateCurrentDocument()
  const [inProgress, setInProgress] = useState(false)
  const [isCreating] = useRoute(createDocumentSchemaRoute())
  const [,setLocation] = useLocation()
  const redirectToNewDocument = useRecoilCallback(({ snapshot }) => async () => {
    const name = await snapshot.getPromise(currentSchemaName)
    const module = await snapshot.getPromise(currentSchemaModule)
    setLocation(editDocumentSchemaRoute({ module, name }))
  }, [setLocation])

  const onSave = useCallback(async () => {
    try {
      setInProgress(true)
      isCreating ? await createCurrent() : await updateCurrent()
    } finally {
      setInProgress(false)
      if (isCreating) {
        await redirectToNewDocument()
      }
    }
  }, [isCreating, createCurrent, updateCurrent])

  useEffect(() => {
    if (isCreating) {
      setPageButtons([
        {
          text: inProgress
            ? <Translated message='Please Wait' />
            : <Translated message='Save' />,
          intent: 'success',
          disabled: inProgress,
          onClick: onSave
        }])
    } else {
      setPageButtons([
        {
          text: inProgress
            ? <Translated message='Please Wait' />
            : <Translated message='Update' />,
          intent: 'success',
          disabled: inProgress,
          onClick: onSave
        }
      ])
    }
    return () => setPageButtons([])
  }, [isCreating, inProgress])

  return null
}
