/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { renderHook } from '@testing-library/react-hooks'
import { RecoilRoot, useRecoilState, useRecoilValue, useSetRecoilState } from '@linda/frontend/frontend'
import { propertiesIds, propertyByName, schema } from '../state'
import React, { useEffect } from 'react'
import { PropertyType } from '@linda/content-manager/types'

const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>

describe('form state', () => {
  it('set the properties', () => {
    const { result } = renderHook(() => {
      const setSchema = useSetRecoilState(schema('test'))
      const testProperty = useRecoilValue(propertyByName(['test', 'test']))
      const idProperty = useRecoilValue(propertyByName(['test', '_id']))
      const ids = useRecoilValue(propertiesIds('test'))
      useEffect(() => {
        setSchema({
          name: 'name',
          module: 'test',
          properties: {
            _id: {
              type: PropertyType.objectId
            },
            test: {
              type: PropertyType.number
            }
          }
        })
      }, [])
      return { testProperty, idProperty, ids }
    }, { wrapper })
    expect(result.current.ids.length).toEqual(2)
    expect(result.current.idProperty).toEqual({ name: '_id', type: PropertyType.objectId })
    expect(result.current.testProperty).toEqual({ name: 'test', type: PropertyType.number })
  })

  it('can have document properties (object | array)', () => {
    const { result } = renderHook(() => {
      const [doc, setDoc] = useRecoilState(schema('test'))
      const val = useRecoilValue(propertyByName(['test', 'test']))
      useEffect(() => {
        setDoc({
          name: 'test',
          module: 'test',
          properties: {
            _id: {
              type: PropertyType.objectId
            },
            test: {
              type: PropertyType.array,
              document: {
                nested: {
                  type: PropertyType.string
                }
              }
            }
          }
        })
      }, [])
      return { doc, val }
    }, { wrapper })
    expect(result.current.doc.properties).toEqual({
      _id: {
        name: '_id',
        type: PropertyType.objectId
      },
      test: {
        name: 'test',
        type: PropertyType.array,
        document: {
          nested: {
            name: 'nested',
            type: PropertyType.string
          }
        }
      }
    })
    expect(result.current.val).toEqual({
      name: 'test',
      type: PropertyType.array,
      document: {
        nested: {
          name: 'nested',
          type: PropertyType.string
        }
      }
    })
  })
})
