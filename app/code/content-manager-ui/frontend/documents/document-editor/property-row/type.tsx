/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Property, PropertyType } from '@linda/content-manager/types'
import style from '../style.sass'
import React from 'react'
import { useRecoilState } from '@linda/frontend/frontend'
import { schemaPropertyType } from '../state'
import { Button, ButtonGroup, Popover } from '@linda/ui/frontend'

const getTypeName = (property: Pick<Property, 'type'>): string => {
  switch (property.type) {
    case PropertyType.array:
      return 'Array'
    case PropertyType.boolean:
      return 'Boolean'
    case PropertyType.date:
      return 'Date'
    case PropertyType.file:
      return 'File'
    case PropertyType.number:
      return 'Number'
    case PropertyType.object:
      return 'Object'
    case PropertyType.objectId:
      return 'ID'
    case PropertyType.relation:
      return 'Relation'
    case PropertyType.string:
      return 'String'
  }
}
const getTypeClass = (property: Pick<Property, 'type'>): string => {
  switch (property.type) {
    case PropertyType.string:
      return style.string
    case PropertyType.relation:
      return style.relation
    case PropertyType.objectId:
      return style['object-id']
    case PropertyType.object:
      return style.object
    case PropertyType.number:
      return style.number
    case PropertyType.file:
      return style.file
    case PropertyType.date:
      return style.date
    case PropertyType.boolean:
      return style.boolean
    case PropertyType.array:
      return style.array
  }
}
export const types = [
  PropertyType.array,
  PropertyType.string,
  PropertyType.object,
  PropertyType.relation,
  PropertyType.number,
  PropertyType.objectId,
  PropertyType.file,
  PropertyType.date,
  PropertyType.boolean
]

export const PropertyKindButton: React.FC<{ id: string }> = ({ id }) => {
  const [type, setType] = useRecoilState(schemaPropertyType(id))
  return <Popover target={<Button
    small
    className={[style['property-button'], getTypeClass({ type })].join(' ')}
    minimal
  >
    {getTypeName({ type })}
  </Button>} content={
    <div className={style['property-buttons-list']}>
      {types.map(t => <Button
        key={t}
        small
        className={[style['property-button'], getTypeClass({ type: t })].join(' ')}
        minimal
        onClick={() => setType(t)}
      >
        {getTypeName({ type: t })}
      </Button>)}
    </div>
  }/>
}
