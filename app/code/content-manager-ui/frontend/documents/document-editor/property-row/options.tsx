/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useEffect, useState } from 'react'
import { Button, FormGroup, HTMLSelect, Popover, Switch } from '@linda/ui/frontend'
import classes from './style.sass'
import { AjaxError, useRecoilState, useRecoilValue } from '@linda/frontend/frontend'
import {
  currentSchemaModule,
  currentSchemaName,
  schemaPropertyMany,
  schemaPropertyName,
  schemaPropertyOptional,
  schemaPropertyRef,
  schemaPropertyRefProperty,
  schemaPropertyType
} from '../state'
import Translated from '@linda/i18n/frontend/components/Translated'
import { PropertyType, ReducedDocument, RelationProperty } from '@linda/content-manager/types'
import {
  decodeRef,
  getNestedPropertyOfType,
  getOnlyPropertiesWithRelation,
  makeRef
} from '@linda/content-manager/documents-schema/data/calculations'
import { fetchDocumentSchema, fetchReducedDocumentsList } from '../../../api/document-schema'
import { AdminAjaxError } from '@linda/admin/frontend/AdminAjaxError'
import { decodeDoc, decodeModule, encodeDoc } from '@linda/content-manager/transport/http/encode-decode'
import useTranslate from '@linda/i18n/frontend/hooks/useTranslate'
import AdminSuspense from '@linda/admin/frontend/AdminSuspense'

const Options: React.FC<{ id: string }> = ({ id }) => {
  const type = useRecoilValue(schemaPropertyType(id))
  return <Popover
    target={<Button icon='cog' minimal small />}
    content={<div className={classes.options}>
      <IsOptional id={id} />
      {type === PropertyType.relation && <>
        <Ref id={id} />
        <Many id={id} />
        <RefProperty id={id} />
      </>}
    </div>}
  />
}

const IsOptional: React.FC<{ id: string }> = ({ id }) => {
  const [optional, setOptional] = useRecoilState(schemaPropertyOptional(id))
  const name = useRecoilValue(schemaPropertyName(id))
  return <Switch
    labelElement={<Translated message='Optional' />}
    disabled={name === '_id'}
    checked={optional}
    onChange={() => setOptional(!optional)}
  />
}

const Ref: React.FC<{ id: string }> = ({ id }) => {
  const [ref, setRef] = useRecoilState(schemaPropertyRef(id))
  const docs = useRecoilValue(fetchReducedDocumentsList)
  const schemaName = useRecoilValue(currentSchemaName)
  const schemaModule = useRecoilValue(currentSchemaModule)
  return docs instanceof AjaxError
    ? <AdminAjaxError err={docs} />
    : <FormGroup label={<Translated message='Related Document' />}>
      <HTMLSelect
        value={ref}
        onChange={e => setRef(e.target.value)}
      >
        <Translated message='Select a related document' >{msg =>
          <option>{msg}</option>
        }</Translated>
        {docs
          .map(decodeDoc)
          .filter(doc => !(doc.name === schemaName && doc.module === schemaModule))
          .map(doc => <Translated message={doc.name} >{name =>
            <option
              key={makeRef(doc)}
              value={makeRef(doc)}
            >
              {decodeModule(doc.module)} - {name}
            </option>
          }</Translated>
          )}
      </HTMLSelect>
    </FormGroup>
}

const Many: React.FC<{ id: string }> = ({ id }) => {
  const [many, setMany] = useRecoilState(schemaPropertyMany(id))
  const translate = useTranslate()
  return <Switch
    checked={many}
    onChange={() => setMany(typeof many === 'boolean' ? !many : true)}
    labelElement={<Translated message='Relation Kind' />}
    innerLabel={translate('one')}
    innerLabelChecked={translate('many')}
  />
}

const RefProperty: React.FC<{ id: string }> = ({ id }) => {
  const ref = useRecoilValue(schemaPropertyRef(id))
  const doc = ref.length > 0 ? decodeRef(ref) : false
  return doc === false
    ? null
    : <AdminSuspense><RefPropertySelect id={id} doc={doc} /></AdminSuspense>
}

const RefPropertySelect: React.FC<{ id: string, doc: ReducedDocument }> = ({ id, doc }) => {
  const relatedDocument = useRecoilValue(fetchDocumentSchema(encodeDoc(doc)))
  const [refProperty, setRefProperty] = useRecoilState(schemaPropertyRefProperty(id))
  const [options, setOptions] = useState<string[]>([])
  useEffect(() => {
    if (relatedDocument instanceof AjaxError) {
      setOptions([])
      return
    }
    const opts = Object.entries(getOnlyPropertiesWithRelation(relatedDocument.properties))
      .map(([name, p]) => {
        const prop = getNestedPropertyOfType<RelationProperty>(p, PropertyType.relation, name)
        return prop !== undefined ? prop : prop
      })
      .filter((val) => val !== undefined && val[0].refProperty === undefined)
      .map((v: [RelationProperty, string]) => v[1])
    setOptions(opts)
  }, [relatedDocument])

  if (relatedDocument instanceof AjaxError) return null

  return options.length > 0 ? <HTMLSelect
    value={refProperty}
    onChange={v => setRefProperty(v.target.value)}
  >
    <Translated message='Select reverse property'>{msg => <option>{msg}</option>}</Translated>
    {options.map(v => <option key={v} value={v}>{v}</option>)}
  </HTMLSelect> : null
}

export default Options
