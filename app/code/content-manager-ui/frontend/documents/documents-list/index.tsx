/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import AdminList from '@linda/admin/frontend/AdminList'
import { AjaxError, useRecoilValue } from '@linda/frontend/frontend'
import { fetchReducedDocumentsList } from '../../api/document-schema'
import AdminListItem from '@linda/admin/frontend/AdminListItem'
import React from 'react'
import { createDocumentSchemaRoute, listDocumentsRoute } from '../../index'
import { Divider } from '@linda/ui/frontend'
import Translated from '@linda/i18n/frontend/components/Translated'
import { AdminAjaxError } from '@linda/admin/frontend/AdminAjaxError'

const DocumentsList: React.FC = () => {
  const res = useRecoilValue(fetchReducedDocumentsList)
  return res instanceof AjaxError ? <AdminAjaxError err={res} /> : <AdminList>
    {res?.map(v =>
      <AdminListItem
        to={listDocumentsRoute(v)}
      >
        <Translated message={v.name} />
      </AdminListItem>)}
    {res?.length === 0 && <AdminListItem icon='clean' >
      <Translated message="There is no document" />
    </AdminListItem>}
    <Divider />
    <AdminListItem icon='add' to={createDocumentSchemaRoute()}>
      <Translated message="New Document" />
    </AdminListItem>
  </AdminList>
}
export default DocumentsList
