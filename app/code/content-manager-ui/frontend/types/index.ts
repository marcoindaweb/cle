/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { DocumentListLayout } from '@linda/content-manager-ui/documents/DocumentListLayout'
import { DocOverHttp } from '@linda/content-manager/types'

export type DocListLayout = DocOverHttp<DocumentListLayout>
export type DocListLayoutCol = DocListLayout['columns'][0]
