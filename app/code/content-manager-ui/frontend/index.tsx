/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

/* eslint-disable @typescript-eslint/prefer-ts-expect-error */
import React, { useEffect } from 'react'
import { useRoute } from '@linda/frontend/frontend'
import { admin } from '@linda/admin/frontend/Admin'
import AdminSuspense from '@linda/admin/frontend/AdminSuspense'
import { useAddSidebarItem } from '@linda/admin/frontend/Admin/Sidebar'
import { ReducedDocument } from '@linda/content-manager/types'

// eslint-disable-next-line @typescript-eslint/promise-function-async
// @ts-ignore
const Documents = React.lazy(() => import('./documents/index'))

export const documentRoot = admin('/content-manager/documents')
export const documentsRoute = (route?: string): string =>
  admin(`/content-manager/documents${typeof route === 'string' ? route : ''}`)
export const editDocumentSchemaRoute = ({ module, name }: ReducedDocument): string =>
  documentsRoute(`/schema/edit/${module}/${name}`)
export const createDocumentSchemaRoute = (): string =>
  documentsRoute('/schema/create')
export const editListLayoutRoute = ({ module, name }: ReducedDocument) =>
  documentsRoute(`/content/list/edit-layout/${module}/${name}`)
export const listDocumentsRoute = ({ module, name }: ReducedDocument): string =>
  documentsRoute(`/content/list/${module}/${name}`)
export const createDocumentRoute = ({ module, name }: ReducedDocument): string =>
  documentsRoute(`/content/create/${module}/${name}`)
export const updateDocumentRoute = ({ module, name }: ReducedDocument, id: string | number): string =>
  documentsRoute(`/content/update/${module}/${name}/${id}`)

const Index: React.FC = () => {
  const [matchDocuments] = useRoute<{ path: string }>(documentsRoute('/:path*'))
  const addSidebarItem = useAddSidebarItem()
  useEffect(() => {
    addSidebarItem({
      icon: 'document',
      name: 'Documents',
      key: 'content-manager',
      groups: [{
        key: 'content-manager-documents',
        title: 'Documents',
        routes: [
          {
            path: admin('/content-manager/documents'),
            name: 'Documents'
          }
        ]
      }]
    })
  }, [])

  return <>
    {matchDocuments && <AdminSuspense>
      <Documents />
    </AdminSuspense>}
  </>
}

export default Index
