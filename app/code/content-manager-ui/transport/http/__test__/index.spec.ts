/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { clearTestingDB, useTestingDB } from '@linda/database/test'
import { load, start } from '@linda/core'
import { app } from '@linda/core/transport/http'
import { encodeDoc } from '@linda/content-manager/transport/http/encode-decode'
import { encodeListID, getListID } from '../../../list-layout/calculations'
import { DocumentSchema, PropertyType, registerDocument } from '@linda/content-manager'

describe('http-transport', () => {
  beforeAll(async () => {
    useTestingDB()
    load('@linda/database')
    load('@linda/content-manager')
    load('@linda/content-manager-ui')
    await start()
  })

  afterEach(async () => {
    await clearTestingDB()
  })

  it('get document list layout by id', async () => {
    const document: DocumentSchema = {
      name: 'testdoc',
      module: '@test/module',
      properties: {
        _id: { type: PropertyType.objectId },
        array: { type: PropertyType.array, document: { name: { type: PropertyType.string } } },
        name: { type: PropertyType.string },
        age: { type: PropertyType.number }
      }
    }
    registerDocument(document)
    const docId = getListID(encodeDoc(document))
    const res = await app.inject({
      method: 'GET',
      path: `/content-manager-ui/v1/list-layout/${docId}`
    })
    expect(res.statusCode).toEqual(200)
    expect(JSON.parse(res.body)).toEqual({
      _id: encodeListID(docId),
      columns: [
        { property: '_id', order: 0 },
        { property: 'name', order: 1 },
        { property: 'age', order: 2 }
      ]
    })
  })
})
