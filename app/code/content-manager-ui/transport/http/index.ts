/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { app, FastifyPluginCallback, FastifyRequest } from '@linda/core/transport/http'
import { getDocumentListLayout, updateDocumentListLayout } from '../../list-layout/actions'
import { decodeRef } from '@linda/content-manager/documents-schema/data/calculations'
import { DocumentListLayout } from '../../documents/DocumentListLayout'
import { DocOverHttp } from '@linda/content-manager/types'
import { decodeDocumentOverHttp } from '@linda/content-manager/document-data/http-decode'
import { decodeListID, decodeListLayout, encodeListLayout } from '../../list-layout/calculations'

const contentManagerUi: FastifyPluginCallback = (app, opts, next) => {
  type FindListLayoutRequest = FastifyRequest<{ Params: { id: string }}>
  app.get('/list-layout/:id', async (request: FindListLayoutRequest, reply) => {
    const listLayout = await getDocumentListLayout(decodeRef(decodeListID(request.params.id)))
    if (listLayout === undefined) {
      await reply.status(404).send()
      return
    }
    await reply.send(encodeListLayout(listLayout))
  })

  type UpdateListLayoutRequest = FastifyRequest<{
    Params: { id: string }
    Body: { layout: DocOverHttp<DocumentListLayout> }
  }>
  app.put('/list-layout/:id', async (request: UpdateListLayoutRequest, reply) => {
    const decoded = decodeDocumentOverHttp(decodeListLayout(request.body.layout))
    await updateDocumentListLayout(decoded)
    await reply.send(encodeListLayout(decoded))
  })
  next()
}

app.register(contentManagerUi, { prefix: '/content-manager-ui/v1' })
