/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { collection, ObjectID, Collection } from '@linda/database'
import { registerDocument, makeRepository } from '@linda/content-manager'
import { DocumentSchema, PropertyType } from '@linda/content-manager/types'

export interface DocumentListLayout {
  _id: string
  columns: Array<{ property: string, order: number }>
}

export const DocumentListLayoutSchema: DocumentSchema = {
  name: 'DocumentListLayout',
  module: '@linda/content-manager-ui',
  properties: {
    _id: {
      type: PropertyType.string
    },
    columns: {
      type: PropertyType.array,
      document: {
        property: {
          type: PropertyType.string
        },
        order: {
          type: PropertyType.number
        }
      }
    }
  }
}

export const DocumentListLayoutCollection = (): Collection<DocumentListLayout> => collection<DocumentListLayout>('@linda/content-manager-ui__DocumentListLayout')
export const DocumentListLayoutRepository = makeRepository(DocumentListLayoutSchema, DocumentListLayoutCollection())
registerDocument(DocumentListLayoutSchema)
