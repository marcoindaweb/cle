/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */
import { Command$ } from '@linda/core/commands'
import { getOperations } from '../operations/state'
import { getPathsToWatch } from '../watcher/state'
import { createWatcher } from '../watcher'
import WebpackDevServer from 'webpack-dev-server'
import webpack from 'webpack'
import { devConfig } from '../webpack/config'
import { writeIndex } from '../reactRoot'
import { remove } from '@linda/core/fs'
import { outDir } from '../config'
import { VOID } from '@linda/core/utils/promises'
import { log, VERBOSE } from '@linda/core/log'

Command$('frontend:dev').subscribe(() => {
  log(VERBOSE, 'Getting operations')
  const operations = getOperations()
  log(VERBOSE, 'Loading files')
  const toWatch = getPathsToWatch()
  log(VERBOSE, 'Starting watcher')
  createWatcher(toWatch, operations)
  log(VERBOSE, 'Starting webpack')
  writeIndex()
    .then(() => {
      const server = new WebpackDevServer(webpack(devConfig), devConfig.devServer)
      server.listen(8080, 'localhost')
    }).finally(() => {})
})

Command$('frontend:clear').subscribe(() => {
  VOID(() => remove(outDir))()
})
