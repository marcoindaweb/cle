/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { List } from 'immutable'

export type Operations = List<Operation>
export type Operation = AddOperation

interface BaseOperation {
  component: string
  reference: string
}

export enum OperationKind {
  ADD,
}

export enum AddOperationPosition {
  INSIDE,
  AROUND,
  BEFORE,
  AFTER
}

export interface AddOperation extends BaseOperation {
  kind: OperationKind.ADD
  position: AddOperationPosition
  selector: string
}

export const isOperation = (v: any): v is Operation => {
  return typeof v === 'object' && v.hasOwnProperty('component') && v.hasOwnProperty('reference') && v.hasOwnProperty('kind')
}
export const isAddOperation = (v: any): v is AddOperation => {
  return isOperation(v) && v.kind === OperationKind.ADD
}

export const createAddOperation = (v: { component: string, reference: string, position: AddOperationPosition, selector: string }): AddOperation => ({ ...v, kind: OperationKind.ADD })
