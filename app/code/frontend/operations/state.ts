/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Operations } from './types'
import { List } from 'immutable'
import { Operation } from '../types'
import { addOperation as _addOperation } from './manipulation'

export let operations: Operations = List()

export const addOperation = <T extends Operation>(...ops: T[]): void => {
  ops.forEach(op => {
    operations = _addOperation(operations, op)
  })
}

export const getOperations = (): Operations => {
  return operations
}
