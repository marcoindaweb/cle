/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */
import webpack, { Configuration } from 'webpack'
import { join } from '@linda/core/fs/utils'
import { outDir } from '../config'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import { baseDir, storageDir } from '@linda/core/fs'

export const productionConfig: Configuration = {
  mode: 'production',
  entry: join(outDir, 'store.ts'),
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json', '.jsx'],
    fallback: { buffer: false }
  },
  plugins: [
    new HtmlWebpackPlugin({
      hash: true,
      filename: 'index.html',
      templateContent: '<html><body><div id="root"></div></body></html>'
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: 'ts-loader',
            options: {
              configFile: 'tsconfig.fronted.json'
            }
          }
        ],
        exclude: /node_modules/
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          'style-loader',
          // Translates CSS into CommonJS
          {
            loader: 'css-loader',
            options: {
              modules: true
            }
          },
          // Compiles Sass to CSS
          {
            loader: 'sass-loader',
            options: {
              implementation: require('node-sass')
            }
          }
        ]
      }
    ]
  },
  output: {
    filename: 'bundle.js',
    chunkFilename: '[name][chunkhash].js',
    path: join(baseDir, '../dist'),
    publicPath: '/'
  }
}

export const devConfig: Configuration = {
  mode: 'development',
  entry: join(outDir, 'store.ts'),
  optimization: {
    runtimeChunk: 'single'
  },
  devServer: {
    contentBase: join(storageDir, 'public'),
    compress: true,
    hot: true,
    historyApiFallback: true
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json', '.jsx'],
    fallback: {
      buffer: false,
      path: false,
      console: false,
      util: false,
      http: false,
      fs: false,
      assert: false,
      module: false,
      dns: false,
      crypto: false,
      stream: false,
      zlib: false,
      os: false,
      tls: false,
      net: false,
      constants: false,
      https: false,
      fastify: false,
      '@linda/database': false
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      hash: true,
      filename: 'index.html',
      templateContent: '<html><body><div id="root"></div></body></html>'
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    })
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: 'ts-loader',
            options: {
              configFile: 'tsconfig.fronted.json'
            }
          }
        ],
        exclude: /node_modules/
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              implementation: require('node-sass')
            }
          }
        ]
      }
    ]
  },
  output: {
    filename: '[name].js',
    chunkFilename: '[name][chunkhash].js',
    publicPath: '/'
  }
}
