/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */
/* eslint-disable @typescript-eslint/prefer-ts-expect-error */

import React from 'react'
import ReactDOM from 'react-dom'
// @ts-ignore
import Root from '{{root dir}}'

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const renderApp = () => ReactDOM.render(React.createElement(Root), document.getElementById('root'))

// @ts-ignore
// eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
if (module.hot) {
  renderApp()
}
