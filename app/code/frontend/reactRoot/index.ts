/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { mode } from '@linda/core/env'
import { dirname, join, relative } from '@linda/core/fs/utils'
import { outDir } from '../config'
import { ensureFile, readFile, writeFile } from '@linda/core/fs'
import { getOutPath } from '../processing/names'
import { ROOT } from '../index'

export const writeIndex = async (): Promise<void> => {
  const toCopy = mode === 'development' ? join(__dirname, 'templates/dev.ts') : join(__dirname, 'templates/production.ts')
  const outPath = join(outDir, 'store.ts')
  await readFile(toCopy)
    .then(v => v.toString())
    .then(v => writeRootImportModuleSpecifier(v, './' + relative(outDir, dirname(getOutPath(ROOT)))))
    .then(v =>
      ensureFile(outPath)
        .then(() => writeFile(outPath, v))
    )
}

export const writeRootImportModuleSpecifier = (content: string, specifier: string): string => {
  return content.replace('{{root dir}}', specifier)
}
