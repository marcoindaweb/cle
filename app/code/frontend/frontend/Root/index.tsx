/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { ReactNode } from 'react'
import { RecoilRoot } from 'recoil'
import RecoilizeDebugger from 'recoilize'

class ErrorBoundary extends React.Component {
  constructor (props) {
    super(props)
    this.state = { hasError: false }
  }

  static getDerivedStateFromError (error): { hasError: true } {
    // Update state so the next render will show the fallback UI.
    return { hasError: true }
  }

  componentDidCatch (error, errorInfo): void {
    // You can also log the error to an error reporting service
    console.error(error, errorInfo)
  }

  render (): ReactNode {
    return this.props.children
  }
}

const Root: React.FC = () => <RecoilRoot>
  <ErrorBoundary>
    <div></div>
    {/*<RecoilizeDebugger />*/}
  </ErrorBoundary>
</RecoilRoot>
export default Root
