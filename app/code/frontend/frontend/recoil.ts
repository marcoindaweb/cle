/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

/* eslint-disable @typescript-eslint/prefer-ts-expect-error */
import { AjaxError, handleRequest, NotAjaxError, Result, selector } from './index'
import {
  atom,
  atomFamily,
  DefaultValue,
  RecoilState,
  selectorFamily,
  SerializableParam,
  GetRecoilValue,
  SetRecoilState, ResetRecoilState
} from 'recoil'
import { Observable } from 'rxjs'
import { v4 } from 'uuid'

/**
 * Return an async selector that can be refreshed when
 * reset
 */
export const ajaxSelector =
  <T>(key: string, cb: (v: { get: GetRecoilValue }) => Promise<T>): RecoilState<T> => {
    const resetAtom = atom<number>({
      key: `${key}__reset`,
      default: 0
    })
    return selector<T>({
      key: key,
      get: async ({ get }) => {
        get(resetAtom)
        // eslint-disable-next-line standard/no-callback-literal
        return await cb({ get })
      },
      set: ({ set }, val) => {
        if (val instanceof DefaultValue) {
          set(resetAtom, v => v + 1)
        }
      }
    })
  }

/**
 * Same one as above just that it's a family
 */
export const ajaxSelectorFamily =
  <T, P extends SerializableParam>(
    key: string,
    cb: (p: P) => (v: { get: GetRecoilValue }) => Promise<T>
  ): (param: P) => RecoilState<T> => {
    const resetAtomFamily = atomFamily<number, P>({
      key: `${key}__reset`,
      default: 0
    })
    return selectorFamily<T, P>({
      key,
      get: (params) => async ({ get }) => {
        get(resetAtomFamily(params))
        return await cb(params)({ get })
      },
      set: (params) => ({ set }, val) => {
        if (val instanceof DefaultValue) {
          set(resetAtomFamily(params), v => v + 1)
        }
      }
    })
  }

/**
 * Useful for changing the state internally
 * Underneath there is a normal selectorFamily that throw an error when trying to read,
 */
export const action = <P>(
  key: string,
  cb: (v: { get: GetRecoilValue, set: SetRecoilState, reset: ResetRecoilState }, value: P) => any
): RecoilState<undefined> => {
  // @ts-ignore
  return selector<P>({
    key,
    get: () => {
      throw new Error('Cannot get value from an action selector')
    },
    set: async ({ get, set, reset }, val) => {
      if (val instanceof DefaultValue) return
      // eslint-disable-next-line standard/no-callback-literal
      await cb({ get, set, reset }, val)
    }
  })
}

interface RecoilParams { get: GetRecoilValue, set: SetRecoilState, reset: ResetRecoilState }

export const ajaxAction = <P, R = any>(
  key: string,
  request: (v: RecoilParams, value: P) => Promise<R | AjaxError> | Observable<R | AjaxError>,
  onSuccess: (v: RecoilParams, value: NotAjaxError<R>) => any,
  onError?: (v: RecoilParams, error: AjaxError) => any
): [RecoilState<boolean>, RecoilState<undefined>] => {
  const fetching = atom<boolean>({
    key: `${key}__fetching`,
    default: false
  })
  const fetchData = action<P>(
    key,
    async (v, p) => {
      v.set(fetching, true)
      await handleRequest(
        () => request(v, p),
        (value) => onSuccess(v, value),
        (err) => onError !== undefined ? onError(v, err) : null
      )
      v.set(fetching, false)
    }
  )
  return [fetching, fetchData]
}
