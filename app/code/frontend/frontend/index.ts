/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { from, Observable, Subject, Subscription } from 'rxjs'
import { map, tap } from 'rxjs/operators'
import merge from 'lodash.merge'
import { useLocation, useRoute, useRouter, Link } from 'wouter'
// eslint-disable-next-line max-len
import { useRecoilState, useRecoilCallback, atom, useRecoilValue, RecoilState, RecoilRoot, selector, useSetRecoilState, selectorFamily, atomFamily, GetRecoilValue, DefaultValue, SerializableParam, useRecoilSnapshot, constSelector, errorSelector, isRecoilValue, noWait, readOnlySelector, useResetRecoilState } from 'recoil'
// eslint-disable-next-line max-len
export { useRouter, useRoute, Link, useLocation, RecoilState, useRecoilCallback, useRecoilState, RecoilRoot, atom, useRecoilValue, selector, useSetRecoilState, selectorFamily, GetRecoilValue, SerializableParam, atomFamily, DefaultValue, useRecoilSnapshot, constSelector, errorSelector, isRecoilValue, noWait, readOnlySelector, useResetRecoilState }

let ajaxDefaults: RequestInit & { baseUrl?: string } = {
  baseUrl: 'http://127.0.0.1:3000',
  headers: {
    'Content-Type': 'application/json'
  }
}
const applyBaseUrl = (baseUrl: string, url: string): string =>
  baseUrl + (url.charAt(0) === '/' ? url : `/${url}`)
export const setDefaults = (v: RequestInit): void => {
  ajaxDefaults = v
}

interface OkResponse<T> extends Response {
  json: () => Promise<T>
}

type AResponse<T> = OkResponse<T>

export const buildRequestInfo = (info: RequestInfo, baseUrl: string): RequestInfo => {
  if (typeof info === 'string') {
    return applyBaseUrl(baseUrl, info)
  } else {
    return { ...info, url: applyBaseUrl(baseUrl, info.url) }
  }
}

export class AjaxError {
  readonly response: Response
  readonly error?: Error
  constructor (res: Response, error?: Error) {
    this.response = res
    this.error = error
  }
}
export const ajax$ =
  <R>(info: RequestInfo, init?: RequestInit): Observable<AResponse<R>> => {
    return from(fetch(
      buildRequestInfo(info, ajaxDefaults?.baseUrl ?? ''),
      merge({ ...ajaxDefaults }, init)) as Promise<AResponse<R>>
    )
      .pipe(
        tap(r => {
          if (!r.ok) {
            ajaxError$.next(new AjaxError(r))
          }
        })
      )
  }

export const ajax =
  async <R>(info: RequestInfo, init?: RequestInit): Promise<AResponse<R> | AjaxError> =>
    await ajax$<R>(info, init).toPromise()

export const fetchJson =
  async <R>(info: RequestInfo, init?: RequestInit): Promise<R | AjaxError> =>
    await ajax$<R>(info, init).pipe(map(async (v) => {
      if (!v.ok) return new AjaxError(v)
      try {
        return await v.json()
      } catch {
        return new AjaxError(v)
      }
    })).toPromise()

export const ajaxError$ = new Subject<AjaxError>()
export const onAjaxError = (cb: (error: AjaxError) => any): Subscription =>
  ajaxError$.subscribe(cb)

export type NotAjaxError<T> = Exclude<T, AjaxError>

export type Result<T> = T extends PromiseLike<infer U> ? U : T extends Observable<infer O> ? O : T

export const handleRequest =
  async <T extends Promise<any | AjaxError> | Observable<any | AjaxError>, S, E>(
    request: () => T,
    onSuccess: (p: NotAjaxError<Result<T>>) => S,
    onError?: (p: AjaxError) => E
  ): Promise<E | S> => {
    const res = request()
    const out = await (res instanceof Observable ? res.toPromise() : res)
    if (out instanceof AjaxError && onError !== undefined) {
      return onError(out)
    } else {
      return onSuccess(out)
    }
  }
