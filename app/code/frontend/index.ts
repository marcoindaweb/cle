/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { dirname, join } from '@linda/core/fs/utils'
import './commands'
import { addFrontend } from './watcher/state'
import { addOperation } from './operations'
import { AddOperationPosition, createAddOperation } from './operations/types'
import { component } from './selectors'

export const ROOT = join(__dirname, 'frontend/Root/index.tsx')
export { addOperation } from './operations/state'
export { createAddOperation } from './operations/types'
export { addFrontend } from './watcher/state'

addFrontend(dirname(ROOT))
export const wrapFrontendRoot = (path: string): void => addOperation(createAddOperation({
  component: ROOT,
  position: AddOperationPosition.AROUND,
  selector: component('div'),
  reference: path
}))
export const addInsideRoot = (path: string): void => addOperation(createAddOperation({
  component: ROOT,
  position: AddOperationPosition.INSIDE,
  selector: component('div'),
  reference: path
}))
