/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { List } from 'immutable'
import { FSWatcher } from 'chokidar'
import { Project } from 'ts-morph'
import { processFile, shouldProcessFile } from '../processing'
import { Operations } from '../operations/types'
import { copy } from '@linda/core/fs'
import { getOutPath } from '../processing/names'
import { VOID } from '@linda/core/utils/promises'
import { log, SILLY } from '@linda/core/log'

const isJSX = (path: string): boolean => /\.tsx$|\.jsx$/.test(path)

const shouldProcess = (path: string): boolean => !path.includes('__test__')

const processJSX = async (project: Project, path: string, ops: Operations, refresh: boolean = false): Promise<void> => {
  const source = project.addSourceFileAtPath(path)
  if (refresh) { await source.refreshFromFileSystem() }
  log(SILLY, `Applying operations to ${path}`)
  const generated = processFile(source, ops)
  await generated.save()
}

const processStdFile = async (path: string): Promise<void> => {
  await copy(path, getOutPath(path))
}

const process = (project: Project, path: string, ops: Operations, refreshFile: boolean): void => {
  if (!shouldProcess(path)) return
  log(SILLY, `Processing ${path}`)
  if (isJSX(path) && shouldProcessFile(path, ops)) {
    VOID(processJSX)(project, path, ops, refreshFile)
  } else {
    VOID(processStdFile)(path)
  }
}

const handleFileAdd = (project: Project, path: string, ops: Operations): void => {
  process(project, path, ops, false)
  log(SILLY, `Added file ${path}`)
}

const handleFileChange = (project: Project, path: string, ops: Operations): void => {
  process(project, path, ops, true)
  log(SILLY, `Changed file ${path}`)
}

export const createWatcher = (toWatch: List<string>, operations: Operations): FSWatcher => {
  const watcher = new FSWatcher()
  const proj = new Project()
  toWatch.forEach(path => watcher.add(path))
  watcher.on('add', (path) => handleFileAdd(proj, path, operations))
  watcher.on('change', (path) => handleFileChange(proj, path, operations))
  return watcher
}
