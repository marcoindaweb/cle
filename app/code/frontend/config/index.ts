/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { join } from '@linda/core/fs/utils'
import { generated } from '@linda/core/fs'
import { get } from '@linda/core/configuration'
// Directory where the generated frontend will be written
export const OUT_DIR = 'frontend/fs/out_dir'
export const outDir = get(OUT_DIR, join(generated, 'frontend')) as string
