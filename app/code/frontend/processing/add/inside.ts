/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { SourceFile } from 'ts-morph'
import { SyntaxKind, JsxElement } from 'typescript'
import { AddOperation } from '../../types'
import { getFirstNodeBySelectorOrThrow, nodeNeedsToBeOfKind } from '../ast'
import { getReferenceComponentName } from '../names'

export const processAddInside = (file: SourceFile, operation: AddOperation): SourceFile => {
  const node = getFirstNodeBySelectorOrThrow(file, operation.selector)
  const element = nodeNeedsToBeOfKind<JsxElement>(node, [SyntaxKind.JsxElement])
  const elmName = getReferenceComponentName(operation)
  file.insertText(element.openingElement.end, `<${elmName} />`)
  return file
}
