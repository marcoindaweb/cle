/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { SourceFile } from 'ts-morph'
import { Node, SyntaxKind, ScriptKind } from 'typescript'
import { tsquery } from '@phenomnomnominal/tsquery'

export const getNodesBySelector = (file: SourceFile, selector: string): Node[] => {
  const ast = tsquery.ast(
    file.getFullText(),
    file.getFilePath(),
    ScriptKind.TSX
  )
  return tsquery(ast, selector)
}

export const getFirstNodeBySelector = (file: SourceFile, selector: string): Node | undefined => {
  return getNodesBySelector(file, selector)[0]
}

export const getFirstNodeBySelectorOrThrow = (file: SourceFile, selector: string): Node => {
  const value = getFirstNodeBySelector(file, selector)
  if(value === undefined) {
    throw new Error(`Unable to find any element for selector "${selector}"`)
  }
  return value
}

export const isNodeOfKind = (node: Node, acceptedKind: SyntaxKind[]): boolean => {
  return acceptedKind.includes(node.kind)
}

export const nodeNeedsToBeOfKind = <T extends Node>(node: Node, acceptedKing: SyntaxKind[]): T => {
  if (!isNodeOfKind(node, acceptedKing)) {
    throw new Error(`Node should be ${acceptedKing.length > 1 ? 'one of' : 'of type'} ${acceptedKing.map(syntaxKindToName).join(', ')}. Got ${syntaxKindToName(node.kind)}`)
  }
  return node as T
}

export const syntaxKindToName = (kind: SyntaxKind): string => {
  return SyntaxKind[kind]
}
