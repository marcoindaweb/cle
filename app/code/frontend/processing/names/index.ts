/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { baseDir } from '@linda/core/fs'
import { outDir } from '../../config'
import { Operation } from '../../types'

export const getReferenceComponentName = (operation: Operation): string => getComponentNameByPath(operation.reference)
export const getReferenceOutpath = (operation: Operation): string => getOutPath(operation.reference)
export const getComponentOutpath = (operation: Operation): string => getOutPath(operation.component)

export const getComponentNameByPath = (path: string): string => {
  const newPath = path
    .replace(baseDir, '') // Remove baseDir path
    .replace(/\.jsx$|\.tsx$/, '') // Remove file ext
    // eslint-disable-next-line no-useless-escape
    .replace(/[-_\/.@]+/g, '_') // Remove special chars
    .split('_') // Capitalize
    .map((v) => v.charAt(0).toUpperCase() + v.slice(1))
    .join('_')
  return newPath.charAt(0) === '_' ? newPath.replace('_', '') : newPath
}

export const getOutPath = (path: string): string => {
  return path.replace(baseDir, outDir)
}
