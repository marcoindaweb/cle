/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { AddOperationPosition, Operation, OperationKind } from '../../types'
import { Project } from 'ts-morph'
import { getFileOperations } from '../index'
import { List } from 'immutable'
import { Operations } from '../../operations/types'

describe('frontend/processing', () => {
  it('filter operations by file', () => {
    const op1: Operation = {
      position: AddOperationPosition.INSIDE,
      selector: '',
      component: 'a.tsx',
      kind: OperationKind.ADD,
      reference: 'abc'
    }
    const op2: Operation = {
      position: AddOperationPosition.INSIDE,
      selector: '',
      component: 'b.tsx',
      kind: OperationKind.ADD,
      reference: 'abc'
    }
    const project = new Project()
    const src1 = project.createSourceFile('a.tsx')
    const src2 = project.createSourceFile('b.tsx')
    const ops: Operations = List([op1, op2])
    expect(getFileOperations(src1, ops).equals(List([op1])))
    expect(getFileOperations(src2, ops).equals(List([op2])))
  })
})
