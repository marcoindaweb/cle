/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { SourceFile } from 'ts-morph'
import { Operation } from '../../types'
import { getComponentOutpath, getReferenceComponentName, getReferenceOutpath } from '../names'
import {relative, dirname, extname} from '@linda/core/fs/utils'

export const resolveReferenceModuleSpecifier = (operation: Operation): string => {
  const refOutPath = getReferenceOutpath(operation)
  const compOutPath = getComponentOutpath(operation)
  const resolved = relative(dirname(compOutPath), refOutPath).replace(extname(refOutPath), '')
  return resolved.charAt(0) === '.' ? resolved : `./${resolved}`
}

export const addReferenceImportStatement = (source: SourceFile, operation: Operation): SourceFile => {
  const compName = getReferenceComponentName(operation)
  const decl = source.getImportDeclaration(v => v.getDefaultImport()?.getText() === compName)
  if (decl === undefined) {
    source.addImportDeclaration({ defaultImport: compName, moduleSpecifier: resolveReferenceModuleSpecifier(operation) })
  }
  return source
}
