/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { resolveReferenceModuleSpecifier } from '../index'
import { join } from '@linda/core/fs/utils'
import { AddOperationPosition, OperationKind } from '../../../operations/types'
import { start } from '@linda/core'

describe('frontend/processing/imports', () => {
  beforeEach(() => {
    start()
  })

  it('gives the relative path between a component and his reference', () => {
    expect(resolveReferenceModuleSpecifier({
      component: join(__dirname, 'test/a.tsx'),
      kind: OperationKind.ADD,
      position: AddOperationPosition.INSIDE,
      reference: join(__dirname, 'test/nested/b.tsx'),
      selector: 'div'
    })).toEqual('./nested/b')
    expect(resolveReferenceModuleSpecifier({
      component: join(__dirname, 'test/a.tsx'),
      kind: OperationKind.ADD,
      position: AddOperationPosition.INSIDE,
      reference: join(__dirname, 'test/nested/index.tsx'),
      selector: 'div'
    })).toEqual('./nested/index')
    expect(resolveReferenceModuleSpecifier({
      component: join(__dirname, 'test/a.tsx'),
      kind: OperationKind.ADD,
      position: AddOperationPosition.INSIDE,
      reference: join(__dirname, 'test/b.tsx'),
      selector: 'div'
    })).toEqual('./b')
    expect(resolveReferenceModuleSpecifier({
      component: join(__dirname, 'test/nested/a.tsx'),
      kind: OperationKind.ADD,
      position: AddOperationPosition.INSIDE,
      reference: join(__dirname, 'test/b.tsx'),
      selector: 'div'
    })).toEqual('../b')
    expect(resolveReferenceModuleSpecifier({
      component: join(__dirname, 'test/nested/a.tsx'),
      kind: OperationKind.ADD,
      position: AddOperationPosition.INSIDE,
      reference: join(__dirname, 'test/index.tsx'),
      selector: 'div'
    })).toEqual('../index')
  })
})
