/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { SourceFile } from 'ts-morph'
import { Operations } from '../operations/types'
import { Operation, OperationKind } from '../types'
import { Map } from 'immutable'
import { processAddOperation } from './add'
import { getOutPath } from './names'

const processorsMap = Map<OperationKind, (v: SourceFile, op: Operation) => SourceFile>()
  .set(OperationKind.ADD, processAddOperation)

export const processFile = (file: SourceFile, operations: Operations): SourceFile => {
  let actualFileValue = file
  actualFileValue = actualFileValue.copy(getOutPath(actualFileValue.getFilePath()), { overwrite: true })
  for (const operation of getFileOperations(file, operations)) {
    actualFileValue = processFileOperation(actualFileValue, operation)
  }
  return actualFileValue
}

export const shouldProcessFile = (path: string, operations: Operations): boolean =>
  operations.some(v => v.component === path)
export const getFileOperations = (file: SourceFile, operations: Operations): Operations =>
  operations.filter(v => v.component === file.getFilePath())
export const processFileOperation = (file: SourceFile, operation: Operation): SourceFile => {
  const processor = processorsMap.get(operation.kind)
  if (processor !== undefined) {
    return processor(file, operation)
  }
  return file
}
