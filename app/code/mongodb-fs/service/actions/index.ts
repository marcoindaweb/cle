/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import {
  FsDriver,
  writeFile as writeFileInterface,
  readFile as readFileInterface,
  moveFile as moveFileInterface,
  removeFile as removeFileInterface,
  makeDirectory as makeDirectoryInterface,
  moveDirectory as moveDirectoryInterface,
  removeDirectory as removeDirectoryInterface,
  list as listInterface, FileMetadata, File
} from '@linda/fs/types'
import { getMimeType } from '@linda/fs/service/calculations'

import { collection } from '@linda/database'
import {basename} from "@linda/core/fs/utils";

const fileCollection = collection<DbFile>('@linda/fs__File')

interface DbFile extends Omit<File, 'path'> {
  _id: string
}

const dbFileToFile = (dbFile: DbFile): File => ({ ...dbFile, path: dbFile._id })
const fileToDbFile = (file: File): DbFile => ({...file, _id: file.path })

const writeFile: FsDriver['writeFile'] = async (file) => {
  await fileCollection.insertOne(fileToDbFile(file))
  return file
}
const readFile: readFileInterface = async (path) => {
  const res = await fileCollection.findOne({ path })
  if (res !== null) { return res } else { throw Error('Something went wrong finding the file') }
}
const moveFile: moveFileInterface = () => {

}
const removeFile: removeFileInterface = () => {

}
const makeDirectory: makeDirectoryInterface = () => {

}
const moveDirectory: moveDirectoryInterface = () => {

}
const removeDirectory: removeDirectoryInterface = () => {

}
const list: listInterface = () => {

}

export const mongodbDriver: FsDriver = {
  writeFile,
  readFile,
  moveFile,
  removeFile,
  makeDirectory,
  moveDirectory,
  removeDirectory,
  list
}
