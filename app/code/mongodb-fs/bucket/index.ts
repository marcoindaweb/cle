/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { collection, db, GridFSBucket } from '@linda/database'
import { File, FsDriver } from '@linda/fs/types'

export const getBucket = (): GridFSBucket => new GridFSBucket(db())
// export const getDownloadStream = () => getBucket().
export const fsFilesCollection = () => collection('fs.files')

export const writeFile: FsDriver['writeFile'] = async (file: File, stream): Promise<void> => {
  const bucket = getBucket()
  await new Promise((resolve, reject) => {
    stream.pipe(bucket.openUploadStream(
      file.path,
      {
        chunkSizeBytes: file.data.byteLength,
        contentType: file.metadata.mime,
        metadata: file.metadata
      }))
      .on('error', (e) => {
        reject(e)
      })
      .on('finish', () => {
        resolve()
      })
  })
  const coll = fsFilesCollection()
  const res = await coll.findOne({ filename: file.path })
  if (res === null) {
    throw new Error('Unable to find written file from db')
  }
}
