/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { clearTestingDB, useTestingDB } from '@linda/database/test'
import { load, start } from '@linda/core'
import { readFile } from '@linda/core/fs'
import { join } from '@linda/core/fs/utils'
import { createFile } from '@linda/fs/service/calculations'
import { fsFilesCollection, writeFile } from '../index'
import { makeStreamFromFile } from '@linda/fs/service/actions'

describe('bucket', () => {
  beforeAll(async () => {
    useTestingDB()
    load('@linda/database')
    await start()
    await clearTestingDB()
  })

  afterEach(async () => {
    await clearTestingDB()
  })

  it('write a file to the db', async () => {
    const filePath = join(__dirname, 'sample.pdf')
    const buffer = await readFile(filePath)
    const file = createFile(buffer, 'test/db/ciao.txt')
    const [stream, close] = makeStreamFromFile(file)
    await writeFile(file, stream)
    close()
    const count = await fsFilesCollection().countDocuments({})
    expect(count).toBe(1)
  })
})
