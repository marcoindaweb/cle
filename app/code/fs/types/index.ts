import {IReadStream} from "memfs/lib/volume";

export interface File {
  path: string
  data: Buffer
  filename: string
  uploadDate: Date
  length: number
  metadata: FileMetadata
}

export interface FileMetadata {
  mime: string
  [K: string]: any
}

export interface Directory {
  path: string
  name: string
}

//  Metodi che ha il driver
export interface FsDriver {
  writeFile: (file: File, stream: IReadStream) => Promise<void>
  readFile: (path: string) => Promise<File | null>
  removeFile: (path: string) => Promise<void>
  makeDirectory: (dir: Directory) => Promise<Directory>
  readDirectory: (path: string) => Promise<Directory>
  removeDirectory: (path: string) => Promise<void>
  list: (path: string) => Promise<Array<File | Directory>>
}

// Metodi che mette a disposizione il modulo
export type WriteFile = (data: Buffer, path: string, metadata?: FileMetadata) => Promise<File>
export type ReadFile = (path: string) => Promise<File | null>
export type MoveFile = (from: string, to: string) => Promise<File>
export type RemoveFile = (path: string) => Promise<void>
export type MakeDirectory = (dir: Directory) => Promise<Directory>
export type MoveDirectory = (from: string, to: string) => Promise<Directory>
export type RemoveDirectory = (path: string) => Promise<void>
export type List = (path: string) => Promise<Array<File | Directory>>
