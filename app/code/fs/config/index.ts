/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { DEVELOPER_SECTION, SYSTEM_TAB } from '@linda/config/const'
import { DB_NAME, FILESYSTEM_GROUP, DRIVER_NAME } from './const'
import { FieldType } from '@linda/config/types'
import { group, section, tab } from '@linda/config'

export const systemTab = tab(SYSTEM_TAB, [
  section(DEVELOPER_SECTION, [
    group(FILESYSTEM_GROUP, [
      {
        key: DB_NAME,
        name: 'Database name',
        default: 'linda_fs',
        type: FieldType.TEXT
      },
      {
        key: DRIVER_NAME,
        name: 'FileSystem Driver',
        default: 'mongodb',
        type: FieldType.TEXT
      }
    ])
  ])
])
