/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { FsDriver, WriteFile, File } from '../../types'
import { FS_DRIVER } from '../../const'
import { get, getOrThrow } from '@linda/core/configuration'
import { basename, join } from '@linda/core/fs/utils'
import { createFile, getMimeType } from '../calculations'
import { createFsFromVolume, vol, Volume } from 'memfs'
import { IReadStream } from 'memfs/lib/volume'
import { v4 } from 'uuid'

const getDriver = (): FsDriver => {
  return getOrThrow<FsDriver>(FS_DRIVER)
}

export const makeStreamFromFile = (file: File): [IReadStream, () => any] => {
  const volume = new Volume()
  const fs = createFsFromVolume(volume)
  const id = v4()
  const path = join('/', id)
  fs.writeFileSync(path, file.data)
  const stream = fs.createReadStream(path)
  return [stream, () => volume.reset()]
}

export const writeFile: WriteFile = async (data, path, metadata) => {
  const driver = getDriver()
  const file = createFile(data, path, metadata)
  const [stream, close] = makeStreamFromFile(file)
  await driver.writeFile(file, stream)
  close()
  return file
}
