/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { File, FileMetadata } from '../../types'
import { basename } from '@linda/core/fs/utils'
import { allMimeType } from './mime-types'

export const getMimeType: (buffer: Buffer) => string = (buffer) => {
  const getMimetype: (string) => string = (hex) => {
    const res = allMimeType.find(m => {
      return m.signs.find(r => hex.toUpperCase().startsWith(r.toUpperCase()))
    })

    if (res !== null && res !== undefined) { return res.mime } else { return 'Unknown Type' }
  }
  const uint = new Uint8Array(buffer)
  const bytes: string[] = []
  uint.forEach((b) => {
    bytes.push(b.toString(16))
  })
  const hex = bytes.join('').toUpperCase()
  return getMimetype(hex)
}

export const createFile = (data: Buffer, path: string, metadata?: FileMetadata): File => ({
  data,
  filename: basename(path),
  length: data.byteLength,
  metadata: metadata ?? { mime: getMimeType(data) },
  path: path,
  uploadDate: new Date()
})
