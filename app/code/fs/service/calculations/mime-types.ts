/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

export const allMimeType: Array<{signs: string[], mime: string}> = [
  {
    signs: [
      '0D444F43',
      'CF11E0A1',
      'D0CF11E0',
      'DBA52D00',
      'ECA5C100'
    ],
    mime: 'application/msword'
  },
  {
    signs: [
      '25504446'
    ],
    mime: 'application/pdf'
  },
  {
    signs: [
      '7B5C7274'
    ],
    mime: 'application/rtf'
  },
  {
    signs: [
      '09081000',
      'D0CF11E0',
      'FDFFFFFF',
      'FDFFFFFF'
    ],
    mime: 'application/vnd.ms-excel'
  },
  {
    signs: [
      '504B0304'
    ],
    mime: 'application/vnd.oasis.opendocument.presentation'
  },
  {
    signs: [
      '504B0304'
    ],
    mime: 'application/vnd.oasis.opendocument.text'
  },
  {
    signs: [
      '504B0304'
    ],
    mime: 'application/vnd.oasis.opendocument.text-template'
  },
  {
    signs: [
      '504B0304'
    ],
    mime: 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
  },
  {
    signs: [
      '504B0304'
    ],
    mime: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  },
  {
    signs: [
      '504B0304'
    ],
    mime: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
  },
  {
    signs: [
      '52617221',
      '52617221'
    ],
    mime: 'application/vnd.rar'
  },
  {
    signs: [
      '504B0304',
      '504B0304',
      '504B0304',
      '504B0708',
      '504B4C49',
      '504B5370',
      '152,5769'
    ],
    mime: 'application/zip'
  },
  {
    signs: [
      '00000020',
      '66747970'
    ],
    mime: 'audio/mp4'
  },
  {
    signs: [
      '494433',
      'FFD8'
    ],
    mime: 'audio/mpeg'
  },
  {
    signs: [
      '52494646'
    ],
    mime: 'audio/x-wav'
  },
  {
    signs: [
      '4F676753'
    ],
    mime: 'audio/ogg'
  },
  {
    signs: [
      '47494638'
    ],
    mime: 'image/gif'
  },
  {
    signs: [
      'FFD8',
      'FFD8',
      'FFD8',
      'FFD8'
    ],
    mime: 'image/jpeg'
  },
  {
    signs: [
      'FFD8',
      'FFD8'
    ],
    mime: 'image/jpeg'
  },
  {
    signs: [
      '89504E47'
    ],
    mime: 'image/png'
  },
  {
    signs: [
      '38425053'
    ],
    mime: 'image/vnd.adobe.photoshop'
  },
  {
    signs: [
      '00000100'
    ],
    mime: 'image/vnd.microsoft.icon'
  },
  {
    signs: [
      '2A2A2A20'
    ],
    mime: 'text/plain'
  },
  {
    signs: [
      '00000014',
      '00000018',
      '0000001C',
      '66747970',
      '66747970',
      '66747970'
    ],
    mime: 'video/mp4'
  },
  {
    signs: [
      '00000018',
      '00000020',
      '66747970'
    ],
    mime: 'video/mp4'
  },
  {
    signs: [
      '00000100',
      'FFD8'
    ],
    mime: 'video/mpeg'
  },
  {
    signs: [
      '00000100',
      '000001BA',
      'FFD8'
    ],
    mime: 'video/mpeg'
  },
  {
    signs: [
      '4F676753'
    ],
    mime: 'video/ogg'
  },
  {
    signs: [
      '00',
      '00000014',
      '66747970',
      '6D6F6F76'
    ],
    mime: 'video/quicktime'
  },
  {
    signs: [
      '4A415243',
      '504B0304',
      '504B0304',
      '5F27A889'
    ],
    mime: 'application/x-java-archive'
  },
  {
    signs: [
      '4D546864'
    ],
    mime: 'audio/midi'
  },
  {
    signs: [
      '4D546864'
    ],
    mime: 'audio/midi'
  }
]
