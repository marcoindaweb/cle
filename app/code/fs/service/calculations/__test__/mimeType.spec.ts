/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { createFile } from '../index'
import { useTestingDB } from '@linda/database/test'
import { load, start } from '@linda/core'
import { join } from '@linda/core/fs/utils'
import { readFile } from '@linda/core/fs'

describe('Get mime type of an input buffer', () => {
  beforeAll(async () => {
    useTestingDB()
    load('@linda/database')
    await start()
  })

  it('Get Mime From PDF', async () => {
    const filePath = join(__dirname, 'sample.pdf')
    const buffer = await readFile(filePath)
    const file = createFile(buffer, 'test/db/ciao.txt')
    console.log(file.metadata.mime)
    expect(file.metadata.mime).toBe('application/pdf')
  })
  it('Get Mime From IMG', async () => {
    const filePath = join(__dirname, 'sample_150.png')
    const buffer = await readFile(filePath)
    const file = createFile(buffer, 'test/db/ciao.txt')
    console.log(file.data)
    console.log(file.metadata.mime)
    expect(file.metadata.mime).toBe('image/png')
  })
  it('Get Mime From OGG', async () => {
    const filePath = join(__dirname, 'audio_18sec.ogg')
    const buffer = await readFile(filePath)
    const file = createFile(buffer, 'test/db/ciao.txt')
    console.log(file.metadata.mime)
    expect(file.metadata.mime).toBe('audio/ogg')
  })
  it('Get Mime From HTM', async () => {
    const filePath = join(__dirname, '7_1_Text.htm')
    const buffer = await readFile(filePath)
    const file = createFile(buffer, 'test/db/ciao.txt')
    console.log(file.metadata.mime)
    expect(file.metadata.mime).toBe('Unknown Type')
  })
})
