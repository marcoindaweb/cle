/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { LayoutManagerInterface } from '@linda/module-frontend'

export default (l: LayoutManagerInterface) => {
  l.component(
    '@linda/module-frontend::containers/Router/index.tsx'
  ).add('@linda/module-pages::containers/TestRoute/index.tsx', 'inside', ['div'])
}
