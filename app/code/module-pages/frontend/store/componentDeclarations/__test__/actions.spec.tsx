/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { renderHook } from '@testing-library/react-hooks'
import { useAddComponentDeclaration } from '../actions'
import ComponentDeclaration from '../../../types/ComponentDeclaration'
import { RecoilRoot, useRecoilState, useRecoilValue } from 'recoil'
import { componentDeclarations } from '../store'
import React, { useEffect } from 'react'
import ReactDOM from 'react-dom'
import { act } from 'react-dom/test-utils'
import { render, screen, fireEvent } from '@testing-library/react'
import {useAddComponentOrSymbol} from "../../layout/actions";

/**
 * @jest-environment jsdom
 */
describe('component declarations store', () => {
    let container

    beforeEach(() => {
        container = document.createElement('div')
        document.body.appendChild(container)
    })

    afterEach(() => {
        document.body.removeChild(container)
        container = null
    })

    it('add a component declaration', () => {
        let declarations
        let addDeclaration: ReturnType<typeof useAddComponentDeclaration>
        const declaration = { id: 'test', render: () => <span>hi</span> }

        const TestComp = () => {
            const [state] = useRecoilState(componentDeclarations)
            addDeclaration = useAddComponentDeclaration()

            useEffect(() => {
                addDeclaration(declaration)
            }, [])

            useEffect(() => {
                declarations = state
            }, [state])

            return null
        }

        act(() => {
            ReactDOM.render(
                <RecoilRoot>
                    <TestComp />
                </RecoilRoot>,
                container,
            )
        })

        expect(declarations).toEqual([declaration])
    })

    it('dont add multiple declarations with same id', () => {
        expect.assertions(2)

        let declarations
        let addDeclaration: ReturnType<typeof useAddComponentDeclaration>
        const declaration = { id: 'test', render: () => <span>hi</span> }

        const TestComp = () => {
            const [state] = useRecoilState(componentDeclarations)
            addDeclaration = useAddComponentDeclaration()

            useEffect(() => {
                declarations = state
            }, [state])

            return <button onClick={() => addDeclaration(declaration)} data-testid="add-declaration" />
        }
        render(
            <RecoilRoot>
                <TestComp />
            </RecoilRoot>,
        )

        const button = screen.getByTestId('add-declaration')

        if (!button) {
            return
        }

        act(() => {
            fireEvent.click(button)
        })

        expect(declarations).toEqual([declaration])

        act(() => {
            fireEvent.click(button)
        })

        expect(declarations).toEqual([declaration])
    })
})
