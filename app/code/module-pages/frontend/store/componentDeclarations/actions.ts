/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { DefaultValue, useRecoilState } from 'recoil'
import { useCallback } from 'react'
import ComponentDeclaration from '../../types/ComponentDeclaration'
import { componentDeclarations } from './store'
import { action, useRecoilAction } from '../recoilAction'

export const addDeclaration = action<ComponentDeclaration>({
    key: 'layout/declarations/addDeclaration',
    execute: ({ get, set }, newValue) => {
        if (newValue instanceof DefaultValue) return
        const declarations = get(componentDeclarations)
        if (!declarations.find((comp) => comp.id === newValue.id)) {
            set(componentDeclarations, [...declarations, newValue])
        }
    },
})

export const useAddComponentDeclaration = () => {
    const add = useRecoilAction(addDeclaration)
    return useCallback((v: ComponentDeclaration) => add(v), [add])
}
