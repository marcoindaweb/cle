/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { atom, selector, selectorFamily } from 'recoil'
import ComponentDeclaration from '../../types/ComponentDeclaration'
import notUndefined from '../../utils/notUndefined'

export const componentDeclarations = atom<ComponentDeclaration[]>({
    default: [],
    key: 'pages/componentDeclarations',
})

export const componentDeclarationById = selectorFamily<ComponentDeclaration | undefined, string>({
    key: 'pages/componentDeclarationById',
    get: (id: string) => ({ get }) => {
        const components = get(componentDeclarations)
        return components.find((component) => component.id === id)
    },
})

export const visibleComponentDeclarations = selector<ComponentDeclaration[]>({
    key: 'pages/visibleComponentDeclarations',
    get: ({ get }) => {
        return get(componentDeclarations).filter((c) => c.config?.hidden !== true)
    },
})

export const componentTags = selector<string[]>({
    key: 'pages/componentTags',
    get: ({ get }) => {
        return get(componentDeclarations)
            .flatMap((v) => v.meta?.tags)
            .filter(notUndefined)
            .reduce<string[]>((v, n) => {
                if (!v.includes(n)) {
                    v.push(n)
                }
                return v
            }, [])
    },
})

export const visibleComponentsByTag = selector<Array<[string | undefined, ComponentDeclaration[]]>>({
    key: 'pages/visibleComponentsByTag',
    get: ({ get }) => {
        const o: Array<[string | undefined, ComponentDeclaration[]]> = []
        for (const tag of get(componentTags)) {
            o.push([tag, get(visibleComponentDeclarations).filter((c) => c.meta?.tags?.some((v) => v === tag))])
        }
        o.push([undefined, get(visibleComponentDeclarations).filter((c) => c.meta?.tags === undefined)])
        return o
    },
})
