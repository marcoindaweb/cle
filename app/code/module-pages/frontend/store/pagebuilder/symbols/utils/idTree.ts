/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { SymbolLayout } from '../../../../types/SymbolDefinition'
import { IdTree } from '../../../layout/utils/manipulation'
import { v4 } from 'uuid'

export const generateIdTreeFromSymbolLayout = (symbol: SymbolLayout, parentId: string): IdTree => {
    return [parentId, symbol.children.map((v) => generateIdTreeFromSymbolLayout(v, v4()))]
}

export const extractIdsFromIdTree = (idTree: IdTree): string[] => {
    const o: string[] = []
    o.push(idTree[0])
    idTree[1].forEach((v) => {
        o.push(...extractIdsFromIdTree(v))
    })
    return o
}
