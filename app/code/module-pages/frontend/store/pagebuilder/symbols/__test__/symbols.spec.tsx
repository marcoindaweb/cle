/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { renderHook } from '@testing-library/react-hooks'
import { RecoilRoot, useRecoilState, useRecoilValue } from 'recoil'
import { useRecoilAction } from '../../../recoilAction'
import { add, useAddComponent } from '../../../layout/actions'
import { addSymbolDefinition, useAddSymbolDefinition } from '../actions'
import React, { useEffect, useState } from 'react'
import { component, componentProperties } from '../../../layout/store'
import { addDeclaration, useAddComponentDeclaration } from '../../../componentDeclarations/actions'
import div from '../../../../components/Definitions/Div'
import Position from '../../../../types/Position'
import { COMPONENT_ID_PLACEHOLDER, convertComponentInstanceToSymbolLayout } from '../../../layout/utils/manipulation'
import { symbolDefinition } from '../store'
import ComponentInstance from '../../../../types/ComponentInstance'
import List, { LIST_DATA_KEY_PROPERTY_KEY } from '../../../../components/Definitions/List'

describe('pagebuilder symbols', () => {
    it('set component properties to symbol properties when instantiating', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const properties = {
            some: {
                nested: {
                    properties: 'with a value',
                },
            },
            anArray: ['hi', 'how', 'are', 'you'],
            aNumber: 0,
            aString: 'the best',
        }
        const { result } = renderHook(
            () => {
                const a = useRecoilAction(add)
                const addSymbol = useRecoilAction(addSymbolDefinition)
                const addDecl = useAddComponentDeclaration()
                const props = useRecoilValue(componentProperties('a'))
                useEffect(() => {
                    addDecl(div)
                    addSymbol({
                        id: 'test',
                        name: 'test',
                        layout: {
                            componentDeclarationId: div.id,
                            properties,
                            alias: div.id,
                            children: [],
                        },
                    })
                    a({ componentId: 'a', id: 'test', position: Position.inside })
                }, [])
                return { props }
            },
            { wrapper },
        )

        expect(result.current.props).toEqual({ ...properties })
    })

    const symbolLayoutWithProperties = {
        componentDeclarationId: div.id,
        alias: div.id,
        properties: {},
        children: [
            {
                componentDeclarationId: div.id,
                alias: div.id,
                properties: {
                    className: `${COMPONENT_ID_PLACEHOLDER} test`,
                    style: { [COMPONENT_ID_PLACEHOLDER]: { color: 'red', '&>.test': { color: 'red' } } },
                    test: '{{[0].test.name}}',
                },
                children: [
                    {
                        componentDeclarationId: div.id,
                        alias: div.id,
                        properties: {
                            className: `${COMPONENT_ID_PLACEHOLDER} test`,
                            style: { [COMPONENT_ID_PLACEHOLDER]: { color: 'red', '&>.test': { color: 'red' } } },
                            test: { nested: '{{[0][0].test}}' },
                        },
                        children: [],
                    },
                    {
                        componentDeclarationId: div.id,
                        alias: div.id,
                        properties: {
                            className: `${COMPONENT_ID_PLACEHOLDER} test`,
                            style: { [COMPONENT_ID_PLACEHOLDER]: { color: 'red', '&>.test': { color: 'red' } } },
                            test: { nested: '{{[0][0].test}}' },
                        },
                        children: [
                            {
                                componentDeclarationId: div.id,
                                alias: div.id,
                                properties: {
                                    className: `${COMPONENT_ID_PLACEHOLDER} {{[0][0][1].test}}`,
                                    style: {
                                        [COMPONENT_ID_PLACEHOLDER]: { color: 'red', '&>.test': { color: 'red' } },
                                    },
                                    test: { nested: '{{[0][0][1].test}} {{[0][0].test}}' },
                                },
                                children: [],
                            },
                        ],
                    },
                ],
            },
        ],
    }

    it('handle ids in provided component properties', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const a = useRecoilAction(add)
                const addDecl = useAddComponentDeclaration()
                const comp = useRecoilValue(component('a'))
                const symbol = comp ? convertComponentInstanceToSymbolLayout(comp) : undefined
                useEffect(() => {
                    addDecl(div)
                    a({
                        id: div.id,
                        componentId: 'a',
                        position: Position.inside,
                    })
                    a({
                        id: div.id,
                        componentId: 'b',
                        reference: 'a',
                        properties: {
                            test: '{{a.test.name}}',
                            className: 'b test',
                            style: { b: { color: 'red', '&>.test': { color: 'red' } } },
                        },
                        position: Position.inside,
                    })
                    a({
                        id: div.id,
                        componentId: 'c',
                        reference: 'b',
                        properties: {
                            test: { nested: '{{b.test}}' },
                            className: 'c test',
                            style: { c: { color: 'red', '&>.test': { color: 'red' } } },
                        },
                        position: Position.inside,
                    })
                    a({
                        id: div.id,
                        componentId: 'd',
                        reference: 'c',
                        properties: {
                            test: { nested: '{{b.test}}' },
                            className: 'd test',
                            style: { d: { color: 'red', '&>.test': { color: 'red' } } },
                        },
                        position: Position.after,
                    })
                    a({
                        id: div.id,
                        componentId: 'e',
                        reference: 'd',
                        properties: {
                            test: { nested: '{{d.test}} {{b.test}}' },
                            className: 'e {{d.test}}',
                            style: { e: { color: 'red', '&>.test': { color: 'red' } } },
                        },
                        position: Position.inside,
                    })
                }, [])

                return { symbol }
            },
            { wrapper },
        )

        expect(result.current.symbol).toEqual(symbolLayoutWithProperties)
    })

    it('create a symbol layout from a component instance', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addComp = useRecoilAction(add)
                const addDecl = useRecoilAction(addDeclaration)
                const addSymb = useRecoilAction(addSymbolDefinition)
                const [comp, setComp] = useRecoilState(component('a'))
                const [finishedCreatingComponent, setFinished] = useState(false)
                const symbol = useRecoilValue(symbolDefinition('test'))
                useEffect(() => {
                    addDecl(div)
                    addComp({ id: div.id, componentId: 'a', position: Position.inside })
                    addComp({
                        id: div.id,
                        componentId: 'b',
                        reference: 'a',
                        position: Position.inside,
                        properties: { name: '{{a.test}} {{a.name}}' },
                    })
                    addComp({
                        id: div.id,
                        componentId: 'c',
                        reference: 'b',
                        position: Position.after,
                        properties: { some: { nested: '{{a.name}}' }, another: '{{a.test}}' },
                    })
                    addComp({
                        id: div.id,
                        componentId: 'd',
                        reference: 'b',
                        position: Position.inside,
                        properties: { composed: '{{a.name}} {{b.name}}' },
                    })
                    setFinished(true)
                }, [])

                useEffect(() => {
                    if (comp && finishedCreatingComponent) {
                        addSymb({ id: 'test', name: 'test', layout: convertComponentInstanceToSymbolLayout(comp) })
                        setComp(undefined)
                    }
                }, [comp, finishedCreatingComponent])
                return { symbol }
            },
            { wrapper },
        )
        expect(result.current.symbol).toBeTruthy()
        expect(result.current.symbol?.layout.children[0].properties).toEqual({ name: '{{[0].test}} {{[0].name}}' })
        expect(result.current.symbol?.layout.children[1].properties).toEqual({
            some: { nested: '{{[0].name}}' },
            another: '{{[0].test}}',
        })
        expect(result.current.symbol?.layout.children[0].children[0].properties).toEqual({
            composed: '{{[0].name}} {{[0][0].name}}',
        })
    })

    it('create a component from a symbol definition and remap his properties', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addComp = useAddComponent()
                const addDecl = useAddComponentDeclaration()
                const addSymbol = useAddSymbolDefinition()
                const comp = useRecoilValue(component('a'))
                useEffect(() => {
                    addDecl(div)
                    addSymbol({ id: 'test', layout: symbolLayoutWithProperties, name: 'test' })
                    addComp({ id: 'test', position: Position.inside, componentId: 'a' })
                }, [])
                return { comp }
            },
            { wrapper },
        )
        const id0 = result.current.comp?.children?.[0].id || ''
        const id00 = result.current.comp?.children?.[0]?.children?.[0].id || ''
        const id01 = result.current.comp?.children?.[0]?.children?.[1].id || ''
        const id010 = result.current.comp?.children?.[0]?.children?.[1]?.children?.[0].id || ''
        const expectedComponent: ComponentInstance = {
            id: 'a',
            alias: div.id,
            componentDeclarationId: div.id,
            properties: {},
            children: [
                {
                    id: id0,
                    alias: div.id,
                    componentDeclarationId: div.id,
                    properties: {
                        className: `${id0} test`,
                        test: '{{a.test.name}}',
                        style: { [id0]: { color: 'red', '&>.test': { color: 'red' } } },
                    },
                    children: [
                        {
                            id: id00,
                            componentDeclarationId: div.id,
                            alias: div.id,
                            properties: {
                                className: `${id00} test`,
                                test: { nested: `{{${id0}.test}}` },
                                style: { [id00]: { color: 'red', '&>.test': { color: 'red' } } },
                            },
                            children: [],
                        },
                        {
                            id: id01,
                            componentDeclarationId: div.id,
                            alias: div.id,
                            properties: {
                                className: `${id01} test`,
                                test: { nested: `{{${id0}.test}}` },
                                style: { [id01]: { color: 'red', '&>.test': { color: 'red' } } },
                            },
                            children: [
                                {
                                    id: id010,
                                    componentDeclarationId: div.id,
                                    alias: div.id,
                                    properties: {
                                        className: `${id010} {{${id01}.test}}`,
                                        test: { nested: `{{${id01}.test}} {{${id0}.test}}` },
                                        style: { [id010]: { color: 'red', '&>.test': { color: 'red' } } },
                                    },
                                    children: [],
                                },
                            ],
                        },
                    ],
                },
            ],
        }
        expect(result.current.comp).toEqual(expectedComponent)
        expect([id0, id00, id01, id010].filter((v, i, a) => a.indexOf(v) === i).length).toBe(4)
    })

    it('adapt list data key property when creating symbol', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addComp = useRecoilAction(add)
                const addDecl = useRecoilAction(addDeclaration)
                const addSymb = useRecoilAction(addSymbolDefinition)
                const [comp, setComp] = useRecoilState(component('a'))
                const [finishedCreatingComponent, setFinished] = useState(false)
                const symbol = useRecoilValue(symbolDefinition('test'))
                useEffect(() => {
                    addDecl(List)
                    addDecl(div)
                    addComp({
                        id: div.id,
                        componentId: 'a',
                        position: Position.inside,
                    })
                    addComp({
                        id: List.id,
                        componentId: 'b',
                        reference: 'a',
                        position: Position.inside,
                        properties: { dataKey: 'a.test.name' },
                    })
                    setFinished(true)
                }, [])

                useEffect(() => {
                    if (comp && finishedCreatingComponent) {
                        addSymb({ id: 'test', name: 'test', layout: convertComponentInstanceToSymbolLayout(comp) })
                        setComp(undefined)
                    }
                }, [comp, finishedCreatingComponent])
                return { symbol }
            },
            { wrapper },
        )

        expect(result.current.symbol?.layout.children[0].properties).toEqual({ dataKey: '[0].test.name' })
    })

    it('adapt list data key property when instantiating symbol', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addComp = useAddComponent()
                const addDecl = useAddComponentDeclaration()
                const addSymbol = useAddSymbolDefinition()
                const comp = useRecoilValue(component('a'))
                useEffect(() => {
                    addDecl(div)
                    addDecl(List)
                    addSymbol({
                        id: 'test',
                        layout: {
                            componentDeclarationId: div.id,
                            properties: {},
                            alias: div.id,
                            children: [
                                {
                                    componentDeclarationId: List.id,
                                    properties: { [LIST_DATA_KEY_PROPERTY_KEY]: '[0].test.name' },
                                    alias: List.id,
                                    children: [],
                                },
                            ],
                        },
                        name: 'test',
                    })
                    addComp({ id: 'test', position: Position.inside, componentId: 'a' })
                }, [])
                return { comp }
            },
            { wrapper },
        )

        expect(result.current.comp?.children?.[0].properties).toEqual({
            [LIST_DATA_KEY_PROPERTY_KEY]: 'a.test.name',
        })
    })
})
