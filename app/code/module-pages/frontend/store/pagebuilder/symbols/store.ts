/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { atom, atomFamily, DefaultValue, selector, selectorFamily } from 'recoil'
import { SymbolDefinition, SymbolInstance } from '../../../types/SymbolDefinition'
import notUndefined from '../../../utils/notUndefined'
import { componentChildrenIds, declarationByComponent } from '../../layout/store'

export const symbolsIds = atom<string[]>({
    key: 'pagebuilder/symbols/symbolsIds',
    default: [],
})

export const symbolDefinitions = atom<SymbolDefinition[]>({
    key: 'pagebuilder/symbols/definitions',
    default: [],
})

export const symbolDefinition = selectorFamily<SymbolDefinition | undefined, string>({
    key: 'pagebuilder/symbols/definition',
    get: (id) => ({ get }) => {
        return get(symbolDefinitions).find((v) => v.id === id)
    },
})

export const symbol = selectorFamily<SymbolInstance | undefined, string>({
    key: 'pagebuilder/symbols/symbol',
    get: (id) => ({ get }) => {
        const definitionId = get(symbolDefinitionId(id))
        if (!definitionId) {
            return
        }
        return {
            id,
            definitionId,
            components: get(symbolComponents(id)),
            alias: get(symbolAlias(id)),
        }
    },
    set: (id) => ({ get, set }, newValue) => {
        if (newValue instanceof DefaultValue || newValue === undefined) {
            const v = new DefaultValue()
            set(symbolDefinitionId(id), v)
            set(symbolAlias(id), v)
            set(symbolComponents(id), v)
        } else {
            const definitionId = get(symbolDefinitionId(id))
            const components = get(symbolComponents(id))
            const alias = get(symbolAlias(id))
            if (definitionId !== newValue.definitionId) {
                set(symbolDefinitionId(id), newValue.definitionId)
            }
            if (components !== newValue.components) {
                set(symbolComponents(id), newValue.components)
            }
            if (alias !== newValue.alias) {
                set(symbolAlias(id), newValue.alias)
            }
        }
    },
})

export const symbols = selector<SymbolInstance[]>({
    key: 'pagebuilder/symbols/symbols',
    get: ({ get }) => {
        const ids = get(symbolsIds)
        return ids.map((i) => get(symbol(i))).filter(notUndefined)
    },
    set: ({ set }, newValue) => {
        if (newValue instanceof DefaultValue) {
            set(symbolsIds, newValue)
        } else {
            set(
                symbolsIds,
                newValue.map((i) => i.id),
            )
            newValue.forEach((i) => {
                set(symbol(i.id), i)
            })
        }
    },
})

export const componentSymbolId = selectorFamily<string | undefined, string>({
    key: 'pagebuilder/symbols/componentSymbolId',
    get: (id) => ({ get }) => {
        const symbols = get(symbolsIds)
        for (const symbolId of symbols) {
            const components = get(symbolComponents(symbolId))
            if (components.includes(id)) {
                return symbolId
            }
        }
    },
})

/**
 * If the component is part of a symbol, and the current layout isn't editing the component's symbol, then return only the last components of the symbol
 */
export const componentDroppableChildren = selectorFamily<string[], string>({
    key: 'pagebuilder/symbols/droppableChildren',
    get: (id) => ({ get }) => {
        const symbol = get(componentSymbolId(id))
        if (!symbol) {
            return get(componentChildrenIds(id))
        } else {
            const ids = get(symbolComponents(symbol))
            const o: string[] = []
            for (const i of ids) {
                const childrenIds = get(componentChildrenIds(i))
                const decl = get(declarationByComponent(i))
                if (
                    !childrenIds.some((v) => ids.includes(v)) &&
                    decl &&
                    (decl.config?.haveChildren === undefined || decl.config.haveChildren === true)
                ) {
                    o.push(i)
                }
            }
            return o
        }
    },
})

export const componentDroppableChild = selectorFamily<string | undefined, string>({
    key: 'pagebuilder/symbols/symbolDroppableChild',
    get: (id) => ({ get }) => {
        return get(componentDroppableChildren(id))[0]
    },
})

export const symbolDefinitionId = atomFamily<string | undefined, string>({
    key: 'pagebuilder/symbols/symbolDefinitionId',
    default: undefined,
})

export const symbolComponents = atomFamily<string[], string>({
    key: 'pagebuilder/symbols/symbolComponents',
    default: [],
})

export const symbolAlias = atomFamily<string, string>({
    key: 'pagebuilder/symbols/symbolAlias',
    default: selectorFamily({
        key: 'pagebuilder/symbols/symbolAlias/defaultValue',
        get: (id) => ({ get }) => {
            return get(symbolDefinition(get(symbolDefinitionId(id)) || ''))?.name || ''
        },
    }),
})

export const symbolChildrenIds = selectorFamily<string[], string>({
    key: 'pagebuilder/symbol/symbolChildrenIds',
    get: (id) => ({ get }) => {
        const ids = get(symbolComponents(id))
        if (!ids[0]) {
            return []
        }
        const children = get(componentDroppableChildren(ids[0]))
        const o: string[] = []
        for (const child of children) {
            o.push(...get(componentChildrenIds(child)))
        }
        return o
    },
})

export const symbolParentComponentId = selectorFamily<string | undefined, string>({
    key: 'pagebuilder/symbol/symbolParentComponentId',
    get: (id) => ({ get }) => {
        return get(symbolComponents(id))[0]
    },
})

export const symbolTags = selector<string[]>({
    key: 'pagebuilder/symbol/tags',
    get: ({ get }) => {
        return get(symbolDefinitions)
            .flatMap((v) => v.tags)
            .filter(notUndefined)
            .reduce<string[]>((v, n) => {
                if (!v.includes(n)) {
                    v.push(n)
                }
                return v
            }, [])
    },
})

// export const symbolsByTag = selector<Array<[string | undefined, SymbolDefinition[]]>>({
//
// })
