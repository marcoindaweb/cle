/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { action, useRecoilAction } from '../../recoilAction'
import { DefaultValue } from 'recoil'
import { symbolDefinitions } from './store'
import { SymbolDefinition } from '../../../types/SymbolDefinition'
import { useCallback } from 'react'

export const addSymbolDefinition = action<SymbolDefinition>({
    key: 'pagebuilder/symbols/addSymbolDefinition',
    execute: ({ get, set }, newValue) => {
        if (newValue instanceof DefaultValue) return
        const list = get(symbolDefinitions)
        if (list.some((v) => v.id === newValue.id)) return
        set(symbolDefinitions, [...list, newValue])
    },
})

export const useAddSymbolDefinition = () => {
    const addSymbol = useRecoilAction(addSymbolDefinition)
    return useCallback(
        (v: SymbolDefinition) => {
            addSymbol(v)
        },
        [addSymbol],
    )
}
