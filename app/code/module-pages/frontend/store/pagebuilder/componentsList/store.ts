/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { atom, selector } from 'recoil'
import { componentDeclarations } from '../../componentDeclarations/store'
import notUndefined from '../../../utils/notUndefined'

export enum ComponentSeparation {
    none,
    byTag,
}

export const separation = atom<ComponentSeparation>({
    key: 'pagebuilder/componentsList/separation',
    default: ComponentSeparation.none,
})

export const showAsList = atom<boolean>({
    key: 'pagebuilder/componentsList/showAsList',
    default: false,
})

export const nameFilter = atom<string | undefined>({
    key: 'pagebuilder/componentsList/nameFilter',
    default: undefined,
})

export const tagFilter = atom<string[]>({
    key: 'pagebuilder/componentsList/tagFilter',
    default: [],
})

export const groupFilter = atom<string | undefined>({
    key: 'pagebuilder/componentsList/groupFilter',
    default: undefined,
})

export const isSymbolFilter = atom<boolean>({
    key: 'pagebuilder/componentsList/isSymbolFilter',
    default: false,
})

export const groups = selector<string[]>({
    key: 'pagebuilder/componentsList/groups',
    get: ({ get }) => {
        return get(componentDeclarations)
            .map((v) => v.meta?.group)
            .filter(notUndefined)
    },
})

export const tags = selector<string[]>({
    key: 'pagebuilder/componentsList/tags',
    get: ({ get }) => {
        return get(componentDeclarations).reduce<string[]>((curr, decl) => {
            const newVal = [...curr]
            decl.meta?.tags?.forEach((v) => (newVal.includes(v) ? null : newVal.push(v)))
            return newVal
        }, [])
    },
})
