/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { atom, atomFamily, DefaultValue, selector, selectorFamily } from 'recoil'
import ActionInstance from '../../../../types/ActionInstance'
import notUndefined from '../../../../utils/notUndefined'

const actionsIds = atom<string[]>({
    key: 'pagebuilder/actions/instances/actionsIds',
    default: [],
})

const actionAlias = atomFamily<string, string>({
    key: 'pagebuilder/actions/instances/actionAlias',
    default: selectorFamily({
        key: 'pagebuilder/actions/actionAlias/defaultValue',
        get: (id) => ({ get }) => {
            return get(actionDeclarationId(id)) || ''
        },
    }),
})

const actionBindedTo = atomFamily<string[], string>({
    key: 'pagebuilder/actions/instances/actionBindedTo',
    default: [],
})

const actions = selector<ActionInstance[]>({
    key: 'pagebuilder/actions/instances/actions',
    get: ({ get }) => {
        return get(actionsIds)
            .map((v) => get(action(v)))
            .filter(notUndefined)
    },
})

const action = selectorFamily<ActionInstance | undefined, string>({
    key: 'pagebuilder/actions/instances/actionInstance',
    get: (id) => ({ get }) => {
        const declarationId = get(actionDeclarationId(id))
        if (!declarationId) return
        const alias = get(actionAlias(id))
        const bindedTo = get(actionBindedTo(id))
        return {
            alias,
            bindedTo,
            declarationId,
            id,
        }
    },
    set: (id) => ({ get, set, reset }, val) => {
        if (val instanceof DefaultValue || val === undefined) {
            reset(actionDeclarationId(id))
            reset(actionAlias(id))
            reset(actionBindedTo(id))
        } else {
            if (val.declarationId !== get(actionDeclarationId(id))) {
                set(actionDeclarationId(id), val.declarationId)
            }
            if (val.alias !== get(actionAlias(id))) {
                set(actionDeclarationId(id), val.declarationId)
            }
            set(actionBindedTo(id), val.bindedTo)
        }
    },
})

const actionDeclarationId = atomFamily<string | undefined, string>({
    key: 'pagebuilder/actions/instances/actionDeclarationId',
    default: undefined,
})
