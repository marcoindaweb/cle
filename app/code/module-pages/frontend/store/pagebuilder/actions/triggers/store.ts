/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { atom, atomFamily, selectorFamily } from 'recoil'
import ComponentEventTrigger from '../../../../types/ComponentEventListener'

export const eventTriggersIds = atom<string[]>({
    key: 'pagebuilder/actions/listeners/eventTriggersIds',
    default: [],
})

export const eventTriggerComponentId = atomFamily<string | undefined, string>({
    key: 'pagebuilder/actions/listeners/eventTriggerComponentId',
    default: undefined,
})

export const eventTriggerAction = atomFamily<string | undefined, string>({
    key: 'pagebuilder/actions/listeners/eventTriggerActions',
    default: undefined,
})

export const eventTriggerEvent = atomFamily<string | undefined, string>({
    key: 'pagebuilder/actions/listener/eventTriggerEvent',
    default: undefined,
})

export const eventTrigger = selectorFamily<ComponentEventTrigger | undefined, string>({
    key: 'pagebuilder/actions/listeners/eventListener',
    get: (id) => ({ get }) => {
        const component = get(eventTriggerComponentId(id))
        if (!component) return
        const event = get(eventTriggerEvent(id))
        if (!event) return
        const action = get(eventTriggerAction(id))
        return {
            id,
            component,
            action,
            event,
        }
    },
})

export const componentTriggersIds = selectorFamily<string[], string>({
    key: 'pagebuilder/actions/listeners/componentTriggersIds',
    get: (id) => ({ get }) => {
        return get(eventTriggersIds).filter((v) => get(eventTriggerComponentId(v)) === id)
    },
})
