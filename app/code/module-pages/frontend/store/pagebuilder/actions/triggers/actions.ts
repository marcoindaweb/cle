/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useCallback } from 'react'
import { v4 } from 'uuid'
import { action, useRecoilAction } from '../../../recoilAction'
import { DefaultValue } from 'recoil'
import { eventTriggerAction, eventTriggerComponentId, eventTriggerEvent, eventTriggersIds } from './store'

const addTrigger = action<{ eventName: string; componentId: string; eventId?: string; action?: string }>({
    key: 'pagebuilder/actions/addTrigger',
    execute: ({ set, get }, val) => {
        if (val instanceof DefaultValue) return
        const id = val.componentId || v4()
        set(eventTriggersIds, (val) => [...val, id])
        set(eventTriggerComponentId(id), val.componentId)
        set(eventTriggerEvent(id), val.eventName)
        if (val.action) {
            set(eventTriggerAction(id), val.action)
        }
    },
})

export const useAddTrigger = () => {
    return useRecoilAction(addTrigger)
}
