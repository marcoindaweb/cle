/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { atom, selector } from 'recoil'
import { IconName, MaybeElement } from '@blueprintjs/core'
import icons from '../../../icons'

export type Device = {
    name: string
    width: string
    height: string
    icon: IconName | MaybeElement
    mediaQuery: string
}

type DeviceStore = {
    devices: Device[]
    current: Device
}

const devices: Device[] = [
    {
        name: 'laptop',
        icon: icons.laptop,
        width: '1024px',
        height: '800px',
        mediaQuery: '@media (min-width: 1024px)',
    },
    {
        name: 'tablet',
        icon: icons.tablet,
        width: '768px',
        height: '1024px',
        mediaQuery: '@media (min-width: 768px)',
    },
    {
        name: 'mobile',
        icon: 'mobile-phone',
        width: '468px',
        height: '736px',
        mediaQuery: '@media (max-width: 575.98px)',
    },
]

export const fixedDeviceHeight = atom<boolean>({
    key: 'pagebuilder/device/fixedDeviceHeight',
    default: false,
})

export const deviceStore = atom<DeviceStore>({
    key: 'pagebuilder/device',
    default: {
        devices,
        current: devices[0],
    },
})

export const currentDevice = selector<Device>({
    key: 'pagebuilder/device/currentDevice',
    get: ({ get }) => {
        return get(deviceStore).current
    },
    set: ({ set, get }, newValue) => {
        const store = get(deviceStore)
        set(deviceStore, { ...store, current: newValue })
    },
})

export const availableDevices = selector<Device[]>({
    key: 'pagebuilder/device/availableDevices',
    get: ({ get }) => get(deviceStore).devices,
})
