/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentInstance from '@linda/module-pages/frontend/types/ComponentInstance'
import { atom, selector, DefaultValue } from 'recoil'
import { component, componentProperties } from '../../layout/store'
import ComponentDeclaration from '../../../types/ComponentDeclaration'
import { componentDeclarationById } from '../../componentDeclarations/store'

export const hoveredId = atom<string | undefined>({
    key: 'pagebuilder/components/hoveredId',
    default: undefined,
})

export const hovered = selector<ComponentInstance | undefined>({
    key: 'pagebuilder/components/hovered',
    get: ({ get }) => {
        const id = get(hoveredId)
        return get(component(id || ''))
    },
    set: ({ set }, val) => {
        if (val instanceof DefaultValue) {
            return
        }
        if (val) {
            set(hoveredId, val.id)
            set(component(val.id), val)
        } else {
            set(hoveredId, val)
        }
    },
})

export const selectedId = atom<string | undefined>({
    key: 'pagebuilder/components/selectedId',
    default: undefined,
})

export const selected = selector<ComponentInstance | undefined>({
    key: 'pagebuilder/components/selected',
    get: ({ get }) => {
        const id = get(selectedId)
        return get(component(id || ''))
    },
    set: ({ set }, val) => {
        if (val instanceof DefaultValue) {
            return
        }
        if (val) {
            set(selectedId, val.id)
            set(component(val.id), val)
        } else {
            set(selectedId, val)
        }
    },
})

export const selectedComponentDeclaration = selector<ComponentDeclaration | undefined>({
    key: 'pagebuilder/components/selectedDeclaration',
    get: ({ get }) => {
        const selectedComp = get(selected)
        return get(componentDeclarationById(selectedComp?.componentDeclarationId || ''))
    },
})

export const selectedProps = selector<ComponentInstance['properties'] | undefined>({
    key: 'pagebuilder/components/selectedProps',
    get: ({ get }) => {
        return get(selected)?.properties
    },
    set: ({ set, get }, val) => {
        if (val instanceof DefaultValue || val === undefined) {
            return
        } else {
            const id = get(selectedId)
            if (!id) return
            set(componentProperties(id), val)
        }
    },
})

export const selectedStyle = selector<ComponentInstance['properties']['style'] | undefined>({
    key: 'pagebuilder/components/selectedStyle',
    get: ({ get }) => {
        return get(selectedProps)?.style
    },
    set: ({ get, set }, val) => {
        if (val instanceof DefaultValue) {
            return
        }
        const props = get(selectedProps)
        set(selectedProps, { ...props, style: val })
    },
})

export const selectedClasses = selector<ComponentInstance['properties']['className'] | undefined>({
    key: 'pagebuilder/components/selectedClass',
    get: ({ get }) => {
        return get(selectedProps)?.className
    },
    set: ({ get, set }, val) => {
        if (val instanceof DefaultValue) {
            return
        }
        const props = get(selectedProps)
        set(selectedProps, { ...props, className: val })
    },
})
