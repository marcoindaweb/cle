/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { atom } from 'recoil'
import React from 'react'

export const positionX = atom<number>({
    key: 'pagebuilder/contextMenu/positionX',
    default: 0,
})

export const positionY = atom<number>({
    key: 'pagebuilder/contextMenu/positionY',
    default: 0,
})

export const menu = atom<JSX.Element>({
    key: 'pagebuilder/contextMenu/menu',
    default: <></>,
})

export const target = atom<HTMLDivElement | undefined>({
    key: 'pagebuilder/contextMenu/target',
    default: undefined,
})

export const show = atom<boolean>({
    key: 'pagebuilder/contextMenu/show',
    default: false,
})
