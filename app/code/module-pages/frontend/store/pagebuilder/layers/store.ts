/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { atom, atomFamily } from 'recoil'

export const isOpen = atomFamily<boolean, string>({
    key: 'pagebuilder/layers/isOpen',
    default: false,
})

export const draggedId = atom<string | undefined>({
    key: 'pagebuilder/layers/draggedId',
    default: undefined,
})
export const draggedOverId = atom<string | undefined>({
    key: 'pagebuilder/layers/draggedOverId',
    default: undefined,
})
