/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { atom, selector } from 'recoil'

export const frame = atom<HTMLIFrameElement | undefined>({
    key: 'pagebuilder/frame',
    default: undefined,
})

export const body = selector<HTMLBodyElement | undefined>({
    key: 'pagebuilder/frame/body',
    get: ({ get }) => {
        const f = get(frame)
        return f?.contentDocument?.body as HTMLBodyElement | undefined
    },
})

export const head = selector<HTMLHeadElement | undefined>({
    key: 'pagebuilder/frame/head',
    get: ({ get }) => {
        const f = get(frame)
        return f?.contentDocument?.head
    },
})
