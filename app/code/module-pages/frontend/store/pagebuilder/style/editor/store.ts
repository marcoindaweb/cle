/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { atom, DefaultValue, selector, selectorFamily } from 'recoil'
import { selectedId, selectedStyle } from '../../components/store'
import _get from 'lodash.get'
import _clone from 'lodash.clonedeep'
import _set from 'lodash.set'
import { Device } from '../../device/store'

export const selectedClass = selector<string | undefined>({
    key: 'pagebuilder/style/editor/selectedClass',
    get: ({ get }) => {
        const id = get(selectedId)
        if (id) {
            return CSS.escape(id)
        }
    },
})

export const pseudoSelector = atom<string | null>({
    key: 'pagebuilder/style/editor/pseudoSelector',
    default: null,
})

export const media = atom<Device | null>({
    key: 'pagebuilder/style/editor/media',
    default: null,
})

export const mediaQuery = selector<string | undefined>({
    key: 'pagebuilder/style/editor/mediaQuery',
    get: ({ get }) => {
        return get(media)?.mediaQuery
    },
})

export const styleKeys = selector<string[]>({
    key: 'pagebuilder/style/editor/styleKeys',
    get: ({ get }) => {
        const pseudo = get(pseudoSelector)
        const query = get(mediaQuery)
        const selClass = get(selectedClass)
        const keys: string[] = []
        if (!selClass) {
            return []
        }
        keys.push('.' + selClass)
        if (query) keys.push(query)
        if (pseudo) keys.push(`&${pseudo}`)
        return keys
    },
})

export const style = selector<Record<string, any>>({
    key: 'pagebuilder/style/editor/style',
    get: ({ get }) => {
        const keys = get(styleKeys)
        const selStyle = get(selectedStyle)
        const res = _get(selStyle || {}, keys, {})
        return res
    },
    set: ({ get, set }, val) => {
        if (val instanceof DefaultValue) {
            return
        }
        const keys = get(styleKeys)
        const selStyle = get(selectedStyle)
        const newStyle = _clone(selStyle || {})
        _set(newStyle, keys, val)
        set(selectedStyle, newStyle)
    },
})

export const styleProperty = selectorFamily<string | undefined, string>({
    key: 'pagebuilder/style/editor/property',
    get: (p) => ({ get }) => {
        return get(style)[p]
    },
    set: (p) => ({ get, set }, val) => {
        if (val instanceof DefaultValue) {
            return
        }
        set(style, { ...get(style), [p]: val })
    },
})
