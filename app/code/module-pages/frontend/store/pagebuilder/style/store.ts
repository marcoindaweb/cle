/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { atom, DefaultValue, selector, selectorFamily } from 'recoil'

export const sheets = atom<Record<string, HTMLStyleElement>>({
    key: 'page/builder/style/sheets',
    default: {},
})

export const currentSheetKey = atom<string>({
    key: 'page/builder/style/currentSheetKey',
    default: 'main',
})

export const sheet = selectorFamily<HTMLStyleElement | undefined, string>({
    key: 'page/builder/style/sheet',
    get: (key) => ({ get }) => {
        return get(sheets)[key]
    },
    set: (key) => ({ get, set }, val) => {
        if (val === undefined || val instanceof DefaultValue) {
            const newVal = { ...get(sheets) }
            delete newVal[key]
            set(sheets, newVal)
        } else {
            set(sheets, { ...get(sheets), [key]: val })
        }
    },
})

export const currentSheet = selector<HTMLStyleElement | undefined>({
    key: 'page/builder/style/currentSheet',
    get: ({ get }) => {
        const key = get(currentSheetKey)
        return get(sheet(key))
    },
    set: ({ set, get }, val) => {
        const key = get(currentSheetKey)
        if (key) {
            set(sheet(key), val)
        }
    },
})
