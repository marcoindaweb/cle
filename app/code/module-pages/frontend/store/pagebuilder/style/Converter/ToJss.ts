/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Styles } from 'react-jss'
import postcss, { AtRule, Declaration, Rule } from 'postcss'
// we use base postcss since we need only the ast from it
import postcss from 'postcss'
import AdminToast from '@linda/module-admin/frontend/components/AdminToast'

class ToJss {
  convert (v: string): Styles {
    const o = {}
    try {
      const ast = postcss().process(v).root
      if (ast.nodes) {
        for (const node of ast.nodes) {
          switch (node.type) {
            case 'rule':
            case 'atrule':
            case 'decl':
              o[this.getKey(node)] = this.getVal(node)
          }
        }
      }
    } catch (e) {
      AdminToast.show({ intent: 'danger', message: e.toString() })
    }
    return o
  }

  private getKey (node: AtRule | Rule | Declaration) {
    switch (node.type) {
      case 'decl':
        return node.prop.replace(/[-][a-z]/, (v) => v.toUpperCase().replace('-', ''))
      case 'rule':
        return node.selector.trim()
      case 'atrule':
        return `@${node.name} ${node.params}`.trim()
    }
  }

  private getVal (node: AtRule | Rule | Declaration) {
    switch (node.type) {
      case 'atrule':
      case 'rule':
        if (!node.nodes) return {}
        const o = {}
        node.nodes.forEach((v) => {
          if (v.type !== 'comment') {
            o[this.getKey(v)] = this.getVal(v)
          }
        })
        return o
      case 'decl':
        return node.value
    }
  }
}

const toJss = new ToJss()
export default toJss
