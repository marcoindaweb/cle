/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

/* eslint-disable prettier/prettier */
import toJss from '../ToJss'
import toCss from '../ToCss'
import postcss from "../../postcss";

describe('converter', () => {
    it('convert css to jss', () => {
        expect(
            toJss.convert(
                `
            .class {
                color: red;
                &:hover {
                    display: flex;
                    justify-content: center;
                    @media (min-width: 768px) {
                        background: red;
                        margin: 0 10px
                    }
                }
            }
        `,
            ),
        ).toEqual({
                '.class': {
                    color: 'red',
                    '&:hover': {
                        display: 'flex',
                        justifyContent: 'center',
                        '@media (min-width: 768px)': {
                            background: 'red',
                            margin: '0 10px'
                        },
                    },
                },
        })
    })
    it('conert css single property', () => {
        expect(toJss.convert('color: red')).toEqual({ color: 'red'})
    })
    it('convert jss to css', () => {
        expect(
            toCss.convert({
                    '.class': {
                        color: 'red',
                        '&:hover': {
                            display: 'flex',
                            justifyContent: 'center',
                            '@media (min-width: 768px)': {
                                background: 'red',
                            },
                        },
                    },
            }),
        ).toEqual(
`.class {
  color: red;
  &:hover {
    display: flex;
    justify-content: center;
    @media (min-width: 768px) {
      background: red;
    }
  }
}
`
        ,
        )
    })
})
