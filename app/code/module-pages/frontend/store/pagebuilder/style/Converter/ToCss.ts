/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Styles } from 'react-jss'
import postcss from '../postcss'

class ToCss {
    convert(v: Styles): string {
        const style = v
        if (style) {
            return this.toString(style)
        } else {
            return ''
        }
    }

    toString(v: any, nest = 0) {
        let o = ''
        Object.entries(v).forEach(([key, val]) => {
            let tabs = ''
            for (let i = 0; i < nest; i++) {
                tabs += '  '
            }
            if (typeof val === 'string') {
                o += `${tabs}${key.replace(/[A-Z]/g, (v) => `-${v.toLowerCase()}`)}: ${val};\n`
            } else {
                o += `${tabs}${key} {\n`
                o += this.toString(val, nest + 1)
                o += `${tabs}}\n`
            }
        })
        return o
    }
}

const toCss = new ToCss()

export default toCss
