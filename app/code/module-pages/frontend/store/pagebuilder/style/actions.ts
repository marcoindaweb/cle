/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import { currentSheetKey, sheet } from './store'
import { useCallback, useEffect } from 'react'
import { frame } from '../frame/store'

export const useCreateStylesheet = (key) => {
    const setSheet = useSetRecoilState(sheet(key))
    const iframe = useRecoilValue(frame)
    const contentDocument = iframe?.contentDocument

    return useCallback(() => {
        if (contentDocument) {
            const sheet = contentDocument.createElement('style')
            sheet.id = key
            const head = contentDocument.querySelector('head')
            if (!head) {
                throw new Error('Head is missing on iframe')
            }
            head.appendChild(sheet)
            setSheet(sheet)
        }
    }, [setSheet, contentDocument, key])
}

export const useDeleteStylesheet = (key) => {
    const setSheet = useSetRecoilState(sheet(key))
    const iframe = useRecoilValue(frame)
    const contentDocument = iframe?.contentDocument

    return useCallback(() => {
        if (contentDocument) {
            const sheet = contentDocument.querySelector(`style#${CSS.escape(key)}`)
            if (sheet) {
                sheet.remove()
                setSheet(undefined)
            }
        }
    }, [setSheet, contentDocument, key])
}

// Use a stylesheet, create it if it doesn't exist
export const useStylesheet = (key: string, createIfNotExists = false): HTMLStyleElement | undefined => {
    const s = useRecoilValue(sheet(key))
    const create = useCreateStylesheet(key)

    useEffect(() => {
        if (!s && createIfNotExists) {
            create()
        }
    }, [!s, createIfNotExists])

    return s
}

export const useCurrentStylesheet = (defaultKey: string) => {
    const currentK = useRecoilValue(currentSheetKey)
    return useStylesheet(currentK || defaultKey)
}
