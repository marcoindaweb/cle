/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { atomFamily, selectorFamily } from 'recoil'
import { declarationByComponent } from '../../layout/store'
import { componentSymbolId, symbolAlias } from '../symbols/store'

export const componentAlias = selectorFamily<string, string>({
    key: 'pagebuilder/layers/componentAlias',
    get: (id) => ({ get }) => {
        const symbol = get(componentSymbolId(id))
        if (symbol) {
            return get(symbolAlias(symbol))
        } else {
            return get(_componentAlias(id))
        }
    },
    set: (id) => ({ get, set }, val) => {
        const symbol = get(componentSymbolId(id))
        if (symbol) {
            set(symbolAlias(symbol), val)
        } else {
            set(_componentAlias(id), val)
        }
    },
})

export const _componentAlias = atomFamily<string, string>({
    key: 'pagebuilder/layers/componentAlias',
    default: selectorFamily<string, string>({
        key: 'pagebuilder/layers/componentAlias/defaultValue',
        get: (param) => ({ get }) => {
            return get(declarationByComponent(param))?.id || ''
        },
    }),
})
