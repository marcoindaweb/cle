/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { DefaultValue, ReadWriteSelectorOptions, selector, useSetRecoilState } from 'recoil'

type Params<T> = {
    key: string
    execute: (
        opts: {
            set: Parameters<ReadWriteSelectorOptions<T>['set']>[0]['set']
            get: Parameters<ReadWriteSelectorOptions<T>['set']>[0]['get']
            reset: Parameters<ReadWriteSelectorOptions<T>['set']>[0]['reset']
        },
        newValue: DefaultValue | T,
    ) => void
}

export const action = <T>(p: Params<T>) => {
    return selector<T | DefaultValue>({
        key: p.key,
        get: () => new DefaultValue(),
        set: p.execute,
    })
}

export const useRecoilAction = useSetRecoilState
