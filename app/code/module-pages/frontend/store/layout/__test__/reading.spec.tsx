/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { RecoilRoot, useRecoilValue, useSetRecoilState } from 'recoil'
import React, { useEffect } from 'react'
import {
    allChildrenOfType,
    allComponentChildrenIds,
    component,
    componentChildrenIds,
    componentData,
    componentDeclarationId,
    componentsIds,
    currentRootComponentId,
    parentId,
    parentsIds,
    providedData,
} from '../store'
import Position from '../../../types/Position'
import { useAddComponentDeclaration } from '../../componentDeclarations/actions'
import { renderHook } from '@testing-library/react-hooks'
import { v4 } from 'uuid'
import div from '../../../components/Definitions/Div'
import { useRecoilAction } from '../../recoilAction'
import { add, move, useAddComponent, useMoveComp } from '../actions'
import { useAddSymbolDefinition } from '../../pagebuilder/symbols/actions'
import Group from '../../../components/Definitions/Group'
import Input from '../../../components/Definitions/Input'
import { SymbolDefinition } from '../../../types/SymbolDefinition'
import text from '../../../components/Definitions/Text'
import { componentDroppableChildren } from '../../pagebuilder/symbols/store'

const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
/**
 * Tests about reading the components tree (get parent, children ecc...)
 */
describe('tree reading', () => {
    it('get the parent id a component', async () => {
        const aId = v4()
        const bId = v4()

        const { result } = renderHook(
            () => {
                const addComp = useAddComponent()
                const addDecl = useAddComponentDeclaration()
                const pId = useRecoilValue(parentId(bId))
                useEffect(() => {
                    addDecl(div)
                    addComp({ id: div.id, componentId: aId, position: Position.inside })
                    addComp({ id: div.id, componentId: bId, position: Position.inside, reference: aId })
                }, [])

                return { pId }
            },
            { wrapper },
        )

        expect(result.current.pId).toEqual(aId)
    })

    it('get the parents ids of a component', () => {
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const addComp = useAddComponent()
                const gPId = useRecoilValue(parentsIds('g'))
                const fPId = useRecoilValue(parentsIds('f'))
                useEffect(() => {
                    addDecl(div)
                    addComp({ componentId: 'a', position: Position.inside, id: div.id })
                    addComp({ reference: 'a', componentId: 'b', position: Position.inside, id: div.id })
                    addComp({ reference: 'b', componentId: 'c', position: Position.inside, id: div.id })
                    addComp({ reference: 'b', componentId: 'f', position: Position.inside, id: div.id })
                    addComp({ reference: 'c', componentId: 'd', position: Position.inside, id: div.id })
                    addComp({ reference: 'd', componentId: 'e', position: Position.inside, id: div.id })
                    addComp({ reference: 'e', componentId: 'g', position: Position.inside, id: div.id })
                }, [])

                return { fPId, gPId }
            },
            { wrapper },
        )

        expect(result.current).toEqual({ fPId: ['a', 'b'], gPId: ['a', 'b', 'c', 'd', 'e'] })
    })

    it('get the parents ids of a component with also symbols as parents', () => {
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const addSymb = useAddSymbolDefinition()
                const addComp = useAddComponent()
                const gPId = useRecoilValue(parentsIds('g'))
                const fPId = useRecoilValue(parentsIds('f'))
                const cChildren = useRecoilValue(componentChildrenIds('c'))
                const eChildren = useRecoilValue(componentChildrenIds('e'))
                const cChildId = cChildren[0]
                const eChildId = eChildren[0]
                useEffect(() => {
                    addDecl(div)
                    addSymb({
                        id: 'test',
                        name: 'test',
                        layout: {
                            componentDeclarationId: div.id,
                            properties: {},
                            alias: 'div',
                            children: [
                                {
                                    componentDeclarationId: div.id,
                                    properties: {},
                                    alias: 'div',
                                    children: [],
                                },
                            ],
                        },
                    })
                    addComp({ componentId: 'a', position: Position.inside, id: div.id })
                    addComp({ reference: 'a', componentId: 'b', position: Position.inside, id: div.id })
                    addComp({ reference: 'b', componentId: 'c', position: Position.inside, id: 'test' })
                    addComp({ reference: 'b', componentId: 'f', position: Position.inside, id: div.id })
                    addComp({ reference: 'c', componentId: 'd', position: Position.inside, id: div.id })
                    addComp({ reference: 'd', componentId: 'e', position: Position.inside, id: 'test' })
                    addComp({ reference: 'e', componentId: 'g', position: Position.inside, id: div.id })
                }, [])

                return { fPId, gPId, cChildId, eChildId }
            },
            { wrapper },
        )

        expect(result.current.fPId).toEqual(['a', 'b'])
        expect(result.current.gPId).toEqual(['a', 'b', 'c', result.current.cChildId, 'd', 'e', result.current.eChildId])
    })

    it('get the provided data of a component', () => {
        const { result } = renderHook(
            () => {
                const setBData = useSetRecoilState(componentData({ component: 'b' }))
                const setCData = useSetRecoilState(componentData({ component: 'c' }))
                const setDData = useSetRecoilState(componentData({ component: 'd' }))
                const setEData = useSetRecoilState(componentData({ component: 'e' }))
                const providedA = useRecoilValue(providedData({ component: 'a' }))
                const providedB = useRecoilValue(providedData({ component: 'b' }))
                const providedC = useRecoilValue(providedData({ component: 'c' }))
                const providedD = useRecoilValue(providedData({ component: 'd' }))
                const providedE = useRecoilValue(providedData({ component: 'e' }))
                const addDecl = useAddComponentDeclaration()
                const addComp = useAddComponent()
                useEffect(() => {
                    addDecl(div)
                    addComp({ componentId: 'a', position: Position.inside, id: div.id })
                    addComp({ reference: 'a', componentId: 'b', position: Position.inside, id: div.id })
                    addComp({ reference: 'b', componentId: 'c', position: Position.inside, id: div.id })
                    addComp({ reference: 'c', componentId: 'd', position: Position.inside, id: div.id })
                    addComp({ reference: 'd', componentId: 'e', position: Position.inside, id: div.id })
                    setBData({ data: 'b' })
                    setCData({ data: 'c' })
                    setDData({ data: 'd' })
                    setEData({ data: 'e' })
                }, [])

                return { a: providedA, b: providedB, c: providedC, d: providedD, e: providedE }
            },
            { wrapper },
        )

        expect(result.current).toEqual({
            a: {},
            b: {},
            c: { b: { data: 'b' } },
            d: { b: { data: 'b' }, c: { data: 'c' } },
            e: { b: { data: 'b' }, c: { data: 'c' }, d: { data: 'd' } },
        })
    })

    it('add component as child of another component', () => {
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const a = useSetRecoilState(add)
                const children = useRecoilValue(componentChildrenIds('root'))
                useEffect(() => {
                    addDecl(div)
                    a({ id: div.id, componentId: 'root', position: Position.inside })
                    a({ id: div.id, componentId: 'a', position: Position.inside, reference: 'root' })
                }, [])
                return { children }
            },
            { wrapper },
        )

        expect(result.current.children).toEqual(['a'])
    })

    it('get all childrens ids', () => {
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const a = useRecoilAction(add)
                const ids = useRecoilValue(allComponentChildrenIds('a'))
                useEffect(() => {
                    addDecl(div)
                    a({ id: div.id, componentId: 'a', position: Position.inside })
                    a({ id: div.id, componentId: 'bb', reference: 'a', position: Position.inside })
                    a({ id: div.id, componentId: 'cc', reference: 'bb', position: Position.inside })
                    a({ id: div.id, componentId: 'dd', reference: 'cc', position: Position.inside })
                    a({ id: div.id, componentId: 'ee', reference: 'dd', position: Position.inside })
                }, [])
                return { ids }
            },
            { wrapper },
        )

        expect(result.current.ids).toEqual(['bb', 'cc', 'dd', 'ee'])
    })

    it('get all childrens ids of a specific type', () => {
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const a = useRecoilAction(add)
                const inputIds = useRecoilValue(allChildrenOfType(['a', Input.id]))
                const inputAndGroupIds = useRecoilValue(allChildrenOfType(['a', [Input.id, Group.id]]))
                useEffect(() => {
                    addDecl(div)
                    addDecl(Group)
                    addDecl(Input)
                    a({ id: div.id, componentId: 'a', position: Position.inside })
                    a({ id: div.id, componentId: 'bb', reference: 'a', position: Position.inside })
                    a({ id: Input.id, componentId: 'cc', reference: 'bb', position: Position.inside })
                    a({ id: div.id, componentId: 'dd', reference: 'cc', position: Position.inside })
                    a({ id: div.id, componentId: 'ee', reference: 'dd', position: Position.inside })
                    a({ id: Input.id, componentId: 'ff', reference: 'ee', position: Position.inside })
                    a({ id: Group.id, componentId: 'gg', reference: 'ff', position: Position.inside })
                    a({ id: div.id, componentId: 'hh', reference: 'gg', position: Position.inside })
                    a({ id: Input.id, componentId: 'ii', reference: 'hh', position: Position.inside })
                    a({ id: Group.id, componentId: 'll', reference: 'ii', position: Position.inside })
                }, [])
                return { inputIds, inputAndGroupIds }
            },
            { wrapper },
        )

        expect(result.current.inputIds).toEqual(['cc', 'ff', 'ii'])
        expect(result.current.inputAndGroupIds).toEqual(['cc', 'ff', 'gg', 'ii', 'll'])
    })

    it('return symbol droppable children', () => {
        const symbol: SymbolDefinition = {
            id: 'symbol',
            name: 'symbol',
            layout: {
                componentDeclarationId: div.id,
                properties: {},
                alias: '',
                children: [
                    {
                        // Not droppable since it have other children
                        componentDeclarationId: div.id,
                        properties: {},
                        alias: '',
                        children: [
                            {
                                componentDeclarationId: div.id,
                                properties: {},
                                alias: '',
                                children: [
                                    {
                                        // Droppable
                                        componentDeclarationId: div.id,
                                        properties: {},
                                        alias: '',
                                        children: [],
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        // Droppable
                        componentDeclarationId: div.id,
                        properties: {},
                        alias: '',
                        children: [],
                    },
                    {
                        // Not droppable since text.config.haveChildren === false
                        componentDeclarationId: text.id,
                        properties: {},
                        alias: '',
                        children: [],
                    },
                ],
            },
        }

        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const addSymb = useAddSymbolDefinition()
                const addComp = useAddComponent()
                const droppable = useRecoilValue(componentDroppableChildren('s'))
                const comp = useRecoilValue(component('s'))
                useEffect(() => {
                    addDecl(div)
                    addDecl(text)
                    addSymb(symbol)
                    addComp({ id: symbol.id, componentId: 's' })
                }, [])
                return { droppable, comp }
            },
            { wrapper },
        )
        const { droppable, comp } = result.current
        expect(droppable).toEqual([comp?.children?.[0].children?.[0].children?.[0].id, comp?.children?.[1].id])
    })
})
