/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { renderHook } from '@testing-library/react-hooks'
import { RecoilRoot, useRecoilValue, useSetRecoilState } from 'recoil'
import React, { useEffect } from 'react'
import { useRecoilAction } from '../../recoilAction'
import { add } from '../actions'
import { useAddComponentDeclaration } from '../../componentDeclarations/actions'
import div from '../../../components/Definitions/Div'
import Position from '../../../types/Position'
import {
    buildDataKeyFromListKeys,
    componentData,
    componentPropertiesKeys,
    componentPropertiesWithProvidedData,
    componentProperty,
} from '../store'
import List from '../../../components/Definitions/List'

/**
 * Tests about components properties and data
 */
describe('component properties and provided data', () => {
    it('get component properties', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addComponent = useRecoilAction(add)
                const addDeclaration = useAddComponentDeclaration()
                const propertiesKeys = useRecoilValue(componentPropertiesKeys('a'))
                const aPropertyValue = useRecoilValue(componentProperty({ component: 'a', key: 'a' }))
                const bPropertyValue = useRecoilValue(componentProperty({ component: 'a', key: 'b' }))
                useEffect(() => {
                    addDeclaration(div)
                    addComponent({
                        id: div.id,
                        componentId: 'a',
                        position: Position.inside,
                        properties: { a: 'a', b: 'b' },
                    })
                }, [])
                return { propertiesKeys, aPropertyValue, bPropertyValue }
            },
            { wrapper },
        )

        expect(result.current.aPropertyValue).toEqual('a')
        expect(result.current.bPropertyValue).toEqual('b')
        expect(result.current.propertiesKeys).toEqual(['a', 'b'])
    })

    it('get component properties with provided data', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addComponent = useRecoilAction(add)
                const addDeclaration = useAddComponentDeclaration()
                const setAProvided = useSetRecoilState(componentData({ component: 'a' }))
                const bProperties = useRecoilValue(componentPropertiesWithProvidedData('b'))
                useEffect(() => {
                    addDeclaration(div)
                    addComponent({
                        id: div.id,
                        componentId: 'a',
                        position: Position.inside,
                    })
                    setAProvided({ data: { test: 'hello' } })
                    addComponent({
                        id: div.id,
                        componentId: 'b',
                        position: Position.inside,
                        reference: 'a',
                        properties: { a: '{{a.data.test}}', b: 'b' },
                    })
                }, [])
                return { bProperties }
            },
            { wrapper },
        )

        expect(result.current.bProperties).toEqual({ a: 'hello', b: 'b' })
    })

    it('build key from indexes, dataKey and listKeys', () => {
        const res1 = buildDataKeyFromListKeys([0, 1], 'data.some.deeply.nested.properties.oh.yes', [
            'data.some.deeply',
            'data.some.deeply.nested.properties',
        ])
        const res2 = buildDataKeyFromListKeys([0, 1], 'data.some.deeply.nested', [
            'data.some.deeply',
            'data.some.deeply.nested.properties',
        ])
        expect(res1).toEqual('data.some.deeply[0].nested.properties[1].oh.yes')
        expect(res2).toEqual('data.some.deeply[0].nested')
    })

    it('get component properties with provided data on array when providing an index and the parent is of type list', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addComponent = useRecoilAction(add)
                const addDeclaration = useAddComponentDeclaration()
                const setAProvided = useSetRecoilState(componentData({ component: 'a' }))
                const cProperties0 = useRecoilValue(
                    componentPropertiesWithProvidedData({ component: 'c', indexes: [0] }),
                )
                const cProperties1 = useRecoilValue(
                    componentPropertiesWithProvidedData({ component: 'c', indexes: [1] }),
                )
                const cProperties2 = useRecoilValue(
                    componentPropertiesWithProvidedData({ component: 'c', indexes: [2] }),
                )
                const cProperties3 = useRecoilValue(
                    componentPropertiesWithProvidedData({ component: 'c', indexes: [3] }),
                )
                const eProperties11 = useRecoilValue(
                    componentPropertiesWithProvidedData({ component: 'e', indexes: [0, 0] }),
                )
                const eProperties21 = useRecoilValue(
                    componentPropertiesWithProvidedData({ component: 'e', indexes: [1, 0] }),
                )
                const eProperties31 = useRecoilValue(
                    componentPropertiesWithProvidedData({ component: 'e', indexes: [2, 0] }),
                )
                const eProperties41 = useRecoilValue(
                    componentPropertiesWithProvidedData({ component: 'e', indexes: [3, 0] }),
                )
                const eProperties12 = useRecoilValue(
                    componentPropertiesWithProvidedData({ component: 'e', indexes: [0, 1] }),
                )
                const eProperties22 = useRecoilValue(
                    componentPropertiesWithProvidedData({ component: 'e', indexes: [1, 1] }),
                )
                const eProperties32 = useRecoilValue(
                    componentPropertiesWithProvidedData({ component: 'e', indexes: [2, 1] }),
                )
                const eProperties42 = useRecoilValue(
                    componentPropertiesWithProvidedData({ component: 'e', indexes: [3, 1] }),
                )
                useEffect(() => {
                    addDeclaration(div)
                    addDeclaration(List)
                    addComponent({
                        id: div.id,
                        componentId: 'a',
                        position: Position.inside,
                    })
                    setAProvided({
                        data: {
                            test: [
                                { name: 'hi', values: [{ name: 'a' }, { name: 'aa' }] },
                                { name: 'how', values: [{ name: 'b' }, { name: 'bb' }] },
                                { name: 'are', values: [{ name: 'c' }, { name: 'cc' }] },
                                { name: 'you', values: [{ name: 'd' }, { name: 'dd' }] },
                            ],
                            b: 'b',
                        },
                    })
                    addComponent({
                        id: List.id,
                        componentId: 'b',
                        position: Position.inside,
                        reference: 'a',
                        properties: {
                            dataKey: 'a.data.test',
                        },
                    })
                    addComponent({
                        id: div.id,
                        componentId: 'c',
                        position: Position.inside,
                        reference: 'b',
                        properties: {
                            test: '{{a.data.test.name}}',
                            b: '{{a.data.b}}',
                        },
                    })
                    addComponent({
                        id: List.id,
                        componentId: 'd',
                        position: Position.inside,
                        reference: 'c',
                        properties: {
                            dataKey: 'a.data.test.values',
                        },
                    })
                    addComponent({
                        id: div.id,
                        componentId: 'e',
                        position: Position.inside,
                        reference: 'd',
                        properties: {
                            b: '{{a.data.test.values.name}}',
                            a: '{{a.data.test.name}}',
                            c: '{{a.data.b}}',
                            d: '{{a.data.b}} {{a.data.test.values.name}} {{a.data.test.name}}',
                        },
                    })
                }, [])
                return {
                    cProperties0,
                    cProperties1,
                    cProperties2,
                    cProperties3,
                    eProperties11,
                    eProperties21,
                    eProperties31,
                    eProperties41,
                    eProperties12,
                    eProperties22,
                    eProperties32,
                    eProperties42,
                }
            },
            { wrapper },
        )

        expect(result.current.cProperties0).toEqual({ test: 'hi', b: 'b' })
        expect(result.current.cProperties1).toEqual({ test: 'how', b: 'b' })
        expect(result.current.cProperties2).toEqual({ test: 'are', b: 'b' })
        expect(result.current.cProperties3).toEqual({ test: 'you', b: 'b' })
        expect(result.current.eProperties11).toEqual({ a: 'hi', b: 'a', c: 'b', d: 'b a hi' })
        expect(result.current.eProperties21).toEqual({ a: 'how', b: 'b', c: 'b', d: 'b b how' })
        expect(result.current.eProperties31).toEqual({ a: 'are', b: 'c', c: 'b', d: 'b c are' })
        expect(result.current.eProperties41).toEqual({ a: 'you', b: 'd', c: 'b', d: 'b d you' })
        expect(result.current.eProperties12).toEqual({ a: 'hi', b: 'aa', c: 'b', d: 'b aa hi' })
        expect(result.current.eProperties22).toEqual({ a: 'how', b: 'bb', c: 'b', d: 'b bb how' })
        expect(result.current.eProperties32).toEqual({ a: 'are', b: 'cc', c: 'b', d: 'b cc are' })
        expect(result.current.eProperties42).toEqual({ a: 'you', b: 'dd', c: 'b', d: 'b dd you' })
    })

    it('uses provided data in deeply nested properties', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addComponent = useRecoilAction(add)
                const addDeclaration = useAddComponentDeclaration()
                const setAProvided = useSetRecoilState(componentData({ component: 'a' }))
                const bProperties = useRecoilValue(componentPropertiesWithProvidedData('b'))
                useEffect(() => {
                    addDeclaration(div)
                    addComponent({
                        id: div.id,
                        componentId: 'a',
                        position: Position.inside,
                    })
                    setAProvided({ data: { test: 'hello' } })
                    addComponent({
                        id: div.id,
                        componentId: 'b',
                        position: Position.inside,
                        reference: 'a',
                        properties: {
                            a: [
                                {
                                    deeply: {
                                        nested: [{ property: '{{a.data.test}}', nulled: null, undefined: undefined }],
                                    },
                                },
                            ],
                            b: 'b',
                        },
                    })
                }, [])
                return { bProperties }
            },
            { wrapper },
        )

        expect(result.current.bProperties).toEqual({
            a: [{ deeply: { nested: [{ property: 'hello', nulled: null, undefined: undefined }] } }],
            b: 'b',
        })
    })
})
