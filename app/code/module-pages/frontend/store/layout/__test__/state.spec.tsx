/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { RecoilRoot, useRecoilValue, useSetRecoilState } from 'recoil'
import { renderHook } from '@testing-library/react-hooks'
import React, { useEffect } from 'react'
import { componentChildrenIds, componentIsDisabled, disabledComponentsIds } from '../store'
import { useAddComponent, useMoveComp } from '../actions'
import { useAddComponentDeclaration } from '../../componentDeclarations/actions'
import div from '../../../components/Definitions/Div'
import Position from '../../../types/Position'

/**
 * Tests about component state, and their effects (disabled components)
 */

const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
describe('disabled components', () => {
    it('cannot move a disabled component', () => {
        const { result } = renderHook(
            () => {
                const setIsDisabled = useSetRecoilState(componentIsDisabled('a'))
                const addComp = useAddComponent()
                const addDecl = useAddComponentDeclaration()
                const move = useMoveComp()
                const children = useRecoilValue(componentChildrenIds('root'))
                useEffect(() => {
                    addDecl(div)
                    addComp({ id: div.id, componentId: 'root' })
                    addComp({ id: div.id, componentId: 'a', reference: 'root' })
                    addComp({ id: div.id, componentId: 'b', reference: 'root' })
                    setIsDisabled(true)
                    move({ component: 'a', reference: 'b', position: Position.inside })
                }, [])
                return { children }
            },
            { wrapper },
        )

        expect(result.current.children).toEqual(['a', 'b'])
    })
})
