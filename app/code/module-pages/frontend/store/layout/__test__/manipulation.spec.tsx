/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { renderHook } from '@testing-library/react-hooks'
import { useAddComponentDeclaration } from '../../componentDeclarations/actions'
import { add, move, useAddComponent, useMoveComp } from '../actions'
import { RecoilRoot, useRecoilValue, useSetRecoilState } from 'recoil'
import {
    componentChildrenIds,
    componentDeclarationId,
    componentsIds,
    currentRootComponentId,
    parentId,
    parentsIds,
} from '../store'
import React, { useEffect } from 'react'
import div from '../../../components/Definitions/Div'
import Position from '../../../types/Position'
import { useRecoilAction } from '../../recoilAction'
import { addSymbolDefinition, useAddSymbolDefinition } from '../../pagebuilder/symbols/actions'
import { componentDroppableChildren, componentSymbolId, symbolsIds } from '../../pagebuilder/symbols/store'
import { SymbolDefinition, SymbolLayout } from '../../../types/SymbolDefinition'
import text from '../../../components/Definitions/Text'

/**
 * Tests about page manipulation (add, move, delete, copy....)
 */
const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
describe('page manipulation', () => {
    it('adding a component adds it to the id list', () => {
        const { result } = renderHook(
            () => {
                const addComp = useAddComponent()
                const addDecl = useAddComponentDeclaration()
                const ids = useRecoilValue(componentsIds)
                useEffect(() => {
                    addDecl(div)
                    addComp({ id: div.id, componentId: 'a', position: Position.inside })
                }, [])
                return { ids }
            },
            { wrapper },
        )
        expect(result.current.ids).toEqual(['a'])
    })

    it('add component inside other component', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const a = useRecoilAction(add)
                const children = useRecoilValue(componentChildrenIds('a'))
                useEffect(() => {
                    addDecl(div)
                    a({ id: div.id, componentId: 'a', position: Position.inside })
                    a({ id: div.id, componentId: 'b', reference: 'a', position: Position.inside })
                }, [])
                return { children }
            },
            { wrapper },
        )

        expect(result.current.children).toEqual(['b'])
    })

    it('add component after and before other component', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const a = useRecoilAction(add)
                const children = useRecoilValue(componentChildrenIds('root'))
                useEffect(() => {
                    addDecl(div)
                    a({ id: div.id, componentId: 'root', position: Position.inside })
                    a({ id: div.id, componentId: 'a', position: Position.inside, reference: 'root' })
                    a({ id: div.id, componentId: 'b', reference: 'a', position: Position.before })
                    a({ id: div.id, componentId: 'c', reference: 'a', position: Position.after })
                }, [])
                return { children }
            },
            { wrapper },
        )

        expect(result.current.children).toEqual(['b', 'a', 'c'])
    })

    it('add a symbol inside, after and before another component', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const addDef = useRecoilAction(addSymbolDefinition)
                const a = useRecoilAction(add)
                const children = useRecoilValue(componentChildrenIds('a'))
                const bSymbol = useRecoilValue(componentSymbolId('b'))
                const cSymbol = useRecoilValue(componentSymbolId('c'))
                const dSymbol = useRecoilValue(componentSymbolId('d'))
                useEffect(() => {
                    addDecl(div)
                    addDef({
                        id: 'test',
                        layout: {
                            componentDeclarationId: div.id,
                            alias: 'top',
                            properties: { data: 'eh' },
                            children: [],
                        },
                        name: 'yo',
                    })
                    a({ id: div.id, componentId: 'a', position: Position.inside })
                    a({ id: 'test', componentId: 'b', reference: 'a', position: Position.inside })
                    a({ id: 'test', componentId: 'c', reference: 'b', position: Position.after })
                    a({ id: 'test', componentId: 'd', reference: 'b', position: Position.before })
                }, [])
                return { children, bSymbol, cSymbol, dSymbol }
            },
            { wrapper },
        )

        expect(result.current.children).toEqual(['d', 'b', 'c'])
        expect(result.current.bSymbol).toBeTruthy()
        expect(result.current.cSymbol).toBeTruthy()
        expect(result.current.dSymbol).toBeTruthy()
    })

    it("add a component inside a symbol last child, even if the reference isn't a last child and the position isn't inside", () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const addDef = useRecoilAction(addSymbolDefinition)
                const a = useRecoilAction(add)
                const list = useRecoilValue(componentChildrenIds('root'))
                const droppable = useRecoilValue(componentDroppableChildren('a'))
                const aDecl = useRecoilValue(componentDeclarationId('a'))
                const aCDecl = useRecoilValue(componentDeclarationId(droppable[0] || ''))
                const parent = useRecoilValue(parentId('b'))
                const sIds = useRecoilValue(symbolsIds)
                useEffect(() => {
                    addDecl(div)
                    addDef({
                        id: 'test',
                        layout: {
                            componentDeclarationId: div.id,
                            alias: 'top',
                            properties: {},
                            children: [
                                {
                                    componentDeclarationId: div.id,
                                    alias: 'child',
                                    properties: {},
                                    children: [],
                                },
                            ],
                        },
                        name: 'yo',
                    })
                    a({ id: div.id, componentId: 'root', position: Position.inside })
                    a({ id: 'test', componentId: 'a', position: Position.inside, reference: 'root' })
                    a({ id: div.id, componentId: 'b', reference: 'a', position: Position.inside })
                }, [])
                return { sIds, list, droppable, parent, aDecl, aCDecl }
            },
            { wrapper },
        )

        expect(result.current.sIds.length).toEqual(1)
        expect(result.current.list).toEqual(['a'])
        expect(result.current.parent).toBeTruthy()
        expect(result.current.parent).not.toBe('a')
        expect(result.current.aDecl).toEqual(div.id)
        expect(result.current.aCDecl).toEqual(div.id)
    })

    it('add a component before and after a symbol parent component', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const addDef = useRecoilAction(addSymbolDefinition)
                const a = useRecoilAction(add)
                const list = useRecoilValue(componentChildrenIds('root'))
                const droppable = useRecoilValue(componentDroppableChildren('a'))
                const aDecl = useRecoilValue(componentDeclarationId('a'))
                const aCDecl = useRecoilValue(componentDeclarationId(droppable[0] || ''))
                useEffect(() => {
                    addDecl(div)
                    addDef({
                        id: 'test',
                        layout: {
                            componentDeclarationId: div.id,
                            alias: 'top',
                            properties: {},
                            children: [
                                {
                                    componentDeclarationId: div.id,
                                    alias: 'child',
                                    properties: {},
                                    children: [],
                                },
                            ],
                        },
                        name: 'yo',
                    })
                    a({ id: div.id, componentId: 'root', position: Position.before })
                    a({ id: 'test', componentId: 'a', position: Position.inside, reference: 'root' })
                    a({ id: div.id, componentId: 'b', reference: 'a', position: Position.before })
                    a({ id: div.id, componentId: 'c', reference: 'a', position: Position.after })
                }, [])
                return { list, droppable, aDecl, aCDecl }
            },
            { wrapper },
        )

        expect(result.current.list).toEqual(['b', 'a', 'c'])
        expect(result.current.aDecl).toEqual(div.id)
        expect(result.current.aCDecl).toEqual(div.id)
    })

    it('add a component inside first droppable child when adding component on symbol parent component', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const addDef = useRecoilAction(addSymbolDefinition)
                const a = useRecoilAction(add)
                const list = useRecoilValue(componentChildrenIds('root'))
                const droppable = useRecoilValue(componentDroppableChildren('a'))
                const aDecl = useRecoilValue(componentDeclarationId('a'))
                const acId = droppable[0]
                const acChildren = useRecoilValue(componentChildrenIds(acId || ''))
                useEffect(() => {
                    addDecl(div)
                    addDef({
                        id: 'test',
                        layout: {
                            componentDeclarationId: div.id,
                            alias: 'top',
                            properties: {},
                            children: [
                                {
                                    componentDeclarationId: div.id,
                                    alias: 'child',
                                    properties: {},
                                    children: [],
                                },
                            ],
                        },
                        name: 'yo',
                    })
                    a({ id: div.id, componentId: 'root', position: Position.inside })
                    a({ id: 'test', componentId: 'a', position: Position.inside, reference: 'root' })
                    a({ id: div.id, componentId: 'b', reference: 'a', position: Position.inside })
                }, [])
                return { list, droppable, aDecl, acId, acChildren }
            },
            { wrapper },
        )

        expect(result.current.list).toEqual(['a'])
        expect(result.current.aDecl).toEqual(div.id)
        expect(result.current.acChildren).toEqual(['b'])
    })

    it('add a component after or before a component inside a symbol', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const addDef = useRecoilAction(addSymbolDefinition)
                const a = useRecoilAction(add)
                const list = useRecoilValue(componentChildrenIds('root'))
                const parent = useRecoilValue(parentId('b'))
                const children = useRecoilValue(componentChildrenIds(parent || ''))
                useEffect(() => {
                    addDecl(div)
                    addDef({
                        id: 'test',
                        layout: {
                            componentDeclarationId: div.id,
                            alias: 'top',
                            properties: {},
                            children: [
                                {
                                    componentDeclarationId: div.id,
                                    alias: 'child',
                                    properties: {},
                                    children: [],
                                },
                            ],
                        },
                        name: 'yo',
                    })
                    a({ id: div.id, componentId: 'root', position: Position.inside })
                    a({ id: 'test', componentId: 'a', position: Position.inside, reference: 'root' })
                    a({ id: div.id, componentId: 'b', reference: 'a', position: Position.inside })
                    a({ id: div.id, componentId: 'c', reference: 'b', position: Position.after })
                    a({ id: div.id, componentId: 'd', reference: 'b', position: Position.before })
                    a({ id: div.id, componentId: 'e', reference: 'c', position: Position.before })
                    a({ id: div.id, componentId: 'f', reference: 'e', position: Position.after })
                }, [])
                return { list, children }
            },
            { wrapper },
        )

        expect(result.current.list).toEqual(['a'])
        expect(result.current.children).toEqual(['d', 'b', 'e', 'f', 'c'])
    })

    it('move a component after and before another component on the page', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const a = useRecoilAction(add)
                const m = useRecoilAction(move)
                const list = useRecoilValue(componentChildrenIds('root'))
                const children = useRecoilValue(componentChildrenIds('a'))
                useEffect(() => {
                    addDecl(div)
                    a({ id: div.id, componentId: 'root', position: Position.inside })
                    a({ id: div.id, componentId: 'a', position: Position.inside, reference: 'root' })
                    a({ id: div.id, componentId: 'b', reference: 'a', position: Position.inside })
                    a({ id: div.id, componentId: 'c', reference: 'b', position: Position.after })
                    a({ id: div.id, componentId: 'd', reference: 'b', position: Position.before })
                    m({ component: 'd', position: Position.before, reference: 'a' })
                    m({ component: 'b', position: Position.after, reference: 'a' })
                }, [])
                return { list, children }
            },
            { wrapper },
        )

        expect(result.current.list).toEqual(['d', 'a', 'b'])
        expect(result.current.children).toEqual(['c'])
    })

    it('move a component after, before, inside another component', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const a = useRecoilAction(add)
                const m = useRecoilAction(move)
                const list = useRecoilValue(componentChildrenIds('root'))
                const children = useRecoilValue(componentChildrenIds('a'))
                useEffect(() => {
                    addDecl(div)
                    a({ id: div.id, componentId: 'root', position: Position.inside })
                    a({ id: div.id, componentId: 'a', position: Position.inside, reference: 'root' })
                    a({ id: div.id, componentId: 'b', reference: 'a', position: Position.inside })
                    a({ id: div.id, componentId: 'c', reference: 'a', position: Position.after })
                    a({ id: div.id, componentId: 'd', reference: 'a', position: Position.before })
                    m({ component: 'd', position: Position.before, reference: 'b' })
                }, [])
                return { list, children }
            },
            { wrapper },
        )

        expect(result.current.list).toEqual(['a', 'c'])
        expect(result.current.children).toEqual(['d', 'b'])
    })

    it("move a component inside a symbol last child, even if position !== inside and reference isn't one of last children", () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const addDef = useRecoilAction(addSymbolDefinition)
                const a = useRecoilAction(add)
                const m = useRecoilAction(move)
                const list = useRecoilValue(componentChildrenIds('root'))
                const droppable = useRecoilValue(componentDroppableChildren('a'))
                const parent = useRecoilValue(parentId('b'))
                useEffect(() => {
                    addDecl(div)
                    addDef({
                        id: 'test',
                        layout: {
                            componentDeclarationId: div.id,
                            alias: 'top',
                            properties: {},
                            children: [
                                {
                                    componentDeclarationId: div.id,
                                    alias: 'child',
                                    properties: {},
                                    children: [],
                                },
                            ],
                        },
                        name: 'yo',
                    })
                    a({ id: div.id, componentId: 'root', position: Position.inside })
                    a({ id: 'test', componentId: 'a', position: Position.inside, reference: 'root' })
                    a({ id: div.id, componentId: 'b', position: Position.inside, reference: 'root' })
                    m({ component: 'b', reference: 'a', position: Position.inside })
                }, [])
                return { list, droppable, parent }
            },
            { wrapper },
        )

        expect(result.current.list).toEqual(['a'])
        expect(result.current.parent).toBeTruthy()
        expect(result.current.parent).not.toBe('a')
    })

    it('moving a component updates his parent', () => {
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const addComp = useAddComponent()
                const move = useMoveComp()
                const parent = useRecoilValue(parentId('d'))
                useEffect(() => {
                    addDecl(div)
                    addComp({ id: div.id, componentId: 'a' })
                    addComp({ id: div.id, componentId: 'b', reference: 'a' })
                    addComp({ id: div.id, componentId: 'c', reference: 'a' })
                    addComp({ id: div.id, componentId: 'd', reference: 'a' })
                    move({ component: 'd', reference: 'c', position: Position.inside })
                }, [])

                return { parent }
            },
            { wrapper },
        )
        expect(result.current.parent).toEqual('c')
    })

    it('regression: moving a component after or before another, remove it from his last position on list', () => {
        const { result } = renderHook(
            () => {
                const addComp = useAddComponent()
                const moveComp = useMoveComp()
                const addDecl = useAddComponentDeclaration()
                const setRoot = useSetRecoilState(currentRootComponentId)
                const list = useRecoilValue(componentChildrenIds('root'))
                useEffect(() => {
                    addDecl(div)
                    addComp({ id: div.id, componentId: 'root', position: Position.inside })
                    setRoot('a')
                    addComp({ id: div.id, componentId: 'b', position: Position.inside, reference: 'root' })
                    addComp({ id: div.id, componentId: 'c', position: Position.inside, reference: 'root' })
                    moveComp({ component: 'c', reference: 'b', position: Position.before })
                }, [])
                return { list }
            },
            { wrapper },
        )

        expect(result.current.list).toEqual(['c', 'b'])
    })

    it('add a symbol with multiple children', () => {
        const layout: SymbolLayout = {
            componentDeclarationId: div.id,
            properties: {},
            alias: 'div',
            children: [
                {
                    componentDeclarationId: text.id,
                    properties: {},
                    children: [],
                    alias: 'text',
                },
                {
                    componentDeclarationId: div.id,
                    properties: {},
                    children: [],
                    alias: 'div',
                },
            ],
        }
        const s: SymbolDefinition = {
            id: 's',
            name: 's',
            layout,
        }
        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const addSymb = useAddSymbolDefinition()
                const addComp = useAddComponent()
                const children = useRecoilValue(componentChildrenIds('s'))
                useEffect(() => {
                    addDecl(div)
                    addDecl(text)
                    addSymb(s)
                    addComp({ id: s.id, componentId: 's' })
                }, [])
                return { children }
            },
            { wrapper },
        )
        const { children } = result.current

        expect(children.length).toBe(2)
    })

    it('add symbol inside another symbol first droppable child when trying to add it to the parent', () => {
        const layout = {
            componentDeclarationId: div.id,
            properties: {},
            alias: 'div',
            children: [
                {
                    componentDeclarationId: text.id,
                    properties: {},
                    children: [],
                    alias: 'text',
                },
                {
                    componentDeclarationId: div.id,
                    properties: {},
                    children: [],
                    alias: 'div',
                },
            ],
        }
        const s1: SymbolDefinition = {
            id: 's1',
            name: 's1',
            layout,
        }
        const s2: SymbolDefinition = {
            id: 's2',
            name: 's2',
            layout,
        }
        const { result } = renderHook(
            () => {
                const addSymb = useAddSymbolDefinition()
                const addComp = useAddComponent()
                const addDecl = useAddComponentDeclaration()
                const s1Children = useRecoilValue(componentChildrenIds('s1'))
                const s1Child1Id = s1Children[0]
                const s1Child2Id = s1Children[1]
                const s2Children = useRecoilValue(componentChildrenIds('s2'))
                const s2ChildId = s2Children[0]
                const s2Parents = useRecoilValue(parentsIds('s2'))
                useEffect(() => {
                    addDecl(div)
                    addDecl(text)
                    addSymb(s1)
                    addSymb(s2)
                    addComp({ id: s1.id, componentId: 's1' })
                    addComp({ id: s2.id, reference: 's1', position: Position.inside, componentId: 's2' })
                }, [])
                return { s1Child1Id, s1Child2Id, s2ChildId, s2Parents }
            },
            { wrapper },
        )
        const { s1Child1Id, s1Child2Id, s2Parents, s2ChildId } = result.current

        expect(s2Parents).toEqual(['s1', s1Child2Id])
    })

    it('adding a component before/after a symbol droppable child will add it inside instead', () => {
        const symbol: SymbolDefinition = {
            id: 's',
            name: 's',
            layout: {
                componentDeclarationId: div.id,
                properties: {},
                alias: '',
                children: [
                    {
                        componentDeclarationId: div.id,
                        properties: {},
                        alias: '',
                        children: [],
                    },
                ],
            },
        }

        const { result } = renderHook(
            () => {
                const addDecl = useAddComponentDeclaration()
                const addSymbol = useAddSymbolDefinition()
                const addComp = useAddComponent()
                const symbolChild = useRecoilValue(componentChildrenIds('s'))[0]
                const parent = useRecoilValue(parentId('b'))
                useEffect(() => {
                    addDecl(div)
                    addSymbol(symbol)
                    addComp({ id: symbol.id, componentId: 's' })
                }, [])
                useEffect(() => {
                    if (symbolChild) {
                        addComp({ id: div.id, componentId: 'b', position: Position.before, reference: symbolChild })
                    }
                }, [symbolChild])
                return { parent, symbolChild }
            },
            { wrapper },
        )

        expect(result.current.parent).toEqual(result.current.symbolChild)
    })

    it('add a component inside a droppable child when trying to add it before/after', () => {
        const symbol: SymbolDefinition = {
            id: 's',
            name: 's',
            layout: {
                componentDeclarationId: div.id,
                properties: {},
                alias: '',
                children: [
                    {
                        componentDeclarationId: div.id,
                        properties: {},
                        alias: '',
                        children: [],
                    },
                    {
                        componentDeclarationId: div.id,
                        properties: {},
                        alias: '',
                        children: [],
                    },
                ],
            },
        }
        const { result } = renderHook(
            () => {
                const addSymb = useAddSymbolDefinition()
                const addComp = useAddComponent()
                const addDecl = useAddComponentDeclaration()
                const aChildren = useRecoilValue(componentChildrenIds('a'))
                const bParent = useRecoilValue(parentId('b'))
                const cParent = useRecoilValue(parentId('c'))
                useEffect(() => {
                    addSymb(symbol)
                    addDecl(div)
                    addComp({ id: symbol.id, componentId: 'a' })
                }, [])
                useEffect(() => {
                    if (aChildren.length === 2) {
                        addComp({ id: div.id, componentId: 'b', reference: aChildren[1], position: Position.before })
                        addComp({ id: div.id, componentId: 'c', reference: aChildren[1], position: Position.after })
                    }
                }, [aChildren])

                return { bParent, cParent, aChildren }
            },
            { wrapper },
        )

        const { bParent, aChildren, cParent } = result.current

        expect(bParent).toEqual(aChildren[1])
        expect(cParent).toEqual(aChildren[1])
    })
})
