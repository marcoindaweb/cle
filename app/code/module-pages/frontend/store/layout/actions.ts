/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { action, useRecoilAction } from '../recoilAction'
import Position from '../../types/Position'
import { DefaultValue } from 'recoil'
import { componentDeclarationById } from '../componentDeclarations/store'
import { v4 } from 'uuid'
import {
    componentChildrenIds,
    componentDeclarationId,
    componentIsDisabled,
    componentProperties,
    componentsIds,
    currentRootComponentId,
    parentId,
} from './store'
import {
    componentDroppableChild,
    componentDroppableChildren,
    componentSymbolId,
    symbolAlias,
    symbolComponents,
    symbolDefinition,
    symbolDefinitionId,
    symbolParentComponentId,
    symbolsIds,
} from '../pagebuilder/symbols/store'
import { DeclarationNotFoundError, SymbolDefinitionNotFound } from './utils/errors'
import ComponentDeclaration from '../../types/ComponentDeclaration'
import { convertSymbolLayoutPropertiesToComponentProperties, IdTree } from './utils/manipulation'
import { SymbolDefinition, SymbolLayout } from '../../types/SymbolDefinition'
import { useCallback } from 'react'
import { extractIdsFromIdTree, generateIdTreeFromSymbolLayout } from '../pagebuilder/symbols/utils/idTree'
import { componentAlias } from '../pagebuilder/aliases/store'

export const move = action<{ component: string; reference?: string; position?: Position }>({
    key: 'pages/layout/move',
    execute: ({ get, set, reset }, val) => {
        if (val instanceof DefaultValue) return
        const isDisabled = get(componentIsDisabled(val.component))
        if (isDisabled) return
        const rootId = get(currentRootComponentId)
        const position = val.reference ? val.position : Position.inside
        const reference = val.reference || rootId
        if (!reference) return
        let componentNewParent = position === Position.inside ? reference : get(parentId(reference))
        if (!componentNewParent)
            throw new Error(
                "Cannot get component new parent, probably you're trying to move a component after o before another component without any parent",
            )
        // Makes sure it's trying to drop onto a droppable children
        const componentNewParentSymbol = get(componentSymbolId(componentNewParent))
        if (componentNewParentSymbol) {
            const droppable = get(componentDroppableChildren(componentNewParent))
            if (!droppable.includes(componentNewParent)) {
                componentNewParent = get(componentDroppableChild(componentNewParent))
                if (!componentNewParent) throw new Error('Cannot find a component to drop into')
            }
        }
        // If trying to move a symbol make sure that the symbol parent component is the one being moved
        const componentSymbol = get(componentSymbolId(val.component))
        const component = componentSymbol ? get(symbolParentComponentId(componentSymbol)) : val.component
        if (!component) throw new Error('Cannot find symbol parent component for move')
        const currentParentComponent = get(parentId(component))
        set(componentChildrenIds(componentNewParent), (val) => {
            let newVal = [...val]
            if (currentParentComponent === componentNewParent) {
                newVal = newVal.filter((v) => v !== component)
            }
            switch (position) {
                case Position.after:
                    newVal.splice(newVal.indexOf(reference) + 1, 0, component)
                    break
                case Position.before:
                    newVal.splice(newVal.indexOf(reference), 0, component)
                    break
                case Position.inside:
                    newVal = [...newVal, component]
                    break
            }
            return newVal
        })
        if (currentParentComponent && currentParentComponent !== componentNewParent) {
            set(componentChildrenIds(currentParentComponent), (val) => val.filter((v) => v !== component))
        }
        reset(parentId(component))
    },
})

export const add = action<{
    id: string
    reference?: string
    position?: Position
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    properties?: Record<string, any>
    componentId?: string
}>({
    key: 'pages/layout/add',
    execute: ({ set, get }, params) => {
        if (params instanceof DefaultValue) return

        const { id, properties, componentId } = params
        const rootId = get(currentRootComponentId)
        let reference: string | undefined = params.reference
        let position = params.position

        if (!reference) {
            reference = rootId
            position = Position.inside
        }

        if (reference) {
            // Makes sure is added onto a droppable child if trying to add onto a symbol
            const symbol = get(componentSymbolId(reference))
            if (symbol) {
                const symbolParent = get(symbolParentComponentId(symbol))
                const droppable = get(componentDroppableChildren(reference))
                if (
                    (position !== Position.inside && reference !== symbolParent) ||
                    (!droppable.includes(reference) && reference !== symbolParent)
                ) {
                    position = Position.inside
                    if (!droppable.includes(reference) && reference !== symbolParent) {
                        reference = droppable[0]
                    }
                }
            }
        }
        position = position === undefined ? Position.inside : position
        set(addTo, {
            id,
            reference,
            properties,
            position,
            componentId,
        })
    },
})

/**
 * Add component by symbol / declaration to an existing component
 */
const addTo = action<{
    id: string
    reference?: string
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    properties?: Record<string, any>
    position: Position
    componentId?: string
}>({
    key: 'pages/layout/addTo',
    execute: ({ set, get }, newValue) => {
        if (newValue instanceof DefaultValue) return
        const { id } = newValue
        const decl = get(componentDeclarationById(id))
        decl ? set(addDeclarationTo, newValue) : set(addSymbolTo, newValue)
    },
})

const addDeclarationTo = action<{
    id: string
    reference?: string
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    properties?: Record<string, any>
    position: Position
    componentId?: string
}>({
    key: 'pages/layout/addDeclarationTo',
    execute: ({ get, set }, newValue) => {
        if (newValue instanceof DefaultValue) return
        const declaration = get(componentDeclarationById(newValue.id))
        if (!declaration) {
            throw new DeclarationNotFoundError(newValue.id)
        }
        const componentId = newValue.componentId || v4()
        set(createComponent, { declaration, componentId, properties: newValue.properties })
        if (newValue.reference) {
            set(addOnPosition, { ...newValue, component: componentId, reference: newValue.reference })
        }
    },
})

const addSymbolTo = action<{
    id: string
    reference?: string
    position: Position
    componentId?: string
}>({
    key: 'pages/layout/addSymbolTo',
    execute: ({ set }, newValue) => {
        if (newValue instanceof DefaultValue) return
        const componentId = newValue.componentId || v4()
        set(instantiateSymbolDefinition, { parentComponentId: componentId, id: newValue.id })
        if (newValue.reference) {
            set(addOnPosition, { ...newValue, component: componentId, reference: newValue.reference })
        }
    },
})

const addOnPosition = action<{ position: Position; reference: string; component: string }>({
    key: 'pages/layout/addOnPosition',
    execute: ({ set, get }, newValue) => {
        if (newValue instanceof DefaultValue) return
        const reference = newValue.reference
        const symbol = get(componentSymbolId(reference))
        const symbolParent = get(symbolParentComponentId(symbol || ''))
        if (
            symbol &&
            ((symbolParent !== reference && !get(componentDroppableChildren(reference)).includes(reference)) ||
                (symbolParent === reference && newValue.position === Position.inside))
        ) {
            set(addOnSymbol, { symbol, ...newValue })
            return
        }
        switch (newValue.position) {
            case Position.inside:
                set(addInside, newValue)
                break
            case Position.before:
                set(addBefore, newValue)
                break
            case Position.after:
                set(addAfter, newValue)
                break
        }
    },
})

const addOnSymbol = action<{ symbol: string; component: string; reference: string }>({
    key: 'pages/layout/addOnSymbol',
    execute: ({ get, set }, val) => {
        if (val instanceof DefaultValue) return
        const droppable = get(componentDroppableChildren(val.reference))
        const actualReference = droppable.includes(val.reference)
            ? val.reference
            : droppable.length > 0
            ? droppable[0]
            : null
        if (!actualReference) throw new Error("The component don't accept any children")
        set(addInside, { component: val.component, reference: actualReference })
    },
})

const addInside = action<{ reference: string; component: string }>({
    key: 'pages/layout/addInside',
    execute: ({ set }, newValue) => {
        if (newValue instanceof DefaultValue) return
        set(componentChildrenIds(newValue.reference), (val) => [...val, newValue.component])
    },
})
const addBefore = action<{ reference: string; component: string }>({
    key: 'pages/layout/addBefore',
    execute: ({ set, get }, newValue) => {
        if (newValue instanceof DefaultValue) return
        const { reference, component } = newValue
        const parent = get(parentId(reference))
        if (!parent) {
            return
        }
        set(componentChildrenIds(parent), (val) => {
            const v = [...val]
            v.splice(v.indexOf(reference), 0, component)
            return v
        })
    },
})
const addAfter = action<{ reference: string; component: string }>({
    key: 'pages/layout/addAfter',
    execute: ({ set, get }, newValue) => {
        if (newValue instanceof DefaultValue) return
        const { component, reference } = newValue
        const parent = get(parentId(reference))
        if (!parent) {
            return
        }
        set(componentChildrenIds(parent), (val) => {
            const v = [...val]
            v.splice(v.indexOf(reference) + 1, 0, component)
            return v
        })
    },
})

export const createComponent = action<{
    componentId?: string
    declaration: ComponentDeclaration
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    properties?: Record<string, any>
    dontAddToList?: boolean
}>({
    key: 'pages/layout/setNewComponent',
    execute: ({ set, get }, val) => {
        if (val instanceof DefaultValue) return
        const { declaration, properties } = val
        const componentId = val.componentId || v4()
        set(componentDeclarationId(componentId), declaration.id)
        const ids = get(componentsIds)
        if (!val.dontAddToList) {
            set(componentsIds, [...ids, componentId])
        }
        if (declaration.defaultProperties || properties) {
            set(componentProperties(componentId), properties || declaration.defaultProperties)
        }
    },
})

const instantiateSymbolDefinition = action<{ id: string; parentComponentId: string }>({
    key: 'pages/layout/instanciateSymbolDefinition',
    execute: ({ get, set }, val) => {
        if (val instanceof DefaultValue) return
        const { id, parentComponentId } = val
        const definition = get(symbolDefinition(id))
        if (!definition) {
            throw new SymbolDefinitionNotFound(id)
        }
        const symbolInstanceId = v4()
        set(symbolDefinitionId(symbolInstanceId), id)
        set(symbolAlias(symbolInstanceId), definition.name)
        const decl = get(componentDeclarationById(definition.layout.componentDeclarationId))
        if (!decl) {
            throw new DeclarationNotFoundError(definition.layout.componentDeclarationId)
        }
        const idTree = generateIdTreeFromSymbolLayout(definition.layout, parentComponentId)

        set(createComponent, {
            declaration: decl,
            componentId: parentComponentId,
            properties: convertSymbolLayoutPropertiesToComponentProperties(
                definition.layout.properties,
                parentComponentId,
                idTree,
                decl.id,
            ),
        })
        set(componentAlias(parentComponentId), definition.layout.alias)
        set(symbolsIds, [...get(symbolsIds), symbolInstanceId])

        const setChildren = (v: SymbolLayout[], parentId: string, idTree: IdTree, fullTree: IdTree) => {
            v.forEach((l, i) => {
                const cIdTree = idTree[1][i]
                const cId = cIdTree[0]
                const decl = get(componentDeclarationById(l.componentDeclarationId))
                if (!decl) {
                    throw new DeclarationNotFoundError(l.componentDeclarationId)
                }
                set(createComponent, {
                    declaration: decl,
                    componentId: cId,
                    properties: convertSymbolLayoutPropertiesToComponentProperties(
                        l.properties,
                        cId,
                        fullTree,
                        decl.id,
                    ),
                })
                set(componentAlias(cId), l.alias)
                setChildren(l.children, cId, cIdTree, fullTree)
            })
            set(
                componentChildrenIds(parentId),
                idTree[1].map((v) => v[0]),
            )
        }
        setChildren(definition.layout.children, parentComponentId, idTree, idTree)
        const ids = extractIdsFromIdTree(idTree)
        set(symbolComponents(symbolInstanceId), ids)
        set(componentsIds, (v) => [...v, ...ids])
    },
})

export const useAddComponent = () => {
    return useRecoilAction(add)
}

export const useAddComponentOrSymbol = () => {
    const a = useRecoilAction(add)

    return useCallback(
        (
            compDeclarationIdOrSymbol: string | SymbolDefinition,
            options?: string | { element?: string; properties?: Record<string, any>; position?: Position },
        ): void => {
            const id =
                typeof compDeclarationIdOrSymbol === 'string' || typeof compDeclarationIdOrSymbol === 'number'
                    ? compDeclarationIdOrSymbol
                    : compDeclarationIdOrSymbol.id
            if (typeof options === 'object') {
                a({
                    id,
                    reference: options.element,
                    position: options.position || Position.inside,
                    properties: options.properties,
                })
            } else {
                a({ id, reference: options, position: Position.inside })
            }
        },
        [a],
    )
}

export const useMoveComp = () => {
    return useRecoilAction(move)
}

export const useMoveComponent = () => {
    const m = useRecoilAction(move)
    console.warn('useMoveComponent is deprecated, use useMoveComp')

    return useCallback(
        (component: string, options?: string | { element?: string; position?: Position }): void => {
            if (typeof options === 'object') {
                m({ component, reference: options.element, position: options.position })
            } else {
                m({ component, reference: options, position: Position.inside })
            }
        },
        [m],
    )
}
