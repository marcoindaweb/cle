/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { atom, atomFamily, DefaultValue, selector, selectorFamily, useRecoilState } from 'recoil'
import Layout from '../../types/Layout'
import ComponentInstance, { ReducedComponentInstance } from '../../types/ComponentInstance'
import { componentDeclarationById } from '../componentDeclarations/store'
import { IconName, MaybeElement } from '@blueprintjs/core'
import notUndefined from '../../utils/notUndefined'
import ComponentDeclaration from '../../types/ComponentDeclaration'
import { componentAlias } from '../pagebuilder/aliases/store'
import { symbol, symbols, symbolsIds } from '../pagebuilder/symbols/store'
import _get from 'lodash.get'
import List, { LIST_DATA_KEY_PROPERTY_KEY } from '../../components/Definitions/List'

export const componentsIds = atom<string[]>({
  key: 'pages/componentsIds',
  default: []
})

export const disabledComponentsIds = atom<string[]>({
  key: 'pages/disabledComponentsIds',
  default: []
})

export const componentIsDisabled = selectorFamily<boolean, string>({
  key: 'pages/componentIsDisabled',
  get: (id) => ({ get }) => {
    return get(disabledComponentsIds).includes(id)
  },
  set: (id) => ({ get, set, reset }, val) => {
    if (val instanceof DefaultValue || !val) {
      set(disabledComponentsIds, (val) => val.filter((v) => v !== id))
    } else {
      set(disabledComponentsIds, (val) => [...val, id])
    }
  }
})

export const layoutName = atomFamily<string | undefined, number>({
  key: 'pages/layoutName',
  default: undefined
})

export const currentLayoutId = atom<number>({
  key: 'pages/currentLayoutId',
  default: 0
})

export const layout = selector<Layout>({
  key: 'pages/layout',
  get: ({ get }) => {
    const components: ComponentInstance[] = []
    const rootComponent = get(currentRootComponentId)
    if (rootComponent) {
      const compById = get(component(rootComponent))
      if (compById) components.push(compById)
    }
    const id = get(currentLayoutId)
    const name = get(layoutName(id))
    const layout: Layout = {
      id: get(currentLayoutId),
      name: name || '',
      components,
      symbols: get(symbolsIds)
        .map((i) => get(symbol(i)))
        .filter(notUndefined)
    }
    return layout
  },
  set: ({ set, get, reset }, val) => {
    if (val instanceof DefaultValue) {
      for (const id of get(componentsIds)) {
        reset(component(id))
      }
      for (const id of get(symbolsIds)) {
        reset(symbol(id))
      }
    } else {
      const compIds: string[] = []
      set(currentLayoutId, val.id)
      set(layoutName(val.id), val.name)
      const addComp = (c: ComponentInstance) => {
        set(component(c.id), c)
        compIds.push(c.id)
        if (c.children) {
          c.children.forEach((v) => addComp(v))
        }
      }
      addComp(val.components[0])
      set(componentsIds, (v) => [...v, ...compIds])
      set(symbols, val.symbols)
    }
  }
})

export const rootComponentsIds = atom<string[]>({
  key: 'pages/currentComponentsListId',
  default: []
})

export const _currentRootComponentId = atom<string | undefined>({
  key: 'pages/_currentRootComponentId',
  default: undefined
})

export const currentRootComponentId = selector<string | undefined>({
  key: 'pages/currentRootComponentId',
  get: ({ get }) => {
    return get(_currentRootComponentId)
  },
  set: ({ set, get, reset }, val) => {
    if (val instanceof DefaultValue || val === undefined) {
      reset(_currentRootComponentId)
      return
    }
    const rootComponents = get(rootComponentsIds)
    set(_currentRootComponentId, val)
    if (!rootComponents.includes(val)) {
      set(rootComponentsIds, [...rootComponents, val])
    }
  }
})

export const currentRootComponentChildren = selector<string[]>({
  key: 'pages/currentRootComponentChildren',
  get: ({ get }) => {
    const id = get(currentRootComponentId)
    if (!id) return []
    return get(componentChildrenIds(id))
  },
  set: ({ get, set, reset }, val) => {
    const id = get(currentRootComponentId)
    if (!id) return
    if (val instanceof DefaultValue) {
      reset(componentChildrenIds(id))
    } else {
      set(componentChildrenIds(id), [])
    }
  }
})

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const componentData = atomFamily<Record<string, any>, { component: string, indexes?: number[] }>({
  key: 'pages/layout/data',
  default: {}
})

export const componentChildrenIds = atomFamily<string[], string>({
  key: 'pages/layout/components/children',
  default: []
})

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const componentProperties = selectorFamily<Record<string, any>, string>({
  key: 'pages/layout/components/properties',
  get: (id) => ({ get }) => {
    const keys = get(componentPropertiesKeys(id))
    const o = {}
    for (const key of keys) {
      o[key] = get(componentProperty({ component: id, key }))
    }
    return o
  },
  set: (id) => ({ get, set, reset }, val) => {
    if (val instanceof DefaultValue) {
      const keys = get(componentPropertiesKeys(id))
      for (const key of keys) {
        reset(componentProperty({ component: id, key }))
      }
      reset(componentPropertiesKeys(id))
    } else {
      const keys = Object.keys(val)
      set(componentPropertiesKeys(id), keys)
      for (const key of keys) {
        set(componentProperty({ component: id, key }), val[key])
      }
    }
  }
})

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const componentPropertiesWithProvidedData = selectorFamily<
Record<string, any>,
{ component: string, indexes?: number[] } | string
>({
  key: 'pages/layout/component/propertiesWithProvidedData',
  get: (params) => ({ get }) => {
    const compId = typeof params === 'string' ? params : params.component
    const indexes = typeof params === 'string' ? undefined : params.indexes
    const keys = get(componentPropertiesKeys(compId))
    const o = {}
    for (const key of keys) {
      o[key] = get(componentPropertyWithProvidedData({ component: compId, key, indexes }))
    }
    return o
  },
  set: (params) => ({ set }, val) => {
    const compId = typeof params === 'string' ? params : params.component
    set(componentProperties(compId), val)
  }
})

export const parentListsDataKeys = selectorFamily<string[], string>({
  key: 'pages/layout/components/paretListDataKeyProperty',
  get: (id) => ({ get }) => {
    const parents = get(parentsIds(id))
    const o: string[] = []
    for (const pId of parents) {
      if (get(componentDeclarationId(pId)) !== List.id) {
        continue
      }
      const listDataKey = get(componentProperty({ component: pId, key: LIST_DATA_KEY_PROPERTY_KEY }))
      if (listDataKey) {
        o.push(listDataKey)
      }
    }
    return o
  }
})

/**
 * When a property has a value enclosed between {{ }} then we look for the data in the provided data of the component by the enclosed key
 * if there's nothing we return the key
 */
export const componentPropertyWithProvidedData = selectorFamily<
any,
{ component: string, key: string, indexes?: number[] }
>({
  key: 'pages/layout/components/propertyWithProvidedData',
  get: ({ component, key, indexes }) => ({ get }) => {
    const data = get(componentProperty({ component, key }))
    const resolve = (data: any): any => {
      if (!data || typeof data === 'function') return data
      if (typeof data === 'object') {
        if (Array.isArray(data)) {
          return data.map((v) => resolve(v))
        } else {
          const o = {}
          for (const [key, val] of Object.entries(data)) {
            o[key] = resolve(val)
          }
          return o
        }
      } else {
        return get(resolveComponentProvidedDataKey({ key: data, component, indexes }))
      }
    }
    return resolve(data)
  },
  set: ({ component, key }) => ({ set }, val) => {
    set(componentProperty({ component, key }), val)
  }
})

export const resolveComponentProvidedDataKey = selectorFamily<
any,
{ key: string, component: string, indexes?: number[] }
>({
  key: 'pages/layout/components/resolveComponentProvidedDataKey',
  get: ({ component, key, indexes }) => ({ get }) => {
    if (typeof key === 'string' && /{{[^}}]*}}/.test(key)) {
      const provided = get(providedData({ component, indexes }))
      return key.replace(/{{[^}}]*}}/g, (key) => {
        if (indexes !== undefined && indexes.length > 0) {
          const listDataKeys = get(parentListsDataKeys(component))
          const finalKey = buildDataKeyFromListKeys(indexes, key.replace(/^{{|}}$/g, ''), listDataKeys)
          const r = _get(provided, finalKey, false)
          return r
        } else {
          return _get(provided || {}, key.replace(/^{{|}}$/g, ''))
        }
      })
    } else {
      return key
    }
  }
})

export const buildDataKeyFromListKeys = (indexes: number[], dataKey: string, listKeys: string[]): string => {
  const actualKeys = listKeys.filter((v) => dataKey.includes(v))
  const actualIndexes = [...indexes].splice(0, actualKeys.length)
  const parts = actualKeys.reduce<string[]>((prev, curr) => {
    const v = prev.find((v) => curr.includes(v))
    if (v) {
      return [...prev, curr.replace(v, '')]
    } else {
      return [curr]
    }
  }, [])
  const lastKey = actualKeys[actualKeys.length - 1]
  const finalDataKey = dataKey.replace(lastKey, '')
  return [...parts, finalDataKey].reduce((final, key, index) => {
    const i = actualIndexes[index]
    if (i !== undefined) {
      return `${final}${key}[${i}]`
    } else {
      return `${final}${key}`
    }
  }, '')
}

export const componentProperty = atomFamily<any, { component: string, key: string }>({
  key: 'pages/layout/components/properties',
  default: undefined
})

export const componentPropertiesKeys = atomFamily<string[], string>({
  key: 'pages/layout/components/propertiesKeys',
  default: []
})

export const componentDeclarationId = atomFamily<string | undefined, string>({
  key: 'pages/layout/components/declarationId',
  default: undefined
})

export const componentIcon = selectorFamily<IconName | MaybeElement, string>({
  key: 'pages/layout/componentIcon',
  get: (id) => ({ get }) => {
    return get(componentDeclarationById(get(component(id))?.componentDeclarationId || ''))?.meta?.icon || 'cube'
  }
})

export const firstNonAbstractParent = selectorFamily<ComponentInstance | undefined, string>({
  key: 'pages/layout/firstNonAbstractParent',
  get: (id: string) => ({ get }) => {
    const cc = get(parents(id)).reverse()
    for (const c of cc) {
      const decl = get(componentDeclarationById(c.componentDeclarationId))
      if (decl && decl.config?.isAbstract !== true) {
        return c
      }
    }
  }
})

export const allComponentChildrenIds = selectorFamily<string[], string>({
  key: 'pages/layout/allComponentChildrenIds',
  get: (id) => ({ get }) => {
    const getChild = (id: string): string[] => {
      const o: string[] = []
      const children = get(componentChildrenIds(id))
      o.push(...children)
      for (const child of children) {
        o.push(...getChild(child))
      }
      return o
    }
    return getChild(id)
  }
})

/**
 * 0 = component id
 * 1 = type id / types id
 */
export const allChildrenOfType = selectorFamily<string[], [string, string | string[]]>({
  key: 'pages/layout/allChildrenOfType',
  get: ([id, type]) => ({ get }) => {
    if (typeof type === 'string') {
      return get(allComponentChildrenIds(id)).filter((i) => get(componentDeclarationId(i)) === type)
    } else {
      return get(allComponentChildrenIds(id)).filter((i) => type.includes(get(componentDeclarationId(i)) || ''))
    }
  }
})

export const parentOfType = selectorFamily<ComponentInstance | undefined, { component: string, type: string }>({
  key: 'pages/layout/parentOfType',
  get: ({ component, type }) => ({ get }) => {
    const pp = get(parents(component))
    return pp.find((v) => v.componentDeclarationId === type)
  }
})

export const parentId = atomFamily<string | undefined, string>({
  key: 'pages/layout/parentId',
  default: selectorFamily<string | undefined, string>({
    key: 'pages/layout/parentId/defaultValue',
    get: (id) => ({ get }) => {
      const ids = get(componentsIds)
      for (const i of ids) {
        if (get(componentChildrenIds(i)).includes(id)) {
          return i
        }
      }
    }
  })
})

export const parentsIds = selectorFamily<string[], string>({
  key: 'pages/layout/parentsIds',
  get: (id) => ({ get }) => {
    const parents: string[] = []
    let currentId: string | undefined = id
    while (typeof currentId === 'string') {
      const pId = get(parentId(currentId))
      if (pId) {
        parents.unshift(pId)
        currentId = pId
      } else {
        currentId = undefined
      }
    }
    return parents
  }
})

export const reducedParents = selectorFamily<ReducedComponentInstance[], string>({
  key: 'pages/layout/reducedParents',
  get: (id) => ({ get }) => {
    return get(parentsIds(id)).map((id) => get(reducedComponentById(id)))
  }
})

export const parents = selectorFamily<ComponentInstance[], string>({
  key: 'pages/layout/parents',
  get: (id) => ({ get }) => {
    return get(parentsIds(id)).map((id) => get(component(id))) as ComponentInstance[]
  }
})

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const providedData = selectorFamily<Record<string, any> | undefined, { component: string, indexes?: number[] }>({
  key: 'pages/layout/providedData',
  get: ({ component, indexes }) => ({ get }) => {
    const ids = get(parentsIds(component))
    const o = {}
    for (const parent of ids) {
      const parentListKeys = get(parentListsDataKeys(parent))
      const actualIndexes = [...(indexes || [])].splice(0, parentListKeys.length)
      const data = get(
        componentData({ component: parent, indexes: actualIndexes.length > 0 ? actualIndexes : undefined })
      )
      if (Object.keys(data).length > 0) {
        o[parent] = data
      }
    }
    return o
  }
})

export const component = selectorFamily<ComponentInstance | undefined, string>({
  key: 'pages/component',
  get: (id: string) => ({ get }) => {
    const reduced = get(reducedComponentById(id))
    const declaration = get(componentDeclarationId(id))
    if (!declaration) return
    const children = reduced.children.map((id) => get(component(id))).filter(notUndefined)
    const c: ComponentInstance = {
      ...reduced,
      children,
      componentDeclarationId: declaration,
      alias: get(componentAlias(id)),
      properties: get(componentProperties(id))
    }
    return c
  },
  set: (id: string) => ({ set, get }, c) => {
    if (c instanceof DefaultValue || c === undefined) {
      const v = new DefaultValue()
      set(componentDeclarationId(id), v)
      set(componentProperties(id), v)
      set(componentAlias(id), v)
      for (const child of get(componentChildrenIds(id))) {
        set(component(child), v)
      }
      set(reducedComponentById(id), v)
    } else {
      const str = JSON.stringify
      if (get(componentDeclarationId(id)) !== c.componentDeclarationId) {
        set(componentDeclarationId(id), c.componentDeclarationId)
      }
      if (str(get(componentProperties(id))) !== str(c.properties)) {
        set(componentProperties(id), c.properties)
      }
      if (str(convertToReducedComponent(c)) !== str(get(reducedComponentById(c.id)))) {
        set(reducedComponentById(id), convertToReducedComponent(c))
      }
      if (c.alias !== get(componentAlias(c.id))) {
        set(componentAlias(c.id), c.alias)
      }
      for (const child of c?.children || []) {
        set(component(child.id), child)
      }
    }
  }
})

/**
 * Strips out properties from a component and give only the children ids
 */
export const reducedComponentById = selectorFamily<ReducedComponentInstance, string>({
  key: 'page/reducedComponentById',
  get: (id: string) => ({ get }) => {
    const children = get(componentChildrenIds(id))
    const reduced: ReducedComponentInstance = {
      id,
      children
    }
    return reduced
  },
  set: (id: string) => ({ get, set }, c) => {
    if (c instanceof DefaultValue) {
      set(componentChildrenIds(id), c)
      const parent = get(parentId(id))
      if (parent) {
        set(
          componentChildrenIds(parent),
          get(componentChildrenIds(parent)).filter((v) => v !== id)
        )
      }
    } else if (c !== undefined) {
      const isEqual = JSON.stringify(c.children) === JSON.stringify(get(componentChildrenIds(c.id)))
      if (!isEqual) {
        set(componentChildrenIds(id), c.children)
      }
    }
  }
})

export const declarationByComponent = selectorFamily<ComponentDeclaration | undefined, string>({
  key: 'page/declarationByComponent',
  get: (id) => ({ get }) => {
    const dId = get(componentDeclarationId(id))
    return get(componentDeclarationById(dId || ''))
  }
})

const convertToReducedComponent = (c: ComponentInstance): ReducedComponentInstance => {
  return {
    id: c.id,
    children: (c?.children || []).map((c) => c.id)
  }
}
