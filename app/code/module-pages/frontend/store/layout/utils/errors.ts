/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import ComponentInstance from '../../../types/ComponentInstance'

export class RequiredParentMissingError extends Error {
    constructor(declaration: ComponentDeclaration) {
        super(
            `The component ${
                declaration.id
            } is missing one of his required parents (${declaration.config?.mustBeChildrenOf?.join(', ')})`,
        )
    }
}

export class ComponentCacheMissingError extends Error {
    constructor(component: ComponentInstance | string) {
        super(`Component ${typeof component === 'string' ? component : component.id} is missing from the cache`)
    }
}

export class ComponentNotFoundError extends Error {
    constructor(component: ComponentInstance | string) {
        super(`Component ${typeof component === 'string' ? component : component.id} not found`)
    }
}

export class DeclarationNotFoundError extends Error {
    constructor(declaration: ComponentDeclaration | string) {
        super(`Declaration ${typeof declaration === 'string' ? declaration : declaration.id} not found`)
    }
}

export class SymbolDefinitionNotFound extends Error {
    constructor(declaration: string) {
        super(`Symbol definition with id ${declaration} not found`)
    }
}

export class ParentNotFoundError extends Error {
    constructor(component: ComponentInstance | string) {
        super(`Parent of component ${typeof component === 'string' ? component : component.id} not found`)
    }
}
