/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { getIdFromIdTree, getIndexesFromIdTree, IdTree } from '../manipulation'

it('get the correct indexes from id', () => {
    const idTree: IdTree = [
        'a',
        [
            [
                'b',
                [
                    ['g', [['i', []]]],
                    ['h', []],
                    ['l', [['n', []]]],
                    ['m', []],
                ],
            ],
            ['c', [['d', [['e', [['f', []]]]]]]],
        ],
    ]
    const res1 = getIndexesFromIdTree(idTree, 'f')
    const res2 = getIndexesFromIdTree(idTree, 'n')
    const res3 = getIndexesFromIdTree(idTree, 'l')
    const res4 = getIndexesFromIdTree(idTree, 'i')
    expect(res1).toEqual([0, 1, 0, 0, 0])
    expect(res2).toEqual([0, 0, 2, 0])
    expect(res3).toEqual([0, 0, 2])
    expect(res4).toEqual([0, 0, 0, 0])
})

it('get the corret id from indexes', () => {
    const idTree: IdTree = [
        'a',
        [
            [
                'b',
                [
                    ['g', [['i', []]]],
                    ['h', []],
                    ['l', [['n', []]]],
                    ['m', []],
                ],
            ],
            ['c', [['d', [['e', [['f', []]]]]]]],
        ],
    ]
    const res1 = getIdFromIdTree(idTree, [0, 1, 0, 0, 0])
    const res2 = getIdFromIdTree(idTree, [0, 0, 2, 0])
    const res3 = getIdFromIdTree(idTree, [0, 0, 2])
    const res4 = getIdFromIdTree(idTree, [0, 0, 0, 0])
    expect(res1).toEqual('f')
    expect(res2).toEqual('n')
    expect(res3).toEqual('l')
    expect(res4).toEqual('i')
})
