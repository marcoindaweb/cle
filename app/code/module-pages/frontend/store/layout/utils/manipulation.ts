/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import ComponentInstance from '../../../types/ComponentInstance'
import { v4 } from 'uuid'
import { SymbolDefinition, SymbolLayout } from '../../../types/SymbolDefinition'
import { PageBuilderSymbol } from 'types/graphql'
import List, { LIST_DATA_KEY_PROPERTY_KEY } from '../../../components/Definitions/List'

export const symbolDefinitionToPageBuilderSymbol = (
    s: Omit<SymbolDefinition, 'id'> & { id?: number },
): Omit<PageBuilderSymbol, 'id' | 'tags'> & { id?: number } => ({
    ...s,
    layout: JSON.stringify(s.layout),
})

export const pageBuilderSymbolToSymbolDefinition = (s: PageBuilderSymbol): SymbolDefinition => ({
    ...s,
    layout: JSON.parse(s.layout),
    tags: s.tags ? s.tags.map((v) => v.name) : [],
})

export const createComponent = (
    declaration: ComponentDeclaration,
    properties?: Record<string, any>,
): ComponentInstance => ({
    componentDeclarationId: declaration.id,
    id: v4(),
    properties: properties || declaration.defaultProperties || {},
    alias: declaration.id,
})

export type IdTree = [string, IdTree[]]

const buildIdTree = (c: ComponentInstance): IdTree => {
    return [c.id, (c.children || []).map(buildIdTree)]
}

export const getIndexesFromIdTree = (idTree: IdTree, id: string, indexes?: number[]): number[] => {
    if (idTree[0] === id) {
        return indexes || [0]
    } else {
        for (const [i, v] of idTree[1].entries()) {
            const res = getIndexesFromIdTree(v, id, [...(indexes || [0]), i])
            if (res.length) {
                return res
            }
        }
        return []
    }
}

export const getIdFromIdTree = (idTree: IdTree, indexes: number[]): string | undefined => {
    let currTree: IdTree = [...idTree]
    const currIndexes = [...indexes]
    currIndexes.shift()
    while (currIndexes.length > 0) {
        currTree = currTree[1][currIndexes[0]]
        currIndexes.shift()
    }
    return currTree[0]
}

export const COMPONENT_ID_PLACEHOLDER = '<component-id>'

export const convertComponentInstanceToSymbolLayout = (c: ComponentInstance, idTree?: IdTree): SymbolLayout => {
    idTree = idTree || buildIdTree(c)

    return {
        alias: c.alias,
        children: ((c.children || []) as ComponentInstance[]).map((v, i) =>
            convertComponentInstanceToSymbolLayout(v, idTree),
        ),
        componentDeclarationId: c.componentDeclarationId,
        properties: convertComponentPropertiesToSymbolLayoutProperties(
            c.properties,
            c.id,
            idTree,
            c.componentDeclarationId,
        ),
    }
}

const convertSingleStringForSymbol = (val: string, idTree: IdTree, enclosed = true): string => {
    const dataKey = enclosed ? val.replace(/^{{|}}$/g, '') : val
    const id = dataKey.split('.')[0]
    const indexes = getIndexesFromIdTree(idTree, id)
    const res = `${indexes.map((v) => '[' + v + ']').join('')}.${dataKey.split('.').splice(1).join('.')}`
    return enclosed ? `{{${res}}}` : res
}

export const convertStringValueForSymbol = (v: string, idTree: IdTree) => {
    return v.replace(/{{[^}}]*}}/g, (val) => convertSingleStringForSymbol(val, idTree)) as any
}

const convertPropertiesForSymbol = <T>(v: T, idTree: IdTree): T => {
    if (typeof v === 'string') {
        if (stringNeedsToBeConverted(v)) {
            return convertStringValueForSymbol(v, idTree)
        } else {
            return v
        }
    } else if (typeof v === 'object') {
        if (Array.isArray(v)) {
            return v.map((i) => convertPropertiesForSymbol(i, idTree)) as any
        } else {
            const conv = {}
            for (const [key, val] of Object.entries(v)) {
                if (val) {
                    conv[key] = convertPropertiesForSymbol(val, idTree)
                }
            }
            return conv as any
        }
    } else {
        return v
    }
}

export const convertStyleForSymbol = (
    v: ComponentInstance['properties']['style'] | string,
    id: string,
    idTree: IdTree,
) => {
    if (!v) return undefined
    if (typeof v === 'string') return convertPropertiesForSymbol(v, idTree)
    const res = {}
    for (const [key, val] of Object.entries(v)) {
        if (key === id) {
            res[COMPONENT_ID_PLACEHOLDER] = convertStyleForSymbol(val, id, idTree)
        } else {
            res[key] = convertStyleForSymbol(val, id, idTree)
        }
    }
    return res
}

export const convertClassNameForSymbol = (
    v: ComponentInstance['properties']['className'],
    id: string,
    idTree: IdTree,
) => {
    return v
        ? convertPropertiesForSymbol(
              v.replace(new RegExp(`(^|\\s+)(${id})(\\s+|$)`), ' ' + COMPONENT_ID_PLACEHOLDER + ' ').trim(),
              idTree,
          )
        : v
}

export const convertComponentPropertiesToSymbolLayoutProperties = (
    v: ComponentInstance['properties'],
    id: string,
    idTree: IdTree,
    decl: ComponentInstance['componentDeclarationId'],
): SymbolLayout['properties'] => {
    const style = v.style
    const className = v.className
    const otherProps = { ...v }
    delete otherProps.className
    delete otherProps.style
    const out = { ...convertPropertiesForSymbol(otherProps, idTree) }
    if (style) {
        out.style = convertStyleForSymbol(style, id, idTree)
    }
    if (className) {
        out.className = convertClassNameForSymbol(className, id, idTree)
    }
    switch (decl) {
        case List.id:
            if (out[LIST_DATA_KEY_PROPERTY_KEY]) {
                out[LIST_DATA_KEY_PROPERTY_KEY] = convertSingleStringForSymbol(
                    out[LIST_DATA_KEY_PROPERTY_KEY],
                    idTree,
                    false,
                )
            }
            break
    }
    return out
}

const stringNeedsToBeConverted = (v: string) => {
    return /{{[^}}]*}}/.test(v)
}

const convertSingleStringForComponentInstance = (v: string, idTree: IdTree, surrounded = true): string => {
    const parts = surrounded ? v.replace(/^{{|}}$/g, '').split('.') : v.split('.')
    const indexes = Array.from(parts[0].replace(/[\[\]]/g, '')).map((v) => parseInt(v))
    const id = getIdFromIdTree(idTree, indexes)
    return surrounded ? `{{${id}.${parts.splice(1).join('.')}}}` : `${id}.${parts.splice(1).join('.')}`
}

const convertStringForComponentInstance = (v: string, idTree: IdTree): string => {
    const res = v.replace(/{{[^}}]*}}/g, (v) => convertSingleStringForComponentInstance(v, idTree, true))
    return res as any
}

const convertPropertiesForComponentInstance = <T>(v: T, idTree: IdTree): T => {
    if (typeof v === 'string') {
        if (stringNeedsToBeConverted(v)) {
            return convertStringForComponentInstance(v, idTree) as any
        } else {
            return v
        }
    } else if (typeof v === 'object') {
        if (Array.isArray(v)) {
            return v.map((i) => convertPropertiesForComponentInstance(i, idTree)) as any
        } else {
            const conv = {}
            for (const [key, val] of Object.entries(v)) {
                conv[key] = convertPropertiesForComponentInstance(val, idTree)
            }
            return conv as any
        }
    } else {
        return v
    }
}

export const convertStyleForComponentInstance = (
    v: SymbolLayout['properties']['style'] | string,
    id: string,
    idTree: IdTree,
) => {
    if (!v) return undefined
    if (typeof v === 'string') return convertPropertiesForComponentInstance(v, idTree)
    const res = {}
    for (const [key, val] of Object.entries(v)) {
        if (key === COMPONENT_ID_PLACEHOLDER) {
            res[id] = convertStyleForSymbol(val, id, idTree)
        } else {
            res[key] = convertStyleForSymbol(val, id, idTree)
        }
    }
    return res
}

export const convertClassNameForComponentInstance = (
    v: SymbolLayout['properties']['className'],
    id: string,
    idTree: IdTree,
) => {
    return v ? convertPropertiesForComponentInstance(v.replace(COMPONENT_ID_PLACEHOLDER, id), idTree) : v
}

export const convertSymbolLayoutPropertiesToComponentProperties = (
    v: SymbolLayout['properties'],
    id: string,
    idTree: IdTree,
    decl: SymbolLayout['componentDeclarationId'],
): ComponentInstance['properties'] => {
    const style = v.style
    const className = v.className
    const otherProps = { ...v }
    delete otherProps.className
    delete otherProps.style
    const out = convertPropertiesForComponentInstance(otherProps, idTree)
    if (style) {
        out.style = convertStyleForComponentInstance(style, id, idTree)
    }
    if (className) {
        out.className = convertClassNameForComponentInstance(className, id, idTree)
    }
    switch (decl) {
        case List.id:
            if (out[LIST_DATA_KEY_PROPERTY_KEY]) {
                out[LIST_DATA_KEY_PROPERTY_KEY] = convertSingleStringForComponentInstance(
                    out[LIST_DATA_KEY_PROPERTY_KEY],
                    idTree,
                    false,
                )
            }
            break
    }
    return out
}
