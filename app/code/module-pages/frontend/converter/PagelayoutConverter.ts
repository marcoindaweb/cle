/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Pagelayout } from 'types/graphql'
import Layout from '../types/Layout'
import { symbol } from '../store/pagebuilder/symbols/store'

export const convertPagelayoutToLayout = (layout: Pagelayout): Layout => {
    const components = JSON.parse(layout.layout)
    const symbols = JSON.parse(layout.symbols)
    return {
        id: layout.id,
        name: layout.name,
        components,
        symbols,
    }
}

export const convertLayoutToPagelayout = (layout: Layout): Omit<Pagelayout, 'entity'> => {
    return {
        id: layout.id,
        name: layout.name,
        layout: JSON.stringify(layout.components),
        symbols: JSON.stringify(layout.symbols),
    }
}
