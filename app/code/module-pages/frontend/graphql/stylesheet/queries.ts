/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { QueryHookOptions, useMutation, useQuery } from '@apollo/client'
import {
    MutationCreateStylesheetArgs,
    MutationUpdateStylesheetArgs,
    QueryFindOneStylesheetArgs,
    QueryRenderStylesheetArgs,
    RenderedStylesheet,
    Stylesheet,
} from 'types/graphql'
import { gql } from '@apollo/client/core'

const findStylesheetQuery = gql`
    query FindStylesheet {
        findStylesheet(filter: {}) {
            id
            name
        }
    }
`
export const useStylesheetsQuery = () => useQuery<{ findStylesheet: Stylesheet[] }>(findStylesheetQuery)

const findOneStylesheetQuery = gql`
    query FindOneStylesheet($filter: JSON!) {
        findOneStylesheet(filter: $filter) {
            id
            name
            style
        }
    }
`

export const useFindOneStylesheetQuery = (
    v: QueryHookOptions<{ findOneStylesheet: Stylesheet }, QueryFindOneStylesheetArgs>,
) => useQuery<{ findOneStylesheet: Stylesheet }, QueryFindOneStylesheetArgs>(findOneStylesheetQuery, v)

const renderedStylesheetQuery = gql`
    query RenderStylesheet($id: Int!) {
        renderStylesheet(id: $id) {
            id
            content
        }
    }
`
export const useRenderedStylesheet = (
    v: QueryHookOptions<{ renderStylesheet: RenderedStylesheet }, QueryRenderStylesheetArgs>,
) => useQuery<{ renderStylesheet: RenderedStylesheet }, QueryRenderStylesheetArgs>(renderedStylesheetQuery)

export const createStylesheetMutation = gql`
    mutation CreateStylesheetMutation($input: StylesheetCreateInput!) {
        createStylesheet(input: $input) {
            id
            name
            content
        }
    }
`
export const useCreateStylesheetMutation = () =>
    useMutation<{ createStylesheet: Stylesheet }, MutationCreateStylesheetArgs>(createStylesheetMutation)

export const updateStylesheetMutation = gql`
    mutation UpdateStylesheetMutation($input: StylesheetUpdateInput!) {
        updateStylesheet(input: $input) {
            id
            name
            content
        }
    }
`
export const useUpdateStylesheetMutation = () =>
    useMutation<{ updateStylesheet: Stylesheet }, MutationUpdateStylesheetArgs>(updateStylesheetMutation)
