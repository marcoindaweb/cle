/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { gql } from '@apollo/client/core'
import { useMutation } from '@apollo/client'
import { MutationCreatePageBuilderSymbolArgs, PageBuilderSymbol } from 'types/graphql'
import { findPageBuilderSymbolQuery } from './queries'

export const createPageBuilderSymbolMutation = gql`
    mutation CreatePageBuilderSymbolMutation($input: PageBuilderSymbolCreateInput!) {
        createPageBuilderSymbol(input: $input) {
            id
            name
            layout
        }
    }
`

export const useCreatePageBuilderSymbol = () =>
    useMutation<{ createPagebuildersymbol: PageBuilderSymbol }, MutationCreatePageBuilderSymbolArgs>(
        createPageBuilderSymbolMutation,
        {
            refetchQueries: () => {
                return [{ query: findPageBuilderSymbolQuery, variables: { filter: {} } }]
            },
        },
    )
