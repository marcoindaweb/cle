/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { gql } from '@apollo/client/core'
import { useMutation } from '@apollo/client'
import { MutationCreatePagelayoutArgs, MutationUpdatePagelayoutArgs, Pagelayout } from 'types/graphql'

export const createLayoutMutation = gql`
    mutation CreatePagelayoutMutation($input: PagelayoutCreateInput!) {
        createPagelayout(input: $input) {
            id
            name
            layout
            symbols
        }
    }
`

export const useCreatePagelayout = () =>
    useMutation<{ createPagelayout: Pagelayout }, MutationCreatePagelayoutArgs>(createLayoutMutation)

export const updateLayoutMutation = gql`
    mutation UpdateLayoutMutation($input: PagelayoutUpdateInput!) {
        updatePagelayout(input: $input) {
            id
            name
            layout
            symbols
        }
    }
`

export const useUpdatePageLayout = () =>
    useMutation<{ updatePagelayout: Pagelayout }, MutationUpdatePagelayoutArgs>(updateLayoutMutation)
