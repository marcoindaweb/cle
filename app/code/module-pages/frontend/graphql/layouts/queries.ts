/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { gql } from '@apollo/client/core'
import { QueryHookOptions, useLazyQuery, useQuery } from '@apollo/client'
import { Pagelayout, QueryFindOnePagelayoutArgs } from 'types/graphql'

const findOneLayoutQuery = gql`
    query FindOneLayoutQuery($filter: JSON!) {
        findOnePagelayout(filter: $filter) {
            id
            name
            layout
            symbols
        }
    }
`
export const useFindOneLayoutQuery = (
    params?: QueryHookOptions<{ findOnePagelayout: Pagelayout }, QueryFindOnePagelayoutArgs>,
) => useQuery<{ findOnePagelayout: Pagelayout }, QueryFindOnePagelayoutArgs>(findOneLayoutQuery, params)

export const useLazyFindOneLayoutQuery = (
    params?: QueryHookOptions<{ findOnePagelayout: Pagelayout }, QueryFindOnePagelayoutArgs>,
) => useLazyQuery<{ findOnePagelayout: Pagelayout }, QueryFindOnePagelayoutArgs>(findOneLayoutQuery, params)

const getAllLayoutsQuery = gql`
    query GetAllLayoutsQuery {
        findPagelayout(filter: {}) {
            id
            name
        }
    }
`

export const useGetAllLayouts = () =>
    useQuery<{ findPagelayout: Array<Omit<Pagelayout, 'layout' | 'symbols'>> }>(getAllLayoutsQuery)
