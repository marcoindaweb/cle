/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default interface ComponentInstance<T = Record<string, any>> {
  id: string
  alias: string
  componentDeclarationId: string
  properties: T & { className?: string, style?: Record<string, any> }
  children?: ComponentInstance[]
}

export interface ReducedComponentInstance {
  id: string
  children: string[]
}

export const COMPONENT_INSTANCE = 'COMPONENT_INSTANCE'

export interface ComponentInstanceDrag { id: string, type: typeof COMPONENT_INSTANCE }

export const REDUCED_COMPONENT_INSTANCE = 'REDUCED_COMPONENT_INSTANCE'

export type ReducedComponentInstanceDrag = ReducedComponentInstance & { type: typeof REDUCED_COMPONENT_INSTANCE }
