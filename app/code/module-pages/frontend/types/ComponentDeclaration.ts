/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { JSXElementConstructor, PropsWithChildren, ReactNode } from 'react'
import { IconName } from '@blueprintjs/core'

export type ComponentDeclarationOptions<T> = {
    value: T
    setValue: (v: T) => void
}
export type CDO<T> = ComponentDeclarationOptions<T>

export const COMPONENT_DECLARATION = 'COMPONENT_DECLARATION'

export type RenderProps<T> = T & { id: string; className?: string; indexes?: number[] }
export type WithProvide<T> = { provide: (val: T) => void; provided: T }
export type Props<T, V> = RenderProps<T> & WithProvide<V>
export type Options<T, V, P = RenderProps<T>> = JSXElementConstructor<{
    value: P
    setValue: (v: T) => void
}>

export default interface ComponentDeclaration<T = { [key: string]: any }, V = any> {
    id: string
    render: JSXElementConstructor<PropsWithChildren<Props<T, V>>>
    options?: Options<T, V>
    defaultProperties?: Omit<RenderProps<T>, 'id'> & { style?: Record<string, any> }
    provideData?: V // A component can provide some data that can then be used by other components, don't pass this prop, is only used for typing porpoises
    meta?: {
        displayName?: string | JSX.Element
        tags?: string[]
        description?: string | React.ReactNode
        icon?: IconName | JSX.Element
        group?: string
    }
    config?: {
        haveStyle?: boolean // If false disable style tab
        haveOptions?: boolean // if false disable the options tab
        haveChildren?: boolean // Don't allow children
        showStyleAsCode?: boolean // Show code editor instead of inputs for style
        isAbstract?: boolean // Mark the components as abstract, use this only for components that don't render anything (ex: stylesheet, guards, ecc...)
        mustBeChildrenOf?: string[] // The component must be a child of a specific kind of components
        hidden?: boolean // The component won't be shown in the components picker
        availableTriggers?: string[]
    }
}

export const isComponentDeclaration = (v: any): v is ComponentDeclaration => {
    return v.hasOwnProperty('render')
}

export type ComponentDeclarationDrag = ComponentDeclaration & { type: typeof COMPONENT_DECLARATION }
