/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentInstance from './ComponentInstance'

export interface SymbolDefinition {
    id: string
    name: string
    tags?: string[]
    layout: SymbolLayout
}

export type SymbolLayout = Omit<ComponentInstance, 'id' | 'children'> & {
    children: SymbolLayout[]
}

export type SymbolInstance = {
    id: string
    definitionId: string
    alias: string
    components: string[]
}

export const SYMBOL_DEFINITION = 'SYMBOL_DEFINITION'

export type SymbolDefinitionDrag = SymbolDefinition & { type: typeof SYMBOL_DEFINITION }
