/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import Builder from '../../../Builder'
import * as React from 'react'
import { render } from '@testing-library/react'
import ComponentDeclaration from '../../../../types/ComponentDeclaration'
import { JSXElementConstructor, useEffect, useState } from 'react'
import { useAddComponentDeclaration } from '../../../../store/componentDeclarations/actions'
import Route from '../index'
import { RecoilRoot, useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import { layout, parentOfType, providedData } from '../../../../store/layout/store'
import { useAddComponentOrSymbol } from '../../../../store/layout/actions'
import { path } from '../../../../store/pagebuilder/router/store'
import DndProvider from '@linda/module-admin/frontend/containers/DndProvider'
import I18n from '@linda/module-i18n/frontend/containers/I18n'
import { renderHook } from '@testing-library/react-hooks'
import RenderRoute from '../render'

interface PbHelper {
    toRegister: ComponentDeclaration[]
    readers: Array<() => void>
    register: (d: ComponentDeclaration) => this
    getWrapper: () => JSXElementConstructor<any>
    addAction: (v: () => void) => this
}

class PB implements PbHelper {
    toRegister: ComponentDeclaration[] = []
    readers: Array<() => void> = []
    register(v) {
        this.toRegister.push(v)
        return this
    }
    getWrapper() {
        const Actions = () => {
            return (
                <>
                    {this.readers.map((a, i) => (
                        <Action key={i} action={a} />
                    ))}
                </>
            )
        }

        const Action: React.FC<{ action: () => void }> = ({ action }) => {
            action()
            return null
        }

        const Definitions: React.FC = ({ children }) => {
            const [c, setC] = useState(0)
            const [render, setRender] = useState(false)
            const add = useAddComponentDeclaration()

            useEffect(() => {
                if (this.toRegister[c]) {
                    add(this.toRegister[c])
                }
                if (c === this.toRegister.length - 1) {
                    setRender(true)
                } else {
                    setC(c + 1)
                }
            }, [c])

            return render ? <>{children}</> : null
        }

        const wrapper = ({ children }) => (
            <>
                <RecoilRoot>
                    <DndProvider>
                        <I18n>
                            <Definitions>
                                <Builder value={{ components: [] }} onChange={() => {}} />
                                {children}
                            </Definitions>
                        </I18n>
                    </DndProvider>
                </RecoilRoot>
            </>
        )
        return wrapper
    }
    addAction(v) {
        this.readers.push(v)
        return this
    }
}

describe('route component', () => {
    it('provide route parameters', async () => {
        let provided = {}

        const provide = jest.fn((v) => {
            provided = v
        })

        const SetPath = ({ children }) => {
            const set = useSetRecoilState(path)
            useEffect(() => {
                set('/1')
            })
            return <>{children}</>
        }
        render(
            <RecoilRoot>
                <SetPath>
                    <RenderRoute provided={provided} provide={provide} route={'/:id'} id={'test'} />
                </SetPath>
            </RecoilRoot>,
        )

        expect(provided).toEqual({ params: { id: '1' } })
    })
})
