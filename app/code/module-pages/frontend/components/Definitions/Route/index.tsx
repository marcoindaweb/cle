/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import { Entity, EntityProperty } from 'types/graphql'
import RenderRoute from './render'
import Options from './options'

export type RouteProperties = {
    route: string
}

export type RouteProvidedData = {
    params?: Record<string, string>
}

const Route: ComponentDeclaration<RouteProperties, RouteProvidedData> = {
    config: {},
    defaultProperties: { route: '' },
    id: 'route',
    meta: {
        icon: 'flow-branch',
    },
    options: Options,
    render: RenderRoute,
}

export default Route
