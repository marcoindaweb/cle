/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Button, Classes, Colors, InputGroup, Tag } from '@blueprintjs/core'
import React, { useEffect, useState } from 'react'
import Route from './index'
import Translated from '@linda/module-i18n/frontend/components/Translated'
const Options: typeof Route['options'] = ({ value, setValue }) => {
    const [v, setV] = useState(value)

    useEffect(() => {
        setV(value)
    }, [value, setV])

    return (
        <>
            <InputGroup value={v.route} onChange={(v) => setV({ ...value, route: v.target.value })} />
            <Button onClick={() => setValue(v)}>
                <Translated message="Update" />
            </Button>
        </>
    )
}

export default Options
