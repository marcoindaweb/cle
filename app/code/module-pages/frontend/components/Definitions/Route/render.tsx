/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import Route from './index'
import React, { useEffect } from 'react'
import { match } from 'path-to-regexp'
import { useRecoilValue } from 'recoil'
import { path } from '../../../store/pagebuilder/router/store'

const RenderRoute: typeof Route['render'] = (props) => {
    const p = useRecoilValue(path)
    const res = match(props.route)(p)
    const params = res ? res.params : false
    const shouldUpdate = JSON.stringify(params) !== JSON.stringify(props.provided.params)

    useEffect(() => {
        if (params) {
            props.provide({ params: params as Record<string, string> })
        } else {
            props.provide({})
        }
    }, [shouldUpdate, p])

    return res ? (
        <div id={props.id} className={props.className}>
            {props.children}
        </div>
    ) : null
}

export default RenderRoute
