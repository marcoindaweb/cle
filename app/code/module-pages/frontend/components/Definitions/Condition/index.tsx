/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import RenderCondition from './render'
import ConditionOptions from './options'

type ConditionProperties = {
    values: Record<string, any>
    condition: string
}
const Condition: ComponentDeclaration<ConditionProperties> = {
    id: 'Condition',
    meta: {
        tags: ['data manipulation'],
        icon: 'flows',
    },
    config: {
        isAbstract: true,
    },
    defaultProperties: {
        values: {},
        condition: '',
    },
    render: RenderCondition,
    options: ConditionOptions,
}

export default Condition
