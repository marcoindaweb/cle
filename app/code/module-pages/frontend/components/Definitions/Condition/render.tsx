/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import Condition from './index'
import React, { useEffect, useState } from 'react'
import _template from 'lodash.template'
import AdminToast from '@linda/module-admin/frontend/components/AdminToast'

const RenderCondition: typeof Condition['render'] = ({ values, condition, children }) => {
    const [result, setResult] = useState(false)

    useEffect(() => {
        const o = {}
        for (const [key, val] of Object.entries(values)) {
            o[key] = val
        }
        try {
            setResult(!!JSON.parse(_template(condition)(o)))
        } catch (e) {
            AdminToast.show({ intent: 'warning', message: e })
        }
    }, [setResult, values, condition])

    return result ? <>{children}</> : null
}

export default RenderCondition
