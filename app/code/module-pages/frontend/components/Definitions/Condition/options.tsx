/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useEffect, useState } from 'react'
import Forger from './index'
import { Button, FormGroup, InputGroup } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import ValuesInput from '../Utils/ValuesInput'
import Condition from './index'

const ConditionOptions: typeof Condition['options'] = ({ value, setValue }) => {
    const [val, setVal] = useState(() => value)
    useEffect(() => {
        setVal(value)
    }, [value, setVal])

    return (
        <>
            <FormGroup label={<Translated message="Values" />}>
                <ValuesInput id={val.id} value={val.values} onChange={(v) => setVal({ ...val, values: v })} />
            </FormGroup>
            <FormGroup label={<Translated message="Cast" />}>
                <InputGroup value={val.condition} onChange={(e) => setVal({ ...val, condition: e.target.value })} />
            </FormGroup>
            <Button onClick={() => setValue(val)}>
                <Translated message="Update" />
            </Button>
        </>
    )
}

export default ConditionOptions
