/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import Text from './render'
import Options from './options'

export enum Tag {
    h1 = 'h1',
    h2 = 'h2',
    h3 = 'h3',
    h4 = 'h4',
    h5 = 'h5',
    p = 'p',
    span = 'span',
    i = 'i',
    b = 'b',
    label = 'label',
}

export const tags: Array<Tag> = [Tag.b, Tag.h1, Tag.h2, Tag.h3, Tag.h4, Tag.h5, Tag.i, Tag.p, Tag.span, Tag.label]

type TextProperties = {
    text?: string
    tag: Tag
    for?: string
}
const text: ComponentDeclaration<TextProperties> = {
    id: 'text',
    render: Text,
    options: Options,
    defaultProperties: {
        tag: Tag.span,
    },
    meta: {
        icon: 'font',
    },
    config: { haveChildren: false },
}
export default text
