/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import text, { Tag, tags } from './index'
import React, { useCallback } from 'react'
import { FormGroup, HTMLSelect } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import ProvidedInputGroup from '../Utils/ProvidedData/Inputs/ProvidedInputGroup'
import SelectChildrenOfType from '../Utils/SelectChildrenOfType'
import { useRecoilValue } from 'recoil'
import { currentRootComponentId } from '../../../store/layout/store'
import Input from '../Input'

const Text: React.FC<{ id: string; value: string | undefined; onChange: (v: string | undefined) => void }> = ({
    id,
    value,
    onChange,
}) => {
    return (
        <FormGroup label={<Translated message="Text" />}>
            <ProvidedInputGroup id={id} value={value} onChange={(v) => onChange(v)} />
        </FormGroup>
    )
}

const TagOptions = () => (
    <>
        {tags.map((v) => (
            <option key={v} value={v}>
                {v}
            </option>
        ))}
    </>
)

const TagInput: React.FC<{ value: Tag; onChange: (v: Tag) => void }> = ({ value, onChange }) => {
    return (
        <FormGroup label={<Translated message="Tag" />}>
            <HTMLSelect value={value} onChange={(e) => onChange(e.target.value as Tag)}>
                {TagOptions()}
            </HTMLSelect>
        </FormGroup>
    )
}

const ForInput: React.FC<{ value?: string; onChange: (v: string) => void }> = ({ value, onChange }) => {
    const rootId = useRecoilValue(currentRootComponentId)
    return rootId ? (
        <SelectChildrenOfType
            componentId={rootId}
            typeIds={[Input.id]}
            value={value ? [value] : []}
            onChange={(v) => onChange(v[v.length - 1])}
            buttonText={<Translated message="For" />}
        />
    ) : null
}

const Options: typeof text['options'] = ({ value, setValue }) => {
    const onTextChange = useCallback(
        (v: string | undefined) => {
            setValue({ ...value, text: v })
        },
        [value, setValue],
    )

    const onTagChange = useCallback(
        (v: Tag) => {
            setValue({ ...value, tag: v })
        },
        [value, setValue],
    )

    return (
        <>
            <Text id={value.id} value={value.text} onChange={onTextChange} />
            <TagInput value={value.tag} onChange={onTagChange} />
            {value.tag === Tag.label && <ForInput onChange={(v) => setValue({ ...value, for: v })} value={value.for} />}
        </>
    )
}

export default Options
