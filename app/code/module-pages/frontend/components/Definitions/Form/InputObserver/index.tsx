/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { memo, useEffect, useRef } from 'react'
import { useRecoilValue } from 'recoil'
import { body } from '../../../../store/pagebuilder/frame/store'
import { getValFromEl } from '../utils'

const InputObserver: React.FC<{
    id: string
    provided: Record<string, any>
    provide: (v: Record<string, any>) => void
    shouldUpdateValue?: boolean
    formElement: HTMLFormElement
}> = ({ id, provide, provided, shouldUpdateValue, formElement }) => {
    const setData = useRef(provide)
    const data = useRef(provided)

    useEffect(() => {
        const elements = formElement.querySelectorAll<HTMLInputElement>(`#${CSS.escape(id)}`)
        if (elements.length === 1) {
            provide({ ...provided, [elements[0].name]: getValFromEl(elements[0]) })
        }
    }, [formElement])

    useEffect(() => {
        const elements = formElement.querySelectorAll<HTMLInputElement>(`#${CSS.escape(id)}`)
        if (elements.length === 0) return
        const observer = new MutationObserver((mutations) => {
            const newData = { ...data.current }
            mutations.forEach((mutation) => {
                if (mutation.oldValue) {
                    delete newData[mutation.oldValue]
                }
                if (mutation.attributeName) {
                    const el = mutation.target as HTMLInputElement
                    newData[el.name] = shouldUpdateValue ? getValFromEl(el) : ''
                }
            })
            setData.current(newData)
        })
        elements.forEach((e) =>
            observer.observe(e, { attributeFilter: ['name'], attributes: true, attributeOldValue: true }),
        )
        return () => {
            observer.disconnect()
        }
    }, [formElement, id, shouldUpdateValue])
    return null
}

export default InputObserver
