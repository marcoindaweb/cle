/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import Form, { FormType } from './index'
import { HTMLSelect } from '@blueprintjs/core'
import useTranslate from '@linda/module-i18n/frontend/hooks/useTranslate'
import Input from '../Input'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import SelectChildrenOfType from '../Utils/SelectChildrenOfType'

const FormOptions: typeof Form['options'] = ({ value, setValue }) => {
    const { inputsIds, type, id } = value
    const translate = useTranslate()
    return (
        <>
            <HTMLSelect
                value={type}
                onChange={(e) => setValue({ ...value, type: parseInt(e.target.value) as FormType })}
            >
                <option value={FormType.change}>{translate('Update value on input update')}</option>
                <option value={FormType.submit}>{translate('Update value on submit button click')}</option>
            </HTMLSelect>
            <SelectChildrenOfType
                componentId={id}
                typeIds={[Input.id]}
                value={inputsIds}
                onChange={(v) => setValue({ ...value, inputsIds: v })}
                buttonText={<Translated message="Connect Inputs" />}
            />
        </>
    )
}

export default FormOptions
