/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useCallback, useEffect, useRef } from 'react'
import Form, { FormType } from './index'
import { useRecoilValue } from 'recoil'
import { body } from '../../../store/pagebuilder/frame/store'
import _set from 'lodash.set'
import _get from 'lodash.get'
import { getValFromEl } from './utils'
import InputObserver from './InputObserver'

const FormRender: typeof Form['render'] = ({ type, inputsIds, children, provide, provided, id, className }) => {
    const ref = useRef<HTMLFormElement>(null)

    const onSubmit = useCallback(
        (e: Event) => {
            e.preventDefault()
            if (!ref.current) return
            if (type !== FormType.submit) return
            const toProvide = {}

            for (const id of inputsIds) {
                const elements = ref.current.querySelectorAll<HTMLInputElement>(`#${CSS.escape(id)}`)
                if (elements.length > 1) {
                    toProvide[elements[0].name] = []
                    elements.forEach((el) => {
                        if (el.name) {
                            ;(toProvide[el.name] as Array<any>).push(getValFromEl(el))
                        }
                    })
                } else if (elements[0]) {
                    const el = elements[0]
                    if (el.name) {
                        toProvide[el.name] = getValFromEl(el)
                    }
                }
            }
        },
        [type, provide, ref, inputsIds],
    )

    useEffect(() => {
        if (!ref.current) return
        if (type !== FormType.change) return
        const toRemove: Array<[HTMLElement, (e: any) => void]> = []

        for (const id of inputsIds) {
            const elements = ref.current.querySelectorAll<HTMLInputElement>(`#${CSS.escape(id)}`)
            if (elements.length > 1) {
                elements.forEach((el, i) => {
                    if (!el.name) {
                        return
                    }
                    const func = (e) => {
                        const el = e.currentTarget as HTMLInputElement
                        provide({
                            ...provided,
                            [el.name]: _set(_get(provided, el.name, []), `[${i}]`, getValFromEl(el)),
                        })
                    }
                    el.addEventListener('change', func)
                })
            } else if (elements[0]) {
                const el = elements[0]
                if (!el.name) {
                    continue
                }
                const func = (e) => {
                    const el = e.currentTarget as HTMLInputElement
                    provide({
                        ...provided,
                        [el.name]: getValFromEl(el),
                    })
                }
                el.addEventListener('change', func)
                toRemove.push([el, func])
            }
        }

        return () => toRemove.forEach(([el, func]) => el.removeEventListener('change', func))
    }, [inputsIds, type, ref, provide, provided])

    useEffect(() => {
        if (!ref.current) return
        const el = ref.current
        el.addEventListener('submit', onSubmit)
        return () => el.removeEventListener('submit', onSubmit)
    }, [ref, onSubmit])

    return (
        <form id={id} className={className} ref={ref}>
            {inputsIds.map((id) =>
                ref.current ? (
                    <InputObserver
                        key={id}
                        id={id}
                        provided={provided}
                        provide={provide}
                        shouldUpdateValue={type === FormType.change}
                        formElement={ref.current}
                    />
                ) : null,
            )}
            {children}
        </form>
    )
}

export default FormRender
