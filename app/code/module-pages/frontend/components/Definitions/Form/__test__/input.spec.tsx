/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { RecoilRoot, useRecoilValue } from 'recoil'
import { renderHook } from '@testing-library/react-hooks'
import { useRecoilAction } from '../../../../store/recoilAction'
import { add } from '../../../../store/layout/actions'
import { useAddComponentDeclaration } from '../../../../store/componentDeclarations/actions'
import React, { useEffect } from 'react'
import { componentPropertyWithProvidedData } from '../../../../store/layout/store'
import Input from '../../Input'
import { v4 } from 'uuid'
import Position from '../../../../types/Position'

describe('input element', () => {
    it('regression: pass the input ids as an array of string', () => {
        const id = v4()
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>
        const { result } = renderHook(
            () => {
                const addComp = useRecoilAction(add)
                const registerDecl = useAddComponentDeclaration()
                const props = useRecoilValue(componentPropertyWithProvidedData({ component: 'a', key: 'inputIds' }))
                useEffect(() => {
                    registerDecl(Input)
                    addComp({
                        id: Input.id,
                        componentId: 'a',
                        properties: { inputIds: [id] },
                        position: Position.inside,
                    })
                }, [])
                return { props }
            },
            { wrapper },
        )

        expect(result.current.props).toEqual([id])
    })
})
