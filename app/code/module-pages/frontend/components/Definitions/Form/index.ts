/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import FormRender from './render'
import FormOptions from './options'

export enum FormType {
    submit,
    change,
}

type FormProps = {
    type: FormType
    inputsIds: string[]
}

const Form: ComponentDeclaration<FormProps> = {
    id: 'Form',
    meta: {
        icon: 'form',
    },
    render: FormRender,
    options: FormOptions,
    defaultProperties: {
        inputsIds: [],
        type: FormType.submit,
    },
}

export default Form
