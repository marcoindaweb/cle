/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import OptionsWrapperRenderer from './OptionsWrapperRenderer'
import { Button } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import React, { useCallback } from 'react'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { allChildrenOfType, component, currentRootComponentId } from '../../../store/layout/store'
import ComponentInstance from '../../../types/ComponentInstance'
import Form from '../Form'

/**
 * THIS COMPONENT IS USED BY ../Options
 */

type OptionsWrapperData = {
    optionsId: string // The related options component
    setOptionsComponent: { call: (v: ComponentInstance) => void }
    previousRoot: string // The previous root component, will go back to this after updating the options component
}

const OptionsWrapper: ComponentDeclaration<OptionsWrapperData> = {
    id: 'OptionsWrapper',
    config: {
        hidden: true,
    },
    defaultProperties: {
        style: {
            '.options-wrapper-renderer': {
                display: 'flex',
                justifyContent: 'center',
                background: 'rgba(0,0,0,.4)',
                height: '100vh',
                '&>form': {
                    maxWidth: '300px',
                    height: '100%',
                    width: '100%',
                    background: 'white',
                },
            },
        },
        className: 'options-wrapper-renderer',
        optionsId: '',
        previousRoot: '',
        setOptionsComponent: { call: () => {} },
    },
    render: OptionsWrapperRenderer,
    options: Object.assign(
        ({ value }) => {
            const setRoot = useSetRecoilState(currentRootComponentId)
            const children = useRecoilValue(allChildrenOfType([value.id, Form.id]))
            const form = children[0]
            const comp = useRecoilValue(component(form || ''))

            const onClick = useCallback(() => {
                if (!comp) {
                    throw new Error('Please add a form')
                }
                setRoot(value.previousRoot)
                value.setOptionsComponent.call(comp)
            }, [value.previousRoot, comp, setRoot, value.setOptionsComponent])

            return (
                <>
                    <Button onClick={onClick} text={<Translated message="Save" />} />
                </>
            )
        },
        { displayName: 'OptionsWrapperOptions' },
    ),
}

export default OptionsWrapper
