/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useEffect } from 'react'
import OptionsWrapper from './index'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import bp3styles from '!!raw-loader!@blueprintjs/core/lib/css/blueprint.css'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import normalizecssstyles from '!!raw-loader!normalize.css'
import { useAddComponent } from '../../../store/layout/actions'
import Form from '../Form'
import { useRecoilValue } from 'recoil'
import { componentChildrenIds } from '../../../store/layout/store'

const OptionsWrapperRenderer: typeof OptionsWrapper['render'] = ({ children, id, className }) => {
    const addComp = useAddComponent()
    const childrenIds = useRecoilValue(componentChildrenIds(id))
    useEffect(() => {
        if (childrenIds.length === 0) {
            addComp({ id: Form.id, reference: id })
        }
    }, [childrenIds])

    return (
        <>
            <style>
                {bp3styles} {normalizecssstyles}
            </style>
            <div id={id} className={className + ' ' + 'options-wrapper'}>
                {children}
            </div>
        </>
    )
}

export default OptionsWrapperRenderer
