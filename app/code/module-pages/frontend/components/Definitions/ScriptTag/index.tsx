/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import React from 'react'
import { SEO } from '../groups'
import HeadElementRenderer from '../Utils/HeadElement/renderer'
import HeadElementOptions from '../Utils/HeadElement/options'

type ScriptProps = {
    uid: string
    async: boolean
    defer: boolean
    src: string
    referrerpolicy: string
    type: string
}

const ScriptTag: ComponentDeclaration<ScriptProps> = {
    id: 'ScriptTag',
    meta: {
        icon: 'tag',
        displayName: <pre>{'<script/>'}</pre>,
        tags: [SEO],
        group: SEO,
    },
    defaultProperties: {
        uid: '',
        src: '',
        async: false,
        defer: false,
        type: '',
        referrerpolicy: '',
    },
    render: Object.assign(
        ({ uid, src, async, defer, type, referrerpolicy }) => {
            return <HeadElementRenderer values={{ uid, src, async, defer, type, referrerpolicy }} tag="link" />
        },
        { displayName: 'RenderScriptTag' },
    ),
    options: Object.assign(
        ({ value, setValue }) => {
            const { id, uid, src, async, defer, type, referrerpolicy } = value
            return (
                <HeadElementOptions
                    id={id}
                    values={{ uid, src, async, defer, type, referrerpolicy }}
                    setValues={(v) => setValue({ ...value, ...v })}
                />
            )
        },
        { displayName: 'ScriptTagOptions' },
    ),
}

export default ScriptTag
