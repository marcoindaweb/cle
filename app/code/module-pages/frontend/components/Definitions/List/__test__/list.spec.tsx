/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { renderHook } from '@testing-library/react-hooks'
import { RecoilRoot } from 'recoil'
import React from 'react'
import { useAddComponentDeclaration } from '../../../../store/componentDeclarations/actions'
import { add, useAddComponentOrSymbol } from '../../../../store/layout/actions'
import { useRecoilAction } from '../../../../store/recoilAction'

describe('list component', () => {
    it('provide each item of the selected array to each component', () => {
        const wrapper = ({ children }) => <RecoilRoot>{children}</RecoilRoot>

        const result = renderHook(() => {
            const addDecl = useAddComponentDeclaration()
            const addComponent = useRecoilAction(add)
        })
    })
})
