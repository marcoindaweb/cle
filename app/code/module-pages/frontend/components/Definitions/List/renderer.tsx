/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import List from './index'
import { useRecoilValue } from 'recoil'
import { componentChildrenIds, providedData } from '../../../store/layout/store'
import Component from '../../Builder/Main/Page/Layout/Component'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import _get from 'lodash.get'

const ListRenderer: typeof List['render'] = ({ dataKey, id, indexes }) => {
    const chilrenIds = useRecoilValue(componentChildrenIds(id))
    const provided = useRecoilValue(providedData({ component: id, indexes }))
    const data = _get(provided, dataKey || '', undefined)
    return dataKey ? (
        <>
            {Array.isArray(data) ? (
                data.map((_, i) => {
                    return chilrenIds.map((v) => <Component key={v + i} id={v} indexes={[...(indexes || []), i]} />)
                })
            ) : (
                <Translated message={"The selected data isn't of type array"} />
            )}
        </>
    ) : (
        <Translated message="Select some data from the options" />
    )
}

export default ListRenderer
