/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import ListRenderer from './renderer'
import ListOptions from './options'

export const LIST_DATA_KEY_PROPERTY_KEY = 'dataKey'

type ListProps = {
    [LIST_DATA_KEY_PROPERTY_KEY]?: string
}

const List: ComponentDeclaration<ListProps> = {
    id: 'List',
    meta: {
        icon: 'list',
    },
    render: ListRenderer,
    options: ListOptions,
}

export default List
