/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import MetaTag from './index'
import HeadElementRenderer from '../Utils/HeadElement/renderer'

const MetaTagRender: typeof MetaTag['render'] = ({ name, content, uid }) => {
    return <HeadElementRenderer values={{ name, content, uid }} tag="meta" />
}

export default MetaTagRender
