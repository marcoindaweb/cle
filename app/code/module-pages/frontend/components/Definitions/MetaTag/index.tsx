/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import ComponentDeclaration from '../../../types/ComponentDeclaration'
import MetaTagRender from './render'
import MetaTagOptions from './options'
import { SEO } from '../groups'

type MetaTagProps = {
    uid: string
    name: string
    content: string
}

const MetaTag: ComponentDeclaration<MetaTagProps> = {
    id: 'Meta Tag',
    meta: {
        icon: 'tag',
        displayName: <pre>{'<meta/>'}</pre>,
        tags: [SEO],
        group: SEO,
    },
    defaultProperties: {
        uid: '',
        name: '',
        content: '',
    },
    render: MetaTagRender,
    options: MetaTagOptions,
}

export default MetaTag
