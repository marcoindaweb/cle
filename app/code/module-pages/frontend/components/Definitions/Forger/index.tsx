/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import React from 'react'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import RenderCaster from './render'
import CasterOptions from './options'
import icons from '../../../icons'

type ForgerProperties = {
    values: Record<string, string>
    cast: string
}

type ForgerProvidedData = {
    value: string
}

const Forger: ComponentDeclaration<ForgerProperties, ForgerProvidedData> = {
    id: 'Forger',
    meta: {
        tags: ['data manipulation'],
        description: (
            <>
                <Translated message="Takes a series of values and a cast string. Provide the casted string" />
                <br />
                <Translated message='Example: values = { name: "Bob", company: "Google" } cast = "My name is <%= name %>, and I work for <%= company %>", The resulting string will be "My name is Bob, and I work for Google"' />
            </>
        ),
        icon: 'build',
    },
    config: {
        isAbstract: true,
    },
    defaultProperties: {
        values: {},
        cast: '',
    },
    render: RenderCaster,
    options: CasterOptions,
}

export default Forger
