/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useEffect } from 'react'
import Forger from './index'
import _template from 'lodash.template'

const RenderCaster: typeof Forger['render'] = ({ id, children, provide, cast, values }) => {
    useEffect(() => {
        const o = {}
        for (const [key, val] of Object.entries(values)) {
            o[key] = val
        }
        try {
            provide({ value: _template(cast)(o) })
        } catch (e) {
            provide({ value: e.toString() })
        }
    }, [cast, values, provide])

    return <>{children}</>
}

export default RenderCaster
