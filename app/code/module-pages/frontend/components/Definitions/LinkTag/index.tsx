/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import React from 'react'
import { SEO } from '../groups'
import HeadElementRenderer from '../Utils/HeadElement/renderer'
import HeadElementOptions from '../Utils/HeadElement/options'

type LinkProps = {
    uid: string
    rel: string
    href: string
    type: string
    hreflang: string
    media: string
    sizes: string
    title: string
    imagesizes: string
    imagesrcset: string
}

const LinkTag: ComponentDeclaration<LinkProps> = {
    id: 'LinkTag',
    meta: {
        icon: 'tag',
        displayName: <pre>{'<link/>'}</pre>,
        tags: [SEO],
        group: SEO,
    },
    defaultProperties: {
        uid: '',
        rel: '',
        href: '',
        type: '',
        hreflang: '',
        media: '',
        sizes: '',
        title: '',
        imagesizes: '',
        imagesrcset: '',
    },
    render: Object.assign(
        ({ uid, rel, href, type, hreflang, media, sizes, title, imagesizes, imagesrcset }) => {
            return (
                <HeadElementRenderer
                    values={{ uid, rel, href, type, hreflang, media, sizes, title, imagesizes, imagesrcset }}
                    tag="link"
                />
            )
        },
        { displayName: 'RenderLinkTag' },
    ),
    options: Object.assign(
        ({ value, setValue }) => {
            const { id, uid, rel, href, type, hreflang, media, sizes, title, imagesizes, imagesrcset } = value
            return (
                <HeadElementOptions
                    id={id}
                    values={{ uid, rel, href, type, hreflang, media, sizes, title, imagesizes, imagesrcset }}
                    setValues={(v) => setValue({ ...value, ...v })}
                />
            )
        },
        { displayName: 'LinkTagOptions' },
    ),
}

export default LinkTag
