/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import LinkRenderer from './renderer'
import LinkOptions from './options'

type LinkProperties = {
    to: string
    target: '_blank' | '_self' | '_top' | '_parent'
}

const Link: ComponentDeclaration<LinkProperties> = {
    id: 'Link',
    meta: {
        icon: 'link',
    },
    defaultProperties: {
        to: '',
        target: '_self',
    },
    render: LinkRenderer,
    options: LinkOptions,
}

export default Link
