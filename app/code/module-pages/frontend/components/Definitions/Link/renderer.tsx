/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import Link from './index'
import React, { useCallback, useEffect, useRef } from 'react'
import { useSetRecoilState } from 'recoil'
import { path } from '../../../store/pagebuilder/router/store'

const InternalLink: React.FC<{ id: string; to: string; className?: string }> = ({ to, id, className, children }) => {
    const setPath = useSetRecoilState(path)
    const ref = useRef<HTMLAnchorElement>(null)

    const handleClick = useCallback(
        (e: MouseEvent) => {
            e.preventDefault()
            setPath(to)
        },
        [to, setPath],
    )

    useEffect(() => {
        if (ref.current) {
            const el = ref.current
            el.addEventListener('click', handleClick)
            return () => el.removeEventListener('click', handleClick)
        }
    }, [ref, handleClick])

    return (
        <>
            <a ref={ref} href={to} id={id} className={className}>
                {children}
            </a>
        </>
    )
}

const LinkRenderer: typeof Link['render'] = ({ to, target, className, id, children }) => {
    return target === '_self' ? (
        <InternalLink id={id} to={to} className={className}>
            {children}
        </InternalLink>
    ) : (
        <a id={id} href={to} target={target}>
            {children}
        </a>
    )
}

export default LinkRenderer
