/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useEffect, useState } from 'react'
import Link from './index'
import { useForm } from 'react-hook-form'
import { Button, HTMLSelect, InputGroup } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'

const Options = () => (
    <>
        {['_blank', '_parent', '_self', '_top'].map((v) => (
            <option key={v} value={v}>
                {v}
            </option>
        ))}
    </>
)

const LinkOptions: typeof Link['options'] = ({ value, setValue }) => {
    const [v, setV] = useState(() => value)
    useEffect(() => setV(value), [value])

    return (
        <>
            <InputGroup value={v.to} onChange={(e) => setV({ ...v, to: e.target.value })} />
            <HTMLSelect value={v.target} onChange={(e) => setV({ ...v, target: e.target.value as typeof v.target })}>
                <Options />
            </HTMLSelect>
            <Button onClick={() => setValue(v)} text={<Translated message="Update" />} />
        </>
    )
}

export default LinkOptions
