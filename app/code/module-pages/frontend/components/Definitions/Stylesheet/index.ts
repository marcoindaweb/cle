/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'

const Stylesheet: ComponentDeclaration = {
    id: 'stylesheet',
    render: () => null,
    config: {
        isAbstract: true,
        showStyleAsCode: true,
        haveStyle: true,
        haveChildren: false,
        haveOptions: false,
    },
    meta: {
        icon: 'style',
    },
}

export default Stylesheet
