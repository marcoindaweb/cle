/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useEffect, useState } from 'react'
import { useAddComponentDeclaration } from '../../store/componentDeclarations/actions'
import span from './RichText'
import ComponentDeclaration from '../../types/ComponentDeclaration'
import div from './Div'
import Stylesheet from './Stylesheet'
import Route from './Route'
import text from './Text'
import DataProvider from './DataProvider'
import Forger from './Forger'
import List from './List'
import Group from './Group'
import MetaTag from './MetaTag'
import LinkTag from './LinkTag'
import TitleTag from './TitleTag'
import ScriptTag from './ScriptTag'
import Link from './Link'
import Form from './Form'
import Input from './Input'
import OptionsWrapper from './OptionsWrapper'
import Options from './Options'
import Condition from './Condition'
import Body from './Body'

const Definitions = () => {
    const addDeclaration = useAddComponentDeclaration()
    const toAdd: ComponentDeclaration[] = [
        Stylesheet,
        span,
        div,
        Route,
        text,
        DataProvider,
        Forger,
        List,
        Group,
        TitleTag,
        MetaTag,
        LinkTag,
        ScriptTag,
        Link,
        Form,
        Input,
        OptionsWrapper,
        Options,
        Condition,
        Body,
    ]
    const [willAdd, setWillAdd] = useState(0)

    useEffect(() => {
        if (willAdd === toAdd.length) {
            return
        }
        addDeclaration(toAdd[willAdd])
        if (willAdd < toAdd.length) {
            setWillAdd(willAdd + 1)
        }
    }, [willAdd, toAdd, setWillAdd, addDeclaration])
    return null
}

export default Definitions
