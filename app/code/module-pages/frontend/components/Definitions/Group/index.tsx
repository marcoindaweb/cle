/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import ComponentDeclaration from '../../../types/ComponentDeclaration'

const RenderGroup: React.FC = ({ children }) => {
    return <>{children}</>
}

const Group: ComponentDeclaration = {
    id: 'Group',
    render: RenderGroup,
    config: {
        isAbstract: true,
    },
    meta: {
        icon: 'folder-close',
        group: 'utilities',
    },
}

export default Group
