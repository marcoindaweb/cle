/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import React from 'react'
import { createUseStyles } from 'react-jss'

const useStyle = createUseStyles({
    main: {
        padding: '10px',
        border: '1px solid rgba(0,0,0,.1)',
    },
})

const div: ComponentDeclaration = {
    id: 'div',
    render: ({ children, id, className }) => {
        return (
            <div id={id} className={className}>
                {children}
            </div>
        )
    },
    defaultProperties: {},
    meta: {
        icon: 'square',
    },
    config: {
        availableTriggers: ['click', 'mouseEnter', 'mouseLeave', 'mouseOver'],
    },
}

export default div
