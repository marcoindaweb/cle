/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { CDO } from '../../../types/ComponentDeclaration'
import span, { SpanProperties } from './index'
import CKEditor from '@ckeditor/ckeditor5-react'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import React from 'react'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
    editor: {
        color: 'black',
    },
})

const Options: typeof span['options'] = ({ value, setValue }) => {
    const { editor } = useStyles()
    return (
        <div className={editor}>
            <CKEditor
                editor={ClassicEditor}
                data={value.text}
                onChange={(event, editor) => {
                    const data = editor.getData()
                    setValue({ text: data })
                }}
            />
        </div>
    )
}

export default Options
