/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import Span from './render'
import Options from './options'

export type SpanProperties = {
    text: string
}

const span: ComponentDeclaration<SpanProperties> = {
    id: 'RichText',
    render: Span,
    defaultProperties: { text: 'lorem ipsum' },
    options: Options,
    meta: {
        icon: 'manually-entered-data',
    },
}

export default span
