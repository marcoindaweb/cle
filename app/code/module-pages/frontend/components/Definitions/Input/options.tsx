/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useEffect, useState } from 'react'
import { Button, FormGroup, HTMLSelect } from '@blueprintjs/core'
import Input, { InputProps } from './index'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import OptionsRenderer, { Option } from '../Utils/Helpers/OptionsRenderer'

const types: Array<InputProps['type']> = [
    'text',
    'radio',
    'checkbox',
    'file',
    'hidden',
    'email',
    'password',
    'url',
    'number',
    'range',
]

const options: Array<[string[], Option[]]> = [
    [
        ['text', 'email', 'password', 'url'],
        [
            {
                name: 'required',
                type: 'boolean',
            },
            {
                name: 'name',
                type: 'string',
            },
            {
                name: 'placeholder',
                type: 'string',
            },
            {
                name: 'value',
                label: 'defaultValue',
                type: 'string',
            },
            {
                name: 'pattern',
                type: 'string',
            },
            {
                name: 'minlength',
                type: 'number',
            },
            {
                name: 'maxlength',
                type: 'number',
            },
        ],
    ],
    [
        ['checkbox', 'radio'],
        [
            {
                name: 'required',
                type: 'boolean',
            },
            {
                name: 'name',
                type: 'string',
            },
            {
                name: 'checked',
                type: 'boolean',
            },
            {
                name: 'indeterminate',
                type: 'boolean',
            },
        ],
    ],
    [
        ['hidden'],
        [
            {
                name: 'name',
                type: 'string',
            },
            {
                name: 'value',
                type: 'string',
            },
        ],
    ],
    [
        ['file'],
        [
            {
                name: 'name',
                type: 'string',
            },
            {
                name: 'multiple',
                type: 'boolean',
            },
        ],
    ],
]

const InputOptions: typeof Input['options'] = ({ value, setValue }) => {
    const [v, setV] = useState<Omit<typeof value, 'indexes'>>(value)
    useEffect(() => setV(value), [value])

    return (
        <>
            <FormGroup label={<Translated message="type" />}>
                <HTMLSelect
                    value={v.type}
                    /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
                    // @ts-ignore
                    onChange={(e) => setV({ ...v, type: e.target.value })}
                >
                    {types.map((v) => (
                        <option key={v} value={v}>
                            {v}
                        </option>
                    ))}
                </HTMLSelect>
            </FormGroup>
            <OptionsRenderer
                /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
                // @ts-ignore
                value={v}
                options={options.find(([types]) => types.includes(v.type))?.[1] || []}
                onChange={(val) => setV({ ...v, ...val })}
                id={v.id}
            />
            <Button
                /* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
                // @ts-ignore
                onClick={() => setValue({ ...value, ...v })}
                text={<Translated message="Update" />}
            />
        </>
    )
}

export default InputOptions
