/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import InputRender from './render'
import InputOptions from './options'

export type TextInputProps = {
    type: 'text' | 'password' | 'url'
    autocomplete?: string
    maxlength?: number
    minlength?: number
    pattern?: string
    placeholder?: string
    readonly?: boolean
    size?: number
    value: string
}

export type PasswordInputProps = TextInputProps & { type: 'password' }
export type UrlInputProps = TextInputProps & { type: 'url' }

export type EmailInputProps = {
    type: 'email'
    multiple?: boolean
    value?: string
}

export type CheckboxInputProps = {
    type: 'checkbox'
    checked?: boolean
    indeterminate?: true
}

export type FileInputProps = {
    type: 'file'
    accept?: string
    multiple?: boolean
    capture?: string
    files?: FileList
}

export type HiddenInputProps = {
    type: 'hidden'
    value?: string
}

export type NumberInputProps = {
    type: 'number'
    value?: number
}

export type RadioInputProps = {
    type: 'radio'
    value?: string
    checked?: boolean
}

export type RangeInputProps = {
    type: 'range'
    value?: number
    min?: number
    max?: number
    step?: number
}

export type InputProps = (
    | TextInputProps
    | EmailInputProps
    | CheckboxInputProps
    | FileInputProps
    | HiddenInputProps
    | NumberInputProps
    | RadioInputProps
    | RangeInputProps
    | PasswordInputProps
    | UrlInputProps
) & { name?: string; required?: boolean }

const Input: ComponentDeclaration<InputProps> = {
    id: 'Input',
    meta: {
        icon: 'text-highlight',
    },
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    defaultProperties: {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        type: 'text' as const,
    },
    render: InputRender,
options: InputOptions,
}

export default Input
