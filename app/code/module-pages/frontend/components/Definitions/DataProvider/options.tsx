/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Button, Colors, Drawer, FormGroup, Position, Switch, Tag } from '@blueprintjs/core'
import React, { useEffect, useState } from 'react'
import EntitySelect from '@linda/module-content-manager/frontend/components/EntitySelect'
import EntityPropertySelect from '@linda/module-content-manager/frontend/components/EntityPropertySelect'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import DataProvider, { ProviderKind } from './index'
import EntityFilterBuilder from '@linda/module-content-manager/frontend/components/EntityFilterBuilder'
import { createUseStyles } from 'react-jss'
import { useRecoilValue } from 'recoil'
import { pagebuilderRoot } from '../../../store/pagebuilder/elements'
import useTranslate from '@linda/module-i18n/frontend/hooks/useTranslate'

const useStyle = createUseStyles({
    propertySelect: {
        borderRadius: '3px',
        border: Colors.DARK_GRAY5,
        padding: '4px',
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%',
        '& > *:not(:last-child)': {
            marginRight: '4px',
        },
    },
})

const Options: typeof DataProvider['options'] = ({ value, setValue }) => {
    const [v, setV] = useState(value)
    const [showFilters, setShowFilters] = useState(false)
    const buiderRoot = useRecoilValue(pagebuilderRoot)
    const translate = useTranslate()

    const { propertySelect } = useStyle()

    useEffect(() => {
        setV(value)
    }, [value, setV])

    return (
        <>
            <Switch
                checked={v.query === 'findOne'}
                onChange={() => setV({ ...v, query: v.query === 'findOne' ? 'find' : 'findOne' })}
                innerLabel={translate('all')}
                innerLabelChecked={translate('one')}
            />
            <EntitySelect
                value={v.entity}
                onChange={(e) => {
                    setV({ ...v, entity: e.id })
                }}
            />
            {v.entity && (
                <>
                    <EntityPropertySelect
                        withRelations
                        withManyRelations
                        canSelectRelation
                        id={v.entity}
                        onChange={(p) => {
                            setV({ ...v, properties: [...(v.properties || []), p] })
                        }}
                    >
                        <div className={propertySelect}>
                            {v.properties?.map((p, i) => {
                                const name = Array.isArray(p) ? p.map((e) => e.name).join('.') : p.name
                                return (
                                    <Tag
                                        key={name}
                                        onRemove={() => {
                                            setV({
                                                ...v,
                                                properties: v.properties?.filter((_, index) => index !== i),
                                            })
                                        }}
                                    >
                                        {name}
                                    </Tag>
                                )
                            })}
                        </div>
                        <Button icon="add" text={<Translated message="Add property" />} />
                    </EntityPropertySelect>
                    <FormGroup inline label={<Translated message="Use route as filter" />}>
                        <Switch
                            checked={v.useRouteAsFilter}
                            onChange={() => setV({ ...v, useRouteAsFilter: !v.useRouteAsFilter })}
                        />
                    </FormGroup>
                    {!v.useRouteAsFilter && (
                        <>
                            <Drawer
                                portalContainer={buiderRoot}
                                position={Position.BOTTOM}
                                isOpen={showFilters}
                                title={<Translated message="Filters" />}
                                onClose={() => setShowFilters(false)}
                            >
                                <EntityFilterBuilder
                                    value={v.filter || {}}
                                    onChange={(e) => setV({ ...v, filter: e })}
                                    entityId={v.entity}
                                />
                            </Drawer>
                            <Button onClick={() => setShowFilters(!showFilters)}>
                                <Translated message="Filters" />
                            </Button>
                        </>
                    )}
                </>
            )}
            <Button onClick={() => setValue(v)}>
                <Translated message="Update" />
            </Button>
        </>
    )
}

export default Options
