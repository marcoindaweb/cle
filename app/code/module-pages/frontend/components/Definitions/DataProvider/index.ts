/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import { Entity, EntityProperty } from 'types/graphql'
import DataProviderRender from './render'
import Options from './options'

export enum ProviderKind {
    provided,
    custom,
}

export type DataProviderProps = {
    entity?: Entity['id']
    properties?: Array<EntityProperty | EntityProperty[]>
    filter?: Record<string, any>
    query: 'find' | 'findOne'
    useRouteAsFilter?: boolean
    kind: ProviderKind
}

export type DataProviderProvided = {
    entity?: Entity
    data?: Record<string, any>
}

const DataProvider: ComponentDeclaration<DataProviderProps, DataProviderProvided> = {
    id: 'dataprovider',
    config: {
        isAbstract: true,
    },
    defaultProperties: {
        kind: ProviderKind.provided,
        query: 'findOne',
    },
    render: DataProviderRender,
    options: Options,
    meta: {
        icon: 'database',
    },
}

export default DataProvider
