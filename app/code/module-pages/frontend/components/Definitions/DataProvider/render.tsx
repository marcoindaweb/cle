/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import DataProvider, { DataProviderProvided } from './index'
import { useBuildedQuery } from '@linda/module-content-manager/frontend/hooks/useBuildedQuery'
import React, { useEffect, useState } from 'react'
import { Entity, EntityProperty } from 'types/graphql'
import { WithProvide } from '../../../types/ComponentDeclaration'
import { useRecoilValue } from 'recoil'
import { componentData, parentOfType, providedData } from '../../../store/layout/store'
import Route from '../Route'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import _set from 'lodash.set'

const RouteFilter: React.FC<{ children: (v: Record<string, any>) => React.ReactElement; id: string }> = ({
    id,
    children,
}) => {
    return null
    // const route = useRecoilValue(parentOfType({ type: Route.id, component: id }))
    // const data = useRecoilValue(componentData(route?.id || ''))
    // return route ? (
    //     data && data.params ? (
    //         children(data.params)
    //     ) : (
    //         <Translated message="Route can't be used since it doesn't have parameters" />
    //     )
    // ) : (
    //     <Translated message="There isn't any route element as parent" />
    // )
}

const FetchData: React.FC<
    {
        entity: Entity['id']
        properties: Array<EntityProperty | EntityProperty[]>
        filter: Record<string, any>
        query: 'find' | 'findOne'
    } & WithProvide<DataProviderProvided>
> = (props) => {
    const { filter, query, entity, properties, provide, provided, children } = props

    const builded = useBuildedQuery(entity, query, properties, {
        skip: !entity,
        variables: { filter },
    })

    useEffect(() => {
        if (builded.loading) {
            let data = {}

            for (const pp of properties) {
                let prev: EntityProperty | undefined = undefined
                for (const p of Array.isArray(pp) ? pp : [pp]) {
                    data = _set(data, `${typeof prev !== 'undefined' ? prev.name + '.' : ''}${p.name}`, undefined)
                    prev = p
                }
            }

            provide({ ...provided, data })
            return
        }

        if (builded.data) {
            const k = Object.keys(builded.data)[0]
            if (builded.data[k]) {
                provide({ ...provided, data: builded.data[k] })
            }
        }
    }, [builded.data, builded.loading])

    return builded.loading ? null : <>{children}</>
}

const DataProviderRender: typeof DataProvider['render'] = (props) => {
    const { id, className, children, useRouteAsFilter, provided, provide, query, properties, entity, filter } = props
    const [usingRoute, setUsingRoute] = useState<boolean>(useRouteAsFilter || false)

    useEffect(() => {
        if (!!useRouteAsFilter !== usingRoute) {
            setUsingRoute(!!useRouteAsFilter)
        }
    }, [useRouteAsFilter, usingRoute, setUsingRoute])

    return (
        <div id={id} className={className}>
            {!!query && !!entity && !!properties ? (
                usingRoute ? (
                    <RouteFilter id={id}>
                        {(f) => (
                            <FetchData
                                entity={entity}
                                properties={properties}
                                filter={f}
                                query={query}
                                provide={provide}
                                provided={provided}
                            >
                                {children}
                            </FetchData>
                        )}
                    </RouteFilter>
                ) : (
                    filter && (
                        <FetchData
                            filter={filter}
                            provide={provide}
                            provided={provided}
                            entity={entity}
                            properties={properties}
                            query={query}
                        >
                            {children}
                        </FetchData>
                    )
                )
            ) : null}
        </div>
    )
}

export default DataProviderRender
