/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import ComponentInstance from '../../../types/ComponentInstance'
import OptionsRenderer from './render'
import OptionsOptions from './options'

export type OptionsData = {
    component?: ComponentInstance
    formId?: string
    options: Record<string, any>
    relatedWrapperId?: string
}

const Options: ComponentDeclaration<OptionsData> = {
    id: 'Options',
    render: OptionsRenderer,
    options: OptionsOptions,
    defaultProperties: {
        options: {},
        relatedWrapperId: '',
    },
    meta: {
        icon: 'settings',
    },
}

export default Options
