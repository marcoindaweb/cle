/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import Options from './index'
import { useEffect } from 'react'
import React from 'react'

const OptionsRenderer: typeof Options['render'] = ({ provide, children, options }) => {
    useEffect(() => {
        provide(options)
    }, [options, provide])
    return <>{children}</>
}

export default OptionsRenderer
