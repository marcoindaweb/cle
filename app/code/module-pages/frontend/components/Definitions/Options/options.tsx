/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import Options, { OptionsData } from './index'
import Component from '../../Builder/Main/Page/Layout/Component'
import React, { useCallback, useEffect, useState } from 'react'
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import {
    component,
    component as pbComponent,
    componentData, componentIsDisabled,
    componentProperties,
    currentRootComponentId,
    providedData,
    rootComponentsIds,
} from '../../../store/layout/store'
import Form from '../Form'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import SelectChildrenOfType from '../Utils/SelectChildrenOfType'
import { v4 } from 'uuid'
import OptionsWrapper from '../OptionsWrapper'
import { Button } from '@blueprintjs/core'
import ComponentInstance from '../../../types/ComponentInstance'
import { createComponent, useAddComponent } from '../../../store/layout/actions'
import { useRecoilAction } from '../../../store/recoilAction'

const SwitchToOptionsComponent: React.FC<{
    id: string
    wrapperId: string
    setOptionsComponent: (v: ComponentInstance) => void
    formComponent?: ComponentInstance
}> = ({ id, wrapperId, setOptionsComponent, formComponent }) => {
    const rootId = wrapperId
    const rootComponents = useRecoilValue(rootComponentsIds)
    const [currentRoot, setCurrentRoot] = useRecoilState(currentRootComponentId)
    const setComponent = useSetRecoilState(component(rootId))
    const createComp = useRecoilAction(createComponent)
    const setIsRootDisabled = useSetRecoilState(componentIsDisabled(rootId))
    const properties = {
        ...OptionsWrapper.defaultProperties,
        optionsId: id,
        previousRoot: currentRoot,
        setOptionsComponent: { call: setOptionsComponent },
    }
    const onSwitch = useCallback(() => {
        if (!rootComponents.includes(rootId)) {
            createComp({
                properties,
                declaration: OptionsWrapper,
                componentId: rootId,
            })
            setIsRootDisabled(true)
        }
        setCurrentRoot(rootId)
        setComponent({
            alias: 'Options Form',
            componentDeclarationId: OptionsWrapper.id,
            id: rootId,
            properties,
            children: formComponent ? [formComponent] : [],
        })
    }, [rootComponents, rootId, setComponent, setCurrentRoot])

    return (
        <Button
            onClick={onSwitch}
            disabled={currentRoot === rootId}
            text={<Translated message="Build the options" />}
        />
    )
}

const Renderer: React.FC<Pick<OptionsData, 'component'>> = ({ component }) => {
    const [actualComponent, setActualComponent] = useRecoilState(pbComponent(component?.id || ''))

    useEffect(() => {
        if (!actualComponent && component) {
            setActualComponent(component)
        }
    }, [actualComponent, component, setActualComponent])

    return component ? <Component id={component?.id} /> : null
}

const DataProvider: React.FC<{
    formId: string
    currentData: Record<string, any>
    setData: (v: Record<string, any>) => void
}> = ({ formId, setData, currentData }) => {
    const [formData, setFormData] = useRecoilState(componentData({ component: formId }))
    useEffect(() => {
        if (Object.keys(currentData).length > 0 && Object.keys(formData).length === 0) {
            setFormData(currentData)
        }
    }, [currentData, formData])
    useEffect(() => {
        if (formData) {
            setData(formData)
        }
    }, [formData])

    return null
}

const OptionsOptions: typeof Options['options'] = ({ value, setValue }) => {
    useEffect(() => {
        if (!value.relatedWrapperId) {
            setValue({ ...value, relatedWrapperId: v4() })
        }
    }, [value.relatedWrapperId])

    useEffect(() => {
        if (!value.formId && value.component) {
            setValue({ ...value, formId: value.component.id })
        }
    }, [value.component, value.formId])

    return value.relatedWrapperId ? (
        <>
            <SwitchToOptionsComponent
                id={value.id}
                wrapperId={value.relatedWrapperId}
                setOptionsComponent={(v) => setValue({ ...value, component: v })}
                formComponent={value.component}
            />
            <Renderer component={value.component} />
            {value.formId && (
                <DataProvider
                    currentData={value.options}
                    formId={value.formId}
                    setData={(v) => setValue({ ...value, options: v })}
                />
            )}
        </>
    ) : null
}

export default OptionsOptions
