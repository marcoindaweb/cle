/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration from '../../../types/ComponentDeclaration'
import React from 'react'
import { SEO } from '../groups'
import HeadElementRenderer from '../Utils/HeadElement/renderer'
import HeadElementOptions from '../Utils/HeadElement/options'

type TitleProps = {
    text: string
}

const TitleTag: ComponentDeclaration<TitleProps> = {
    id: 'TitleTag',
    meta: {
        icon: 'tag',
        displayName: <pre>{'<title/>'}</pre>,
        tags: [SEO],
        group: SEO,
    },
    defaultProperties: {
        text: '',
    },
    render: Object.assign(
        ({ text }) => {
            return <HeadElementRenderer values={{ uid: 'title', text }} tag="title" />
        },
        { displayName: 'RenderTitleTag' },
    ),
    options: Object.assign(
        ({ value, setValue }) => {
            const { id, text } = value
            return <HeadElementOptions id={id} values={{ text }} setValues={(v) => setValue({ ...value, ...v })} />
        },
        { displayName: 'TitleTagOptions' },
    ),
}

export default TitleTag
