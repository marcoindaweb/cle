/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import { FormGroup, InputGroup, NumericInput, Switch } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import ProvidedInputWrapper from '../ProvidedData/Inputs/ProvidedInputWrapper'
import ToggleButton from '@linda/module-admin/frontend/components/ToggleButton'

export type Option = {
    name: string
    label?: string
    type: 'string' | 'number' | 'boolean'
}

const OptionsRenderer: React.FC<{
    options: Option[]
    onChange: (v: Record<string, string | number | boolean | undefined>) => void
    value: Record<string, string | number | boolean | undefined>
    id: string
}> = ({ options, onChange, value, id }) => {
    return (
        <>
            {options.map((v) => (
                <OptionRenderer
                    id={id}
                    key={v.name}
                    {...v}
                    value={value[v.name]}
                    onChange={([key, val]) => onChange({ ...value, [key]: val })}
                />
            ))}
        </>
    )
}

const OptionRenderer: React.FC<
    Option & {
        onChange: (v: [string, string | number | boolean | undefined]) => void
        value: string | number | boolean | undefined
        id: string
    }
> = ({ name, label, type, value, onChange, id }) => {
    let Input
    switch (type) {
        case 'number':
            Input = (
                <NumericInput
                    value={value as number | undefined}
                    onChange={(e) => onChange([name, parseInt(e.target.value)])}
                />
            )
            break
        case 'boolean':
            Input = (
                <ToggleButton
                    value={value as boolean | undefined}
                    onChange={(v) => onChange([name, v])}
                    indeterminate
                />
            )
            break
        case 'string':
            Input = (
                <InputGroup
                    value={(value as string | undefined) || ''}
                    onChange={(e) => onChange([name, e.target.value])}
                />
            )
            break
    }
    return (
        <FormGroup label={<Translated message={label || name} />}>
            <ProvidedInputWrapper
                wrapped
                value={value}
                id={id}
                onChange={(v) => onChange([name, v])}
                selectorProps={{ onlyKey: true, allowArrays: false }}
            >
                {Input}
            </ProvidedInputWrapper>
        </FormGroup>
    )
}

export default OptionsRenderer
