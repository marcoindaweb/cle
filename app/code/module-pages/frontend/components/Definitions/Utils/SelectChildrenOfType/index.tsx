/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useState } from 'react'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { hoveredId } from '../../../../store/pagebuilder/components/store'
import { componentAlias } from '../../../../store/pagebuilder/aliases/store'
import { Button, IButtonProps, Menu, MenuItem, Popover } from '@blueprintjs/core'
import { allChildrenOfType } from '../../../../store/layout/store'
import Input from '../../Input'
import Translated from '@linda/module-i18n/frontend/components/Translated'

const InputsIdsItem: React.FC<{ id: string; selected: boolean; onClick: () => void }> = ({ id, selected, onClick }) => {
    const setHover = useSetRecoilState(hoveredId)
    const alias = useRecoilValue(componentAlias(id))

    return (
        <MenuItem
            active={selected}
            text={alias}
            onClick={onClick}
            onMouseEnter={() => setHover(id)}
            onMouseLeave={() => setHover(undefined)}
        />
    )
}

const SelectChildrenOfType: React.FC<{
    componentId: string
    typeIds: string[]
    value: string[]
    onChange: (v: string[]) => void
    buttonText?: IButtonProps['text']
}> = ({ componentId, value, onChange, typeIds, buttonText }) => {
    const ids = useRecoilValue(allChildrenOfType([componentId, typeIds]))
    const [open, setOpen] = useState(false)
    return (
        <Popover
            isOpen={open}
            target={
                <Button
                    minimal
                    text={buttonText || <Translated message="Select a component" />}
                    onClick={() => setOpen(!open)}
                />
            }
            content={
                <>
                    <Button minimal icon="cross" onClick={() => setOpen(false)} />
                    <Menu>
                        {ids.map((i) => (
                            <InputsIdsItem
                                key={i}
                                id={i}
                                selected={value.includes(i)}
                                onClick={() =>
                                    value.includes(i) ? onChange(value.filter((v) => v !== i)) : onChange([...value, i])
                                }
                            />
                        ))}
                    </Menu>
                </>
            }
        />
    )
}

export default SelectChildrenOfType
