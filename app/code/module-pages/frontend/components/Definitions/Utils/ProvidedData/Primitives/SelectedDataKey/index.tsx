/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import { useRecoilValue } from 'recoil'
import { componentAlias } from '../../../../../../store/pagebuilder/aliases/store'

/**
 * Render a provided data key selector to a user readable string
 * @param value
 * @constructor
 */

const SelectedDataKey: React.FC<{ value: string; children?: (v: string) => React.ReactNode }> = ({
    value,
    children,
}) => {
    const parts = value.replace(/^{{|}}$/g, '').split('.')
    const id = parts[0]
    const alias = useRecoilValue(componentAlias(id))
    const text = alias.length > 0 ? alias + '.' + parts.splice(1).join('.') : value
    return children ? <>{children(text)}</> : <>{text}</>
}

export default SelectedDataKey
