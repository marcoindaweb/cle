/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilValue, useSetRecoilState } from 'recoil'
import { componentDeclarationId, providedData } from '../../../../../../store/layout/store'
import { Menu, MenuItem, Popover } from '@blueprintjs/core'
import React, { useCallback } from 'react'
import { hoveredId } from '../../../../../../store/pagebuilder/components/store'
import _clone from 'lodash.clonedeep'
import { componentAlias } from '../../../../../../store/pagebuilder/aliases/store'

/**
 * Render a popover with a menu to select provided data, on select calls onChange with the key that needs to be used to get the data on the actual element
 * Tip: you can pass the value to the element UseProvidedData
 *
 * @param id
 * @param children
 * @param onChange
 * @param target
 * @constructor
 */

export type SelectProvidedDataProps = {
    id: string
    onChange: (v: string | undefined) => void
    target: JSX.Element
    allowArrays?: boolean
    onlyKey?: boolean
}

const SelectProvidedData: React.FC<SelectProvidedDataProps> = ({
    id,
    children,
    onChange,
    target,
    allowArrays,
    onlyKey,
}) => {
    const data = useRecoilValue(providedData({ component: id }))

    return data ? (
        <Popover
            content={
                <Menu>
                    <Item
                        firstLevel
                        allowArrays={allowArrays}
                        value={data}
                        onClick={(v) => onChange(onlyKey ? v : `{{${v}}}`)}
                    />
                </Menu>
            }
            target={target}
        />
    ) : null
}

const FirstLevelItem: React.FC<{ id: string; allowArrays?: boolean; value: any; onClick: (v: string) => void }> = ({
    id,
    value,
    onClick,
    allowArrays,
}) => {
    const alias = useRecoilValue(componentAlias(id))
    const setHovered = useSetRecoilState(hoveredId)

    const onHover = useCallback(() => {
        setHovered(id)
    }, [id, setHovered])
    const v = _clone(value)
    delete v.id

    return (
        <MenuItem text={alias} onMouseOverCapture={onHover} onMouseLeave={() => setHovered(undefined)}>
            {(Array.isArray(v) && allowArrays) || !Array.isArray(v) ? (
                <Item allowArrays={allowArrays} value={v} onClick={(v) => onClick(`${id}.${v}`)} />
            ) : null}
        </MenuItem>
    )
}

const Item: React.FC<{
    value: Record<string, any>
    onClick: (v: string) => void
    firstLevel?: boolean
    allowArrays?: boolean
}> = ({ value, onClick, firstLevel, allowArrays }) => {
    return (
        <>
            {Object.entries(value).map(([key, val]) => {
                return (Array.isArray(val) && allowArrays) || !Array.isArray(val) ? (
                    firstLevel ? (
                        <FirstLevelItem allowArrays={allowArrays} id={key} value={val} onClick={onClick} />
                    ) : typeof val === 'object' ? (
                        <MenuItem key={key} text={key} onClick={() => onClick(key)}>
                            <Item allowArrays={allowArrays} value={val} onClick={(v) => onClick(`${key}.${v}`)} />
                        </MenuItem>
                    ) : (
                        <MenuItem key={key} text={`${key}${val ? `: (${val})` : ''}`} onClick={() => onClick(key)} />
                    )
                ) : val[0] ? (
                    <Item value={val[0]} onClick={(v) => onClick(`${key}.${v}`)} />
                ) : null
            })}
        </>
    )
}

export default SelectProvidedData
