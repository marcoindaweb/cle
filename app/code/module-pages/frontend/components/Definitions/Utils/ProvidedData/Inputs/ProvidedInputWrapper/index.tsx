/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import { Button, ControlGroup, InputGroup } from '@blueprintjs/core'
import SelectProvidedData, { SelectProvidedDataProps } from '../../Primitives/SelectProvidedData'

const Wrapper: React.FC<{ wrapped?: boolean }> = ({ children, wrapped }) =>
    wrapped ? <ControlGroup>{children}</ControlGroup> : <>{children}</>

const ProvidedInputWrapper: React.FC<{
    value: any
    id: string
    target?: JSX.Element
    onChange: (v: string | undefined) => void
    wrapped?: boolean
    selectorProps?: Partial<SelectProvidedDataProps>
}> = ({ value, id, target, onChange, wrapped, children, selectorProps }) => {
    const hasData = typeof value === 'string' && /^{{|}}$/.test(value)

    return hasData ? (
        <Wrapper wrapped={wrapped}>
            <InputGroup value={value} disabled />
            <Button icon="cross" onClick={() => onChange(undefined)} />
        </Wrapper>
    ) : (
        <Wrapper wrapped={wrapped}>
            {children}
            <SelectProvidedData
                {...selectorProps}
                id={id}
                onChange={onChange}
                target={target || <Button icon="data-connection" />}
            />
        </Wrapper>
    )
}

export default ProvidedInputWrapper
