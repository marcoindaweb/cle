/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useCallback, useEffect } from 'react'
import { Button, ControlGroup, IControlGroupProps, IInputGroupProps, InputGroup } from '@blueprintjs/core'
import SelectProvidedData from '../../Primitives/SelectProvidedData'
import { useSetRecoilState } from 'recoil'
import { hoveredId } from '../../../../../../store/pagebuilder/components/store'
import SelectedDataKey from '../../Primitives/SelectedDataKey'

const ProvidedInputGroup: React.FC<{
    id: string
    value: string | undefined
    onChange: (v: string | undefined) => void
    inputGroup?: IInputGroupProps
    controlGroup?: IControlGroupProps
    useControlGroup?: boolean
    allowArrays?: boolean
    onlyKey?: boolean
}> = ({ allowArrays, value, id, onChange, inputGroup, controlGroup, useControlGroup, onlyKey }) => {
    useEffect(() => {
        console.warn('ProvidedInputGroup is deprecated, use ProvidedInputWrapper instead')
    }, [])
    const hasData = /^{{|}}$/.test(value || '')
    const setHover = useSetRecoilState(hoveredId)
    const onMouseEnter = useCallback(() => {
        if (hasData) {
            setHover(id)
        }
    }, [hasData, setHover, id])
    const onMouseLeave = useCallback(() => {
        setHover(undefined)
    }, [setHover])

    const Inputs = (
        <SelectedDataKey value={value || ''}>
            {(v) => (
                <>
                    <InputGroup
                        {...inputGroup}
                        disabled={hasData}
                        value={v}
                        onChange={(e) => onChange(e.target.value)}
                    />
                    {hasData ? (
                        <Button icon="cross" onClick={() => onChange(undefined)} />
                    ) : (
                        <SelectProvidedData
                            onlyKey={onlyKey}
                            allowArrays={allowArrays}
                            id={id}
                            onChange={onChange}
                            target={<Button icon="database" />}
                        />
                    )}
                </>
            )}
        </SelectedDataKey>
    )

    return useControlGroup === false ? (
        Inputs
    ) : (
        <ControlGroup {...controlGroup} onMouseOverCapture={onMouseEnter} onMouseLeave={onMouseLeave}>
            {Inputs}
        </ControlGroup>
    )
}

export default ProvidedInputGroup
