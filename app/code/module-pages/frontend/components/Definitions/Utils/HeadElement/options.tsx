/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useEffect, useState } from 'react'
import React from 'react'
import { Button, FormGroup, Switch } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import ProvidedInputGroup from '../ProvidedData/Inputs/ProvidedInputGroup'

const HeadElementOptions: React.FC<{
    id: string
    values: Record<string, string | boolean>
    setValues: (v: Record<string, string | boolean>) => void
}> = ({ values, setValues, id }) => {
    const [v, setV] = useState(() => values)
    useEffect(() => {
        setV(values)
    }, [values])

    return (
        <>
            {Object.entries(v).map(([key, val]) => (
                <FormGroup key={key} label={<Translated message={key} />}>
                    {typeof val === 'string' ? (
                        <ProvidedInputGroup id={id} value={val} onChange={(e) => setV({ ...v, [key]: e || '' })} />
                    ) : (
                        <Switch checked={val} onChange={(e) => setV({ ...v, [key]: !val })} />
                    )}
                </FormGroup>
            ))}
            <Button onClick={() => setValues(v)} text={<Translated message="Update" />} />
        </>
    )
}

export default HeadElementOptions
