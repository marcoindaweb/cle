/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useEffect } from 'react'
import { useRecoilValue } from 'recoil'
import { head } from '../../../../store/pagebuilder/frame/store'

const HeadElementRenderer: React.FC<{ values: Record<string, string | boolean> & { uid: string }; tag: string }> = ({
    values,
    tag,
}) => {
    const headEl = useRecoilValue(head)
    const { uid } = values

    useEffect(() => {
        if (!headEl) return
        let tagEl = headEl.querySelector(`${tag}[uid="${uid}"]`)
        if (!tagEl) {
            tagEl = headEl.ownerDocument.createElement(tag)
            headEl.appendChild(tagEl)
        }
        for (const [key, value] of Object.entries(values)) {
            if (typeof value === 'string') {
                if (key === 'text') {
                    tagEl.textContent = value
                } else {
                    if (value.length > 0) {
                        tagEl.setAttribute(key, value)
                    } else {
                        tagEl.removeAttribute(key)
                    }
                }
            } else {
                if (value) {
                    tagEl.setAttribute(key, '')
                } else {
                    tagEl.removeAttribute(key)
                }
            }
        }
        return () => tagEl?.remove()
    }, [values, tag])
    return null
}

export default HeadElementRenderer
