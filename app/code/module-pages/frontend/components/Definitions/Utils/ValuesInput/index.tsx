/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useCallback, useState } from 'react'
import _clone from 'lodash.clonedeep'
import { Button, ControlGroup, InputGroup } from '@blueprintjs/core'
import ProvidedInputGroup from '../ProvidedData/Inputs/ProvidedInputGroup'

const ValuesInput: React.FC<{
    id: string
    value: Record<string, string>
    onChange: (v: Record<string, string>) => void
}> = ({ value, onChange, id }) => {
    const onKeyChange = useCallback(
        (oldKey: string, newKey: string) => {
            const newVal = _clone(value)
            const v = newVal[oldKey]
            delete newVal[oldKey]
            newVal[newKey] = v
            onChange(newVal)
        },
        [value],
    )
    const [newVal, setNew] = useState<string>('')

    const onValueAdd = useCallback(() => {
        onChange({ ...value, [newVal]: '' })
        setNew('')
    }, [newVal, setNew, value, onChange])

    return (
        <>
            {Object.entries(value).map(([key, v]) => (
                <ControlGroup key={key}>
                    <InputGroup value={key} onChange={(e) => onKeyChange(key, e.target.value)} />
                    <ProvidedInputGroup
                        id={id}
                        value={v}
                        onChange={(v) => (v ? onChange({ ...value, [key]: v }) : null)}
                        useControlGroup={false}
                    />
                </ControlGroup>
            ))}
            <ControlGroup>
                <InputGroup value={newVal} onChange={(e) => setNew(e.target.value)} />
                <Button disabled={newVal.length < 1} icon="add" onClick={onValueAdd} />
            </ControlGroup>
        </>
    )
}

export default ValuesInput
