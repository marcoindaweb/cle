/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { Fragment } from 'react'
import { parseAndCheckHttpResponse } from '@apollo/client'

const AutocompleteTextInput = ({ value, onChange }) => {
    return (
        <div>
            <Parsed value={value} />
        </div>
    )
}

const Parsed: React.FC<{ value: string }> = ({ value }) => {
    const pattern = /{{[^}}]*}}/g
    const matches = value.match(pattern)
    if (!matches) return <>{value}</>
    const parts = value.split(pattern)
    return (
        <Fragment>
            {parts.map((part, index) =>
                matches.includes(part) ? (
                    <ProvidedDataTag key={index} dataKey={part} />
                ) : (
                    <Fragment key={index}>{part}</Fragment>
                ),
            )}
        </Fragment>
    )
}

const ProvidedDataTag: React.FC<{ dataKey: string }> = ({ dataKey }) => {
    return null
}

export default AutocompleteTextInput
