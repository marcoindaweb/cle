/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import { createUseStyles } from 'react-jss'
import Container from '../../../etc/Container'
import Style from './Style'

const useStyle = createUseStyles({
    main: {
        display: 'flex',
        flexDirection: 'column',
        padding: 0,
    },
})

const LeftBar: React.FC<{ className?: string }> = ({ className }) => {
    const { main } = useStyle()

    return (
        <Container className={className + ' ' + main}>
            {/*<Style />*/}
        </Container>
    )
}

export default LeftBar
