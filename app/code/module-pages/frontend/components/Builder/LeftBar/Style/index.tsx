/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Button, Drawer, Position } from '@blueprintjs/core'
import React, { useState } from 'react'
import Manager from './Manager'
import { useRecoilValue } from 'recoil'
import { pagebuilderRoot } from '../../../../store/pagebuilder/elements'
import Translated from '@linda/module-i18n/frontend/components/Translated'

const Style = () => {
    const [open, setOpen] = useState(false)
    const portalContainer = useRecoilValue(pagebuilderRoot)

    return (
        <>
            <Drawer
                portalContainer={portalContainer}
                position={Position.RIGHT}
                isOpen={open}
                onClose={() => setOpen(false)}
                title={<Translated message="Stylesheets" />}
            >
                <Manager />
            </Drawer>
            <Button onClick={() => setOpen(!open)} icon={open ? 'cross' : 'style'} />
        </>
    )
}

export default Style
