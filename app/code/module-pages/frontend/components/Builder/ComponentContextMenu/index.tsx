/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useCallback } from 'react'
import { Menu, MenuDivider, MenuItem } from '@blueprintjs/core'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { componentAlias } from '../../../store/pagebuilder/aliases/store'
import { component } from '../../../store/layout/store'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import ContextMenuTrigger from '../ContextMenuTrigger'
import { symbolFormComponentId } from '../../../store/pagebuilder/symbols/form/store'
import {
    componentSymbolId,
    symbol,
    symbolAlias,
    symbolParentComponentId,
} from '../../../store/pagebuilder/symbols/store'

const ComponentInstanceContextMenu: React.FC<{ id: string }> = ({ id }) => {
    const alias = useRecoilValue(componentAlias(id))
    const setComponent = useSetRecoilState(component(id))
    const setSymbolId = useSetRecoilState(symbolFormComponentId)
    const deleteComponent = useCallback(() => {
        setComponent(undefined)
    }, [setComponent])
    const makeSymbol = useCallback(() => {
        setSymbolId(id)
    }, [id, setSymbolId])

    return (
        <Menu>
            <MenuDivider title={alias} />
            <MenuItem onClick={deleteComponent} text={<Translated message="Delete" />} />
            <MenuItem onClick={makeSymbol} text={<Translated message="Make a Symbol" />} />
        </Menu>
    )
}

const SymbolInstanceComponentMenu: React.FC<{ id: string }> = ({ id }) => {
    const alias = useRecoilValue(symbolAlias(id))
    const parentComponentId = useRecoilValue(symbolParentComponentId(id))
    const setSymbol = useSetRecoilState(symbol(id))
    const setParentComponent = useSetRecoilState(component(parentComponentId || ''))
    const deleteSymbol = useCallback(() => {
        setSymbol(undefined)
        setParentComponent(undefined)
    }, [setSymbol, setParentComponent])

    return (
        <Menu>
            <MenuDivider title={alias} />
            <MenuItem onClick={deleteSymbol} text={<Translated message="Delete" />} />
        </Menu>
    )
}

const ComponentContextMenu: React.FC<{ id: string }> = ({ id, children }) => {
    const symbolId = useRecoilValue(componentSymbolId(id))
    return (
        <ContextMenuTrigger
            menu={symbolId ? <SymbolInstanceComponentMenu id={symbolId} /> : <ComponentInstanceContextMenu id={id} />}
        >
            {children}
        </ContextMenuTrigger>
    )
}

export default ComponentContextMenu
