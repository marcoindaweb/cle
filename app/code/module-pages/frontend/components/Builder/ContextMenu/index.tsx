/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useCallback, useEffect, useRef } from 'react'
import { useRecoilState, useRecoilValue } from 'recoil'
import { menu, positionX, positionY, show, target } from '../../../store/pagebuilder/contextMenu/store'
import { Manager, Popper } from 'react-popper'
import { createUseStyles } from 'react-jss'
import { Classes } from '@blueprintjs/core'
import { VirtualElement } from '@popperjs/core'

const useStyles = createUseStyles({
    context: {
        zIndex: 100,
    },
})

const ContextMenu: React.FC = () => {
    const [isShowing, setShowing] = useRecoilState(show)
    const contextMenu = useRecoilValue(menu)
    const { context } = useStyles()
    const x = useRecoilValue(positionX)
    const y = useRecoilValue(positionY)
    const containerRef = useRef<HTMLDivElement>(null)
    const getBoundingClientRect = useCallback(
        (): ClientRect => ({
            top: y,
            right: x,
            width: 0,
            height: 0,
            bottom: y,
            left: x,
        }),
        [x, y],
    )
    const virtualElement: VirtualElement = {
        getBoundingClientRect,
    }
    const hidePopover = useCallback(
        (e: MouseEvent) => {
            if (e.target !== containerRef.current) {
                setShowing(false)
            }
        },
        [setShowing],
    )
    useEffect(() => {
        if (isShowing) {
            document.addEventListener('click', hidePopover)
            document.querySelectorAll('iframe').forEach((v) => {
                v.contentDocument?.body.addEventListener('click', hidePopover)
            })
            return () => {
                document.removeEventListener('click', hidePopover)
                document.querySelectorAll('iframe').forEach((v) => {
                    v.contentDocument?.body.removeEventListener('click', hidePopover)
                })
            }
        }
    }, [hidePopover, isShowing])
    return (
        <Manager>
            {isShowing && (
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                //@ts-ignore
                <Popper referenceElement={virtualElement} placement="bottom-start" strategy={'fixed'}>
                    {({ style, ref }) => {
                        return (
                            <div className={context + ' ' + Classes.POPOVER} style={style} ref={containerRef}>
                                <div ref={ref}>{contextMenu}</div>
                            </div>
                        )
                    }}
                </Popper>
            )}
        </Manager>
    )
}

export default ContextMenu
