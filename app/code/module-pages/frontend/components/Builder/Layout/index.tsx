/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { createUseStyles } from 'react-jss'
import React, { useEffect, useRef } from 'react'
import TopBar from '../TopBar'
import LeftBar from '../LeftBar'
import RightBar from '../RightBar'
import Main from '../Main'
import { useRecoilState, useRecoilValue } from 'recoil'
import { pagebuilderRoot } from '../../../store/pagebuilder/elements'
import { rightBarWidth } from '../../../store/pagebuilder/ui/store'
import ContextMenu from '../ContextMenu'

const useStyles = createUseStyles({
    main: (props) => ({
        '& *': {
            boxSizing: 'border-box',
        },
        height: '100vh',
        display: 'grid',
        gridTemplateColumns: `35px calc(100% - 35px - ${props.rightWidth}px) ${props.rightWidth}px`,
        gridTemplateRows: '35px calc(100% - 35px)',
        '& > *': {
            position: 'relative',
        },
        '& > .top': {
            gridRow: '1',
            gridColumn: '1 / 3',
            zIndex: '2',
        },
        '& > .left': {
            gridRow: '2',
            gridColumn: '1',
            zIndex: '1',
        },
        '& > .right': {
            gridRow: '1 / 3',
            gridColumn: '3',
            zIndex: '2',
        },
        '& > .main': {
            gridRow: '2',
            gridColumn: '2',
        },
    }),
})

const Layout = () => {
    const width = useRecoilValue(rightBarWidth)
    const { main } = useStyles({ rightWidth: width })
    const [root, setRoot] = useRecoilState(pagebuilderRoot)
    const ref = useRef<HTMLDivElement>(null)
    useEffect(() => {
        if (ref.current && !root) {
            setRoot(ref.current)
        }
    }, [root, ref, setRoot])
    return (
        <div ref={ref} className={main + ' pb-root'}>
            <ContextMenu />
            <TopBar className="top" />
            <LeftBar className="left" />
            <RightBar className="right" />
            <Main className="main" />
        </div>
    )
}

export default Layout
