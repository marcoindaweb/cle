/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import AdminToast from '@linda/module-admin/frontend/components/AdminToast'

class ErrorBoundary extends React.Component {
  constructor (props) {
    super(props)
  }

  componentDidCatch (error, errorInfo) {
    debugger
    AdminToast.show({ intent: 'danger', message: error.message })
  }

  render () {
    return this.props.children
  }
}

export default ErrorBoundary
