/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useEffect, useState } from 'react'
import { useRecoilValue } from 'recoil'
import { currentDevice } from '../../../../store/pagebuilder/device/store'
import ResizeObserver from 'resize-observer-polyfill'
import { cssUnitToInt } from '../../../../utils/cssUnitToInt'
import { Classes, Colors, NonIdealState } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
    backdrop: {
        position: 'absolute',
        height: '100%',
        width: '100%',
        '& .bp3-heading, &': {
            color: Colors.WHITE,
        },
        '& svg': {
            color: 'var(--warning-color)',
        },
    },
})

const WarningOverlay: React.FC = () => {
    const { backdrop } = useStyles()
    return (
        <div className={Classes.OVERLAY_BACKDROP + ' ' + backdrop}>
            <NonIdealState
                icon="warning-sign"
                title={<Translated message="The window isn't wide enough" />}
                description={
                    <Translated message="The window isn't wide enough to show the canvas for the current device, please increase window size" />
                }
            />
        </div>
    )
}

const DeviceCheck: React.FC<{ el: HTMLElement }> = ({ el }) => {
    const device = useRecoilValue(currentDevice)
    const [isOk, setIsOk] = useState(true)

    useEffect(() => {
        const observer = new ResizeObserver((entries) => {
            for (const entry of entries) {
                if (isOk && entry.contentRect.width < cssUnitToInt(device.width)) {
                    setIsOk(false)
                } else if (!isOk && entry.contentRect.width > cssUnitToInt(device.width)) {
                    setIsOk(true)
                }
            }
        })
        observer.observe(el)
        return () => {
            observer.disconnect()
        }
    }, [el, device, setIsOk, isOk])
    return isOk ? null : <WarningOverlay />
}

export default DeviceCheck
