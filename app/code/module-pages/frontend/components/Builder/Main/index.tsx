/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useRef } from 'react'
import Page from './Page'
import { createUseStyles } from 'react-jss'
import '../../../style/pagebuilder.scss'
import { Colors, NonIdealState } from '@blueprintjs/core'
import DeviceCheck from './DeviceCheck'
import { useRecoilValue } from 'recoil'
import { currentRootComponentId } from '../../../store/layout/store'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import NoPage from './NoPage'

const useStyles = createUseStyles({
  main: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    background: Colors.GRAY1,
    padding: '0'
  }
})

const Main: React.FC<{ className?: string }> = ({ className }) => {
  const { main } = useStyles()
  const ref = useRef<HTMLDivElement>(null)
  const rootId = useRecoilValue(currentRootComponentId)

  return rootId ? (
    <div ref={ref} className={className + ' ' + main}>
      {ref.current && <DeviceCheck el={ref.current} />}
      <Page />
    </div>
  ) : (
    <NoPage />
  )
}

export default Main
