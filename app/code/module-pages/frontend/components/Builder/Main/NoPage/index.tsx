/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Button, Classes, NonIdealState } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import React from 'react'
import { useSetRecoilState } from 'recoil'
import { isLayoutFormOpen } from '../../../../store/pagebuilder/layout/form/store'
import style from './style.scss'

const NoPage = () => {
    const setShowCreateForm = useSetRecoilState(isLayoutFormOpen)

    return (
        <NonIdealState
            icon="clean"
            title={
                <>
                    <Button
                        minimal
                        onClick={() => setShowCreateForm(true)}
                        text={
                            <h4 className={Classes.HEADING + ' ' + style.h4}>
                                <Translated message="create" />
                            </h4>
                        }
                    />{' '}
                    <Translated message="a new layout or load one" />
                </>
            }
        />
    )
}

export default NoPage
