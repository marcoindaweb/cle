/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import Frame from './Frame'
import React, { useEffect } from 'react'
import { createUseStyles, JssProvider } from 'react-jss'
import { useRecoilValue } from 'recoil'
import { currentDevice, fixedDeviceHeight } from '../../../../store/pagebuilder/device/store'
import Controls from './Controls'
import Listeners from './Listeners'
import Dropzone from './Dropzone'
import Layout from './Layout'
import { useCurrentStylesheet } from '../../../../store/pagebuilder/style/actions'
import { Router } from 'wouter'
import DeviceCheck from '../DeviceCheck'

const useStyles = createUseStyles({
    main: {
        height: 'calc(100% - 10px)',
        overflowY: 'auto',
    },
    container: {
        width: (props) => props.width,
        height: (props) => (props.isFixed ? props.height : '100%'),
        position: 'relative',
        '& > .controls': {
            '&.active *': {
                pointerEvents: 'auto',
            },
            pointerEvents: 'none',
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
        },
    },
    frame: {
        border: 'none',
        height: '100%',
        width: '100%',
        background: '#FFF',
    },
})

const Page: React.FC = () => {
    const device = useRecoilValue(currentDevice)
    const isFixed = useRecoilValue(fixedDeviceHeight)
    const { frame, container, main } = useStyles({ width: device.width, height: device.height, isFixed })
    useCurrentStylesheet('main')

    return (
        <div className={main}>
            <div className={container}>
                <Listeners />
                <Dropzone />
                <Controls />
                <Frame className={frame}>
                    <Layout />
                </Frame>
            </div>
        </div>
    )
}

export default Page
