/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useEffect, useRef } from 'react'
import ReactDOM from 'react-dom'
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import { frame } from '../../../../../store/pagebuilder/frame/store'
import { Router } from 'wouter'
import { path } from '../../../../../store/pagebuilder/router/store'

const Frame: React.FC<{ className?: string }> = ({ children, className }) => {
    const frameRef = useRef<HTMLIFrameElement>(null)
    const p = useRecoilValue(path)
    const setFrame = useSetRecoilState(frame)
    useEffect(() => {
        if (frameRef.current) {
            setFrame(frameRef.current)
        }
    }, [setFrame, frameRef])

    return (
        <iframe ref={frameRef} className={className}>
            <Router base={p}>
                {frameRef.current &&
                    frameRef.current.contentDocument &&
                    ReactDOM.createPortal(children, frameRef.current.contentDocument.body)}
            </Router>
        </iframe>
    )
}

export default Frame
