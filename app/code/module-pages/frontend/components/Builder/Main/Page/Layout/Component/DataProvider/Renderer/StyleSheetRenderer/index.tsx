/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useEffect } from 'react'
import { useRecoilValue } from 'recoil'
import { componentPropertyWithProvidedData } from '../../../../../../../../../store/layout/store'
import { postcss } from '@linda/module-pages'
import toCss from '../../../../../../../../../store/pagebuilder/style/Converter/ToCss'
import {
    useCreateStylesheet,
    useDeleteStylesheet,
    useStylesheet,
} from '../../../../../../../../../store/pagebuilder/style/actions'

const StyleSheetRenderer: React.FC<{ id: string; indexes?: number[] }> = ({ id, indexes }) => {
    const style = useRecoilValue(componentPropertyWithProvidedData({ component: id, key: 'style', indexes }))
    const actualSheetId = `${id}${(indexes || []).join('')}`
    const sheet = useStylesheet(actualSheetId)
    const create = useCreateStylesheet(actualSheetId)
    const deleteStylesheet = useDeleteStylesheet(actualSheetId)

    useEffect(() => {
        return () => deleteStylesheet()
    }, [])

    useEffect(() => {
        if (style) {
            if (Object.keys(style).length === 0) {
                return
            } else if (!sheet) {
                create()
            } else if (sheet) {
                sheet.textContent = postcss.process(toCss.convert(style)).css
            }
        }
    }, [JSON.stringify(style), sheet, create])
    return null
}

export default StyleSheetRenderer
