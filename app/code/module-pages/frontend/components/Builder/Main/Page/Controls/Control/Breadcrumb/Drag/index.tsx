/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Icon } from '@blueprintjs/core'
import React from 'react'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { body } from '../../../../../../../../store/pagebuilder/frame/store'
import { dragging } from '../../../../../../../../store/pagebuilder/drag/store'
import { useDrag } from 'react-dnd'
import { COMPONENT_INSTANCE, ComponentInstanceDrag } from '../../../../../../../../types/ComponentInstance'

const Drag: React.FC<{ id: string }> = ({ id }) => {
    const b = useRecoilValue(body)
    const setDragging = useSetRecoilState(dragging)
    const [, ref, preview] = useDrag<ComponentInstanceDrag, any, any>({
        item: {
            id,
            type: COMPONENT_INSTANCE,
        },
        begin: () => {
            setDragging(true)
        },
        end: () => {
            setDragging(false)
        },
    })

    preview(b?.querySelector(`#${CSS.escape(id)}`) || null)

    return (
        <div ref={ref} className="drag">
            <Icon icon="move" iconSize={10} />
        </div>
    )
}

export default Drag
