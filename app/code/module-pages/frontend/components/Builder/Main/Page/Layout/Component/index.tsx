/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilValue } from 'recoil'
import { componentChildrenIds } from '../../../../../../store/layout/store'
import Renderer from './DataProvider/Renderer'
import React from 'react'
import DataProvider from './DataProvider'

const Component: React.FC<{ id: string; indexes?: number[] }> = ({ id, indexes }) => {
    const cc = useRecoilValue(componentChildrenIds(id))

    return (
        <DataProvider indexes={indexes} id={id}>
            {(v) => (
                <Renderer {...v} id={id} indexes={indexes}>
                    {cc.map((v) => (
                        <Component key={v} id={v} indexes={indexes} />
                    ))}
                </Renderer>
            )}
        </DataProvider>
    )
}

export default Component
