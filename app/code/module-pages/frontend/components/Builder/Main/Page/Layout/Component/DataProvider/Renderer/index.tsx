/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilValue } from 'recoil'
import { componentDeclarationId, componentPropertiesWithProvidedData } from '../../../../../../../../store/layout/store'
import React from 'react'
import { componentDeclarationById } from '../../../../../../../../store/componentDeclarations/store'
import StyleSheetRenderer from './StyleSheetRenderer'

const Renderer: React.FC<{ id: string; provided: any; setProvided: (v: any) => void; indexes?: number[] }> = ({
    id,
    children,
    provided,
    setProvided,
    indexes,
}) => {
    const props = useRecoilValue(componentPropertiesWithProvidedData({ component: id, indexes }))
    const dId = useRecoilValue(componentDeclarationId(id))
    const d = useRecoilValue(componentDeclarationById(dId || ''))
    const Component = d?.render

    return Component ? (
        <>
            <Component
                {...props}
                id={id}
                className={props.className}
                provide={setProvided}
                provided={provided}
                indexes={indexes}
            >
                {children}
            </Component>
            <StyleSheetRenderer id={id} indexes={indexes} />
        </>
    ) : null
}

export default Renderer
