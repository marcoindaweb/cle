/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilValue } from 'recoil'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import React from 'react'
import { Colors, Divider, Icon, Popover, PopoverPosition } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'
import ComponentOptions from '../../../../../ComponentOptions'
import { declarationByComponent } from '../../../../../../../store/layout/store'
import Drag from './Drag'
import { componentAlias } from '../../../../../../../store/pagebuilder/aliases/store'
import {
    componentSymbolId,
    symbolAlias,
    symbolParentComponentId,
} from '../../../../../../../store/pagebuilder/symbols/store'

const useStyles = createUseStyles({
    breadcrumb: {
        zIndex: 1,
        display: 'flex',
        position: 'relative',
        alignItems: 'center',
        width: 'fit-content',
        background: 'var(--primary-color)',
        '&.symbol': {
            background: 'var(--violet-color)',
        },
        padding: '0 2px',
        color: Colors.WHITE,
        '&>small': {
            fontSize: '10px',
            textTransform: 'uppercase',
        },
        '& .cog-icon': {
            cursor: 'pointer',
            height: '100%',
            display: 'flex',
            alignItems: 'center',
        },
        '&>.drag': {
            cursor: 'grab',
        },
        '& .cog, &>.drag': {
            display: 'flex',
            alignItems: 'center',
        },
        '&.bottom': {
            bottom: '-100%',
            borderBottomLeftRadius: '2px',
            borderBottomRightRadius: '2px',
        },
        '&.top': {
            transform: 'translateY(-100%)',
            borderTopLeftRadius: '2px',
            borderTopRightRadius: '2px',
        },
    },
})

export enum Position {
    top,
    bottom,
}

const SymbolBreadCrumb: React.FC<{ id: string; position: Position }> = ({ id, position }) => {
    const alias = useRecoilValue(symbolAlias(id))
    const parentId = useRecoilValue(symbolParentComponentId(id))
    return parentId ? (
        <BreadCrumbBox alias={alias} position={position} componentDragId={parentId} isSymbol />
    ) : null
}

const ComponentBreadCrumb: React.FC<{ id: string; position: Position }> = ({ id, position }) => {
    const alias = useRecoilValue(componentAlias(id))
    const declaration = useRecoilValue(declarationByComponent(id))
    const options = declaration?.options ? <ComponentOptions id={id} /> : undefined
    return <BreadCrumbBox position={position} alias={alias} options={options} componentDragId={id} />
}

const BreadCrumbBox: React.FC<{
    options?: JSX.Element
    componentDragId: string
    alias: string
    position: Position
    isSymbol?: boolean
}> = ({ position, alias, options, isSymbol, componentDragId }) => {
    const { breadcrumb } = useStyles()

    return (
        <div className={breadcrumb + ` ${isSymbol ? 'symbol' : ''} ` + (position === Position.top ? 'top' : 'bottom')}>
            <Drag id={componentDragId} />
            <Divider />
            <small>
                <Translated message={alias} />
            </small>
            {options && (
                <>
                    <Divider />
                    <Popover minimal position={PopoverPosition.BOTTOM} content={options}>
                        <Icon className="cog-icon" icon="cog" iconSize={10} />
                    </Popover>
                </>
            )}
        </div>
    )
}

const Breadcrumb: React.FC<{ id: string; className?: string; position: Position }> = ({ id, position }) => {
    const symbol = useRecoilValue(componentSymbolId(id))
    return symbol ? (
        <SymbolBreadCrumb id={symbol} position={position} />
    ) : (
        <ComponentBreadCrumb id={id} position={position} />
    )
}

export default Breadcrumb
