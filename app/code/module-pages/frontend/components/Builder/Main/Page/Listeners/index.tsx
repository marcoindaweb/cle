/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

/**
 * Add mouseover and click listener to all pagebuilder components
 */
import React, { useEffect } from 'react'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { frame } from '../../../../../store/pagebuilder/frame/store'
import { hovered, selected } from '../../../../../store/pagebuilder/components/store'

const Listeners = () => {
    const setHovered = useSetRecoilState(hovered)
    const setSelected = useSetRecoilState(selected)
    const iframe = useRecoilValue(frame)

    useEffect(() => {
        const listener1 = iframe?.contentDocument?.body.addEventListener('click', (e) => {
            e.stopPropagation()
            setSelected(undefined)
        })

        const listener2 = iframe?.contentDocument?.body.addEventListener('mouseover', (e) => {
            e.stopPropagation()
            setHovered(undefined)
        })

        return () => {
            if (listener1) iframe?.contentDocument?.body.removeEventListener('click', listener1)
            if (listener2) iframe?.contentDocument?.body.removeEventListener('mouseover', listener2)
        }
    }, [iframe?.contentDocument, setHovered, setSelected])

    return null
}

export default Listeners
