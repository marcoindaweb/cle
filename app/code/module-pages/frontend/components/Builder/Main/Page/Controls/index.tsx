/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilValue } from 'recoil'
import React from 'react'
import Control from './Control'
import { frame } from '../../../../../store/pagebuilder/frame/store'
import { currentRootComponentChildren, layout } from '../../../../../store/layout/store'
import { createUseStyles } from 'react-jss'
import { dragging } from '../../../../../store/pagebuilder/drag/store'
import { interactive } from '../../../../../store/pagebuilder/state/store'

const useStyles = createUseStyles({
    controls: {
        '&.active *': {
            pointerEvents: 'auto',
        },
        pointerEvents: 'none',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        overflow: 'hidden',
    },
})

const Controls: React.FC<{ className?: string }> = ({ className }) => {
    const { controls } = useStyles()
    const iframe = useRecoilValue(frame)
    const components = useRecoilValue(currentRootComponentChildren)
    const isDragging = useRecoilValue(dragging)
    const isInteractive = useRecoilValue(interactive)

    return isInteractive ? null : iframe ? (
        <div className={`${className} ${controls} ${!isDragging ? 'active' : ''}`}>
            {components.map((c) => (
                <Control parentTop={0} parentLeft={0} id={c} key={c} />
            ))}
        </div>
    ) : null
}

export default Controls
