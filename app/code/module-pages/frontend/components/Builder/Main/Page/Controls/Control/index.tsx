/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { MouseEvent, useCallback, useEffect, useMemo, useState } from 'react'
import { useRecoilState, useRecoilValue } from 'recoil'
import { frame } from '../../../../../../store/pagebuilder/frame/store'
import { createUseStyles } from 'react-jss'
import { Colors } from '@blueprintjs/core'
import { hoveredId, selectedId } from '../../../../../../store/pagebuilder/components/store'
import { show } from '../../../../../../store/pagebuilder/borders/store'
import Breadcrumb, { Position } from './Breadcrumb'
import { componentChildrenIds } from '../../../../../../store/layout/store'
import ComponentContextMenu from '../../../../ComponentContextMenu'
import {
    componentSymbolId,
    symbolChildrenIds,
    symbolParentComponentId,
} from '../../../../../../store/pagebuilder/symbols/store'

type StyleProps = {
    top: string
    left: string
    height: string
    width: string
}

const useStyles = createUseStyles({
    main: (props: StyleProps) => ({
        position: 'absolute',
        height: props.height,
        width: props.width,
        top: props.top,
        left: props.left,
        '&.with-borders': {
            outline: '1px solid rgba(0,0,0,.3)',
        },
        '&.hovered': {
            outline: '1px dashed var(--primary-color)',
            '&.symbol': {
                outlineColor: 'var(--violet-color)',
            },
        },
        '&.selected': {
            outline: '1px dashed var(--primary-color)',
            '&.symbol': {
                outlineColor: 'var(--violet-color)',
            },
        },
    }),
})

const SymbolControl: React.FC<{ id: string; parentTop: number; parentLeft: number }> = ({
    id,
    parentLeft,
    parentTop,
}) => {
    const children = useRecoilValue(symbolChildrenIds(id))
    const parentId = useRecoilValue(symbolParentComponentId(id))
    return parentId ? (
        <ControlBox id={parentId} parentLeft={parentLeft} parentTop={parentTop} childrenIds={children} isSymbol />
    ) : null
}

const ComponentControl: React.FC<{ id: string; parentTop: number; parentLeft: number }> = ({
    id,
    parentLeft,
    parentTop,
}) => {
    const children = useRecoilValue(componentChildrenIds(id))
    return <ControlBox id={id} childrenIds={children} parentTop={parentTop} parentLeft={parentLeft} />
}

const ControlBox: React.FC<{
    childrenIds: string[]
    id: string
    isSymbol?: boolean
    parentLeft: number
    parentTop: number
}> = ({ id, childrenIds, parentTop, parentLeft, isSymbol }) => {
    const iframe = useRecoilValue(frame)
    const element: HTMLElement | undefined | null = iframe?.contentDocument?.body.querySelector(`#${CSS.escape(id)}`)
    const rect = element?.getBoundingClientRect()
    const [hovered, setHovering] = useRecoilState(hoveredId)
    const [selected, setSelected] = useRecoilState(selectedId)
    const borders = useRecoilValue(show)
    const isHovered = hovered === id
    const isSelected = selected === id
    const showBreadcrumb = isHovered || isSelected
    const [stylePropsDimensions, setStylePropsDimensions] = useState<Omit<StyleProps, 'border'>>({
        height: '',
        left: '',
        top: '',
        width: '',
    })

    const breadcrumbPosition = useMemo<Position>(() => {
        if (!(rect?.top && rect?.height)) {
            return Position.top
        }
        return rect.top < 10 ? Position.bottom : Position.top
    }, [rect?.height, rect?.top])

    const [childTop, setChildTop] = useState(0)
    const [childLeft, setChildLeft] = useState(0)

    const onMouseMove = useCallback(
        (e: MouseEvent<HTMLDivElement>) => {
            e.stopPropagation()
            if (hovered !== id) {
                setHovering(id)
            }
        },
        [setHovering, id, hovered],
    )

    const onClick = useCallback(
        (e: MouseEvent<HTMLDivElement>) => {
            e.stopPropagation()
            if (selected !== id) {
                setSelected(id)
            }
        },
        [setSelected, id, selected],
    )

    // Update the size
    useEffect(() => {
        const topDistance = (rect?.top || 0) - parentTop
        const leftDistance = (rect?.left || 0) - parentLeft
        const width = rect?.width || 0
        const height = rect?.height || 0
        setStylePropsDimensions({
            top: topDistance + 'px',
            height: height + 'px',
            width: width + 'px',
            left: leftDistance + 'px',
        })
    }, [rect?.top, rect?.width, rect?.height, rect?.left, parentLeft, parentTop])

    // Update child top
    useEffect(() => {
        setChildTop(rect?.top || 0)
    }, [rect?.top])

    // Update child left
    useEffect(() => {
        setChildLeft(rect?.left || 0)
    }, [rect?.left])

    const { main } = useStyles({ ...stylePropsDimensions })

    return (
        <ComponentContextMenu id={id}>
            <div
                className={
                    main +
                    ` ${isSymbol ? 'symbol' : ''} ${
                        isSelected ? 'selected' : isHovered ? 'hovered' : borders ? 'with-borders' : ''
                    }`
                }
                onMouseMove={onMouseMove}
                onClick={onClick}
            >
                {showBreadcrumb && <Breadcrumb id={id} className="breadcrumb" position={breadcrumbPosition} />}
                {childrenIds.map((v) => (
                    <Control key={v} id={v} parentLeft={childLeft} parentTop={childTop} />
                ))}
            </div>
        </ComponentContextMenu>
    )
}

const Control: React.FC<{ id: string; parentTop: number; parentLeft: number }> = ({ id, parentLeft, parentTop }) => {
    const symbol = useRecoilValue(componentSymbolId(id))
    return symbol ? (
        <SymbolControl id={symbol} parentLeft={parentLeft} parentTop={parentTop} />
    ) : (
        <ComponentControl id={id} parentTop={parentTop} parentLeft={parentLeft} />
    )
}

export default Control
