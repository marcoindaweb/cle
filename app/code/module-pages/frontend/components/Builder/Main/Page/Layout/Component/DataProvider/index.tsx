/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { ReactNode } from 'react'
import { useRecoilState } from 'recoil'
import { componentData } from '../../../../../../../store/layout/store'

const DataProvider: React.FC<{
    id: string
    indexes?: number[]
    children: (v: { provided: any; setProvided: (v: any) => void }) => ReactNode
}> = ({ id, children, indexes }) => {
    const [provided, setProvided] = useRecoilState(componentData({ component: id, indexes }))
    return <>{children({ provided, setProvided })}</>
}

export default DataProvider
