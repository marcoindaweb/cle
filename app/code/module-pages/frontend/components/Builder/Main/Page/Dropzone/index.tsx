/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useDrop } from 'react-dnd'
import { COMPONENT_INSTANCE, ComponentInstanceDrag } from '../../../../../types/ComponentInstance'
import { COMPONENT_DECLARATION, ComponentDeclarationDrag } from '../../../../../types/ComponentDeclaration'
import React, { CSSProperties, useEffect, useState } from 'react'
import { createUseStyles } from 'react-jss'
import { useRecoilValue } from 'recoil'
import { frame } from '../../../../../store/pagebuilder/frame/store'
import { dragging } from '../../../../../store/pagebuilder/drag/store'
import {
    useAddComponent,
    useAddComponentOrSymbol,
    useMoveComp,
    useMoveComponent,
} from '../../../../../store/layout/actions'
import Position from '../../../../../types/Position'
import useTranslate from '@linda/module-i18n/frontend/hooks/useTranslate'
import { interactive } from '../../../../../store/pagebuilder/state/store'
import { SYMBOL_DEFINITION, SymbolDefinitionDrag } from '../../../../../types/SymbolDefinition'
import {
    componentDroppableChildren,
    componentSymbolId,
    symbolParentComponentId,
} from '../../../../../store/pagebuilder/symbols/store'

const useStyles = createUseStyles({
    main: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
        pointerEvents: 'none',
        '&.active': {
            pointerEvents: 'auto',
        },

        '& .bar': {
            transition: 'transform .5s, width .5s',
            transformOrigin: 'left',
            top: '-1px',
            height: '3px',
            left: 0,
            position: 'absolute',
            display: 'none',
            '&.active': {
                display: 'block',
            },
            background: 'var(--pr-cl)',
        },
    },
})

const Dropzone: React.FC = ({ children }) => {
    const { main } = useStyles()
    const iframe = useRecoilValue(frame)
    const isDragging = useRecoilValue(dragging)
    const isInteractive = useRecoilValue(interactive)
    const [dropEl, setDropEl] = useState<undefined | HTMLElement>()
    const [barStyle, setBarStyle] = useState<CSSProperties>({})
    const [dropPlacement, setDropPlacement] = useState<Position>(Position.inside)
    const addComponent = useAddComponent()
    const moveComponent = useMoveComp()
    const translate = useTranslate()
    const [, ref] = useDrop<ComponentDeclarationDrag | ComponentInstanceDrag | SymbolDefinitionDrag, any, undefined>({
        accept: [COMPONENT_INSTANCE, COMPONENT_DECLARATION, SYMBOL_DEFINITION],
        drop: (item, monitor) => {
            const id = dropEl?.id?.length ? dropEl?.id : undefined
            switch (item.type) {
                case 'COMPONENT_DECLARATION':
                    addComponent({ id: item.id, reference: id, position: dropPlacement })
                    break
                case 'SYMBOL_DEFINITION':
                    addComponent({ id: item.id, reference: id, position: dropPlacement })
                    break
                case 'COMPONENT_INSTANCE':
                    if (id === item.id) {
                        throw new Error(translate("You can't move a component inside himself"))
                    }
                    moveComponent({ component: item.id, reference: id, position: dropPlacement })
                    break
            }
            return
        },
        hover: (item, monitor) => {
            const x = (monitor.getClientOffset()?.x || 0) - (iframe?.getBoundingClientRect().x || 0)
            const y = (monitor.getClientOffset()?.y || 0) - (iframe?.getBoundingClientRect().y || 0)
            const el = iframe?.contentDocument?.elementFromPoint(x, y) as HTMLElement | undefined
            setDropEl(el)
            const rect = el?.getBoundingClientRect()
            if (!rect) return
            const top = y <= rect.top + rect.height / 3
            if (top) {
                setDropPlacement(Position.before)
            } else {
                const middle = y >= rect.top + rect.height / 3 && y <= rect.top + (rect.height / 3) * 2
                setDropPlacement(middle ? Position.inside : Position.after)
            }
        },
    })

    const rect = dropEl?.getBoundingClientRect()

    useEffect(() => {
        if (rect === undefined) {
            setBarStyle({})
            return
        }
        const style = {
            transform: `translate(${rect.left}px, ${
                dropPlacement === Position.before
                    ? rect.top
                    : (dropPlacement === Position.inside ? rect.height / 2 : rect.height) + rect.top
            }px)`,
            width: `${rect.width}px`,
        }
        setBarStyle(style)
    }, [rect?.top, rect?.left, rect?.width, dropPlacement, rect?.height])

    return isInteractive ? null : isDragging ? (
        <div ref={ref} className={`${main} ${isDragging && 'active'}`}>
            <div className={`bar ${dropEl ? 'active' : ''}`} style={barStyle} />
        </div>
    ) : null
}

export default Dropzone
