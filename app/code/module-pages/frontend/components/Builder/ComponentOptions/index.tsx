/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilState, useRecoilValue } from 'recoil'
import { componentProperties, declarationByComponent } from '../../../store/layout/store'
import React from 'react'

const ComponentOptions: React.FC<{ id: string }> = ({ id }) => {
    const [properties, setProperties] = useRecoilState(componentProperties(id))
    const declaration = useRecoilValue(declarationByComponent(id))

    const Options = declaration?.options
    return Options ? <Options value={{ ...properties, id: id }} setValue={(v) => setProperties(v)} /> : null
}

export default ComponentOptions
