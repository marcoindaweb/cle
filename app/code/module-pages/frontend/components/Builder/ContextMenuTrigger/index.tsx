/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useCallback, MouseEvent, useRef, useEffect } from 'react'
import { useSetRecoilState } from 'recoil'
import { positionX, positionY, menu as contextMenu, show, target } from '../../../store/pagebuilder/contextMenu/store'

const ContextMenuTrigger: React.FC<{ menu: JSX.Element }> = ({ children, menu }) => {
    const setX = useSetRecoilState(positionX)
    const setY = useSetRecoilState(positionY)
    const setMenu = useSetRecoilState(contextMenu)
    const setShow = useSetRecoilState(show)
    const setTarget = useSetRecoilState(target)
    const ref = useRef<HTMLDivElement>(null)
    const showContextMenu = useCallback(
        (e: MouseEvent<HTMLDivElement>) => {
            e.preventDefault()
            e.stopPropagation()
            if (!ref.current) return
            setX(e.clientX)
            setY(e.clientY)
            setMenu(menu)
            setTarget(ref.current)
            setShow(true)
        },
        [menu, setX, setY, setMenu, ref],
    )

    useEffect(() => {
        return () => setShow(false)
    }, [])

    return (
        <div ref={ref} onContextMenu={showContextMenu}>
            {children}
        </div>
    )
}

export default ContextMenuTrigger
