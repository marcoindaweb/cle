/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilValue } from 'recoil'
import { selectedId } from '../../../../../../store/pagebuilder/components/store'
import { useAddTrigger } from '../../../../../../store/pagebuilder/actions/triggers/actions'
import { Button, Menu, MenuItem, Popover } from '@blueprintjs/core'
import React from 'react'
import { componentDeclarationId, declarationByComponent } from '../../../../../../store/layout/store'
import PbButton from '../../../../../Ui/Inputs/PbButton'

const AddButton = () => {
    const selId = useRecoilValue(selectedId)
    const selDecl = useRecoilValue(declarationByComponent(selId || ''))
    const addTrigger = useAddTrigger()

    return selId && selDecl?.config?.availableTriggers?.length ? (
        <Popover
            target={<PbButton minimal mini icon="add" />}
            content={
                <Menu>
                    {selDecl?.config?.availableTriggers.map((v) => (
                        <MenuItem key={v} text={v} onClick={() => addTrigger({ componentId: selId, eventName: v })} />
                    ))}
                </Menu>
            }
        />
    ) : null
}

export default AddButton
