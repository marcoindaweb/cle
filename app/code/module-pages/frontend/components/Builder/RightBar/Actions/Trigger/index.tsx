/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilValue } from 'recoil'
import { eventTriggerComponentId, eventTriggerEvent } from '../../../../../store/pagebuilder/actions/triggers/store'
import React from 'react'
import style from './style.scss'
import Translated from '@linda/module-i18n/frontend/components/Translated'

const Trigger: React.FC<{ id: string }> = ({ id }) => {
    const eventName = useRecoilValue(eventTriggerEvent(id))
    const eventComponent = useRecoilValue(eventTriggerComponentId(id))
    return eventName ? (
        <div className={style.trigger}>
            <div>
                <b>
                    <Translated message="On" />:
                </b>
                <span>{eventName}</span>
            </div>
            <div>
                <b>
                    <Translated message="Do" />:
                </b>
                <span>{eventName}</span>
            </div>
        </div>
    ) : null
}

export default Trigger
