/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import { useRecoilState, useRecoilValue } from 'recoil'
import { selectedId } from '../../../../store/pagebuilder/components/store'
import { Divider, EditableText, NonIdealState } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import ComponentOptions from '../../ComponentOptions'
import { componentSymbolId, symbolComponents } from '../../../../store/pagebuilder/symbols/store'
import { _componentAlias } from '../../../../store/pagebuilder/aliases/store'

const RenderOptions: React.FC<{ id: string }> = ({ id }) => {
    const symbolId = useRecoilValue(componentSymbolId(id))
    const components = useRecoilValue(symbolComponents(symbolId || ''))
    return symbolId ? (
        <>
            {components.map((v) => (
                <>
                    <RenderOption id={v} key={v} />
                    <Divider />
                </>
            ))}
        </>
    ) : (
        <RenderOption id={id} />
    )
}

const RenderOption: React.FC<{ id: string }> = ({ id }) => {
    const [alias, setAlias] = useRecoilState(_componentAlias(id))
    return (
        <>
            <EditableText value={alias} onChange={setAlias} />
            <ComponentOptions id={id} />
        </>
    )
}

const Settings = () => {
    const id = useRecoilValue(selectedId)
    return id ? (
        <div>
            <RenderOptions id={id} />
        </div>
    ) : (
        <NonIdealState icon="search" title={<Translated message="Select a component" />} />
    )
}

export default Settings
