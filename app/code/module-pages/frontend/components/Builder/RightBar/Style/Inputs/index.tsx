/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useCallback, useEffect, useState } from 'react'
import { Divider } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'
import MediaSelector from './MediaSelector'
import _set from 'lodash.set'
import _clone from 'lodash.clonedeep'
import _get from 'lodash.get'
import { useRecoilState } from 'recoil'
import { selected, selectedClasses, selectedStyle } from '../../../../../store/pagebuilder/components/store'
import { Styles } from 'jss'
import Properties from './Properties'
import PseudoSelector from './PseudoSelector'

const useStyles = createUseStyles({
    main: {
        display: 'flex',
        flexDirection: 'column',
        '& > *:not(:last-child)': {
            marginBottom: '10px',
        },
    },
})

const Inputs: React.FC<{ id: string }> = ({ id }) => {
    const { main } = useStyles()
    const [classes, setClasses] = useRecoilState(selectedClasses)

    useEffect(() => {
        if (!classes || !classes?.includes(id)) {
            setClasses(`${classes || ''} ${id}`)
        }
    }, [classes])

    return (
        <div className={main}>
            <MediaSelector />
            <PseudoSelector />
            <Divider />
            <Properties />
        </div>
    )
}

export default Inputs
