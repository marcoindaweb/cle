/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import Editor from './Editor'
import toJss from '../../../../../store/pagebuilder/style/Converter/ToJss'
import toCss from '../../../../../store/pagebuilder/style/Converter/ToCss'
import { useRecoilState } from 'recoil'
import { selected } from '../../../../../store/pagebuilder/components/store'
const Code = () => {
    const [selectedComponent, setSelected] = useRecoilState(selected)

    return selectedComponent ? (
        <Editor
            value={toCss.convert({ ...(selectedComponent?.properties.style || {}) })}
            onChange={(e) => {
                const converted = toJss.convert(e)
                setSelected({
                    ...selectedComponent,
                    properties: { ...selectedComponent?.properties, style: converted },
                })
            }}
        />
    ) : null
}

export default Code
