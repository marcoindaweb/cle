/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilValue } from 'recoil'
import { selected, selectedClasses } from '../../../../../../store/pagebuilder/components/store'
import { HTMLSelect } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import React from 'react'

const ClassSelector: React.FC<{ value: string; onChange: (v: string) => void }> = ({ value, onChange }) => {
    const component = useRecoilValue(selected)
    const rawClasses = useRecoilValue(selectedClasses)
    const classes: string[] = rawClasses?.split(' ').filter((v) => v !== component?.id) || []
    return component ? (
        <HTMLSelect value={value} onChange={(e) => onChange(e.target.value)}>
            <Translated message="Current component">{(msg) => <option value={component?.id}>{msg}</option>}</Translated>
            {classes.map((c) => (
                <option key={c} value={c}>
                    {c}
                </option>
            ))}
        </HTMLSelect>
    ) : null
}

export default ClassSelector
