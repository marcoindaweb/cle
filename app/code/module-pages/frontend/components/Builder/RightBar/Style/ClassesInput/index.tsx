/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { FormGroup, TagInput } from '@blueprintjs/core'
import React, { useCallback } from 'react'
import { useRecoilState } from 'recoil'
import { selected } from '../../../../../store/pagebuilder/components/store'
import Translated from '@linda/module-i18n/frontend/components/Translated'

const ClassesInput = () => {
    const [current, setCurrent] = useRecoilState(selected)
    const onSelect = useCallback(
        (v: string[]) => {
            if (current) {
                setCurrent({
                    ...current,
                    properties: { ...current.properties, className: v.join(' ') },
                })
            }
        },
        [current],
    )
    const currentClasses = current?.properties?.className?.split(' ').filter((v) => v !== current?.id) || []
    return (
        <FormGroup label={<Translated message="Classes" />}>
            <TagInput values={currentClasses} onChange={(v) => onSelect(v.map((i) => (i as string).toString()))} />
        </FormGroup>
    )
}

export default ClassesInput
