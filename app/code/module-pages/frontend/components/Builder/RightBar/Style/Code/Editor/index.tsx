/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { FormEvent, KeyboardEvent, useCallback, useEffect, useRef } from 'react'
import { createUseStyles } from 'react-jss'
import 'prismjs/themes/prism-okaidia.css'
import prism from 'prismjs'
import { Button } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'

const useStyles = createUseStyles({
    code: {
        '&:focus': {
            outline: 'none',
        },
    },
})

const Editor: React.FC<{ value: string; onChange: (v: string) => void }> = ({ value, onChange }) => {
    const ref = useRef<HTMLElement>(null)
    const { code } = useStyles()

    useEffect(() => {
        prism.highlightAll()
    }, [])

    useEffect(() => {
        if (ref.current) {
            ref.current.textContent = value
        }
    }, [value, ref])

    const onKeyDown = useCallback((event: KeyboardEvent) => {
        if (event.keyCode === 9) {
            event.preventDefault()
            const sel = window.getSelection()
            if (!sel) {
                return
            }
            const range = sel.getRangeAt(0)

            const tabNode = document.createTextNode('  ')
            range.insertNode(tabNode)

            range.setStartAfter(tabNode)
            range.setEndAfter(tabNode)
        }
    }, [])

    const onSave = useCallback(() => {
        if (ref.current) {
            onChange(ref.current.textContent || '')
        }
    }, [onChange, ref])

    return (
        <>
            <pre>
                <code ref={ref} contentEditable onKeyDown={onKeyDown} className={'language-css ' + code} />
            </pre>
            <Button onClick={onSave}>
                <Translated message="Update" />
            </Button>
        </>
    )
}
export default Editor
