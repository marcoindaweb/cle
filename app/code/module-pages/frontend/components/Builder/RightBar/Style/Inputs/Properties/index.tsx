/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { ReactElement } from 'react'
import PropertiesGroupCollapse from './PropertiesGroupCollapse'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import Property from './Property'
import TextPropertyInput from './TextPropertyInput'

type PropertyControl =
    | [string, Parameters<typeof Property>[0]['input']] // Used for single properties
    | [string[], ReactElement] // Used for multiple/dynamic properties, set and get of the value must be handled by the element
type PropertiesGroup = [string, PropertyControl[]]

const properties: PropertiesGroup[] = [
    ['layout', [['display', (props) => <TextPropertyInput {...props} label={<Translated message="Display" />} />]]],
    [
        'spacing',
        [
            ['margin', (props) => <TextPropertyInput {...props} label={<Translated message="Margin" />} />],
            ['padding', (props) => <TextPropertyInput {...props} label={<Translated message="Padding" />} />],
        ],
    ],
    [
        'size',
        [
            ['height', (props) => <TextPropertyInput {...props} label={<Translated message="Height" />} />],
            ['width', (props) => <TextPropertyInput {...props} label={<Translated message="Width" />} />],
        ],
    ],
    ['borders', []],
    ['position', []],
    ['typography', []],
    ['backgrounds', []],
]

const Properties: React.FC = () => {
    return (
        <>
            {properties.map((p) => (
                <PropertiesGroupCollapse key={p[0]} text={<Translated message={p[0]} />}>
                    {p[1].map((v) => {
                        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                        // @ts-ignore
                        return Array.isArray(v[0]) ? v[1] : <Property key={v[0]} property={v[0]} input={v[1]} />
                    })}
                </PropertiesGroupCollapse>
            ))}
        </>
    )
}

export default Properties
