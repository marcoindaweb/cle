/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { ReactNode, useState } from 'react'
import { Collapse, Icon } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
    top: {
        display: 'flex',
        justifyContent: 'space-between',
        padding: '3px 5px',
    },
})

const PropertiesGroupCollapse: React.FC<{ text: ReactNode }> = ({ children, text }) => {
    const [open, setOpen] = useState(false)
    const { top } = useStyles()

    return (
        <>
            <div className={top} onClick={() => setOpen(!open)}>
                <span>{text}</span>
                <Icon icon={open ? 'caret-up' : 'caret-down'} />
            </div>
            <Collapse isOpen={open}>{children}</Collapse>
        </>
    )
}

export default PropertiesGroupCollapse
