/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { JSXElementConstructor, ReactElement, ReactNode } from 'react'
import { useRecoilState } from 'recoil'
import { styleProperty } from '../../../../../../../store/pagebuilder/style/editor/store'

export type PropertyInput<V = {}> = JSXElementConstructor<
    { value: string | undefined; onChange: (v: string) => void } & V
>

const Property: React.FC<{
    property: string
    input: (v: { value: string | undefined; onChange: (v: string) => void }) => ReactElement
}> = ({ property, input }) => {
    const [prop, setProp] = useRecoilState(styleProperty(property))

    return input({ value: prop, onChange: setProp })
}

export default Property
