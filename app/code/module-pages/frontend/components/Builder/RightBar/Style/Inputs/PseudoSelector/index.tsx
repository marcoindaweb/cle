/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import { Button, ButtonGroup, FormGroup, MenuItem } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import { Select } from '@blueprintjs/select'
import { createUseStyles } from 'react-jss'
import { useRecoilState } from 'recoil'
import { pseudoSelector } from '../../../../../../store/pagebuilder/style/editor/store'

const useStyles = createUseStyles({
    popover: {
        '& .bp3-menu': {
            maxHeight: '300px',
            overflowY: 'auto',
        },
    },
})

const pseudoSelectors = [
    null,
    ':active',
    '::after',
    '::backdrop',
    '::before',
    ':checked',
    '::cue',
    '::cue-region',
    ':default',
    ':dir',
    ':disabled',
    ':empty',
    ':enabled',
    ':first',
    ':first-child',
    '::first-letter',
    '::first-line',
    ':first-of-type',
    ':focus',
    ':fullscreen',
    ':hover',
    ':in-range',
    ':indeterminate',
    ':invalid',
    ':lang',
    ':last-child',
    ':last-of-type',
    ':left',
    ':link',
    '::marker',
    ':not',
    ':nth-child',
    ':nth-last-child',
    ':nth-last-of-type',
    ':nth-of-type',
    ':only-child',
    ':only-of-type',
    ':optional',
    ':out-of-range',
    '::placeholder',
    ':read-only',
    ':read-write',
    ':required',
    ':right',
    ':root',
    ':scope',
    '::selection',
    '::slotted',
    ':target',
    ':valid',
    ':visited',
]

const PseudoSelector: React.FC = () => {
    const { popover } = useStyles()
    const [value, onChange] = useRecoilState(pseudoSelector)

    return (
        <FormGroup label={<Translated message="Pseudo Selector" />}>
            <Select<string | null>
                items={pseudoSelectors}
                popoverProps={{ popoverClassName: popover }}
                itemRenderer={(v, { handleClick, modifiers }) => (
                    <MenuItem onClick={handleClick} {...modifiers} text={v || <Translated message="None" />} />
                )}
                activeItem={value}
                onItemSelect={onChange}
                filterable
                itemPredicate={(q, v) => v?.includes(q) || false}
            >
                <ButtonGroup>
                    <Button rightIcon="caret-down" text={value || <Translated message="None" />} />
                    {value !== null && <Button icon="cross" onClick={() => onChange(null)} />}
                </ButtonGroup>
            </Select>
        </FormGroup>
    )
}

export default PseudoSelector
