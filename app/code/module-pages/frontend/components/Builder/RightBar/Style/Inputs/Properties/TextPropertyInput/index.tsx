/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { PropertyInput } from '../Property'
import { ReactNode } from 'react'
import { FormGroup, InputGroup } from '@blueprintjs/core'
import React from 'react'

const TextPropertyInput: PropertyInput<{ label: ReactNode }> = ({ value, onChange, label }) => {
    return (
        <FormGroup inline label={label}>
            <InputGroup small value={value} onChange={(e) => onChange(e.target.value)} />
        </FormGroup>
    )
}

export default TextPropertyInput
