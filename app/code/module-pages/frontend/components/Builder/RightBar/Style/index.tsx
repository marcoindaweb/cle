/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import Code from './Code'
import { useRecoilValue } from 'recoil'
import { selectedComponentDeclaration, selectedId } from '../../../../store/pagebuilder/components/store'
import ClassesInput from './ClassesInput'
import { NonIdealState } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import Inputs from './Inputs'

const Style = () => {
    const selId = useRecoilValue(selectedId)
    const declaration = useRecoilValue(selectedComponentDeclaration)
    const showCode = declaration?.config?.showStyleAsCode === true

    return declaration && selId ? (
        <>
            {!(declaration?.config?.isAbstract === true) && <ClassesInput />}
            {showCode ? <Code /> : <Inputs id={selId} />}
        </>
    ) : (
        <NonIdealState title={<Translated message="Select a component" />} />
    )
}

export default Style
