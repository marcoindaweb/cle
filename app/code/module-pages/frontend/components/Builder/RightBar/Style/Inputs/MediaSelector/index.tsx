/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Button, ButtonGroup } from '@blueprintjs/core'
import React from 'react'
import { useRecoilState, useRecoilValue } from 'recoil'
import { availableDevices } from '../../../../../../store/pagebuilder/device/store'
import { media, mediaQuery } from '../../../../../../store/pagebuilder/style/editor/store'

const MediaSelector: React.FC = () => {
    const devices = useRecoilValue(availableDevices)
    const [value, setVal] = useRecoilState(media)

    return (
        <ButtonGroup>
            <Button onClick={() => setVal(null)} icon="asterisk" disabled={value === null} />
            {devices.map((d) => (
                <Button key={d.name} icon={d.icon} onClick={() => setVal(d)} disabled={d.name === value?.name} />
            ))}
        </ButtonGroup>
    )
}

export default MediaSelector
