/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilValue } from 'recoil'
import { ComponentSeparation, separation } from '../../../../../store/pagebuilder/componentsList/store'
import { SymbolDefinition } from '../../../../../types/SymbolDefinition'
import ComponentDeclaration from '../../../../../types/ComponentDeclaration'
import Base from './Base'
import React from "react";

const List: React.FC<{ items: Array<SymbolDefinition | ComponentDeclaration> }> = ({ items }) => {
    const groupType = useRecoilValue(separation)

    return groupType === ComponentSeparation.none ? <Base items={items} /> : null
}
