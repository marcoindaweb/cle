/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { SymbolDefinition } from '../../../../../../../types/SymbolDefinition'
import ComponentDeclaration from '../../../../../../../types/ComponentDeclaration'
import { useState } from 'react'
import { createUseStyles } from 'react-jss'
import { Icon } from '@blueprintjs/core'
import React from 'react'
import Base from '../../Base'

const useStyles = createUseStyles({
    group: {
        display: 'grid',
        '&>.top': {
            background: 'var(--light-gray-color)',
            borderBottom: 'var(--gray-color)',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: '3px 5px',
            '&>.toggle': {
                cursor: 'pointer',
            },
        },
    },
})

const Group: React.FC<{ name: string; items: Array<SymbolDefinition | ComponentDeclaration> }> = ({ name, items }) => {
    const [open, setOpen] = useState(false)
    const { group } = useStyles()

    return (
        <div className={group}>
            <div className="top">
                <span>{name}</span>
                <Icon
                    className="toggle"
                    onClick={() => setOpen(!open)}
                    icon={open ? 'caret-up' : 'caret-down'}
                    iconSize={10}
                />
            </div>
            {open && <Base items={items} />}
        </div>
    )
}

export default Group
