/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import ComponentDeclaration, {
    COMPONENT_DECLARATION,
    ComponentDeclarationDrag,
} from '../../../../../../types/ComponentDeclaration'
import { useDrag } from 'react-dnd'
import React from 'react'
import { useSetRecoilState } from 'recoil'
import { dragging } from '../../../../../../store/pagebuilder/drag/store'
import Element from '../Element'

const Component: React.FC<{ declaration: ComponentDeclaration }> = ({ declaration }) => {
    const setIsDragging = useSetRecoilState(dragging)
    const [, ref] = useDrag<ComponentDeclarationDrag, any, any>({
        item: { ...declaration, type: COMPONENT_DECLARATION },
        begin: () => {
            setIsDragging(true)
        },
        end: () => {
            setIsDragging(false)
        },
        options: {
            dropEffect: 'copy',
        },
    })

    return (
        <Element
            name={declaration.meta?.displayName || declaration.id}
            icon={declaration.meta?.icon || 'cube'}
            ref={ref}
            isSymbol={false}
            nameString={
                typeof declaration.meta?.displayName === 'string' ? declaration.meta.displayName : declaration.id
            }
        />
    )
}

export default Component
