/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { SYMBOL_DEFINITION, SymbolDefinition, SymbolDefinitionDrag } from '../../../../../../types/SymbolDefinition'
import Element from '../Element'
import React from 'react'
import { useDrag } from 'react-dnd'
import { useSetRecoilState } from 'recoil'
import { dragging } from '../../../../../../store/pagebuilder/drag/store'

const Symbol: React.FC<{ symbol: SymbolDefinition }> = ({ symbol }) => {
    const setIsDragging = useSetRecoilState(dragging)
    const [, ref] = useDrag<SymbolDefinitionDrag, any, any>({
        item: { ...symbol, type: SYMBOL_DEFINITION },
        begin: () => {
            setIsDragging(true)
        },
        end: () => {
            setIsDragging(false)
        },
        options: {
            dropEffect: 'copy',
        },
    })
    return <Element name={symbol.name} icon={'star'} ref={ref} isSymbol nameString={symbol.name} />
}

export default Symbol
