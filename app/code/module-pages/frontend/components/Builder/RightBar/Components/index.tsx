/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useEffect } from 'react'
import { useRecoilState, useRecoilValue } from 'recoil'
import { visibleComponentDeclarations } from '../../../../store/componentDeclarations/store'
import Component from './ListItem/Component'
import { createUseStyles } from 'react-jss'
import { Colors, InputGroup } from '@blueprintjs/core'
import { useFindPageBuilderSymbol } from '../../../../graphql/symbols/queries'
import { pageBuilderSymbolToSymbolDefinition } from '../../../../store/layout/utils/manipulation'
import Symbol from './ListItem/Symbol'
import { symbolDefinitions } from '../../../../store/pagebuilder/symbols/store'
import Filters from './Filters'
import { nameFilter, showAsList } from '../../../../store/pagebuilder/componentsList/store'
import useTranslate from '@linda/module-i18n/frontend/hooks/useTranslate'

const useStyles = createUseStyles({
    container: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    controls: {
        display: 'flex',
    },
    main: {
        overflowY: 'auto',
        display: 'grid',
        gridTemplateColumns: (props) => (props.asList ? '1fr' : 'repeat(2, 1fr)'),
        gridColumnGap: '1px',
        gridRowGap: '1px',
        background: Colors.GRAY5,
        '&>div': {
            cursor: 'grab',
            background: Colors.WHITE,
            height: (props) => (props.asList ? 'auto' : '100px'),
        },
    },
})

const NameFilter = () => {
    const [val, setVal] = useRecoilState(nameFilter)
    const translate = useTranslate()
    return <InputGroup placeholder={translate('Filter by name')} value={val} onChange={(e) => setVal(e.target.value)} />
}

const Components = () => {
    const asList = useRecoilValue(showAsList)
    const { main, controls, container } = useStyles({ asList })
    const declarations = useRecoilValue(visibleComponentDeclarations)
    const data = useFindPageBuilderSymbol()
    const [definitions, setDefinitions] = useRecoilState(symbolDefinitions)
    useEffect(() => {
        if (data.data?.findPageBuilderSymbol) {
            setDefinitions(data.data?.findPageBuilderSymbol.map(pageBuilderSymbolToSymbolDefinition))
        }
    }, [data.data])
    return (
        <div className={container}>
            <div className={controls}>
                <Filters />
                <NameFilter />
            </div>
            <div className={main}>
                {declarations.map((v) => (
                    <Component declaration={v} key={v.id} />
                ))}
                {definitions.map((v) => (
                    <Symbol key={v.id} symbol={v} />
                ))}
            </div>
        </div>
    )
}

export default Components
