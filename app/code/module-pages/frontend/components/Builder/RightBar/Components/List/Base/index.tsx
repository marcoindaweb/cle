/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import { Colors } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'
import ComponentInstance from '../../../../../../types/ComponentInstance'
import { SymbolDefinition } from '../../../../../../types/SymbolDefinition'
import ListItem from '../../ListItem'
import ComponentDeclaration from '../../../../../../types/ComponentDeclaration'

const useStyles = createUseStyles({
    base: {
        overflowY: 'auto',
        display: 'grid',
        gridTemplateColumns: (props) => (props.asList ? '1fr' : 'repeat(2, 1fr)'),
        gridColumnGap: '1px',
        gridRowGap: '1px',
        background: Colors.GRAY5,
        '&>div': {
            cursor: 'grab',
            background: Colors.WHITE,
            height: (props) => (props.asList ? 'auto' : '100px'),
        },
    },
})

const Base: React.FC<{ items: Array<ComponentDeclaration | SymbolDefinition> }> = ({ children, items }) => {
    const { base } = useStyles()
    return (
        <div className={base}>
            {items.map((item) => (
                <ListItem key={item.id} item={item} />
            ))}
        </div>
    )
}

export default Base
