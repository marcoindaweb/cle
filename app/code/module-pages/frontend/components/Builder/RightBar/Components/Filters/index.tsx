/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilState, useRecoilValue } from 'recoil'
import {
    ComponentSeparation,
    isSymbolFilter,
    nameFilter,
    separation,
    showAsList,
    tagFilter,
    tags,
} from '../../../../../store/pagebuilder/componentsList/store'
import {
    Button,
    ControlGroup,
    FormGroup,
    InputGroup,
    MenuItem,
    Popover,
    Position,
    Switch,
    Divider,
    ButtonGroup,
    HTMLSelect,
} from '@blueprintjs/core'
import React, { useState } from 'react'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import { createUseStyles } from 'react-jss'
import { MultiSelect } from '@blueprintjs/select'
import useTranslate from '@linda/module-i18n/frontend/hooks/useTranslate'

const NameFilter = () => {
    const [name, setName] = useRecoilState(nameFilter)

    return (
        <FormGroup label={<Translated message="Name" />}>
            <ControlGroup>
                <InputGroup value={name || ''} onChange={(e) => setName(e.target.value)} />
                {name && <Button icon="cross" onClick={() => setName(undefined)} />}
            </ControlGroup>
        </FormGroup>
    )
}

const TagFilter = () => {
    const [filter, setFilter] = useRecoilState(tagFilter)
    const availableValues = useRecoilValue(tags)

    return (
        <FormGroup label={<Translated message="Tags" />}>
            <MultiSelect<string>
                selectedItems={filter}
                itemPredicate={(query, item) => item.toLowerCase().includes(query.toLowerCase())}
                itemRenderer={(v, i) => (
                    <MenuItem
                        key={i.index}
                        {...i.modifiers}
                        onClick={i.handleClick}
                        text={<Translated message={v} />}
                    />
                )}
                fill
                onItemSelect={(v) => setFilter([...filter, v])}
                items={availableValues.filter((v) => !filter.includes(v))}
                tagInputProps={{ onRemove: (_, i) => setFilter(filter.filter((_, index) => index !== i)) }}
                tagRenderer={(v) => <Translated message={v} />}
            />
        </FormGroup>
    )
}

const SymbolsFilter = () => {
    const [filter, setFilter] = useRecoilState(isSymbolFilter)
    return (
        <FormGroup inline label={<Translated message="Only Symbols" />}>
            <Switch checked={filter} onChange={() => setFilter(!filter)} />{' '}
        </FormGroup>
    )
}

const useStyles = createUseStyles({
    button: {
        marginBottom: '10px',
    },
    filters: {
        display: 'flex',
        flexDirection: 'column',
        padding: '5px',
        '&>.close': {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end',
        },
    },
})

const ShowAsListToggle = () => {
    const [val, setVal] = useRecoilState(showAsList)

    return (
        <ButtonGroup>
            <Button onClick={() => setVal(false)} disabled={!val} minimal icon="grid-view" />
            <Button onClick={() => setVal(true)} disabled={val} minimal icon="list" />
        </ButtonGroup>
    )
}

const SelectSeparationType = () => {
    const [val, setVal] = useRecoilState(separation)
    const translate = useTranslate()
    return (
        <HTMLSelect value={val} onChange={(e) => setVal(parseInt(e.target.value))}>
            <option value={ComponentSeparation.none}>{translate('None')}</option>
            <option value={ComponentSeparation.byTag}>{translate('By Tag')}</option>
        </HTMLSelect>
    )
}

const Filters = () => {
    const [isOpen, setIsOpen] = useState(false)
    const { filters, button } = useStyles()
    return (
        <Popover
            isOpen={isOpen}
            position={Position.RIGHT}
            target={<Button minimal icon="settings" onClick={() => setIsOpen(!isOpen)} className={button} />}
            content={
                <div className={filters}>
                    <div className="close">
                        <Button minimal icon="cross" onClick={() => setIsOpen(false)} />
                    </div>
                    <b>
                        <Translated message="Filters" />
                    </b>
                    <NameFilter />
                    <TagFilter />
                    <SymbolsFilter />
                    <Divider />
                    <ShowAsListToggle />
                    <SelectSeparationType />
                </div>
            }
        />
    )
}

export default Filters
