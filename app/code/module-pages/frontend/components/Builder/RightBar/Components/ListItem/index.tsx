/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import ComponentDeclaration, { isComponentDeclaration } from '../../../../../types/ComponentDeclaration'
import { SymbolDefinition } from '../../../../../types/SymbolDefinition'
import Component from './Component'
import Symbol from './Symbol'

const ListItem: React.FC<{ item: ComponentDeclaration | SymbolDefinition }> = ({ item }) => {
    return isComponentDeclaration(item) ? <Component declaration={item} /> : <Symbol symbol={item} />
}

export default ListItem
