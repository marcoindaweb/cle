/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Divider, Icon, IIconProps } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import React from 'react'
import { useRecoilValue } from 'recoil'
import {
    groupFilter,
    isSymbolFilter,
    nameFilter,
    showAsList,
    tagFilter,
} from '../../../../../../store/pagebuilder/componentsList/store'

const useStyles = createUseStyles({
    grid: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: '20px 0',
    },
    list: {
        display: 'flex',
        alignItems: 'stretch',
        flexDirection: 'row',
        padding: '5px 3px',
        justifyContent: 'flex-start',
        '&>*': {
            marginTop: 0,
            marginBottom: 0,
        },
    },
})

// eslint-disable-next-line @typescript-eslint/no-unused-vars,react/display-name
const Element = React.forwardRef<
    HTMLDivElement,
    {
        name: string | JSX.Element
        icon: IIconProps['icon']
        tags?: string[]
        group?: string
        isSymbol: boolean
        nameString: string
    }
>((props, ref) => {
    const fGroup = useRecoilValue(groupFilter)
    const fTag = useRecoilValue(tagFilter)
    const fName = useRecoilValue(nameFilter)
    const fSymbols = useRecoilValue(isSymbolFilter)
    const asList = useRecoilValue(showAsList)

    let shouldShow = true
    if (fGroup && shouldShow) {
        shouldShow = fGroup === props.group
    }
    if (fTag.length > 0 && shouldShow) {
        shouldShow = props.tags?.some((v) => fTag.includes(v)) || true
    }
    if (fName && shouldShow) {
        const name = props.nameString
        shouldShow = name.toLowerCase().includes(fName.toLowerCase().trim())
    }
    if (fSymbols && shouldShow) {
        shouldShow = props.isSymbol
    }

    return shouldShow ? (
        asList ? (
            <ListViewElement ref={ref} name={props.name} icon={props.icon} />
        ) : (
            <GridViewElement ref={ref} name={props.name} icon={props.icon} />
        )
    ) : null
})

// eslint-disable-next-line @typescript-eslint/no-unused-vars,react/display-name
const GridViewElement = React.forwardRef<HTMLDivElement, { name: string | JSX.Element; icon: IIconProps['icon'] }>(
    (props, ref) => {
        const { grid } = useStyles()
        return (
            <div ref={ref} className={grid}>
                <Icon icon={props.icon} iconSize={22} />
                {typeof props.name === 'string' ? <Translated message={props.name} /> : props.name}
            </div>
        )
    },
)

// eslint-disable-next-line @typescript-eslint/no-unused-vars,react/display-name
const ListViewElement = React.forwardRef<HTMLDivElement, { name: string | JSX.Element; icon: IIconProps['icon'] }>(
    (props, ref) => {
        const { list } = useStyles()
        return (
            <div ref={ref} className={list}>
                <div className="icon">
                    <Icon icon={props.icon} iconSize={18} />
                </div>
                <Divider />
                {typeof props.name === 'string' ? <Translated message={props.name} /> : props.name}
            </div>
        )
    },
)

export default Element
