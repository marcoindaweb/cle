/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useState } from 'react'
import {
    Boundary,
    Button,
    Icon,
    IconName,
    Menu,
    MenuItem,
    OverflowList,
    Popover,
    Position,
    Tab,
    Tabs,
    Tooltip,
} from '@blueprintjs/core'
import Style from './Style'
import Settings from './Settings'
import Actions from './Actions'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import Components from './Components'
import Container from '../../../etc/Container'
import Layers from './Layers'
import { useRecoilState, useRecoilValue } from 'recoil'
import { selectedComponentDeclaration } from '../../../store/pagebuilder/components/store'
import { Resizable } from 're-resizable'
import { minRightBarWidth, rightBarWidth } from '../../../store/pagebuilder/ui/store'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
    main: {
        position: 'relative',
        '& .right-container__resizable': {
            position: 'absolute !important',
            top: 0,
            right: 0,
        },
        '& .right-container__content': {
            height: '100%',
            display: 'grid',
            gridTemplateRows: '4% minmax(0%, 96%)',
            '&>.buttons': {
                display: 'flex',
                justifyContent: 'space-around',
            },
            '&>.tabs>*': {
                maxHeight: '100%',
                marginTop: 0,
            },
        },
    },
})

const RightBar: React.FC<{ className?: string }> = ({ className }) => {
    const { main } = useStyles()
    const [tab, setTab] = useState<string | number>('components')
    const declaration = useRecoilValue(selectedComponentDeclaration)
    const [width, setWidth] = useRecoilState(rightBarWidth)

    return (
        <div className={main + ' ' + className}>
            <Resizable
                enable={{ left: true }}
                className="right-container__resizable"
                size={{ width, height: '100%' }}
                minWidth={minRightBarWidth}
                onResizeStop={(a, b, c, d) => setWidth(width + d.width)}
            >
                <Container className="right-container__content">
                    <OverflowList<[string, IconName]>
                        className="buttons"
                        observeParents
                        items={[
                            ['components', 'widget-button'],
                            ['style', 'style'],
                            ['settings', 'cog'],
                            ['actions', 'offline'],
                            ['layers', 'layers'],
                        ]}
                        collapseFrom={Boundary.END}
                        visibleItemRenderer={(i, index) => (
                            <Tooltip key={index} content={<Translated message={i[0]} />} position={Position.BOTTOM}>
                                <Button
                                    minimal
                                    small
                                    icon={i[1]}
                                    intent={i[0] === tab ? 'primary' : 'none'}
                                    onClick={() => setTab(i[0])}
                                />
                            </Tooltip>
                        )}
                        overflowRenderer={(i) => (
                            <Popover
                                content={
                                    <Menu>
                                        {i.map((v) => (
                                            <MenuItem
                                                icon={v[1]}
                                                active={v[0] === tab}
                                                onClick={() => setTab(v[0])}
                                                key={v[0]}
                                                text={<Translated message={v[0]} />}
                                            />
                                        ))}
                                    </Menu>
                                }
                                target={
                                    <Button
                                        small
                                        minimal
                                        intent={i.findIndex((v) => v[0] === tab) !== -1 ? 'primary' : 'none'}
                                        icon="more"
                                    />
                                }
                            />
                        )}
                    />
                    <Tabs className="tabs" selectedTabId={tab} onChange={(v) => setTab(v)}>
                        <Tab id="components" panel={<Components />} />
                        <Tab
                            disabled={declaration?.config?.haveStyle === false || !declaration}
                            id="style"
                            panel={<Style />}
                        />
                        <Tab
                            id="settings"
                            disabled={declaration?.config?.haveOptions === false || !declaration}
                            panel={<Settings />}
                        />
                        <Tab id="actions" panel={<Actions />} />
                        <Tab id="layers" panel={<Layers />} />
                    </Tabs>
                </Container>
            </Resizable>
        </div>
    )
}

export default RightBar
