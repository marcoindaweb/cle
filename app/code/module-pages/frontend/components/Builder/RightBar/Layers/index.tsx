/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilValue } from 'recoil'
import { currentRootComponentId } from '../../../../store/layout/store'
import React from 'react'
import Layer from './Layer'

const Layers: React.FC = () => {
    const v = useRecoilValue(currentRootComponentId)

    return <div>{v && <Layer nestLevel={0} key={v} id={v} />}</div>
}

export default Layers
