/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useEffect, useRef } from 'react'
import { useSetRecoilState } from 'recoil'
import { useDrag } from 'react-dnd'
import { COMPONENT_INSTANCE, ComponentInstanceDrag } from '../../../../../../types/ComponentInstance'
import { UnknownDrag } from '../../../../../../types/UnknownDrag'
import { draggedId } from '../../../../../../store/pagebuilder/layers/store'

const Drag: React.FC<{ id: string }> = ({ id, children }) => {
    const ref = useRef<HTMLDivElement>(null)
    const setDraggingId = useSetRecoilState(draggedId)
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const [, dragRef, preview] = useDrag<ComponentInstanceDrag | UnknownDrag, any, any>({
        item: { id, type: COMPONENT_INSTANCE },
        begin: () => {
            setDraggingId(id)
        },
        end: () => {
            setDraggingId(undefined)
        },
    })
    useEffect(() => {
        if (!!ref.current) {
            dragRef(ref)
            preview(ref)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [!!ref.current])
    return <div ref={ref}>{children}</div>
}

export default Drag
