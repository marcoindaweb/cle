/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { CSSProperties, useCallback, useEffect, useRef, useState, MouseEvent } from 'react'
import { useRecoilState, useRecoilValue } from 'recoil'
import { componentChildrenIds, componentIcon } from '../../../../../store/layout/store'
import { selectedId } from '../../../../../store/pagebuilder/components/store'
import { Colors, EditableText, Icon } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'
import { componentAlias } from '../../../../../store/pagebuilder/aliases/store'
import { useMoveComponent } from '../../../../../store/layout/actions'
import { COMPONENT_INSTANCE, ComponentInstanceDrag } from '../../../../../types/ComponentInstance'
import Position from '../../../../../types/Position'
import { useDrop } from 'react-dnd'
import { draggedId, draggedOverId, isOpen } from '../../../../../store/pagebuilder/layers/store'
import Drag from './Drag'
import ComponentContextMenu from '../../../ComponentContextMenu'
import { componentSymbolId, symbolAlias, symbolChildrenIds } from '../../../../../store/pagebuilder/symbols/store'

const useStyle = createUseStyles({
    layer: {
        display: 'flex',
        flexDirection: 'column',
        position: 'relative',
        '&>.info': {
            '&>div': {
                borderRadius: '4px',
                cursor: 'pointer',
                padding: '2px 4px',
                '&>div': {
                    display: 'flex',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    '&>*:not(:last-child)': {
                        marginRight: '5px',
                    },
                },
            },
            '&.selected>div': {
                background: 'var(--primary-color)',
                color: Colors.WHITE,
            },
            '&.selected.symbol>div': {
                background: 'var(--violet-color)',
            },
            '&.selected>div input': {
                color: Colors.BLACK,
            },
            '& .hidden-icon': {
                opacity: 0,
            },
        },
        '&>.drag-bar': {
            borderRadius: '4px',
            pointerEvents: 'none',
            transition: 'transform .2s, height .2s, width .2s',
            border: '1px solid var(--pr-cl-l)',
            position: 'absolute',
            top: '0',
        },
    },
})

const Layer: React.FC<{ id: string; draggingParent?: boolean; nestLevel: number }> = ({
    id,
    draggingParent,
    nestLevel,
}) => {
    const { layer } = useStyle()
    const dragged = useRecoilValue(draggedId)
    const [draggedOver, setDraggedOver] = useRecoilState(draggedOverId)
    const compChildren = useRecoilValue(componentChildrenIds(id))
    const icon = useRecoilValue(componentIcon(id))
    const [sId, setSelected] = useRecoilState(selectedId)
    const [open, setOpen] = useRecoilState(isOpen(id))
    const cAliasState = useRecoilState(componentAlias(id))
    const symbol = useRecoilValue(componentSymbolId(id))
    const symbolChildren = useRecoilValue(symbolChildrenIds(symbol || ''))
    const sAliasState = useRecoilState(symbolAlias(symbol || ''))
    const [layerName, setLayerName] = symbol ? sAliasState : cAliasState
    const [editing, setEditing] = useState(false)
    const [dragBarStyle, setDragBarStyle] = useState<CSSProperties>({})
    const infoRef = useRef<HTMLDivElement>(null)
    const insideRef = useRef<HTMLDivElement>(null)
    const move = useMoveComponent()
    const [dropPosition, setDropPosition] = useState<Position>(Position.inside)
    const canDrop = dragged !== id && !draggingParent
    const children = symbol ? symbolChildren : compChildren
    const hasChildren = children.length > 0

    const onDrop = useCallback(
        (comp: { id: string }, position: Position) => {
            if (canDrop) {
                move(comp.id, { element: id, position })
                setDraggedOver(undefined)
            }
        },
        [move, id, canDrop, setDraggedOver],
    )
    const onDoubleClick = useCallback(
        (e: MouseEvent) => {
            e.stopPropagation()
            setEditing(true)
        },
        [setEditing],
    )

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const [, topDrop] = useDrop<ComponentInstanceDrag, any, undefined>({
        accept: COMPONENT_INSTANCE,
        hover: (item, monitor) => {
            const top = (monitor.getClientOffset()?.y || 0) - (infoRef.current?.getBoundingClientRect()?.top || 0)
            const height = infoRef.current?.getBoundingClientRect()?.height || 0
            if (monitor.isOver({ shallow: true })) {
                setDropPosition(
                    top < height * 0.25 ? Position.before : top < height * 0.75 ? Position.inside : Position.after,
                )
            }
            if (monitor.isOver() && draggedOver !== id) {
                setDraggedOver(id)
            }
        },
        drop: (item) => {
            onDrop(item, dropPosition)
        },
    })

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const [, insideDrop] = useDrop<ComponentInstanceDrag, any, undefined>({
        accept: COMPONENT_INSTANCE,
        hover: (_, monitor) => {
            if (hasChildren && !open && monitor.isOver()) {
                setTimeout(() => {
                    if (monitor.isOver({ shallow: true })) {
                        setOpen(true)
                    }
                }, 500)
            }
        },
    })

    useEffect(() => {
        if (infoRef.current) {
            topDrop(infoRef.current)
        }
    }, [infoRef, topDrop])

    useEffect(() => {
        if (insideRef.current) {
            insideDrop(insideRef.current)
        }
    }, [insideRef, insideDrop])

    useEffect(() => {
        const insideRect = insideRef.current?.getBoundingClientRect()
        const infoRect = infoRef.current?.getBoundingClientRect()
        if (!insideRect || !infoRect) return
        setDragBarStyle({
            width: dropPosition === Position.inside ? infoRect.width || 0 : '100%',
            height: dropPosition === Position.inside ? infoRect.height || 0 : '2px',
            transform: dropPosition === Position.after ? `translateY(${infoRect.height}px)` : 'translateY(0)',
        })
    }, [dropPosition])

    return (
        <ComponentContextMenu id={id}>
            <div className={layer}>
                {draggedOver === id && canDrop && dragged && <div className="drag-bar" style={dragBarStyle} />}
                <div
                    ref={infoRef}
                    className={`info ${symbol ? 'symbol' : ''} ${sId === id ? 'selected' : ''}`}
                    onClick={() => setSelected(id)}
                >
                    <div style={{ paddingLeft: nestLevel * 8 }}>
                        <Drag id={id}>
                            <div ref={insideRef}>
                                <Icon
                                    className={hasChildren ? '' : 'hidden-icon'}
                                    icon={open ? 'caret-up' : 'caret-down'}
                                    onClick={() => (hasChildren ? setOpen(!open) : null)}
                                />
                            </div>
                            <Icon iconSize={12} icon={symbol ? 'star' : icon} />
                            <div onClick={(e) => e.stopPropagation()} onDoubleClick={onDoubleClick}>
                                <EditableText
                                    onCancel={(v) => setLayerName(v)}
                                    isEditing={editing}
                                    onConfirm={() => setEditing(false)}
                                    value={layerName}
                                    onChange={setLayerName}
                                />
                            </div>
                        </Drag>
                    </div>
                </div>
                {open && hasChildren && (
                    <div className="children">
                        {children.map((v) => (
                            <Layer draggingParent={draggedOver === id} id={v} key={v} nestLevel={nestLevel + 1} />
                        ))}
                    </div>
                )}
            </div>
        </ComponentContextMenu>
    )
}

export default Layer
