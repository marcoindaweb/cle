/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useLoadLayoutById } from '../../../hooks/useLoadLayoutById'
import LayoutsList from '../LayoutsList'
import React, { useCallback, useEffect } from 'react'
import LoadingOverlay from '@linda/module-admin/frontend/components/LoadingOverlay'
import { useRecoilState, useSetRecoilState } from 'recoil'
import { isSelectionOpen } from '../../../store/pagebuilder/layout/open/store'
import { Button, Classes, Dialog } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'

export const OpenLayoutMenu = () => {
    const [loadById, { loading, called }] = useLoadLayoutById()
    const [isOpen, setIsOpen] = useRecoilState(isSelectionOpen)
    useEffect(() => {
        if (called && !loading) {
            setIsOpen(false)
        }
    }, [called, loading])

    return (
        <>
            {loading && <LoadingOverlay />}
            <Dialog isOpen={isOpen}>
                <div className={Classes.DIALOG_HEADER}>
                    <h4 className={Classes.HEADING}>
                        <Translated message="Select a layout to open" />
                    </h4>
                    <Button
                        aria-label="Close"
                        minimal
                        icon="cross"
                        className={Classes.DIALOG_CLOSE_BUTTON}
                        onClick={() => setIsOpen(false)}
                    />
                </div>
                <LayoutsList onSelect={(v) => loadById(v)} />
            </Dialog>
        </>
    )
}

export default OpenLayoutMenu
