/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useGetAllLayouts } from '../../../graphql/layouts/queries'
import ApolloError from '@linda/module-admin/frontend/components/ApolloError'
import React from 'react'
import LoadingOverlay from '@linda/module-admin/frontend/components/LoadingOverlay'
import { HTMLTable } from '@blueprintjs/core'

const LayoutsList: React.FC<{ onSelect: (id: number) => void }> = ({ onSelect }) => {
    const { data, loading, error } = useGetAllLayouts()
    if (error) {
        return <ApolloError error={error} />
    }

    if (loading) {
        return <LoadingOverlay />
    }

    if (data?.findPagelayout) {
        return (
            <HTMLTable interactive>
                <tbody>
                    {data?.findPagelayout.map((v) => (
                        <tr onClick={() => onSelect(v.id)} key={v.id}>
                            <td>{v.name}</td>
                        </tr>
                    ))}
                </tbody>
            </HTMLTable>
        )
    }

    return null
}

export default LayoutsList
