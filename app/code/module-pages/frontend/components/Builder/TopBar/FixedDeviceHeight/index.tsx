/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import { useRecoilState } from 'recoil'
import { fixedDeviceHeight } from '../../../../store/pagebuilder/device/store'
import { Button, Tooltip } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'

const FixedDeviceHeight: React.FC = () => {
    const [isFixed, setFixed] = useRecoilState(fixedDeviceHeight)

    return (
        <Tooltip content={<Translated message="Fix the device height" />}>
            <Button
                icon="arrows-vertical"
                intent={isFixed ? 'primary' : 'none'}
                minimal
                onClick={() => setFixed(!isFixed)}
            />
        </Tooltip>
    )
}

export default FixedDeviceHeight
