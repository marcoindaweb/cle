/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useCallback, useState } from 'react'
import { Button, Menu, MenuItem, Popover } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import { useSetRecoilState } from 'recoil'
import { isSelectionOpen } from '../../../../store/pagebuilder/layout/open/store'
import { isLayoutFormOpen } from '../../../../store/pagebuilder/layout/form/store'
import { useSaveCurrentLayout } from '../../../../hooks/useSaveCurrentLayout'
import LoadingOverlay from '@linda/module-admin/frontend/components/LoadingOverlay'

const PageMenu: React.FC = () => {
    const setIsSelectionOpen = useSetRecoilState(isSelectionOpen)
    const openSelection = useCallback(() => {
        setIsSelectionOpen(true)
    }, [setIsSelectionOpen])
    const saveLayout = useSaveCurrentLayout()
    const [isSaving, setIsSaving] = useState(false)
    const onSaveClick = useCallback(async () => {
        setIsSaving(true)
        await saveLayout()
        setIsSaving(false)
    }, [setIsSaving, saveLayout])

    const setIsNewFormOpen = useSetRecoilState(isLayoutFormOpen)

    return (
        <>
            <Popover
                target={<Button icon="menu" minimal />}
                content={
                    <Menu>
                        <MenuItem onClick={openSelection} text={<Translated message="Open" />} />
                        <MenuItem onClick={onSaveClick} text={<Translated message="Save" />} />
                        <MenuItem onClick={() => setIsNewFormOpen(true)} text={<Translated message="New" />} />
                        <MenuItem text={<Translated message="Export" />} />
                        <MenuItem text={<Translated message="Import" />} />
                    </Menu>
                }
            />
            {isSaving && <LoadingOverlay />}
        </>
    )
}

export default PageMenu
