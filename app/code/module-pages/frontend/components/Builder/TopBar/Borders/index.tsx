/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilState } from 'recoil'
import { show } from '../../../../store/pagebuilder/borders/store'
import { Button } from '@blueprintjs/core'
import React from 'react'
import icons from '../../../../icons'

const Borders = () => {
    const [borders, setBorders] = useRecoilState(show)

    return (
        <Button
            minimal
            icon={borders ? icons.borderFill : icons.borderNone}
            onClick={() => setBorders(!borders)}
            intent={borders ? 'primary' : 'none'}
        />
    )
}

export default Borders
