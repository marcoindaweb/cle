/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilState } from 'recoil'
import React, { useCallback, useEffect, useState } from 'react'
import { Button, ControlGroup, InputGroup } from '@blueprintjs/core'
import { path } from '../../../../store/pagebuilder/router/store'

const Route = () => {
    const [p, setPath] = useRecoilState(path)
    const [newPath, setNewPath] = useState(p)
    const onChange = useCallback(
        (e: string) => {
            setPath(e)
        },
        [setPath],
    )
    useEffect(() => {
        setNewPath(p)
    }, [p])

    return (
        <ControlGroup>
            <InputGroup value={newPath} onChange={(e) => setNewPath(e.target.value)} />
            <Button icon="refresh" disabled={newPath === p} onClick={() => onChange(newPath)} />
        </ControlGroup>
    )
}

export default Route
