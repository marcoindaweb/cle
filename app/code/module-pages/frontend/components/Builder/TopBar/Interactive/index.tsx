/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilState } from 'recoil'
import { interactive } from '../../../../store/pagebuilder/state/store'
import { Button, Tooltip } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import React from 'react'

const Interactive: React.FC = () => {
    const [v, setV] = useRecoilState(interactive)

    return (
        <Tooltip content={<Translated message="Interactive mode" />}>
            <Button minimal intent={v ? 'primary' : 'none'} icon="select" onClick={() => setV(!v)} />
        </Tooltip>
    )
}

export default Interactive
