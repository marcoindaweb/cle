/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Button, ButtonGroup, Tooltip } from '@blueprintjs/core'
import { useRecoilState, useRecoilValue } from 'recoil'
import { availableDevices, currentDevice } from '../../../../store/pagebuilder/device/store'
import React from 'react'
import Translated from '@linda/module-i18n/frontend/components/Translated'

const DeviceSelect = () => {
    const devices = useRecoilValue(availableDevices)
    const [device, setDevice] = useRecoilState(currentDevice)

    return (
        <ButtonGroup>
            {devices.map((d) => (
                <Tooltip disabled={d.name === device.name} key={d.name} content={<Translated message={d.name} />}>
                    <Button minimal icon={d.icon} onClick={() => setDevice(d)} disabled={d.name === device.name} />
                </Tooltip>
            ))}
        </ButtonGroup>
    )
}

export default DeviceSelect
