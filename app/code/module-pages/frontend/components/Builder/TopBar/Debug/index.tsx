/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Button } from '@blueprintjs/core'
import React, { useEffect, useState } from 'react'
import { useRecoilValue } from 'recoil'
import { hovered, selected } from '../../../../store/pagebuilder/components/store'
import { layout } from '@linda/module-pages/frontend/store/layout/store'
import { componentDeclarations } from '@linda/module-pages/frontend/store/componentDeclarations/store'
import icons from '../../../../icons'
import { symbolDefinitions, symbols } from '../../../../store/pagebuilder/symbols/store'

const Logger = () => {
    const a = useRecoilValue(hovered)
    const b = useRecoilValue(layout)
    const c = useRecoilValue(componentDeclarations)
    const d = useRecoilValue(selected)
    const e = useRecoilValue(symbols)
    const f = useRecoilValue(symbolDefinitions)

    useEffect(() => {
        console.log({
            hovered: a,
            layout: b,
            declarations: c,
            selected: d,
            symbols: e,
            symbolDefinitions: f,
        })
    }, [a, b, c, d, e, f])
    return null
}

const Debug = () => {
    const [debugging, setDebugging] = useState(false)

    return (
        <>
            <Button
                minimal
                onClick={() => setDebugging(!debugging)}
                icon={icons.debug}
                intent={debugging ? 'primary' : 'none'}
            />
            {debugging && <Logger />}
        </>
    )
}

export default Debug
