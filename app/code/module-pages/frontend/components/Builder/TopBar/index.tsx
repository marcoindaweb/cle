/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import DeviceSelect from './DeviceSelect'
import PrintCode from './PrintCode'
import { createUseStyles } from 'react-jss'
import Borders from './Borders'
import Container from '../../../etc/Container'
import Route from './Route'
import Interactive from './Interactive'
import FixedDeviceHeight from './FixedDeviceHeight'
import PageMenu from './PageMenu'
import { Divider } from '@blueprintjs/core'
import Debug from "./Debug";

const useStyles = createUseStyles({
    main: {
        display: 'flex',
        alignItems: 'stretch',
        padding: '3px 5px',
        '& > *:not(:last-child)': {
            marginRight: '10px',
        },
    },
})

const TopBar: React.FC<{ className?: string }> = ({ className }) => {
    const { main } = useStyles()

    return (
        <Container className={className + ' ' + main}>
            <PageMenu />
            <Divider />
            <DeviceSelect />
            <Divider />
            <FixedDeviceHeight />
            <Divider />
            <PrintCode />
            <Divider />
            <Debug />
            <Borders />
            <Divider />
            <Route />
            <Divider />
            <Interactive />
        </Container>
    )
}

export default TopBar
