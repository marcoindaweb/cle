/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilState, useRecoilValue } from 'recoil'
import { symbolFormComponentId } from '../../../store/pagebuilder/symbols/form/store'
import { Button, Classes, Dialog, FormGroup, InputGroup, Spinner } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import React, { useCallback, useState } from 'react'
import { component } from '../../../store/layout/store'
import {
    convertComponentInstanceToSymbolLayout,
    symbolDefinitionToPageBuilderSymbol,
} from '../../../store/layout/utils/manipulation'
import { useCreatePageBuilderSymbol } from '../../../graphql/symbols/mutations'
import { SymbolDefinition } from '../../../types/SymbolDefinition'
import AdminToast from '@linda/module-admin/frontend/components/AdminToast'
import useTranslate from '@linda/module-i18n/frontend/hooks/useTranslate'

const SymbolForm = () => {
    const [componentId, setComponentId] = useRecoilState(symbolFormComponentId)
    const comp = useRecoilValue(component(componentId || ''))
    const [name, setName] = useState('')
    const [createSymbolDefinition, { loading }] = useCreatePageBuilderSymbol()
    const translate = useTranslate()
    const createSymbol = useCallback(async () => {
        if (!comp || name.length === 0) {
            return
        }
        const symbolLayout = convertComponentInstanceToSymbolLayout(comp)
        const symbolDefinition: Omit<SymbolDefinition, 'id'> = {
            name,
            layout: symbolLayout,
        }
        try {
            await createSymbolDefinition({
                variables: { input: symbolDefinitionToPageBuilderSymbol(symbolDefinition) },
            })
            AdminToast.show({ intent: 'success', message: translate('Symbol created') })
            setComponentId(undefined)
        } finally {
            setName('')
        }
    }, [component, name])

    return (
        <Dialog isOpen={!!componentId} onClose={() => setComponentId(undefined)}>
            <div className={Classes.DIALOG_HEADER}>
                <Translated message="Create a new Symbol" />
            </div>
            <div className={Classes.DIALOG_BODY}>
                <FormGroup label={<Translated message="Symbol Name" />}>
                    <InputGroup value={name} onChange={(e) => setName(e.target.value)} />
                </FormGroup>
            </div>
            <div className={Classes.DIALOG_FOOTER}>
                <div className={Classes.DIALOG_FOOTER_ACTIONS}>
                    <Button
                        text={
                            <div>
                                <Translated message={'Create'} />
                                {loading && <Spinner size={10} />}
                            </div>
                        }
                        onClick={createSymbol}
                        disabled={name.length === 0 || loading}
                    />
                </div>
            </div>
        </Dialog>
    )
}

export default SymbolForm
