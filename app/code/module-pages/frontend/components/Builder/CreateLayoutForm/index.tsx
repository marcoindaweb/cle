/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Button, Classes, Dialog, FormGroup, InputGroup, Spinner } from '@blueprintjs/core'
import Translated from '@linda/module-i18n/frontend/components/Translated'
import React, { useCallback, useState } from 'react'
import { useRecoilState, useSetRecoilState } from 'recoil'
import { isLayoutFormOpen } from '../../../store/pagebuilder/layout/form/store'
import { useCreatePagelayout } from '../../../graphql/layouts/mutations'
import { currentRootComponentId, layout } from '../../../store/layout/store'
import Layout from '../../../types/Layout'
import { v4 } from 'uuid'
import Body from '../../Definitions/Body'
import { convertLayoutToPagelayout, convertPagelayoutToLayout } from '../../../converter/PagelayoutConverter'

const useCreateLayout: () => { create: (name: string) => void; loading: boolean } = () => {
    const setLayout = useSetRecoilState(layout)
    const setIsFormOpen = useSetRecoilState(isLayoutFormOpen)
    const [mutate, { loading }] = useCreatePagelayout()
    const setRootId = useSetRecoilState(currentRootComponentId)
    const id = v4()
    const emptyLayout: Omit<Layout, 'name' | 'id'> = {
        components: [
            {
                id,
                componentDeclarationId: Body.id,
                alias: 'body',
                properties: {},
            },
        ],
        symbols: [],
    }
    const pagelayout = convertLayoutToPagelayout({ id: 0, name: '', ...emptyLayout })
    const create = useCallback(
        async (name: string) => {
            const res = await mutate({
                variables: { input: { name, layout: pagelayout.layout, symbols: pagelayout.symbols } },
            })
            setRootId(id)
            setIsFormOpen(false)
            if (res.data) {
                const l = convertPagelayoutToLayout(res.data.createPagelayout)
                setLayout(l)
            }
        },
        [pagelayout, id],
    )

    return { create, loading }
}

const CreateLayoutForm = () => {
    const [name, setName] = useState('')
    const [isOpen, setIsOpen] = useRecoilState(isLayoutFormOpen)
    const { create, loading } = useCreateLayout()

    return (
        <Dialog isOpen={isOpen} onClose={() => setIsOpen(false)}>
            <div className={Classes.DIALOG_HEADER}>
                <Translated message="Create a new Layout" />
            </div>
            <div className={Classes.DIALOG_BODY}>
                <FormGroup label={<Translated message="Name" />}>
                    <InputGroup value={name} onChange={(e) => setName(e.target.value)} />
                </FormGroup>
            </div>
            <div className={Classes.DIALOG_FOOTER}>
                <div className={Classes.DIALOG_FOOTER_ACTIONS}>
                    <Button
                        text={
                            <div>
                                <Translated message={'Create'} />
                                {loading && <Spinner size={10} />}
                            </div>
                        }
                        onClick={() => create(name)}
                        disabled={name.length === 0 || loading}
                    />
                </div>
            </div>
        </Dialog>
    )
}

export default CreateLayoutForm
