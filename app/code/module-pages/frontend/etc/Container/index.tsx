/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import { Card, ICardProps } from '@blueprintjs/core'
import { createUseStyles } from 'react-jss'

const useStyle = createUseStyles({
    card: {
        borderRadius: 0,
        padding: '5px',
    },
})

const Container: React.FC<ICardProps> = (props) => {
    const { card } = useStyle()
    return (
        <Card {...props} className={props.className + ' ' + card}>
            {props.children}
        </Card>
    )
}

Container.displayName = 'Container'

export default Container
