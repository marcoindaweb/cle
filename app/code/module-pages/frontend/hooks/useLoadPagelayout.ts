/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Pagelayout } from 'types/graphql'
import { useCallback } from 'react'
import { convertPagelayoutToLayout } from '../converter/PagelayoutConverter'
import { useSetRecoilState } from 'recoil'
import { currentRootComponentId, layout } from '../store/layout/store'

export const useLoadPagelayout = () => {
    const setLayout = useSetRecoilState(layout)
    const setRootId = useSetRecoilState(currentRootComponentId)

    return useCallback((pagelayout: Pagelayout) => {
        const layout = convertPagelayoutToLayout(pagelayout)
        const rootId = layout.components[0].id
        if (rootId) {
            setRootId(rootId)
            setLayout(layout)
        }
    }, [])
}
