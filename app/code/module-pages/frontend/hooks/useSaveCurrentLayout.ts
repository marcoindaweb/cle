/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useCallback } from 'react'
import { useRecoilValue } from 'recoil'
import { layout } from '../store/layout/store'
import { useUpdatePageLayout } from '../graphql/layouts/mutations'
import { convertLayoutToPagelayout } from '../converter/PagelayoutConverter'
import AdminToast from '@linda/module-admin/frontend/components/AdminToast'
import useTranslate from '@linda/module-i18n/frontend/hooks/useTranslate'

export const useSaveCurrentLayout = () => {
    const currentLayout = useRecoilValue(layout)
    const [update, { loading }] = useUpdatePageLayout()
    const translate = useTranslate()

    return useCallback(async () => {
        try {
            await update({ variables: { input: convertLayoutToPagelayout(currentLayout) } })
            AdminToast.show({ intent: 'success', message: translate('Layout Saved') })
        } catch (e) {
            AdminToast.show({ intent: 'danger', message: translate('There was an error saving the layout') })
            console.error(e)
        }
        return
    }, [currentLayout, update])
}
