/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useLazyFindOneLayoutQuery } from '../graphql/layouts/queries'
import { useLoadPagelayout } from './useLoadPagelayout'

export const useLoadLayoutById = (): [(id: number) => void, ReturnType<typeof useLazyFindOneLayoutQuery>[1]] => {
    const load = useLoadPagelayout()
    const [find, data] = useLazyFindOneLayoutQuery({
        onCompleted: (data) => {
            load(data.findOnePagelayout)
        },
    })

    return [
        (id: number) =>
            find({
                variables: {
                    filter: { id },
                },
            }),
        data,
    ]
}
