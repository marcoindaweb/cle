/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { addFrontend } from '@linda/frontend/watcher/state'
import { join } from '@linda/core/fs/utils'
import { createAddOperation, addOperation, ROOT } from '@linda/frontend'
import { component } from '@linda/frontend/selectors'
import { AddOperationPosition } from '@linda/frontend/operations/types'
addFrontend(join(__dirname, 'frontend'))
addOperation(createAddOperation({ component: ROOT, reference: join(__dirname, 'frontend/Wrapper/index.tsx'), selector: component('div'), position: AddOperationPosition.INSIDE }))
