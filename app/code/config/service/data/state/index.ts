/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Configuration, Configurations, Tab } from '../../../types'
import { configurationToField, group, merge, section, tab } from '../../calculations'
import { DEVELOPER_SECTION, SYSTEM_TAB, UNKNOWN_GROUP } from '../../../const'

export let configurations: Configurations = { tabs: [] }
export const getConfigurations = (): Configurations => configurations
export const registerConfig = (...tabs: Tab[]): void => {
  configurations = merge([...configurations.tabs, ...tabs])
}
export const register = (...configurations: Configuration[]): void => {
  registerConfig(
    tab(SYSTEM_TAB, [
      section(DEVELOPER_SECTION, [
        group(UNKNOWN_GROUP, configurations.map(configurationToField))
      ])
    ])
  )
}
export let config: Configuration[]
