/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Configuration, ConfigValue } from '../../../types'
import { db, Collection } from '@linda/database'
import { CONFIGURATIONS } from './const'
import { removeWhere } from '@linda/core/utils/arrays'
import { transaction } from '@linda/database/utils/session'

const cache: Record<string, ConfigValue | undefined> = {}

export const collection = (): Collection<Configuration> => db().collection<Configuration>(CONFIGURATIONS)

export const get = async <T extends ConfigValue>(key: string): Promise<T | null> => {
  if (!cache.hasOwnProperty(key)) {
    const res = await collection().findOne({ key })
    if (res !== null) {
      cache[key] = res.value as T
    }
  }
  return cache[key] as T | undefined ?? null
}

export const setMany = async (toUpdate: Configuration[]): Promise<void> => {
  const ids = toUpdate.map(v => v._id)
  await transaction(async (session) => {
    await collection().deleteMany({ _id: { $in: ids } }, { session })
    await collection().insertMany(toUpdate, { session })
  })

  for (const [key, val] of Object.entries(cache)) {
    cache[key] = val
  }
}

export const set = async (value: Configuration): Promise<void> => {
  if (value.value !== undefined) {
    await collection().updateOne({ _id: value._id }, { $set: { value: value.value } })
    cache[value._id] = value.value
  } else {
    await collection().deleteOne({ _id: value._id })
    if (cache[value._id] !== undefined) {
      cache[value._id] = undefined
    }
  }
}

export const registerConfigurations = async (values: Configuration[]): Promise<void> => {
  let toInsert = values
  await collection().find().forEach(config => {
    toInsert = removeWhere(toInsert, v => v._id === config._id)
  })
  if (toInsert.length === 0) return
  await collection().insertMany(toInsert)
}
