/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { clearTestingDB, useTestingDB } from '@linda/database/test'
import { load, start } from '@linda/core'
import { Configuration } from '../../../../types'
import { collection, registerConfigurations } from '../index'

describe('config/repository', () => {
  beforeEach(async (done) => {
    load('@linda/database')
    load('@linda/config')
    useTestingDB()
    await start()
    done()
  })

  afterEach(async () => {
    await clearTestingDB()
  })

  it('register all missing configuration with default value (if provided)', async () => {
    const fieldsBefore: Configuration[] = [{ _id: 'a', value: 'a' }, { _id: 'b', value: 'b' }]
    const fieldsAfter: Configuration[] = [...fieldsBefore, { _id: 'c', value: 'c' }, { _id: 'd', value: 'd' }]
    await registerConfigurations(fieldsBefore)
    const resBefore = await collection().find().toArray()
    expect(fieldsBefore).toEqual(resBefore)
    await registerConfigurations(fieldsAfter)
    const resAfter = await collection().find().toArray()
    expect(fieldsAfter).toEqual(resAfter)
  })
})
