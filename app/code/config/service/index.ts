/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Configuration, ConfigValue, Section } from '../types'
import { collection, get, setMany } from './data/repository'
import { getConfigurations, registerConfig } from './data/state'
import { group, section, tab } from './calculations/data'

export const findById = <T extends ConfigValue>(key: string): Promise<ConfigValue | null> => {
  return get<T>(key)
}
export const getAll = (): Promise<Configuration[]> => {
  return collection().find({}).toArray()
}
export const updateMany = async (...v: Configuration[]): Promise<void> => {
  await setMany(v)
}
export const updateOne = (v: Configuration): Promise<void> => updateMany(v)
export const getTabSection = (tab: string, section: string): Section | undefined => {
  return getConfigurations()
    .tabs.find(v => v.name === tab)
    ?.sections.find(v => v.name === section)
}
export { registerConfig, tab, section, group }
