/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { group, merge, section, tab } from '../index'
import { FieldType } from '../../../types'

describe('config/registration', () => {
  it('merge multiple configurations', () => {
    const res = merge([
      tab('test', [
        section('test-section-1', [
          group('test-section-group-1', [
            {
              name: '1',
              key: '1',
              type: FieldType.TEXT
            }
          ])
        ])
      ]),
      tab('test', [
        section('test-section-1', [
          group('test-section-group-1', [
            {
              name: '2',
              key: '2',
              type: FieldType.TEXT
            },
            {
              name: '3',
              key: '3',
              type: FieldType.TEXT
            }
          ])
        ])
      ]),
      tab('newtab', [
        section('s2', [
          group('gs2', [
            {
              name: '1',
              key: '4',
              type: FieldType.TEXTAREA
            }
          ])
        ])
      ]),
      tab('test', [
        section('test-section-1', [
          group('test-section-group-2', [
            {
              name: '5',
              key: '5',
              type: FieldType.TEXT
            }
          ])
        ])
      ])
    ])

    expect(res).toEqual({
      tabs: [
        {
          name: 'test',
          sections: [
            {
              name: 'test-section-1',
              groups: [
                {
                  name: 'test-section-group-1',
                  fields: [
                    {
                      name: '1',
                      key: '1',
                      type: FieldType.TEXT
                    },
                    {
                      name: '2',
                      key: '2',
                      type: FieldType.TEXT
                    },
                    {
                      name: '3',
                      key: '3',
                      type: FieldType.TEXT
                    }
                  ]
                },
                {
                  name: 'test-section-group-2',
                  fields: [
                    {
                      name: '5',
                      key: '5',
                      type: FieldType.TEXT
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          name: 'newtab',
          sections: [
            {
              name: 's2',
              groups: [
                {
                  name: 'gs2',
                  fields: [
                    {
                      name: '1',
                      key: '4',
                      type: FieldType.TEXTAREA
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    })
  })
})
