/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Field, Group, Section, Tab } from '../../types'
import { match, whereKey } from '@linda/core/validation'

export const tab = (name: string, sections: Section[]): Tab => {
  return {
    name,
    sections
  }
}

export const section = (name: string, groups: Group[]): Section => {
  return {
    name,
    groups
  }
}

export const validateField = (field: Field): Field => {
  try {
    whereKey('key', match(/^[a-z-_0-9]+$/))(field)
    return field
  } catch (e) {
    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
    throw new Error(`Invalid field ${field.key}: ${e}`)
  }
}

export const group = (name: string, fields: Field[]): Group => {
  fields.forEach(validateField)
  return {
    name,
    fields
  }
}
