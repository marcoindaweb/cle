/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Configuration, Configurations, Field, FieldType, Tab } from '../../types'
import { joinValueByKey } from '@linda/core/utils/arrays'

export * from './data'
export const merge = (tabs: Tab[]): Configurations => {
  return {
    tabs: joinValueByKey('name', 'sections', tabs)
      .map(v => ({
        name: v.name,
        sections: joinValueByKey('name', 'groups', v.sections)
          .map(v => ({
            name: v.name,
            groups: joinValueByKey('name', 'fields', v.groups)
          }))
      }))
  }
}

export const flatten = (config: Configurations): Field[] => {
  return config.tabs.reduce<Field[]>((a, v) => {
    v.sections.forEach(section => section.groups.forEach(group => a.push(...group.fields)))
    return a
  }, [])
}

export const fieldToConfiguration = (field: Field): Configuration => ({
  _id: field.key,
  value: field.default
})

export const configurationToField = (conf: Configuration): Field => ({
  key: conf._id,
  name: conf._id,
  default: conf.value,
  type: FieldType.UNKNOWN
})

export const getTabBy = <K extends keyof Tab>(config: Configurations, key: K, value: Tab[K]): Tab | undefined => {
  return config.tabs.find(v => v[key] === value)
}
