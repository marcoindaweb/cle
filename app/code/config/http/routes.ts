/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { app, FastifyPluginCallback, FastifyRequest } from '@linda/core/transport/http'
import { configurationsToTabsSummary } from './calculations'
import { getConfigurations } from '../service/data/state'
import { findById, getAll, getTabSection, updateMany } from '../service'
import { Configuration } from '../types'

const config: FastifyPluginCallback = (app, opts, next) => {
  app.get('/configuration/tabs', async (request, reply) => {
    await reply.send(configurationsToTabsSummary(getConfigurations()))
  })

  type ConfigByIdReq = FastifyRequest<{ Params: { id: string }}>
  app.get('/configuration/:id', async (request: ConfigByIdReq, reply) => {
    const res = await findById(request.params.id)
    await reply.send(res)
  })
  type TabSectionReq = FastifyRequest<{ Params: { tab: string, section: string }}>
  app.get('/configuration/tabs/:tab/sections/:section', async (request: TabSectionReq, reply) => {
    const section = getTabSection(request.params.tab, request.params.section)
    if (section === undefined) {
      await reply.status(404).send()
      return
    }
    await reply.send(section)
  })
  app.get('/configuration', async (request, response) => {
    await response.send(await getAll())
  })
  type UpdateConfigsReq = FastifyRequest<{ Body: Configuration[]}>
  app.put('/configuration', async (request: UpdateConfigsReq, response) => {
    await updateMany(...request.body)
  })
  next()
}

// eslint-disable-next-line @typescript-eslint/no-floating-promises
app.register(config, { prefix: '/config/v1' })
