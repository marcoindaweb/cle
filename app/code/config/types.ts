/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'

export type ConfigValue = string | number | Date | boolean | string[] | number[]
export interface Configuration<T extends ConfigValue = ConfigValue> {
  _id: string
  value: T | undefined
}

export type FieldRenderer = React.FC<{ value: ConfigValue, id: string }>

export type Field = CustomField | InputField | ButtonField | UnknownField
export enum FieldType {
  TEXT,
  TEXTAREA,
  SELECT,
  MULTISELECT,
  DATE,
  DATETIME,
  TIME,
  BUTTON,
  BOOLEAN,
  CUSTOM,
  UNKNOWN
}
interface BaseField {
  name: string
  key: string
}

interface FieldWithValue {
  default?: ConfigValue
  value?: ConfigValue
}

type CustomField = BaseField & { type: FieldType.CUSTOM, render: string } & FieldWithValue
type InputField = BaseField & { type: FieldType.TEXT | FieldType.TEXTAREA | FieldType.SELECT | FieldType.MULTISELECT | FieldType.DATE | FieldType.DATETIME | FieldType.TIME | FieldType.BOOLEAN } & FieldWithValue
type ButtonField = BaseField & { type: FieldType.BUTTON, action: () => Promise<string | undefined> | string | undefined} & FieldWithValue
type UnknownField = BaseField & { type: FieldType.UNKNOWN } & FieldWithValue

export interface Group {
  name: string
  fields: Field[]
}

export interface Section {
  name: string
  groups: Group[]
}

export interface Tab {
  name: string
  sections: Section[]
}

export interface Configurations {
  tabs: Tab[]
}
