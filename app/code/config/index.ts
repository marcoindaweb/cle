/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { onStart } from '@linda/core'
import { registerConfigurations, get } from './service/data/repository'
import { fieldToConfiguration, flatten } from './service/calculations'
import { getConfigurations } from './service/data/state'
import './http/routes'

onStart(async () => {
  const configurations = getConfigurations()
  await registerConfigurations(flatten(configurations).map(fieldToConfiguration))
})

export { get }
export {
  registerConfig,
  tab,
  section,
  group,
  updateMany,
  getAll,
  findById,
  updateOne
} from './service'
