/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import {
  atom, atomFamily,
  DefaultValue,
  selector, selectorFamily,
  useRecoilCallback, useRecoilState,
  useRecoilValue, useResetRecoilState,
  useSetRecoilState
} from '@linda/frontend/frontend'

interface Email {
  id: string
  to: string
  from: string
  cc?: string[]
  body: string
}

export const emails = selector<Email[]>({
  key: 'emails/email/emails',
  get: ({ get }) => {
    return get(emailsIds).map(v => get(email(v)))
  },
  set: ({ set, get, reset }, val) => {
    if (val instanceof DefaultValue) {
      const ids = get(emailsIds)
      for (const id of ids) {
        reset(email(id))
      }
      reset(emailsIds)
    } else {
      for (const e of val) {
        set(email(e.id), e)
      }
      set(emailsIds, val.map(i => i.id))
    }
  }
})

export const emailsIds = atom<string[]>({
  key: 'emails/email/emailsIds',
  default: []
})

export const email = selectorFamily<Email, string>({
  key: 'emails/email/email',
  get: (id) => ({ get }) => {
    return {
      to: get(emailTo(id)),
      from: get(emailFrom(id)),
      cc: get(emailCc(id)),
      body: get(emailBody(id))
    }
  },
  set: (id) => ({ get, set, reset }, val) => {
    if (val instanceof DefaultValue) {
      reset(emailTo(id))
      reset(emailFrom(id))
      reset(emailCc(id))
      reset(emailBody(id))
    } else {
      set(emailTo(id), val.to)
      set(emailFrom(id), val.from)
      set(emailCc(id), val.cc)
      set(emailBody(id), val.body)
    }
  }
})

export const emailTo = atomFamily<string, string>({
  key: 'emails/email/emailTo',
  default: ''
})
export const emailFrom = atomFamily<string, string>({
  key: 'emails/email/emailFrom',
  default: ''
})
export const emailBody = atomFamily<string, string>({
  key: 'emails/email/emailBody',
  default: ''
})
export const emailCc = atomFamily<string[] | undefined, string>({
  key: 'emails/email/emailCc',
  default: undefined
})

const useFetchEmailById = (): (id: string) => Promise<Email> => useRecoilCallback(({ snapshot, reset, set }) => async (id: string) => {
  // ...
  reset(email)
}, [])

const Email: React.FC<{ id: string }> = ({ id }) => {
  const email = useRecoilCallback(({ snapshot }) => () => {
    snapshot
  })
}
