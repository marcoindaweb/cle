/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { group, section, tab } from '@linda/config'
import { DEFAULT_EMAIL, ERROR_EMAIL } from './const'
import { FieldType } from '@linda/config/types'

export const configuration = tab('general', [
  section('emails', [
    group('email', [
      {
        key: DEFAULT_EMAIL,
        name: 'Default Email',
        type: FieldType.TEXT
      },
      {
        key: ERROR_EMAIL,
        name: 'Default Email',
        type: FieldType.TEXT
      }
    ])
  ])
])
