/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import stripAnsi from 'strip-ansi'

export const table = (
  v: Array<Record<string, string | number>> | Array<Array<string | number>>,
  headers?: string[]
): string => {
  const colsLength: number[] = []
  let head: string[] = headers ?? Object.keys(v[0])
  // @ts-expect-error
  let rows: Array<Array<string | number>> = v.map((items) =>
    Array.isArray(items) ? items : Object.entries(items).map((v) => v[1])
  )

  const updateColsLength = (index: number, length: number): void => {
    colsLength[index] =
      colsLength[index] !== undefined && colsLength[index] > length
        ? colsLength[index]
        : length
  }

  rows.forEach((columns) =>
    columns.forEach((column, index) =>
      updateColsLength(index, stripAnsi(column.toString()).length)
    )
  )
  head.forEach((header, index) =>
    updateColsLength(index, stripAnsi(header).length)
  )

  head = head.map(
    (val, index) => ' ' + val.padEnd(colsLength[index] + 1, ' ')
  )
  rows = rows.map((columns) =>
    columns.map(
      (v, index) => ' ' + v.toString().padEnd(colsLength[index] + 1, ' ')
    )
  )
  let o = ''
  o += head.join(' ') + '\n'
  o += head.map((v) => v.replace(/./g, '-')).join(' ') + '\n'
  rows.forEach((columns) => {
    o += columns.join(' ') + '\n'
  })
  return o
}
