/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { ReplaySubject } from 'rxjs'
import { map, shareReplay } from 'rxjs/operators'
import { argv$ } from './Argv'

const getNameFromParam = (v: string): string => v.replace(/^--/, '')
const getInputVal = (v: string): string | number =>
  isNaN(parseInt(v)) ? v : parseInt(v)

export interface CommandCall {
  command: string
  parameters: Record<
  string,
  string | boolean | number | (string[] | string) | undefined
  >
}
const ParsedArgv$ = argv$.pipe(
  map<string[], CommandCall>((v) => {
    // Remove first two
    v.splice(0, 2)
    // Then first is the command name
    const command = v[0]
    v.splice(0, 1)
    const parameters = {}
    let goOn = v.length > 0
    let lastParam: string | null = null
    while (goOn) {
      if (v[0].includes('=')) {
        const parts = v[0].split('=')
        const paramName = getNameFromParam(parts[0])
        parameters[paramName] = getInputVal(parts[1])
      } else if (v[0].includes('--')) {
        const paramName = getNameFromParam(v[0])
        parameters[paramName] = true
        lastParam = paramName
      } else if (lastParam !== null) {
        const val = getInputVal(v[0])
        if (typeof val !== 'string') {
          parameters[lastParam] = val
        } else {
          if (val.includes(',')) {
            parameters[lastParam] = val.split(',')
          } else if (parameters[lastParam] !== undefined) {
            Array.isArray(parameters[lastParam])
              ? (parameters[lastParam] = [
                ...parameters[lastParam],
                val
              ])
              : typeof parameters[lastParam] === 'string'
                ? (parameters[lastParam] = [
                  parameters[lastParam],
                  val
                ])
                : (parameters[lastParam] = val)
          } else {
            parameters[lastParam] = val
          }
        }
      }
      v.splice(0, 1)
      goOn = v.length > 0
    }
    return { command, parameters }
  }),
  shareReplay()
)
export const CommandCall$ = new ReplaySubject<CommandCall>(1)
export const call = (
  command: CommandCall | string,
  parameters?: CommandCall['parameters']
): void => {
  const cmd: CommandCall =
        typeof command === 'string'
          ? { command, parameters: parameters ?? {} }
          : command
  CommandCall$.next(cmd)
}
ParsedArgv$.subscribe((v) => {
  call(v)
})
