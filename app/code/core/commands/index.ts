/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { CommandCall, CommandCall$ } from './CommandCall'
import { Observable, OperatorFunction } from 'rxjs'
import { filter } from 'rxjs/operators'

export * from './CommandCall'

const command = <T extends CommandCall = CommandCall>(
  v: string
): OperatorFunction<CommandCall, T> => filter<T>((call) => call.command === v)

export const Command$ = <T extends CommandCall = CommandCall>(
  name: string
): Observable<T> => {
  return CommandCall$.pipe(command<T>(name))
}
