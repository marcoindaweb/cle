/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { joinValueByKey } from '../arrays'

describe('array utils', () => {
  it('join by key', () => {
    const toJoin = [
      {
        name: 'a',
        val: ['a', 'b', 'c']
      },
      {
        name: 'a',
        val: ['d', 'e', 'f', 'g']
      },
      {
        name: 'b',
        val: ['1', '2', '3', '4', '5']
      },
      {
        name: 'b',
        val: ['6', '7', '8']
      },
      {
        name: 'c',
        val: ['#', '!']
      }
    ]
    expect(joinValueByKey('name', 'val', toJoin)).toEqual([
      {
        name: 'a',
        val: ['a', 'b', 'c', 'd', 'e', 'f', 'g']
      },
      {
        name: 'b',
        val: ['1', '2', '3', '4', '5', '6', '7', '8']
      },
      {
        name: 'c',
        val: ['#', '!']
      }
    ])
  })
})
