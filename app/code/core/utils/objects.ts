/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import _get from 'lodash/get'

/**
 * Given object { a: { b: { c: 'a' }}, d: { e: { f: 'a'}}} and key 'a.b.c' returns { b: { c: 'a' }}
 * @param object
 * @param key
 */
export const getPropertyWithNestedKey = <T extends any>(object: object, key: string | string[]): T | undefined => {
  for (const [k, val] of Object.entries(object)) {
    if (_get({ [k]: val }, key, false) !== false) {
      return val
    }
  }
}
