/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

/**
 * takes: [
 *  {name: 'name', value: ['a']}
 *  {name: 'name', value: ['b']}
 *  {name: 'another name', value: ['c']}
 * ]
 * returns: [
 *  {name: 'name', value: ['a','b']}
 *  {name: 'another name', value: ['c']}
 * ]
 *
 *
 * @param key
 * @param valueKey
 * @param values
 */
export const joinValueByKey = <T extends any[]>(key: keyof T[0], valueKey: keyof T[0], values: T): T => {
  const out: Record<string, unknown[]> = {}
  values.forEach(value => {
    if (out[value[key]] === undefined) {
      out[value[key]] = []
    }
    out[value[key]].push(...value[valueKey])
  })
  return Object.entries(out).map(([k, val]) => ({ [key]: k, [valueKey]: val })) as T
}

export const pluck = <T extends object, K extends keyof T>(val: T[], key: K): Array<T[K]> => {
  return val.map(v => v[key])
}

export const removeWhere = <T>(val: T[], cb: (val: T, key: number) => boolean): T[] => {
  const newVal = [...val]
  const index = newVal.findIndex(cb)
  if (index !== -1) {
    newVal.splice(index, 1)
  }
  return newVal
}
