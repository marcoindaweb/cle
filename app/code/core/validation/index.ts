/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

export type Validation = (v: any) => true
export * from './objects'
export * from './utils'
export * from './string'
export * from './number'
export * from './boolean'
export * from './array'
export const isUndefined: Validation = (v: any) => {
  if (v !== undefined) {
    throw new Error(`Expected undefined value, got ${v}`)
  }
  return true
}

export const isNull: Validation = (v: any) => {
  if (v !== null) {
    throw new Error(`Expected null value, got ${v}`)
  }
  return true
}
