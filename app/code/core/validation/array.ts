/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Validation } from './index'

export const isArray: Validation = (v: any) => {
  if (!Array.isArray(v)) {
    throw new Error(`Expected value of array type, got ${typeof v}`)
  }
  return true
}

export const eachItem = (validation: Validation): Validation => (v: any) => {
  isArray(v)
  for (const item of v) {
    validation(item)
  }
  return true
}
