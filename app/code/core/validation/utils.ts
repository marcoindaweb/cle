/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Validation } from './index'
import { multicast } from 'rxjs/operators'
type MessageFunction = (v: string) => string
export const withMessage =
  (validation: Validation, message: string | MessageFunction): Validation => (v) => {
    try {
      validation(v)
    } catch (e) {
      throw new Error(typeof message === 'string' ? message : message(e.message))
    }
    return true
  }

export const oneOf = (...validations: Validation[]): Validation => (value) => {
  let lastErr
  for (const validation of validations) {
    try {
      validation(value)
      return true
    } catch (e) {
      lastErr = e
    }
  }
  throw lastErr
}

export const merge = (...validations: Validation[]): Validation => (value) => {
  for (const validation of validations) {
    validation(value)
  }
  return true
}

export const iif = (condition: () => boolean, then: Validation, elseIf?: Validation): Validation => {
  if (condition()) {
    return then
  } else {
    if (elseIf !== undefined) {
      return elseIf
    } else {
      return () => true
    }
  }
}

export const isEqualTo = (mustMatch: any): Validation => (v: any) => {
  if (v !== mustMatch) {
    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
    throw new Error(`Expected value ${mustMatch}, got ${v}`)
  }
  return true
}
