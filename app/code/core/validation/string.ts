/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Validation } from './index'

export const isString: Validation = (v) => {
  if (typeof v !== 'string') {
    throw new Error(`Expected string, got ${typeof v}`)
  }
  return true
}

export const minLength = (length: number): Validation => (v) => {
  isString(v)
  if ((v as string).length < length) {
    throw new Error(`Must be at least ${length} characters long`)
  }
  return true
}

export const maxLength = (length: number): Validation => (v) => {
  isString(v)
  if ((v as string).length > length) {
    throw new Error(`Must be at most ${length} characters long`)
  }
  return true
}

export const match = (expression: RegExp): Validation => (v) => {
  isString(v)
  if (!expression.test(v as string)) {
    throw new Error(`Must match the expression ${expression.source}`)
  }
  return true
}
