/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { isString, oneOf, Validation, withMessage } from './index'

export const isISODateString: Validation = (v: any) => {
  isString(v)
  const parsed = new Date(Date.parse(v))
  if (parsed.toISOString() === v) {
    return true
  }
  throw new Error('Expected iso formatted string')
}

export const isDate: Validation = (v: any) => {
  if (v instanceof Date) {
    return true
  }
  throw new Error(`Expected date, got ${typeof v}`)
}

export const isDateOrIsoDateString: Validation = withMessage(
  oneOf(
    isDate,
    isISODateString
  ),
  'Expected date or iso formatted string'
)
