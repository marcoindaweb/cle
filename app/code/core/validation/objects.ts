/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Validation, withMessage } from './index'

export const isObject: Validation = (v: any) => {
  if (typeof v !== 'object') {
    throw new Error(`Expected value of type object, got ${typeof v}`)
  }
  return true
}
export const hasKey = (key: string): Validation =>
  (v) => {
    isObject(v)
    if (!(v as object).hasOwnProperty(key)) {
      throw new Error(`Missing property ${key}`)
    }
    return true
  }

export const whereKey =
  (key: string, cb: Validation, message?: string): Validation => (v) => {
    hasKey(key)
    if (!cb(v[key])) {
      throw new Error(message ?? `Invalid property ${key}`)
    }
    return withMessage(cb, v => `${message ?? `Invalid property ${key}:`} ${v}`)(v[key])
  }

export const whereAllKeys = (validation: Validation): Validation => (v) => {
  isObject(v)
  for (const [_, value] of Object.entries(v as object)) {
    validation(value)
  }
  return true
}

export const schema = (schema: Record<string, Validation>): Validation => (v) => {
  isObject(v)
  for (const [key, validation] of Object.entries(schema)) {
    withMessage(validation, (v) => `Invalid property ${key}: ${v}`)(v[key])
  }
  return true
}
