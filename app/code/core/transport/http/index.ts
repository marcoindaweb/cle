/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */
import fastify, { FastifyReply, FastifyRequest, RouteOptions, RequestGenericInterface, FastifyPluginAsync, FastifyPluginCallback } from 'fastify'
import fastifyCors from 'fastify-cors'
import './commands'
import { RouteShorthandMethod } from 'fastify/types/route'

const app = fastify()
app.register(fastifyCors)

type RequestHandler = RouteShorthandMethod
export { app, RequestHandler, fastify, RouteOptions, RequestGenericInterface, FastifyPluginAsync, FastifyRequest, FastifyReply, FastifyPluginCallback }
