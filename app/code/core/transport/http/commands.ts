/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Command$ } from '../../commands'
import { app } from './index'
import { ERROR, INFO, log } from '../../log'

Command$('http:start').subscribe(() => {
  const port = process.env.PORT ?? 3000
  app.listen(port).then(() => {
    log(INFO, `App listening on port ${port}`)
  })
    .catch(err => log(ERROR, err))
    .finally(() => {})
})

Command$('http:list-routes').subscribe(() => {
  // eslint-disable-next-line @typescript-eslint/no-floating-promises
  app.ready()
    .then(() => {
      console.log(app.printRoutes())
    })
})
