/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Configuration } from './types'

export const set = (v: Configuration, key: string, value: string | number): Configuration => ({ ...v, [key]: value })
export const get = <T extends any>(v: Configuration, key: string, defaultValue?: T): undefined | T => {
  const val = v[key]
  if (val === undefined) return defaultValue as T
  return val
}
export const has = (v: Configuration, key: string): boolean => v.has(key)
export const getOrThrow = <T extends any>(v: Configuration, key: string): T => {
  const value = v[key]
  if (value === undefined) {
    throw new Error(`No value found in configuration for ${key}`)
  }
  return value
}
