/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { get, getOrThrow, has, set } from '../manipulation'
import { Map } from 'immutable'
import { Configuration } from '../types'

describe('configuration', () => {
  it('get a value', () => {
    const config = Map({ test: 'value' })
    expect(get(config, 'test')).toEqual('value')
  })

  it('return a default value when there isn\'t a value', () => {
    const config: Configuration = Map()
    expect(get(config, 'test', 'default')).toEqual('default')
  })

  it('throw an error if no value is found', () => {
    const config: Configuration = Map()
    expect(() => getOrThrow(config, 'test')).toThrow()
  })

  it('return the updated config', () => {
    const config: Configuration = Map()
    expect(set(config, 'test', 'value').equals(Map({ test: 'value' }))).toBeTruthy()
  })

  it('checks if there is a value for the provided key', () => {
    const config: Configuration = Map({ a: 'value' })
    expect(has(config, 'a')).toBeTruthy()
    expect(has(config, 'b')).not.toBeTruthy()
  })
})
