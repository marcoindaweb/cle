/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Configuration } from './types'
import { Map } from 'immutable'
import { set as _set, has as _has, getOrThrow as _getOrThrow, get as _get } from './manipulation'

// eslint-disable-next-line prefer-const
export let configuration: Configuration = {}
export const set = (key: string, value: string | number): void => {
  configuration = _set(configuration, key, value)
}
export const has = (key: string): boolean =>
  _has(configuration, key)

export const getOrThrow = <T extends any>(key: string): T => _getOrThrow(configuration, key)
export const get = <T extends any>(key: string, defaultValue: T): T | undefined => _get(configuration, key, defaultValue)
