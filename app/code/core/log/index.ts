/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { createLogger, LogLevel } from './logger'
import { console } from './transports'
import { get } from '../configuration'
import { LOG_LEVEL } from './const'

export let log = createLogger({
  transports: [
    console
  ],
  level: get(LOG_LEVEL, LogLevel.SILLY) as LogLevel

})
export const SILLY = LogLevel.SILLY
export const DEBUG = LogLevel.DEBUG
export const VERBOSE = LogLevel.VERBOSE
export const INFO = LogLevel.INFO
export const HTTP = LogLevel.HTTP
export const WARNING = LogLevel.WARNING
export const ERROR = LogLevel.ERROR
