/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

interface LoggerOptions {
  level: LogLevel
  transports: Transport[]
}

export type Serializable = object | string | number | Date | boolean | undefined | null

export type Transport = (info: Log) => void

export interface Log { level: LogLevel, message: Serializable }
export enum LogLevel {
  ERROR,
  WARNING,
  HTTP,
  INFO,
  DEBUG,
  VERBOSE,
  SILLY
}
export type Logger = (level: LogLevel, message: Serializable) => void

const shouldLog = (logLevel: LogLevel, v: Log): boolean => {
  return v.level <= logLevel
}

export const createLogger = (v: LoggerOptions): Logger => {
  return (level: LogLevel, message: Serializable) => {
    const log: Log = { level, message }
    if (shouldLog(v.level, log)) {
      v.transports.forEach(transport => transport(log))
    }
  }
}
