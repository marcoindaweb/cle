/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */
import { LogLevel, Serializable, Transport } from '../logger'
import chalk, { ChalkFunction } from 'chalk'

export const console: Transport = (log) => {
  switch (log.level) {
    case LogLevel.DEBUG:
      formatWithColor(chalk.white, { level: 'debug', message: log.message })
      break
    case LogLevel.ERROR:
      formatWithColor(chalk.red, { level: 'error', message: log.message })
      break
    case LogLevel.HTTP:
      formatWithColor(chalk.blue, { level: 'http', message: log.message })
      break
    case LogLevel.INFO:
      formatWithColor(chalk.green, { level: 'info', message: log.message })
      break
    case LogLevel.SILLY:
      formatWithColor(chalk.gray, { level: 'silly', message: log.message })
      break
    case LogLevel.VERBOSE:
      formatWithColor(chalk.yellowBright, { level: 'verbose', message: log.message })
      break
    case LogLevel.WARNING:
      formatWithColor(chalk.yellow, { level: 'warning', message: log.message })
      break
  }
}
export const formatWithColor = (chalk: ChalkFunction, info: { level: string, message: Serializable }): void => {
  process.stdout.write(`${chalk(`[${info.level}]`)}: ${serializableToString(info.message)}\n`)
}
export const serializableToString = (value: Serializable): string => {
  if (value instanceof Date) {
    return value.toISOString()
  }

  if (value === null) {
    return 'null'
  }

  if (value === undefined) {
    return 'undefined'
  }

  switch (typeof value) {
    case 'number':
      return value.toString()
    case 'object':
      return JSON.stringify(value)
    case 'string':
      return value
  }
  return 'N/A'
}
