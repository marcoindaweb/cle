/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Command$ } from '../commands'
import { log } from './index'
import { LogLevel } from './logger'

Command$('log:test').subscribe(() => {
  log(LogLevel.ERROR, 'Error')
  log(LogLevel.WARNING, 'Warning')
  log(LogLevel.HTTP, 'Http')
  log(LogLevel.INFO, 'Info')
  log(LogLevel.DEBUG, 'Debug')
  log(LogLevel.VERBOSE, 'Verbose')
  log(LogLevel.SILLY, 'Silly')
})
