/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { format } from 'winston'
import chalk, { ChalkFunction } from 'chalk'

export const formatConsole = format((info, opts) => {
  switch (info.level) {
    case 'silly':
      formatWithColor(chalk.gray, info)
      break
    case 'verbose':
      formatWithColor(chalk.yellowBright, info)
      break
    case 'debug':
      formatWithColor(chalk.white, info)
      break
    case 'http':
      formatWithColor(chalk.blue, info)
      break
    case 'info':
      formatWithColor(chalk.green, info)
      break
    case 'warn':
      formatWithColor(chalk.yellow, info)
      break
    case 'error':
      formatWithColor(chalk.red, info)
      break
  }
  return info
})

export const formatWithColor = (chalk: ChalkFunction, info: { level: string, message: string }): void => {
  info.level = chalk(`[${info.level}]`)
}
