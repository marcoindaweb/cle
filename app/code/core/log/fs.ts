/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { join } from '../fs/utils'
import { varDirectory } from '../fs'

export const logDirectory = join(varDirectory, 'log')
export const errorPath = join(logDirectory, 'error.log')
export const warnPath = join(logDirectory, 'warn.log')
export const infoPath = join(logDirectory, 'info.log')
export const httpPath = join(logDirectory, 'http.log')
export const verbosePath = join(logDirectory, 'verbose.log')
export const debugPath = join(logDirectory, 'debug.log')
export const sillyPath = join(logDirectory, 'silly.log')
