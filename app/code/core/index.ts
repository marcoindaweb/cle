/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { Subject } from 'rxjs'
import { argv$ } from './commands/Argv'
import { getModules } from './modules'

const _onStart: Array<() => any> = []
const _onModuleLoad: Record<string, Array<() => any>> = {}
export const start$ = new Subject()

/**
 * Run the callback after all modules are loaded,
 * right before completing the boot of the app
 */
export const onStart = (cb: () => any): void => {
  _onStart.push(cb)
}

/**
 * Run the callback right after the module gets loaded
 */
export const onModuleLoad = (moduleName: string, cb: () => any): void => {
  if (_onModuleLoad[moduleName] === undefined) {
    _onModuleLoad[moduleName] = []
  }
  _onModuleLoad[moduleName].push(cb)
}

export const start = async (): Promise<void> => {
  start$.next()
  for (const module of getModules()) {
    await import(module)
    for (const onLoad of _onModuleLoad[module] ?? []) {
      await onLoad()
    }
  }
  for (const cb of _onStart) {
    try {
      await cb()
    } catch (e) {
      const err: Error = e
      console.error(err)
      process.exit(1)
    }
  }
  start$.complete()
}

start$.subscribe({ complete: () => argv$.next(process.argv) })
export { load } from './modules/index'
