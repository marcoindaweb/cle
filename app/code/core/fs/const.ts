/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

// Base directory of the whole application
export const BASE_DIRECTORY = 'core/fs/base_directory'
// Directory for generated code / data
export const GENERATED_DIR = 'core/fs/generated_directory'
// Var directory
export const VAR_DIR = 'core/fs/var_directory'
