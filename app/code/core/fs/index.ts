/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { get } from '../configuration'
import { join } from './utils'
import { BASE_DIRECTORY, GENERATED_DIR, VAR_DIR } from './const'
import fs from 'fs-extra'

export const baseDir = get(BASE_DIRECTORY, join(__dirname, '../../../')) as string
export const generated = join(baseDir, get(GENERATED_DIR, 'generated') as string)
export const varDirectory = join(baseDir, get(VAR_DIR, 'var') as string)
export const storageDir = join(baseDir, 'storage')
export const copy = fs.copy
export const readFile = fs.readFile
export const writeFile = fs.writeFile
export const ensureFile = fs.ensureFile
export const remove = fs.remove
