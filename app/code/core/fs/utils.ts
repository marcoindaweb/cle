/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import * as path from 'path'

export const join = path.join
export const relative = path.relative
export const dirname = path.dirname
export const basename = path.basename
export const extname = path.extname
export const removeExt = (v: string): string => {
  const ext = extname(v)
  return v.replace(ext, '')
}
export const isAbsoulte = path.isAbsolute
export const isModulePath = (path: string): boolean =>
  path.charAt(0) === '@' ||
    (path.charAt(0) !== '.' && path.charAt(0) !== '/')
export const relativeToFilePath = (from: string, to: string): string => relative(from, dirname(to))
