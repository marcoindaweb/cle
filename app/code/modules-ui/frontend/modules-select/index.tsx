/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import { AjaxError, fetchJson, useRecoilValue } from '@linda/frontend/frontend'
import { ajaxSelector } from '@linda/frontend/frontend/recoil'
import { HTMLSelect, IHTMLSelectProps } from '@linda/ui/frontend'
import { AdminAjaxError } from '@linda/admin/frontend/AdminAjaxError'
import Translated from '@linda/i18n/frontend/components/Translated'

const fetchModules = ajaxSelector<string[] | AjaxError>(
  'modules-ui/fetchModules',
  () => fetchJson<string[]>('/modules/v1/modules')
)

export const ModulesSelect:
React.FC<
Omit<IHTMLSelectProps, 'onChange' | 'value'> & { value: string, onChange: (v: string) => void }
> =
  (props) => {
    const res = useRecoilValue(fetchModules)
    return res instanceof AjaxError
      ? <AdminAjaxError err={res} />
      : <HTMLSelect {...props} value={props.value} onChange={e => props.onChange(e.target.value)}>
        <Translated message='Select a module'>{msg => <option>{msg}</option>}</Translated>
        {res.map(v => <option value={v}>{v}</option>)}
      </HTMLSelect>
  }
