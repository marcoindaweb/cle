/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { addFrontend } from '@linda/frontend'
import { join } from '@linda/core/fs/utils'
import { addInsideAdminContent } from '@linda/admin'

addFrontend(join(__dirname, 'frontend'))
addInsideAdminContent(join(__dirname, 'frontend/index'))
