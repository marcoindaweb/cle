/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useRecoilValue } from 'recoil'
import { fetchSection } from '../../../store/configuration'
import React, { useState } from 'react'
import AdminTab from '@linda/admin/frontend/AdminTab'
import Translated from '@linda/i18n/frontend/components/Translated'
import { Field, FieldType, Group } from '@linda/config/types'
import { Button, FormGroup, HTMLSelect, InputGroup, Switch } from '@linda/ui/frontend'
import classes from './style.sass'
import { AdminAjaxError } from '@linda/admin/frontend/AdminAjaxError'
import { AjaxError } from '@linda/frontend/frontend'

const Current: React.FC<{ tab: string, section: string }> = ({ tab, section }) => {
  const res = useRecoilValue(fetchSection({ tab, section }))

  return <div>
    {res instanceof AjaxError
      ? <AdminAjaxError err={res} />
      : res.groups.map(group => <GroupTab key={`${tab}-${section}-${group.name}`} group={group}/>)
    }
  </div>
}

const GroupTab: React.FC<{ group: Group }> = ({ group }) => {
  const [open, setOpen] = useState(false)
  return <AdminTab
    isOpen={open}
    onToggle={() => setOpen(!open)}
    name={<Translated message={group.name}/>}
  >
    {group.fields.map(field => <FieldSection key={field.key} field={field} />)}
  </AdminTab>
}

const FieldSection: React.FC<{ field: Field }> = ({ field }) => {
  return <FormGroup
    className={classes.field}
    label={<span className={classes.label}>
      <Translated message={field.name} />
    </span>}
    inline
  >
    <FieldInput field={field}/>
  </FormGroup>
}

const FieldInput: React.FC<{ field: Field }> = ({ field }) => {
  switch (field.type) {
    case FieldType.BOOLEAN:
      return <Switch />
    case FieldType.BUTTON:
      return <Button />
    case FieldType.MULTISELECT:
      return <HTMLSelect />
    case FieldType.SELECT:
      return <HTMLSelect />
    case FieldType.TEXT:
      return <InputGroup />
    case FieldType.TEXTAREA:
      return <InputGroup type='textarea' />
    case FieldType.TIME:
    case FieldType.CUSTOM:
    case FieldType.DATE:
    case FieldType.DATETIME:
    case FieldType.UNKNOWN:
      return <code>{JSON.stringify(field.value ?? 'undefined')}</code>
  }
}

export default Current
