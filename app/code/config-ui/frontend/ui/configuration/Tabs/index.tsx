/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { fetchTabs } from '../../../store/configuration'
import { atom, useRecoilState, useRecoilValue } from 'recoil'
import React, { useCallback } from 'react'
import { TabsSummary } from '@linda/config/http/types'
import AdminTab from '@linda/admin/frontend/AdminTab'
import Translated from '@linda/i18n/frontend/components/Translated'
import AdminList from '@linda/admin/frontend/AdminList'
import AdminListItem from '@linda/admin/frontend/AdminListItem'
import { AjaxError } from '@linda/frontend/frontend'
import { AdminAjaxError } from '@linda/admin/frontend/AdminAjaxError'

const openTab = atom<string | undefined>({
  key: 'config/tabs/openTab',
  default: undefined
})

const Tab: React.FC<{ tab: TabsSummary['tabs'][0]}> = ({ tab }) => {
  const [current, setOpen] = useRecoilState(openTab)
  const isOpen = current === tab.name
  const onToggle = useCallback(() => {
    isOpen ? setOpen(undefined) : setOpen(tab.name)
  }, [isOpen, tab.name])
  return <AdminTab
    isOpen={isOpen}
    onToggle={onToggle}
    name={<Translated message={tab.name} />}
    noPadding
  >
    <AdminList>
      {tab.sections.map(v => <Section key={v} tab={tab.name} name={v} />)}
    </AdminList>
  </AdminTab>
}

const Section: React.FC<{ tab: string, name: string }> = ({ tab, name }) =>
  <Translated message={name}>{msg =>
    <AdminListItem to={`/admin/config/${tab}/${name}`}>
      {msg}
    </AdminListItem>
  }</Translated>

const Tabs: React.FC = () => {
  const res = useRecoilValue(fetchTabs)
  return res instanceof AjaxError
    ? <AdminAjaxError err={res} />
    : <div>
      {res.tabs.map(tab => <Tab tab={tab} key={tab.name}/>)}
    </div>
}

export default Tabs
