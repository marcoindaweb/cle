/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import Tabs from './Tabs'
import { useRoute } from '@linda/frontend/frontend'
import Translated from '@linda/i18n/frontend/components/Translated'
import AdminSuspense from '@linda/admin/frontend/AdminSuspense'
import Current from './Current'
import AdminPage from '@linda/admin/frontend/AdminPage'
import { admin } from '@linda/admin/frontend/Admin'
import useTranslate from '@linda/i18n/frontend/hooks/useTranslate'

const Configuration: React.FC = () => {
  const [match] = useRoute(admin('/config/:path*'))
  const [, params] = useRoute<{ tab: string, section: string }>(admin('/config/:tab/:section'))
  return match ? <AdminPage name={<Translated message="Configuration" />} template='2-cols' buttons={[
    { text: <Translated message='Reset' /> },
    { text: <Translated message='Save' />, intent: 'success' }
  ]}>
    <AdminSuspense>
      <Tabs/>
    </AdminSuspense>
    {params !== null && <AdminSuspense>
      <Current tab={params.tab} section={params.section}/>
    </AdminSuspense>}
  </AdminPage> : null
}

export default Configuration
