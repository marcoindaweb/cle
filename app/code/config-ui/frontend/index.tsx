/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React, { useEffect } from 'react'
import Configuration from './ui/configuration'
import { useAddSidebarItem } from '@linda/admin/frontend/Admin/Sidebar'

const Index: React.FC = () => {
  const addItem = useAddSidebarItem()
  useEffect(() => {
    addItem({
      icon: 'settings',
      name: 'Settings',
      key: 'configuration',
      groups: [
        {
          key: 'config-settings-configurations',
          title: 'Configuration',
          routes: [
            {
              name: 'Configuration',
              path: '/admin/config'
            },
            {
              name: 'Developer',
              path: '/admin/config/system/developer'
            }
          ]
        }
      ]
    })
  }, [])
  return <div><Configuration /></div>
}
export default Index
