/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { AjaxError, fetchJson } from '@linda/frontend/frontend'
import { TabsSummary } from '@linda/config/http/types'
import { ConfigValue, Section } from '@linda/config/types'
import { ajaxSelector, ajaxSelectorFamily } from '@linda/frontend/frontend/recoil'
import { useRecoilValue } from 'recoil'

export const fetchConfiguration = ajaxSelectorFamily<ConfigValue | AjaxError, string>(
  'config/fetchConfiguration',
  (key) => () => fetchJson<ConfigValue>(`/config/v1/configuration/${key}`)
)

export const useConfig = <T extends ConfigValue>(key: string): T | undefined => {
  const value = useRecoilValue(fetchConfiguration(key))
  if (value instanceof AjaxError) {
    console.error(`Error while fetching configuration ${key}`)
  } else {
    return value as T
  }
}

export const fetchTabs = ajaxSelector<TabsSummary | AjaxError>(
  'config/fetchTabs',
  async () => {
    return await fetchJson<TabsSummary>('/config/v1/configuration/tabs')
  }
)

export const fetchSection =
  ajaxSelectorFamily<Section | AjaxError, { tab: string, section: string }>(
    'config/fetchSection',
    ({ tab, section }) => () => {
      return fetchJson<Section>(`/config/v1/configuration/tabs/${tab}/sections/${section}`)
    }
  )
