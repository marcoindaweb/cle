/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { SessionOptions, WithTransactionCallback } from 'mongodb'
import { client } from '../index'

export const transaction = async (cb: WithTransactionCallback<unknown>, options?: SessionOptions): Promise<void> => {
  const session = client.startSession(options)
  try {
    await session.withTransaction(cb)
  } finally {
    await session.endSession()
  }
}
