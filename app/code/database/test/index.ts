/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { set } from '@linda/core/configuration'
import { DB_NAME } from '../const'
import { client } from '../index'

export const useTestingDB = (): void => {
  set(DB_NAME, 'test')
}

export const clearTestingDB = async (): Promise<void> => {
  await client.db('test').dropDatabase()
}
