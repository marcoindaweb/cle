/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */
import { Collection, Db, MongoClient, ObjectID, FilterQuery, Cursor, OptionalId, WithId, DeleteWriteOpResultObject, AggregationCursor, GridFSBucket } from 'mongodb'
import { DB_NAME, DB_URI } from './const'
import { get } from '@linda/core/configuration'
import { DEBUG, log } from '@linda/core/log'
import { onModuleLoad } from '@linda/core'
import * as mongo from 'mongodb'

export let client: MongoClient
export const db = (): Db => client.db(get(DB_NAME, 'linda') as string)
export const collection = <T>(name: string): Collection<T> => db().collection<T>(name)
onModuleLoad('@linda/database', async () => {
  const uri = get(DB_URI, 'mongodb://root:password@localhost:27017/?authSource=admin') as string
  client = new MongoClient(
    uri,
    { useUnifiedTopology: true }
  )
  await client.connect()
    .then(() => log(DEBUG, 'Mongodb client connected'))
})

export { mongo, ObjectID, Collection, DB_URI, FilterQuery, Cursor, OptionalId, WithId, DeleteWriteOpResultObject, AggregationCursor, GridFSBucket }
