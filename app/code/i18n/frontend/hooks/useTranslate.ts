/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { useIntl } from 'react-intl'
import { useCallback } from 'react'

const useTranslate = () => {
  const { formatMessage } = useIntl()

  return useCallback(
    (message: string, descriptor?: { id: string, description: string, values: Record<string, any> }) => {
      const id = descriptor?.id || message.toLowerCase()
      if (!id || id.length === 0) {
        return '!!missing id or message!!'
      }
      return formatMessage({
        id,
        defaultMessage: message,
        description: descriptor?.description
      })
    },
    []
  )
}

export default useTranslate
