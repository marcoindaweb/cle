/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Tag } from '@blueprintjs/core'
import classes from './style.sass'
import { useConfig } from '@linda/config-ui/frontend/store/configuration'
import { TRANSLATE_MODE } from '@linda/i18n/const'
import AdminSuspense from '@linda/admin/frontend/AdminSuspense'

const Translated: React.FC<{
  id?: string
  message: string
  description?: string
  values?: Record<string, any>
  children?: (...nodes: React.ReactNodeArray) => React.ReactNode
}> = (props) => <AdminSuspense><_Translated {...props} /></AdminSuspense>

const _Translated: React.FC<{
  id?: string
  message: string
  description?: string
  values?: Record<string, any>
  children?: (...nodes: React.ReactNodeArray) => React.ReactNode
}> = (props) => {
  const id = props?.id ?? props.message.toLowerCase()
  const mode = useConfig<boolean>(TRANSLATE_MODE)

  if (id === undefined || id.length === 0) {
    return (
      <Tag intent="danger" icon="error">
        Id or message not passed {JSON.stringify({ id, message: props.message })}
      </Tag>
    )
  }

  return (
    mode === true
      ? <div className={classes['translate-mode']}>
        <FormattedMessage id={id} defaultMessage={props.message} description={props.description}>
          {props.children}
        </FormattedMessage>
      </div>
      : <FormattedMessage id={id} defaultMessage={props.message} description={props.description}>
        {props.children}
      </FormattedMessage>
  )
}

export default Translated
