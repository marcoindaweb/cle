/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { collection, ObjectID, Collection } from '@linda/database'
import { registerDocument, makeRepository } from '@linda/content-manager'
import { DocumentSchema, PropertyType } from '@linda/content-manager/types'

export interface Dictionary {
  _id: ObjectID
  language: string
  entries: Array<{ key: string, value: string }>
}

export const DictionarySchema: DocumentSchema = {
  name: 'Dictionary',
  module: '@linda/i18n',
  properties: {
    _id: {
      type: PropertyType.objectId
    },
    language: {
      type: PropertyType.string
    },
    entries: {
      type: PropertyType.array,
      document: {
        key: {
          type: PropertyType.string
        },
        value: {
          type: PropertyType.string
        }
      }
    }
  }
}

export const DictionaryCollection = (): Collection<Dictionary> => collection<Dictionary>('@linda/i18n__Dictionary')
export const DictionaryRepository = makeRepository(DictionarySchema, DictionaryCollection())
registerDocument(DictionarySchema)
