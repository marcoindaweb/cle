/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

import { addFrontend, wrapFrontendRoot } from '@linda/frontend'
import { join } from '@linda/core/fs/utils'
import { group, registerConfig, section, tab } from '@linda/config'
import { DEVELOPER_SECTION, SYSTEM_TAB } from '@linda/config/const'
import { TRANSLATE_MODE } from './const'
import { FieldType } from '@linda/config/types'
import './documents/Dictionary'

addFrontend(join(__dirname, 'frontend'))
wrapFrontendRoot(join(__dirname, 'frontend/containers/I18n/index.tsx'))
registerConfig(tab(SYSTEM_TAB, [
  section(DEVELOPER_SECTION, [
    group('i18n', [
      {
        key: TRANSLATE_MODE,
        name: 'Translate inline',
        default: false,
        type: FieldType.BOOLEAN
      }
    ])
  ])
]))
