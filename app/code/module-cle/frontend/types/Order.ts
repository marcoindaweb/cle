export interface Order {
  id: string
  client: {
    name: string
    address: string
  }
  epal: {
    dimensions: {
      width: number
      height: number
    }
    weight: number
  }
  withdrawal: {
    ready: Date
    delivery: Date
  }
  prices: Price[]
}

export interface Price {
  amount: number
  reason: string
}
