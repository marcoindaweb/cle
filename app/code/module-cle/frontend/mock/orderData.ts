import { Order } from '../types/Order'
import moment from 'moment'

const orders: Order[] = [
  {
    id: 'Ordine 01/01/2020',
    client: {
      name: 'Mengato costruzioni S.r.l.',
      address: 'Via Galileo 1, 35030, Selvazzano Dentro, PD'
    },
    epal: {
      dimensions: {
        height: 80,
        width: 120
      },
      weight: 150
    },
    withdrawal: {
      ready: moment('2020-01-01 10:00:00').toDate(),
      delivery: moment('2020-01-05 10:00:00').toDate()
    },
    prices: [
      {
        amount: 100,
        reason: 'Costo trasporto'
      },
      {
        amount: 20,
        reason: 'Costo assicurazione'
      },
      {
        amount: 10,
        reason: 'Costo gestione servizio'
      }
    ]
  },
  {
    id: 'Ordine 02/01/2020',
    client: {
      name: 'Mengato costruzioni S.r.l.',
      address: 'Via Galileo 1, 35030, Selvazzano Dentro, PD'
    },
    epal: {
      dimensions: {
        height: 80,
        width: 120
      },
      weight: 150
    },
    withdrawal: {
      ready: moment('2020-01-01 10:00:00').toDate(),
      delivery: moment('2020-01-05 10:00:00').toDate()
    },
    prices: [
      {
        amount: 100,
        reason: 'Costo trasporto'
      },
      {
        amount: 20,
        reason: 'Costo assicurazione'
      },
      {
        amount: 10,
        reason: 'Costo gestione servizio'
      }
    ]
  },
  {
    id: 'Ordine 03/01/2020',
    client: {
      name: 'Mengato costruzioni S.r.l.',
      address: 'Via Galileo 1, 35030, Selvazzano Dentro, PD'
    },
    epal: {
      dimensions: {
        height: 80,
        width: 120
      },
      weight: 150
    },
    withdrawal: {
      ready: moment('2020-01-01 10:00:00').toDate(),
      delivery: moment('2020-01-05 10:00:00').toDate()
    },
    prices: [
      {
        amount: 100,
        reason: 'Costo trasporto'
      },
      {
        amount: 20,
        reason: 'Costo assicurazione'
      },
      {
        amount: 10,
        reason: 'Costo gestione servizio'
      }
    ]
  }
]

export default orders
