import { useRoute } from 'wouter'
import Layout from '../../components/Layout'
import AuthGuard from '../../components/AuthGuard'
import React from 'react'
import PageTransportRoute from '../../components/SearchOrder'

export const makeTransportRoute: () => string = () => '/cle/transport/search'

const SearchOrderRoute: React.FC = () => {
  const [match] = useRoute(makeTransportRoute())

  return match ? (
    <AuthGuard>
      <Layout>
        <PageTransportRoute />
      </Layout>
    </AuthGuard>
  ) : null
}

export default SearchOrderRoute
