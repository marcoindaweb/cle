import { useRoute } from 'wouter'
import React from 'react'
import Register from '../../components/Register'
import Layout from '../../components/Layout'

export const makeRoute: () => string = () => '/cle/register'

const RegisterRoute: React.FC = () => {
  const [match] = useRoute(makeRoute())

  return match ? (
    <Layout>
      <Register />
    </Layout>
  ) : null
}

export default RegisterRoute
