import CreateOrder from '../../components/CreateOrder'
import React from 'react'
import { useRoute } from 'wouter'
import AuthGuard from '../../components/AuthGuard'
import Layout from '../../components/Layout'

export const makeCreateOrderRoute: () => string = () => '/cle/orders/create'

const CreateOrderRoute: React.FC = () => {
  const [match] = useRoute(makeCreateOrderRoute())

  return match ? (
    <AuthGuard>
      <Layout>
        <CreateOrder />
      </Layout>
    </AuthGuard>
  ) : null
}

export default CreateOrderRoute
