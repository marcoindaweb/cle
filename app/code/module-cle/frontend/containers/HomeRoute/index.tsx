import { useRoute } from 'wouter'
import Home from '../../components/Home'
import React from 'react'
import AuthGuard from '../../components/AuthGuard'
import Layout from '../../components/Layout'

export const makeRoute: () => string = () => '/cle'
const HomeRoute: React.FC = () => {
  const [match] = useRoute(makeRoute())

  return match ? (
    <AuthGuard>
      <Layout>
        <Home />
      </Layout>
    </AuthGuard>
  ) : null
}

export default HomeRoute
