import React from 'react'
import LoginRoute from '../LoginRoute'
import HomeRoute from '../HomeRoute'
import RegisterRoute from '../RegisterRoute'
import SearchOrderRoute from '../SearchOrderRoute'
import ConfirmOrderRoute from '../ConfirmOrderRoute'
import CreateOrderRoute from '../CreateOrderRoute'

const Router: React.FC = () => {
  return (
    <>
      <HomeRoute />
      <LoginRoute />
      <RegisterRoute />
      <SearchOrderRoute />
      <ConfirmOrderRoute />
      <CreateOrderRoute />
    </>
  )
}

export default Router
