import { useRoute } from 'wouter'
import React from 'react'
import Login from '../../components/Login'
import Layout from '../../components/Layout'

export const makeRoute: () => string = () => '/cle/login'

const LoginRoute: React.FC = () => {
  const [match] = useRoute(makeRoute())

  return match ? (
    <Layout>
      <Login />
    </Layout>
  ) : null
}

export default LoginRoute
