import { useRoute } from 'wouter'
import React from 'react'
import AuthGuard from '../../components/AuthGuard'
import Layout from '../../components/Layout'
import ConfirmOrder from '../../components/ConfirmOrder'

export const makeConfirmOrderRoute: (string) => string =
    (id: string) => `/cle/transport/confirm/${id}`

const ConfirmOrderRoute: React.FC = () => {
  const [match, params] = useRoute<{ id: string }>('/cle/transport/confirm/:id')

  return match && params?.id ? (
    <AuthGuard>
      <Layout>
        <ConfirmOrder id={params.id} />
      </Layout>
    </AuthGuard>
  ) : null
}

export default ConfirmOrderRoute
