export const media = {
  desktop: '@media (min-width: 1024px)'
}
export const background = {
  GREEN: 'background-color: $primary1'
}
