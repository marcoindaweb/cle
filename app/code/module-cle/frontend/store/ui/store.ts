import { atom, selector } from 'recoil'

export const ui = atom<{ sidebar: { show: boolean } }>({
  key: 'cle/ui',
  default: { sidebar: { show: false } }
})

export const sidebar = selector<{ show: boolean }>({
  key: 'cle/ui/sidebar',
  get: ({ get }) => {
    const { sidebar } = get(ui)
    return sidebar
  },
  set: ({ set }, newValue) => {
    set(ui, { sidebar: newValue })
  }
})

export const showSidebar = selector<boolean>({
  key: 'cle/ui/sidebar/show',
  get: ({ get }) => {
    const { show } = get(sidebar)
    return show
  },
  set: ({ set }, newValue) => {
    set(sidebar, { show: newValue })
  }
})
