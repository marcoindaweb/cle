import { atom } from 'recoil'

export type TransportKind = 'gelo' | 'fresco' | 'secco' | 'container'

export interface Location {
  region: string
  province: string
  municipality: string
  cap: string
}

interface TransportCreation {
  kind: TransportKind
  from: Location
  stops: Location[]
  to: Location
  palletToReturn: boolean
}

export const kind = atom<TransportKind | undefined>({
  key: 'cle/transport/create/kind',
  default: undefined
})
export const from = atom<Location | undefined>({
  key: 'cle/transport/create/from',
  default: undefined
})
export const to = atom<Location | undefined>({
  key: 'cle/transport/create/to',
  default: undefined
})
export const stops = atom<Location[]>({
  key: 'cle/transport/create/stops',
  default: []
})
export const palletToReturn = atom<boolean>({
  key: 'cle/transport/create/palletToReturn',
  default: false
})
export const stopsStatus = atom<boolean>({
  key: 'cle/transport/create/stopsStatus',
  default: true
})
export const materialType = atom<string | undefined>({
  key: 'cle/transport/material',
  default: undefined
})
export const palletDimension = atom<string>({
  key: 'cle/transport/dimension',
  default: '80x120'
})
export const weight = atom<string>({
  key: 'cle/transport/weight',
  default: ''
})
export const height = atom<string>({
  key: 'cle/transport/height',
  default: ''
})
export const deliver = atom<string>({
  key: 'cle/transport/deliver',
  default: ''
})

export const transportKinds = atom<string[]>({
  key: 'cle/transport/kinds',
  default: ['Gelo', 'Fresco', 'Secco', 'Container']
})
export const transportFrom = atom<string[]>({
  key: 'cle/transport/from',
  default: ['Milano', 'Figino', 'Chiaravalle', 'Cantalupa', 'Belgioioso']
})
export const transportTo = atom<string[]>({
  key: 'cle/transport/to',
  default: ['Padova', 'Varotto', 'Locchi', 'Pozzoveggiani', 'Palazzetto']
})
export const materialTypes = atom<string[]>({
  key: 'cle/transport/materials',
  default: ['Cerchi in lega', 'materiale costruzione', 'abbigliamento', 'mobili', 'elettronica']
})
export const palletDimensions = atom<string[]>({
  key: 'cle/transport/dimensions',
  default: ['80x120', '120x120', '100x180', 'Altro']
})
