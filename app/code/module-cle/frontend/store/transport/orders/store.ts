import { atom } from 'recoil'
import { Order } from '../../../types/Order'

export const orders = atom<Order[]>({
  key: 'cle/transport/orders',
  default: []
})
