import React from 'react'
// import AuthLogin from '@linda/module-auth/frontend/components/Login'
import { Button, Card, Classes, FormGroup, InputGroup } from '@blueprintjs/core'
import ConnectedFormGroup from '../ConnectedFormGroup'
import Translated from '@linda/i18n/frontend/components/Translated'
import ControlledInputGroup from '../ControlledInputGroup'
import { createUseStyles } from 'react-jss'
import { useLocation } from 'wouter'
import { makeRoute as makeHomeRoute } from '../../containers/HomeRoute'
import { makeRoute as makeRegisterRoute } from '../../containers/RegisterRoute'
import { media } from '../../style/theme'
import useTranslate from '@linda/i18n/frontend/hooks/useTranslate'

const useStyles = createUseStyles({
  main: {
    display: 'flex',
    justifyContent: 'center',
    '& .login-container': {
      width: '70vw',
      marginTop: '50px',
      [media.desktop]: {
        width: '50vw'
      },
      '& .buttons': {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        '& .register-btn': {
          marginRight: '10px'
        }
      }
    }
  },
  title: {
    color: '$primary1'
  },
  loginForm: {
    display: 'flex',
    justifyContent: 'center',
    flexFlow: 'row'
  }
})

const Login: () => JSX.Element = () => {
  const { main, title, loginForm } = useStyles()
  const [, setLocation] = useLocation()
  const translate = useTranslate()

  return (
    <div className={main}>
      <div className="login-container">
        <h3 className={Classes.HEADING}>Centro Logistico
          <span className={title}> EUROPA </span> ti da il
          <span className={title}> BENVENUTO</span></h3>
        <Card>
          <FormGroup>
            <InputGroup name="email" placeholder={translate('P. Iva/Cod. Cliente')} />
          </FormGroup>
          <FormGroup>
            <InputGroup
              type='password'
              name="password"
              placeholder={translate('password')}
            />
          </FormGroup>
          <div className="buttons">
            <Button
              minimal
              className="register-btn"
              onClick={() => setLocation(makeRegisterRoute())}
            >
              <Translated message="Registrati" />
            </Button>
            <Button onClick={() => setLocation(makeHomeRoute())} type="submit">
              <Translated message="Login" />
            </Button>
          </div>
        </Card>
      </div>
    </div>
  )
}

export default Login
