import React, { useState } from 'react'
import {
  Button,
  Classes, Collapse,
  Divider,
  HTMLTable, Icon
} from '@blueprintjs/core'
import Translated from '@linda/i18n/frontend/components/Translated'
import InlineFormContainer from '../InlineFormContainer'
import { createUseStyles } from 'react-jss'
import { useRecoilState } from 'recoil'
import {
  deliver,
  from,
  height,
  materialType, palletDimension, palletToReturn, to, weight
} from '../../store/transport/create/store'
import { useLocation } from '@linda/frontend/frontend'
import { makeRoute as makeHomeRoute } from '../../containers/HomeRoute'
import moment from 'moment'

const useStyles = createUseStyles({
  title: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexFlow: 'row'
  },
  arrow: {
    alignSelf: 'center',
    marginX: '0 10px'
  },
  submitButton: {
    backgroundColor: '#6BCF2C !important'
  }
})

const CreateOrder: () => JSX.Element = () => {
  const [fromValue] = useRecoilState(from)
  const [toValue] = useRecoilState(to)
  const [materialTypeValue] = useRecoilState(materialType)
  const [palletDimensionValue] = useRecoilState(palletDimension)
  const [weightValue] = useRecoilState(weight)
  const [heightValue] = useRecoilState(height)
  const [deliverValue] = useRecoilState(deliver)
  const [palletToReturnValue] = useRecoilState(palletToReturn)
  // const [itemTypes, setItemTypes] = useState(['materiale costruzione', 'abbigliamento', 'mobili', 'elettronica'])
  // const [itemType, setItemType] = useState('')
  const [, setLocation] = useLocation()
  const [showPrice, setShowPrice] = useState(false)
  const { title, arrow, submitButton } = useStyles()

  const prices = {
    prices: [
      { amount: 30, reason: 'Costo trasporto' },
      { amount: 20, reason: 'Costo assicurazione' },
      { amount: 15, reason: 'Costo gestione servizio' }
    ]
  }

  return (
    <InlineFormContainer>
      <h3 className={Classes.HEADING}>La Mia Ceramica</h3>
      <div className={title}>
        <h1>{fromValue !== undefined ? `Partenza ${fromValue.municipality}  ` : ''} </h1>
        {fromValue !== undefined && toValue !== undefined
          ? <Icon icon='arrow-right' tagName='h1' className={arrow} /> : ''}
        <h1>{toValue !== undefined ? `  Arrivo ${toValue.municipality}` : ''}</h1>
      </div>

      <ul className={Classes.LIST_UNSTYLED + ' ' + Classes.LIST}>
        <li>Ordine N. 1\2020</li>
        <br />
        <li>
           Partenza: {fromValue !== undefined ? fromValue.municipality : ''}
        </li>
        <li>
           Arrivo: {toValue !== undefined ? toValue.municipality : ''}
        </li>
        <br />
        <li>
          Materiale: {materialTypeValue !== undefined ? materialTypeValue : ''}
        </li>
        <br />
        <li>
          EPAL: {palletToReturnValue ? 'A  rendere' : 'Non a rendere'}
        </li>
        <br />
        <li>
          Bancale: {palletDimensionValue !== undefined ? palletDimensionValue : ''}
        </li>
        <br/>
        <li>
          Peso: {weightValue !== undefined ? weightValue + ' kg' : ''}
        </li>
        <br />
        <li>
          Altezza: {heightValue !== undefined ? heightValue + ' cm' : ''}
        </li>
        <br />
        <li>
          Consegna: {deliverValue !== undefined ? moment(deliverValue).format('DD/MM/YYYY') : ''}
        </li>
        <br />
        <li>
          <Button
            rightIcon={showPrice ? 'caret-up' : 'caret-down'}
            minimal
            onClick={() => setShowPrice(!showPrice)}
          >
            Prezzo {prices.prices.reduce((prev, current) => prev + current.amount, 0)}€
          </Button>
          <Collapse isOpen={showPrice}>
            <HTMLTable condensed>
              {prices.prices.map((v, index) => (
                <tr key={index}>
                  <td>{v.amount}€</td>
                  <td>{v.reason}</td>
                </tr>
              ))}
            </HTMLTable>
          </Collapse>
        </li>
      </ul>

      <Divider />

      <h4 className={Classes.HEADING}>IL PAGAMENTO AVVERRÀ AL MOMENTO DELLA CONSEGNA IN LOCO</h4>

      <Button className={submitButton} onClick={() => setLocation(makeHomeRoute())}>
        <Translated message="VAI" />
      </Button>

      {/* <div className="grid-container"> */}
      {/*  <FormGroup inline label={<Translated message="Partenza" />}> */}
      {/*    <StopInput /> */}
      {/*  </FormGroup> */}
      {/*  <FormGroup inline label={<Translated message="Arrivo" />}> */}
      {/*    <StopInput /> */}
      {/*  </FormGroup> */}
      {/* </div> */}
      {/* <FormGroup inline label={<Translated message="Tipo Materiale" />}> */}
      {/*  <Select<string> */}
      {/*    activeItem={itemType} */}
      {/*    filterable */}
      {/*    items={itemTypes} */}
      {/*    itemRenderer={(value, props) => ( */}
      {/*      <MenuItem */}
      {/*        text={value} */}
      {/*        {...props.modifiers} */}
      {/*        active={props.modifiers.active} */}
      {/*        onClick={props.handleClick} */}
      {/*      /> */}
      {/*    )} */}
      {/*    itemPredicate={(query, item) => item.includes(query)} */}
      {/*    onItemSelect={(v) => setItemType(v)} */}
      {/*  > */}
      {/*    <Button rightIcon="caret-down" text={itemType || 'Seleziona una tipologia'} /> */}
      {/*  </Select> */}
      {/* </FormGroup> */}
      {/* <div className="grid-container"> */}
      {/*  <FormGroup inline label={<Translated message="Peso lordo" />}> */}
      {/*    <InputGroup /> */}
      {/*  </FormGroup> */}
      {/*  <FormGroup inline label={<Translated message="Valore" />}> */}
      {/*    <InputGroup /> */}
      {/*  </FormGroup> */}
      {/*  <FormGroup inline label={<Translated message="Peso netto" />}> */}
      {/*    <InputGroup /> */}
      {/*  </FormGroup> */}
      {/*  <FormGroup inline label={<Translated message="Materiale pronto" />}> */}
      {/*    <DateInput */}
      {/*      formatDate={(v) => moment(v).format('DD/MM/YYYY HH:mm')} */}
      {/*      timePrecision="minute" */}
      {/*      timePickerProps={{ showArrowButtons: true }} */}
      {/*      closeOnSelection={false} */}
      {/*    /> */}
      {/*  </FormGroup> */}
      {/*  <FormGroup inline label={<Translated message="Num Pellets" />}> */}
      {/*    <InputGroup /> */}
      {/*  </FormGroup> */}
      {/*  <FormGroup inline label={<Translated message="Materiale consegnato" />}> */}
      {/*    <DateInput */}
      {/*      formatDate={(v) => moment(v).format('DD/MM/YYYY HH:mm')} */}
      {/*      timePrecision="minute" */}
      {/*      timePickerProps={{ showArrowButtons: true }} */}
      {/*      closeOnSelection={false} */}
      {/*    /> */}
      {/*  </FormGroup> */}
      {/*  <Checkbox> */}
      {/*    <Translated message="Pellets a rendere" /> */}
      {/*  </Checkbox> */}
      {/* </div> */}
      {/* <Button>Inserisci</Button> */}
    </InlineFormContainer>
  )
}

export default CreateOrder
