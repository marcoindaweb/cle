import React, { useCallback, useState } from 'react'
import orders from '../../mock/orderData'
import { Button, Checkbox, Classes, Collapse, FormGroup, HTMLTable, Icon } from '@blueprintjs/core'
import Translated from '@linda/i18n/frontend/components/Translated'
import InlineFormContainer from '../InlineFormContainer'
import moment from 'moment'
import { createUseStyles } from 'react-jss'
import { useRecoilState } from 'recoil'
import { from, materialType, materialTypes, to, TransportKind } from '../../store/transport/create/store'
import { Select } from '@blueprintjs/select'
import MaterialSelect from '../MaterialSelect'

const useStyles = createUseStyles({
  title: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexFlow: 'row'
  },
  arrow: {
    alignSelf: 'center',
    marginX: '0 10px'
  }
})

const ConfirmOrder: React.FC<{ id: string }> = ({ id }) => {
  const [fromValue] = useRecoilState(from)
  const [toValue] = useRecoilState(to)
  const [materialTypeValue, setMaterialType] = useRecoilState(materialType)
  const [materialTypesValue] = useRecoilState(materialTypes)
  const [showPrice, setShowPrice] = useState(false)
  const { title, arrow } = useStyles()
  const onProductTypeChange = useCallback((v: string) => {
    setMaterialType(v)
  }, [setMaterialType])

  const order = orders[0]
  if (!order) {
    return null
  }

  return (
    <InlineFormContainer>
      <div className={title}>
        <h1>{fromValue !== undefined ? `Partenza ${fromValue.municipality}  ` : ''} </h1>
        {fromValue !== undefined && toValue !== undefined
          ? <Icon icon='arrow-right' tagName='h1' className={arrow} /> : ''}
        <h1>{toValue !== undefined ? `  Arrivo ${toValue.municipality}` : ''}</h1>
      </div>
      <h4 className={Classes.HEADING}>
        <Translated message="Hai scelto" /> {order.id}
      </h4>
      <FormGroup inline label={<Translated message="Partenza:" />}>
        <ul className={Classes.LIST_UNSTYLED}>
          <li>{order.client.name}</li>
          <li>{order.client.address}</li>
          {/* <li> */}
          {/*  EPAL {order.epal.dimensions.height}X{order.epal.dimensions.width}, EPAL A RENDERE */}
          {/* </li> */}
          {/* <li> */}

        </ul>
      </FormGroup>
      <FormGroup inline label={<Translated message="Arrivo:" />}>
        <ul className={Classes.LIST_UNSTYLED}>
          <li>Castellazza Formaggi S.r.l.</li>
          <li>Via Roma 5, 21053, castellanza, VA</li>
        </ul>
      </FormGroup>
      <FormGroup inline label={<Translated message="Dettagli trasporto:" />}>
        <ul className={Classes.LIST_UNSTYLED}>
          <li>
            {order.epal.dimensions.height} X {order.epal.dimensions.width}, {order.epal.weight} kg
          </li>
          <li>
            Ritiro {moment(order.withdrawal.ready).format('DD/MM/YYYY')} dalle{' '}
            {moment(order.withdrawal.ready).format('HH:mm')}
          </li>
          <li>
              Consegna {moment(order.withdrawal.delivery).format('DD/MM/YYYY')} entro{' '}
            {moment(order.withdrawal.delivery).format('HH:mm')}
          </li>
        </ul>
      </FormGroup>
      <FormGroup inline label={<Translated message="Tipo materiale" />}>
        <MaterialSelect
          value={materialTypeValue !== undefined ? materialTypeValue
            : 'Scegli il tipo di materiale'}
          selectOptions={
            ['Scegli il tipo di materiale', ...materialTypesValue]
          }
          onChange={(e) =>
            onProductTypeChange(e.target.value)} />
      </FormGroup>
      <FormGroup inline label={<Translated message="Prezzo:" />}>
        <Button
          rightIcon={showPrice ? 'caret-up' : 'caret-down'}
          minimal
          onClick={() => setShowPrice(!showPrice)}
        >
          {order.prices.reduce((prev, current) => prev + current.amount, 0)}€
        </Button>
        <Collapse isOpen={showPrice}>
          <HTMLTable condensed>
            {order.prices.map((v, index) => (
              <tr key={index}>
                <td>{v.amount}€</td>
                <td>{v.reason}</td>
              </tr>
            ))}
          </HTMLTable>
        </Collapse>
      </FormGroup>
      <FormGroup inline label={<Translated message="Fattura" />}>
        <Checkbox>
          <Translated message="Stampa" />
        </Checkbox>
        <Checkbox>
          <Translated message="Invia" />
        </Checkbox>
      </FormGroup>
      <FormGroup inline label={<Translated message="Doc. di Trasporto" />}>
        <Checkbox>
          <Translated message="Stampa" />
        </Checkbox>
        <Checkbox>
          <Translated message="Invia" />
        </Checkbox>
      </FormGroup>
      <h4 className={Classes.HEADING}>IL PAGAMENTO AVVERRÀ AL MOMENTO DELLA CONSEGNA IN LOCO</h4>
      <Button>Conferma</Button>
      <Button minimal onClick={() => window.history.back()}>Indietro</Button>
    </InlineFormContainer>
  )
}

export default ConfirmOrder
