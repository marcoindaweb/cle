import React from 'react'
import { Controller, UseFormMethods } from 'react-hook-form'
import { IInputGroupProps, InputGroup } from '@blueprintjs/core'

const ControlledInputGroup: React.FC<{
  control?: UseFormMethods<any>['control']
  name: string
  defaultValue?: string
  input?: IInputGroupProps
}> = ({ name, defaultValue, input, control }) => (
  <Controller
    name={name}
    defaultValue={defaultValue || input?.value || ''}
    control={control}
    render={({ onChange, onBlur, value }) => (
      <InputGroup {...input} value={value} onChange={onChange} onBlur={onBlur} />
    )}
  />
)

export default ControlledInputGroup
