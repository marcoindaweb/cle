import {
  Alignment, Button, Navbar as BNavbar,
  NavbarDivider, NavbarGroup, NavbarHeading
} from '@blueprintjs/core'
import React, { useCallback } from 'react'
import { useRecoilState } from 'recoil'
import { showSidebar } from '../../store/ui/store'
import UserMenu from './UserMenu'
import { Link, useLocation } from 'wouter'
import { makeRoute } from '../../containers/HomeRoute'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
  navbar: {
    backgroundColor: '#6BCF2C'
  },
  title: {
    color: 'white !important'
  }
})

const Navbar = () => {
  const [show, setShow] = useRecoilState(showSidebar)

  const { navbar, title } = useStyles()

  return (
    <BNavbar className={navbar}>
      <NavbarGroup align={Alignment.LEFT}>
        <NavbarHeading>
          <Link href={makeRoute()}><h2 className={title}>CLE</h2></Link>
        </NavbarHeading>
        <NavbarDivider />
        <NavbarHeading>
          <small className={title}> IL TUO TRASPORTO IN 5 PASSI </small>
        </NavbarHeading>
      </NavbarGroup>
      <NavbarGroup align={Alignment.RIGHT}>
        <UserMenu />
        <Button intent='primary' minimal
          className={title}
          onClick={() => setShow(!show)} icon={show ? 'cross' : 'menu'} />
      </NavbarGroup>
    </BNavbar>
  )
}

export default Navbar
