// import { useLogout, useMe } from '@linda/module-auth/frontend/store/auth/actions'
import { useLocation } from 'wouter'
import React, { useCallback } from 'react'
import { makeRoute } from '../../../containers/LoginRoute'
import { Button, Menu, MenuItem, Popover } from '@blueprintjs/core'
import Translated from '@linda/i18n/frontend/components/Translated'

const UserMenu: () => JSX.Element | null = () => {
  // const { data } = useMe()
  // const [logout] = useLogout()
  const currentLocation: () => string = () => {
    if (window.location.hash !== '') {
      return window.location.hash.replace(/^#/, '')
    } else return '/'
  }

  const isLoggedIn: () => boolean = () => {
    return currentLocation().endsWith('/cle/')
  }

  const [, setLocation] = useLocation()

  const onLogout = useCallback(async () => {
    // await logout()
    setLocation(makeRoute())
  }, [setLocation])

  return isLoggedIn() ? (
    <Popover
      content={
        <Menu>
          <MenuItem icon="log-out" text={<Translated message="Esci" />} onClick={onLogout} />
        </Menu>
      }
      target={<Button icon="user" minimal />}
    />
  ) : null
}

export default UserMenu
