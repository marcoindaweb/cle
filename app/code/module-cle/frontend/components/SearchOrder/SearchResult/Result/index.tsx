import { Button, Card, Classes, Collapse, Divider, HTMLTable } from '@blueprintjs/core'
import moment from 'moment'
import React, { useState } from 'react'
import { Order } from '../../../../types/Order'
import Translated from '@linda/i18n/frontend/components/Translated'
import { makeConfirmOrderRoute } from '../../../../containers/ConfirmOrderRoute'
import { useLocation } from 'wouter'

const Result: React.FC<{ result: Order }> = ({ result }) => {
  const [showPrice, setShowPrice] = useState(false)
  const [, setLocation] = useLocation()

  return (
    <Card style={{ marginBottom: '10px' }}>
      <h4 className={Classes.HEADING}>{result.id}</h4>
      <ul className={Classes.LIST_UNSTYLED + ' ' + Classes.LIST}>
        <li>{result.client.name}</li>
        <li>{result.client.address}</li>
        <br />
        <li>
          Epal {result.epal.dimensions.width}X{result.epal.dimensions.height}, {result.epal.weight} Kg
        </li>
        <br />
        <li>
          Ritiro:
          <ul style={{ paddingLeft: '20px' }}
            className={Classes.LIST_UNSTYLED + ' ' + Classes.LIST}>
            <li>
              Pronto {moment(result.withdrawal.ready).format('DD/MM/YYYY')} dalle{' '}
              {moment(result.withdrawal.ready).format('HH:mm')}
            </li>
            <li>
              Consegna {moment(result.withdrawal.delivery).format('DD/MM/YYYY')} entro{' '}
              {moment(result.withdrawal.delivery).format('HH:mm')}
            </li>
          </ul>
        </li>
        <br />
        <li>
          <Button
            rightIcon={showPrice ? 'caret-up' : 'caret-down'}
            minimal
            onClick={() => setShowPrice(!showPrice)}
          >
            Prezzo {result.prices.reduce((prev, current) => prev + current.amount, 0)}€
          </Button>
          <Collapse isOpen={showPrice}>
            <HTMLTable condensed>
              {result.prices.map((v, index) => (
                <tr key={index}>
                  <td>{v.amount}€</td>
                  <td>{v.reason}</td>
                </tr>
              ))}
            </HTMLTable>
          </Collapse>

          <Divider />

          <Button
            onClick={() => setLocation(
              makeConfirmOrderRoute(encodeURI(result.id.replace(/\//g, '--')))
            )}>
            <Translated message="Scegli ordine" />
          </Button>
        </li>
      </ul>
    </Card>
  )
}

export default Result
