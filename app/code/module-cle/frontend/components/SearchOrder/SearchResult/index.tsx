import React from 'react'
import { useRecoilValue } from 'recoil'
import { orders } from '../../../store/transport/orders/store'
import Result from './Result'

const SearchResult: () => JSX.Element | null = () => {
  const results = useRecoilValue(orders)

  return results.length > 0 ? (
    <>
      {results.map((result) => (
        <Result key={result.id} result={result} />
      ))}
    </>
  ) : null
}

export default SearchResult
