import React, { useEffect } from 'react'
import InlineFormContainer from '../InlineFormContainer'
import { Classes, Icon } from '@blueprintjs/core'
import Translated from '@linda/i18n/frontend/components/Translated'
import SearchResult from './SearchResult'
import ordersData from '../../mock/orderData'
import { useRecoilState } from 'recoil'
import { orders } from '../../store/transport/orders/store'
import { from, to } from '../../store/transport/create/store'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
  title: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexFlow: 'row'
  },
  arrow: {
    alignSelf: 'center',
    marginX: '0 10px'
  }
})

const SearchOrder: () => JSX.Element = () => {
  const [fromValue] = useRecoilState(from)
  const [toValue] = useRecoilState(to)
  const [, setOrders] = useRecoilState(orders)
  useEffect(() => {
    setOrders(ordersData)
  }, [setOrders])
  const { title, arrow } = useStyles()

  return (
    <>
      <InlineFormContainer>
        <div className={title}>
          <h1>{fromValue !== undefined ? `Partenza ${fromValue.municipality}  ` : ''} </h1>
          {fromValue !== undefined && toValue !== undefined
            ? <Icon icon='arrow-right' tagName='h1' className={arrow} /> : ''}
          <h1>{toValue !== undefined ? `  Arrivo ${toValue.municipality}` : ''}</h1>
        </div>
        <h3 className={Classes.HEADING}>
          <Translated message="Sono stati individuati i seguenti ordini" />:
        </h3>
        <SearchResult />
      </InlineFormContainer>
    </>
  )
}

export default SearchOrder
