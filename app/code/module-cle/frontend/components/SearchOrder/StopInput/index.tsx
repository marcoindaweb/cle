import { Button, Divider, FormGroup, HTMLSelect, InputGroup } from '@blueprintjs/core'
import React from 'react'
import { createUseStyles } from 'react-jss'
import Translated from '@linda/i18n/frontend/components/Translated'

const useStyles = createUseStyles({
  main: {
    display: 'grid',
    gridTemplateColumns: '.9fr .1fr',
    gridColumnGap: '10px',
    '&>.inputs': {
      display: 'grid',
      gridTemplateColumns: '1fr',
      gridColumnGap: '10px',
      gridRowGap: '10px'
    },
    '&>.remove': {
      display: 'flex',
      alignItems: 'flex-start',
      justifyContent: 'center'
    }
  }
})

const StopInput: React.FC<{ onRemove?: () => void }> = ({ onRemove }) => {
  const { main } = useStyles()

  return (
    <div className={main}>
      <div className="inputs">
        <InputGroup />
      </div>
      {onRemove && (
        <div className="remove">
          <Button intent="danger" minimal onClick={onRemove} icon="remove" />
        </div>
      )}
    </div>
  )
}

export default StopInput
