import { useFieldArray, UseFormMethods } from 'react-hook-form'
import { Button, Divider, FormGroup } from '@blueprintjs/core'
import Translated from '@linda/i18n/frontend/components/Translated'
import React from 'react'
import StopInput from '../StopInput'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
  addButton: {
    paddingLeft: 0
  }
})

const Stops: React.FC<{ form: UseFormMethods<any> }> = ({ form }) => {
  const { fields, append, remove } = useFieldArray({ control: form.control, name: 'stops' })
  const { addButton } = useStyles()

  return (
    <>
      {fields.map((v, index) => (
        <div key={v.id}>
          <FormGroup inline label={<Translated message={`Fermata ${index + 1}`} />}>
            <StopInput onRemove={() => remove(index)} />
          </FormGroup>
          <Divider />
        </div>
      ))}
      <Button className={addButton} minimal icon="add" onClick={() => append({})}>
        <Translated message="Aggiungi Fermata" />
      </Button>
    </>
  )
}

export default Stops
