// import { useMe } from '@linda/module-auth/frontend/store/auth/actions'
// import LoadingOverlay from '@linda/module-admin/frontend/components/LoadingOverlay'
// import { useLocation } from 'wouter'
import React, { ReactNode, useEffect } from 'react'
// import { makeRoute as makeLoginRoute } from '../../containers/LoginRoute'

const AuthGuard: React.FC<{ children: ReactNode }> = ({ children }) => {
  // const { data, loading } = useMe()
  // const [, setLocation] = useLocation()

  // useEffect(() => {
  //   if (loading) {
  //     return
  //   }

  //   if (!data?.me) {
  //     setLocation(makeLoginRoute())
  //   }
  // }, [loading, data])
  //
  // if (loading) {
  //   return <LoadingOverlay />
  // }

  // return data?.me ? <>{children}</> : null

  return <>{ children }</>
}

export default AuthGuard
