import React from 'react'
import { HTMLSelect, IHTMLSelectProps } from '@blueprintjs/core'
import Translated from '@linda/i18n/frontend/components/Translated'

const MaterialSelect: React.FC<IHTMLSelectProps &
{selectOptions: string[]}> =
    (props) => {
      const { selectOptions } = props
      return (
        <HTMLSelect {...props}>
          {selectOptions.map((o: string) =>
            <Translated message={o}>{(msg) =>
              <option value={o}>{msg}</option>
            }</Translated>)}
        </HTMLSelect>
      )
    }

export default MaterialSelect
