import { useFieldArray, UseFormMethods } from 'react-hook-form'
import { Button } from '@blueprintjs/core'
import React, { useCallback, useState } from 'react'
import MaterialSelect from '../../MaterialSelect'

const MaterialKind: React.FC<{ form: UseFormMethods<any> }> = ({ form }) => {
  const { fields, append, remove } = useFieldArray({ name: 'materialKinds', control: form.control })
  const canAdd = fields.length < 4
  const [newVal, setNewVal] = useState<string | undefined>()
  const onAddClick = useCallback(() => {
    append({ kind: newVal })
    setNewVal(undefined)
  }, [append, newVal, setNewVal])

  return (
    <>
      {fields.map((v, index) => (
        <div key={v.id}>
          <MaterialSelect
            selectOptions={
              ['Scegli il tipo di trasporto', 'Gelo', 'Fresco', 'Secco', 'Container']
            } minimal
            name={`materialKinds[${index}].kind`} elementRef={form.register()} />
          <Button minimal intent="danger" icon="remove" onClick={() => remove(index)} />
        </div>
      ))}
      {canAdd && (
        <div>
          <MaterialSelect
            selectOptions={
              ['Scegli il tipo di trasporto', 'Gelo', 'Fresco', 'Secco', 'Container']
            } minimal
            value={newVal} onChange={(v) => setNewVal(v.target.value)} />
          <Button onClick={onAddClick} minimal intent="primary" icon="add" />
        </div>
      )}
    </>
  )
}

export default MaterialKind
