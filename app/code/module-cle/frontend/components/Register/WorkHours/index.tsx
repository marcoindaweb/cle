import { Button, ControlGroup, HTMLSelect } from '@blueprintjs/core'
import '@blueprintjs/datetime/src/blueprint-datetime.scss'
import React from 'react'
import Translated from '@linda/i18n/frontend/components/Translated'
import { Controller, useFieldArray, UseFormMethods } from 'react-hook-form'
import { TimePicker } from '@blueprintjs/datetime'
import ConnectedFormGroup from '../../ConnectedFormGroup'
import moment from 'moment'
import { createUseStyles } from 'react-jss'

interface WorkHour {
  day: string
  from: string
  to: string
}

const days = ['lunedì', 'martedì', 'mercoledì', 'giovedì', 'venerdì', 'sabato', 'domenica']

const useStyles = createUseStyles({
  main: {
    display: 'flex',
    alignItems: 'flex-start',
    '& > *:not(:last-child)': {
      marginRight: '10px'
    }
  }
})

const WorkHour: React.FC<{ name: string, form: UseFormMethods<any>, onRemove: () => void }> = ({
  form,
  name,
  onRemove
}) => {
  const { main } = useStyles()
  return (
    <div className={main}>
      <ConnectedFormGroup inline label={<Translated message="Giorno" />} form={form} name={`${name}.day`}>
        <HTMLSelect name={`${name}.day`}>
          {days.map((day) => (
            <Translated key={day} message={day}>
              {(msg) => <option value={day}>{msg}</option>}
            </Translated>
          ))}
        </HTMLSelect>
      </ConnectedFormGroup>
      <ConnectedFormGroup inline label={<Translated message="Dalle" />} form={form} name={`${name}.from`}>
        <Controller
          control={form.control}
          name={`${name}.from`}
          render={({ value, onChange }) => (
            <TimePicker value={value} onChange={(v) => onChange(moment(v).toISOString())} />
          )}
        />
      </ConnectedFormGroup>
      <ConnectedFormGroup inline label={<Translated message="Alle" />} form={form} name={`${name}.to`}>
        <Controller
          control={form.control}
          name={`${name}.to`}
          render={({ value, onChange }) => (
            <TimePicker value={value} onChange={(v) => onChange(moment(v).toISOString())} />
          )}
        />
      </ConnectedFormGroup>
      <Button minimal intent="danger" icon="remove" onClick={onRemove} />
    </div>
  )
}

const WorkHours: React.FC<{ form: UseFormMethods<any> }> = ({ form }) => {
  const { fields, append, remove } = useFieldArray({ control: form.control, name: 'workHours' })
  return (
    <>
      {fields.map((field, index) => (
        <WorkHour key={field.id} name={`workHours[${index}]`} form={form} onRemove={() => remove(index)} />
      ))}
      <Button minimal intent="primary" icon="add" onClick={() => append({})}>
        <Translated message="Aggiungi orario" />
      </Button>
    </>
  )
}

export default WorkHours
