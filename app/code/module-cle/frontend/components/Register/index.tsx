import { useForm } from 'react-hook-form'
import React, { useCallback, useState } from 'react'
import Translated from '@linda/i18n/frontend/components/Translated'
import { Button, Card, Checkbox, Classes, FormGroup, Alignment, Alert } from '@blueprintjs/core'
import ControlledInputGroup from '../ControlledInputGroup'
import ConnectedFormGroup from '../ConnectedFormGroup'
import { createUseStyles } from 'react-jss'
import { media } from '../../style/theme'
import MaterialKind from './MaterialKind'
import WorkHours from './WorkHours'
import { makeRoute as makeLoginRoute } from '../../containers/LoginRoute'
import { useLocation } from '@linda/frontend/frontend'

const useStyles = createUseStyles({
  main: {
    [`& .${Classes.LABEL}`]: {
      width: '30%'
    },
    [`& .${Classes.FORM_CONTENT}`]: {
      width: '70%'
    },
    '& .grid-fields': {
      display: 'grid',
      gridTemplateColumns: 'repeat(2, 1fr)',
      gridColumnGap: '10px',
      gridRowGap: '10px'
    },
    '& .terms-and-conditions': {
      maxHeight: '200px',
      overflowY: 'auto'
    },
    [media.desktop]: {
      width: '70%',
      margin: '50px auto'
    },
    '& .register-button': {
      display: 'block',
      marginLeft: 'auto',
      width: '250px',
      textAlign: 'center'
    }
  }
})

const Register = () => {
  const [isAlertOpen, setIsAlertOpen] = useState<boolean>(false)
  const [tmpPsw, setTmpPsw] = useState<string>('')
  const [,setLocation] = useLocation()
  const form = useForm()
  const { main } = useStyles()

  const registerClick = useCallback(() => {
    const vals = form.getValues()
    const companyName = vals.companyName.toString().replace(/\s/g, '')
    const pIva = vals.piva.toString().substring(0, 4)
    const legalP = vals.legal.province.toString().substring(0, 2)
    const operativeN = vals.operative.number.toString()
    setTmpPsw(companyName + pIva + legalP + operativeN)
    setIsAlertOpen(true)
  }, [])

  return (
    <Card className={main}>
      <form>
        <Alert confirmButtonText='Okay'
          isOpen={(isAlertOpen)} onClose={() => {
            setIsAlertOpen(false)
            setLocation(makeLoginRoute())
          }}>
          <h3>La tua password:</h3>
          <h4>{tmpPsw}</h4>
        </Alert>

        <ConnectedFormGroup inline label={<Translated message="Nome Azienda" />}
          form={form} name="companyName">
          <ControlledInputGroup control={form.control} name="companyName" />
        </ConnectedFormGroup>
        <ConnectedFormGroup inline label={<Translated message="P. Iva" />} form={form} name="piva">
          <ControlledInputGroup control={form.control} name="piva" />
        </ConnectedFormGroup>
        {['legal', 'operative'].map((v) => (
          <FormGroup
            key={v}
            inline
            label={<Translated message={v === 'legal' ? 'Sede legale' : 'Sede Operativa'} />}
          >
            <div className="grid-fields">
              <ConnectedFormGroup
                inline
                label={<Translated message="Via" />}
                form={form}
                name={`${v}.address`}
              >
                <ControlledInputGroup control={form.control} name={`${v}.address`} />
              </ConnectedFormGroup>
              <ConnectedFormGroup
                inline
                label={<Translated message="N." />}
                form={form}
                name={`${v}.number`}
              >
                <ControlledInputGroup control={form.control} name={`${v}.number`} />
              </ConnectedFormGroup>
              <ConnectedFormGroup
                inline
                label={<Translated message="Città" />}
                form={form}
                name={`${v}.city`}
              >
                <ControlledInputGroup control={form.control} name={`${v}.city`} />
              </ConnectedFormGroup>
              <ConnectedFormGroup
                inline
                label={<Translated message="Cap" />}
                form={form}
                name={`${v}.cap`}
              >
                <ControlledInputGroup control={form.control} name={`${v}.cap`} />
              </ConnectedFormGroup>
              <ConnectedFormGroup
                inline
                label={<Translated message="Provincia" />}
                form={form}
                name={`${v}.province`}
              >
                <ControlledInputGroup control={form.control} name={`${v}.province`} />
              </ConnectedFormGroup>
            </div>
          </FormGroup>
        ))}
        {/* <ConnectedFormGroup inline label={<Translated message="Password" />} form={form} name="password"> */}
        {/*  <ControlledInputGroup name="password" input={{ type: 'password' }} control={form.control} /> */}
        {/* </ConnectedFormGroup> */}
        {/* <ConnectedFormGroup */}
        {/*  inline */}
        {/*  label={<Translated message="Conferma Password" />} */}
        {/*  form={form} */}
        {/*  name="passwordConfirm" */}
        {/* > */}
        {/*  <ControlledInputGroup name="passwordConfirm" input={{ type: 'password' }} control={form.control} /> */}
        {/* </ConnectedFormGroup> */}
        <ConnectedFormGroup inline label={<Translated message="Pec" />} form={form} name="pec">
          <ControlledInputGroup name="pec" control={form.control} />
        </ConnectedFormGroup>
        <ConnectedFormGroup inline label={<Translated message="Iban" />} form={form} name="iban">
          <ControlledInputGroup name="iban" control={form.control} />
        </ConnectedFormGroup>
        <ConnectedFormGroup
          inline
          label={<Translated message="Firma digitale" />}
          form={form}
          name="digitalSignature"
        >
          <ControlledInputGroup name="digitalSignature" control={form.control} />
        </ConnectedFormGroup>
        <ConnectedFormGroup inline label={<Translated message="Albo Trasportatori" />}
          form={form} name="albo">
          <ControlledInputGroup name="albo" control={form.control} />
        </ConnectedFormGroup>
        <ConnectedFormGroup
          inline
          label={<Translated message="Tipo di materiale" />}
          form={form}
          name="materialKinds"
        >
          <MaterialKind form={form} />
        </ConnectedFormGroup>
        <ConnectedFormGroup
          inline
          label={<Translated message="Orari di lavoro" />}
          form={form}
          name="workHours"
        >
          <WorkHours form={form} />
        </ConnectedFormGroup>
        <ConnectedFormGroup
          inline
          label={<Translated message="Termini e condizioni" />}
          form={form}
          name="termsAndConditions"
        >
          <Card className="terms-and-conditions">
            <p style={{ whiteSpace: 'normal', fontFamily: 'inherit' }}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Nulla facilisi nullam vehicula ipsum a arcu. Erat imperdiet
                            sed euismod nisi porta lorem. Tortor dignissim convallis aenean et. Lectus magna fringilla
                            urna porttitor rhoncus. Sit amet volutpat consequat mauris nunc congue nisi vitae. Tempor
                            orci dapibus ultrices in iaculis. Sodales ut eu sem integer vitae justo eget. Lacus vel
                            facilisis volutpat est velit egestas dui id ornare. At tempor commodo ullamcorper a lacus
                            vestibulum sed. Vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor. Tempor
                            orci eu lobortis elementum nibh tellus molestie nunc non. Ultricies leo integer malesuada
                            nunc vel risus commodo viverra. Porttitor rhoncus dolor purus non enim. Vitae ultricies leo
                            integer malesuada nunc. Cursus metus aliquam eleifend mi in. At auctor urna nunc id cursus
                            metus aliquam eleifend. Lectus vestibulum mattis ullamcorper velit sed ullamcorper morbi.
                            Urna nec tincidunt praesent semper feugiat nibh sed pulvinar. Ullamcorper a lacus vestibulum
                            sed arcu non odio euismod. Pellentesque sit amet porttitor eget dolor morbi. In hendrerit
                            gravida rutrum quisque non tellus orci ac auctor. Laoreet id donec ultrices tincidunt arcu
                            non sodales neque. Dignissim cras tincidunt lobortis feugiat. Venenatis tellus in metus
                            vulputate eu scelerisque felis. Venenatis tellus in metus vulputate. Pharetra et ultrices
                            neque ornare aenean euismod elementum nisi quis. Sagittis orci a scelerisque purus. Sem
                            viverra aliquet eget sit amet tellus cras adipiscing enim. In est ante in nibh. Ullamcorper
                            malesuada proin libero nunc consequat. Sed nisi lacus sed viverra tellus in hac habitasse
                            platea. Vitae purus faucibus ornare suspendisse sed nisi. Odio aenean sed adipiscing diam
                            donec adipiscing tristique risus. Id aliquet risus feugiat in ante. Amet purus gravida quis
                            blandit turpis cursus in hac habitasse. Consectetur lorem donec massa sapien faucibus. In
                            hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Eget dolor morbi non
                            arcu risus. Sapien et ligula ullamcorper malesuada proin libero nunc. Malesuada fames ac
                            turpis egestas integer eget aliquet nibh. Morbi tristique senectus et netus et malesuada
                            fames ac turpis. A lacus vestibulum sed arcu non odio. Elit ullamcorper dignissim cras
                            tincidunt. Tempus imperdiet nulla malesuada pellentesque elit eget. Elementum tempus egestas
                            sed sed risus pretium quam vulputate dignissim. Felis imperdiet proin fermentum leo vel orci
                            porta non. Pretium quam vulputate dignissim suspendisse in. Fames ac turpis egestas sed
                            tempus urna et pharetra pharetra. Tristique senectus et netus et malesuada fames ac turpis
                            egestas. Vitae justo eget magna fermentum. Sed adipiscing diam donec adipiscing tristique
                            risus. Accumsan in nisl nisi scelerisque eu ultrices vitae auctor eu. Turpis massa sed
                            elementum tempus egestas sed sed risus pretium. Quis risus sed vulputate odio ut enim
                            blandit volutpat maecenas. Adipiscing commodo elit at imperdiet dui. Gravida cum sociis
                            natoque penatibus et magnis dis parturient montes. Fusce ut placerat orci nulla pellentesque
                            dignissim enim sit amet. Molestie at elementum eu facilisis sed odio. Nisl suscipit
                            adipiscing bibendum est ultricies integer quis auctor elit. Sagittis nisl rhoncus mattis
                            rhoncus.
            </p>
            <Checkbox>
              <Translated message="Acconsento" />
            </Checkbox>
          </Card>
        </ConnectedFormGroup>
        <ConnectedFormGroup
          inline
          label={<Translated message="Informativa Privacy" />}
          form={form}
          name="privacyPolicy"
        >
          <Card className="terms-and-conditions">
            <p style={{ whiteSpace: 'normal', fontFamily: 'inherit' }}>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                      labore et dolore magna aliqua. Nulla facilisi nullam vehicula ipsum a arcu. Erat imperdiet
                      sed euismod nisi porta lorem. Tortor dignissim convallis aenean et. Lectus magna fringilla
                      urna porttitor rhoncus. Sit amet volutpat consequat mauris nunc congue nisi vitae. Tempor
                      orci dapibus ultrices in iaculis. Sodales ut eu sem integer vitae justo eget. Lacus vel
                      facilisis volutpat est velit egestas dui id ornare. At tempor commodo ullamcorper a lacus
                      vestibulum sed. Vulputate enim nulla aliquet porttitor lacus luctus accumsan tortor. Tempor
                      orci eu lobortis elementum nibh tellus molestie nunc non. Ultricies leo integer malesuada
                      nunc vel risus commodo viverra. Porttitor rhoncus dolor purus non enim. Vitae ultricies leo
                      integer malesuada nunc. Cursus metus aliquam eleifend mi in. At auctor urna nunc id cursus
                      metus aliquam eleifend. Lectus vestibulum mattis ullamcorper velit sed ullamcorper morbi.
                      Urna nec tincidunt praesent semper feugiat nibh sed pulvinar. Ullamcorper a lacus vestibulum
                      sed arcu non odio euismod. Pellentesque sit amet porttitor eget dolor morbi. In hendrerit
                      gravida rutrum quisque non tellus orci ac auctor. Laoreet id donec ultrices tincidunt arcu
                      non sodales neque. Dignissim cras tincidunt lobortis feugiat. Venenatis tellus in metus
                      vulputate eu scelerisque felis. Venenatis tellus in metus vulputate. Pharetra et ultrices
                      neque ornare aenean euismod elementum nisi quis. Sagittis orci a scelerisque purus. Sem
                      viverra aliquet eget sit amet tellus cras adipiscing enim. In est ante in nibh. Ullamcorper
                      malesuada proin libero nunc consequat. Sed nisi lacus sed viverra tellus in hac habitasse
                      platea. Vitae purus faucibus ornare suspendisse sed nisi. Odio aenean sed adipiscing diam
                      donec adipiscing tristique risus. Id aliquet risus feugiat in ante. Amet purus gravida quis
                      blandit turpis cursus in hac habitasse. Consectetur lorem donec massa sapien faucibus. In
                      hac habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Eget dolor morbi non
                      arcu risus. Sapien et ligula ullamcorper malesuada proin libero nunc. Malesuada fames ac
                      turpis egestas integer eget aliquet nibh. Morbi tristique senectus et netus et malesuada
                      fames ac turpis. A lacus vestibulum sed arcu non odio. Elit ullamcorper dignissim cras
                      tincidunt. Tempus imperdiet nulla malesuada pellentesque elit eget. Elementum tempus egestas
                      sed sed risus pretium quam vulputate dignissim. Felis imperdiet proin fermentum leo vel orci
                      porta non. Pretium quam vulputate dignissim suspendisse in. Fames ac turpis egestas sed
                      tempus urna et pharetra pharetra. Tristique senectus et netus et malesuada fames ac turpis
                      egestas. Vitae justo eget magna fermentum. Sed adipiscing diam donec adipiscing tristique
                      risus. Accumsan in nisl nisi scelerisque eu ultrices vitae auctor eu. Turpis massa sed
                      elementum tempus egestas sed sed risus pretium. Quis risus sed vulputate odio ut enim
                      blandit volutpat maecenas. Adipiscing commodo elit at imperdiet dui. Gravida cum sociis
                      natoque penatibus et magnis dis parturient montes. Fusce ut placerat orci nulla pellentesque
                      dignissim enim sit amet. Molestie at elementum eu facilisis sed odio. Nisl suscipit
                      adipiscing bibendum est ultricies integer quis auctor elit. Sagittis nisl rhoncus mattis
                      rhoncus.
            </p>
            <Checkbox>
              <Translated message="Acconsento" />
            </Checkbox>
          </Card>
        </ConnectedFormGroup>
        <Button alignText={Alignment.CENTER} className="register-button"
          onClick={() => registerClick()}>
          <Translated message="Crea Password" />
        </Button>
      </form>
    </Card>
  )
}

export default Register
