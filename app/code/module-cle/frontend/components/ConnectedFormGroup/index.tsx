import React from 'react'
import { UseFormMethods } from 'react-hook-form'
import { FormGroup, IFormGroupProps } from '@blueprintjs/core'
import Translated from '@linda/i18n/frontend/components/Translated'

const ConnectedFormGroup: React.FC<{ form: UseFormMethods<any>, name: string } & IFormGroupProps> =
    (props) => {
      const { form } = props

      return (
        <FormGroup
          {...props}
          intent={form.errors[props.name] ? 'danger' : 'none'}
          helperText={
            form.errors[props.name]?.message
              ? <Translated message={form.errors[props.name].message} /> : null
          }
        >
          {props.children}
        </FormGroup>
      )
    }

export default ConnectedFormGroup
