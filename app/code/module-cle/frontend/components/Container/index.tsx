import { Card, Classes, ICardProps } from '@blueprintjs/core'
import React from 'react'
import { createUseStyles } from 'react-jss'
import { media } from '../../style/theme'

const useStyles = createUseStyles({
  main: {
    [media.desktop]: {
      width: '70%',
      margin: '0 auto'
    }
  }
})

const Container: React.FC<ICardProps> = (props) => {
  const { main } = useStyles()

  return (
    <Card {...props} className={main + ' ' + props.className}>
      {props.children}
    </Card>
  )
}

export default Container
