import React, { memo } from 'react'
import { createUseStyles } from 'react-jss'
import { Card, Classes, ICardProps } from '@blueprintjs/core'
import { media } from '../../style/theme'

const useStyles = createUseStyles({
  main: {
    [`& .${Classes.LABEL}`]: {
      width: '30%'
    },
    [`& .${Classes.FORM_CONTENT}`]: {
      width: '70%'
    },
    [media.desktop]: {
      width: '70%',
      margin: '50px auto'
    },
    '& button[type="submit"]': {
      display: 'block',
      marginLeft: 'auto',
      width: '250px',
      textAlign: 'center'
    }
  }
})

const InlineFormContainer: React.FC<ICardProps> = (props) => {
  const { main } = useStyles()
  return (
    <Card {...props} className={main + ' ' + props.className}>
      {props.children}
    </Card>
  )
}

export default memo(InlineFormContainer)
