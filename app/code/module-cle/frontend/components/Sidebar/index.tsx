import { Classes, Drawer } from '@blueprintjs/core'
import React from 'react'
import { useRecoilState } from 'recoil'
import { showSidebar } from '../../store/ui/store'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
  nav: {
    padding: '10px'
  }
})

const Sidebar = () => {
  const [show, setShow] = useRecoilState(showSidebar)
  const { nav } = useStyles()
  return (
    <Drawer size={Drawer.SIZE_SMALL} isOpen={show} onClose={() => setShow(false)}>
      <nav className={nav}>
        <ul className={Classes.LIST + ' ' + Classes.LIST_UNSTYLED}>
          <li>Home</li>
          <li>Chi Siamo</li>
          <li>Nostri servizi</li>
          <li>Trasportatori Affiliati</li>
          <li>Privacy</li>
          <li>Contatti</li>
        </ul>
      </nav>
    </Drawer>
  )
}

export default Sidebar
