import React, { useCallback, useState } from 'react'
import { Button, Classes, FormGroup, InputGroup, Radio, RadioGroup, Switch } from '@blueprintjs/core'
import MaterialSelect from '../MaterialSelect'
import Translated from '@linda/i18n/frontend/components/Translated'
import { createUseStyles } from 'react-jss'
import { useRecoilState } from 'recoil'
import {
  kind,
  from,
  to,
  palletToReturn,
  stopsStatus,
  TransportKind,
  Location,
  transportKinds,
  transportFrom,
  transportTo,
  materialType,
  materialTypes,
  palletDimension,
  palletDimensions,
  weight,
  height, deliver
} from '../../store/transport/create/store'
import { useLocation } from 'wouter'
import { makeTransportRoute } from '../../containers/SearchOrderRoute'
import { makeCreateOrderRoute } from '../../containers/CreateOrderRoute'
import useTranslate from '@linda/i18n/frontend/hooks/useTranslate'
import { orders } from '../../store/transport/orders/store'
import ordersData from '../../mock/orderData'
import Stops from '../SearchOrder/Stops'
import { useForm } from 'react-hook-form'

const useStyle = createUseStyles({
  main: {
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    marginTop: '50px'
  },
  title: {
    color: '#6BCF2C'
  },
  submitButton: {
    backgroundColor: '#6BCF2C !important'
  },
  formGroup: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'flex-end'
  }
})

const Home: () => JSX.Element = () => {
  const { main, title, submitButton, formGroup, buttonContainer } = useStyle()
  const [kindValue, setKind] = useRecoilState(kind)
  const [fromValue, setFrom] = useRecoilState(from)
  const [toValue, setTo] = useRecoilState(to)
  const [stopsStatusValue, setStopsStatus] = useRecoilState(stopsStatus)
  const [palletToReturnValue, setPalletToReturn] = useRecoilState(palletToReturn)
  const [transportKindsValue] = useRecoilState(transportKinds)
  const [transportFromValue] = useRecoilState(transportFrom)
  const [transportToValue] = useRecoilState(transportTo)
  const [materialTypeValue, setMaterialType] = useRecoilState(materialType)
  const [materialTypesValue] = useRecoilState(materialTypes)
  const [palletDimensionValue, setPalletDimension] = useRecoilState(palletDimension)
  const [weightValue, setWeight] = useRecoilState(weight)
  const [heightValue, setHeight] = useRecoilState(height)
  const [deliverValue, setDeliver] = useRecoilState(deliver)
  const [palletDimensionsValues] = useRecoilState(palletDimensions)
  const [, setLocation] = useLocation()
  const [transport, setTransport] = useState(true)
  const [, setOrders] = useRecoilState(orders)
  const form = useForm()

  const translate = useTranslate()

  const onChangeType = useCallback((v: TransportKind | undefined) => {
    if (v !== undefined) {
      setKind(v)
    }
  }, [setKind])
  const onChangeFrom = useCallback((v: string | undefined) => {
    if (v !== undefined) {
      const setFromEl: Location = {
        region: 'Piemonte',
        province: 'Milano',
        cap: '20100',
        municipality: v
      }
      setFrom(setFromEl)
    }
  }, [setFrom])
  const onChangeTo = useCallback((v: string | undefined) => {
    if (v !== undefined) {
      const setToEl: Location = {
        region: 'Veneto',
        province: 'Padova',
        cap: '35100',
        municipality: v
      }
      setTo(setToEl)
    }
  }, [setTo])
  const onChangeStopsStatus = useCallback((v: string) => {
    setStopsStatus(v === 'yes')
  }, [setStopsStatus])
  const onChangePalletToReturn = useCallback((v: string) => {
    setPalletToReturn(v === 'yes')
  }, [setPalletToReturn])
  const goToNextPage = useCallback(() => {
    onSearchClick()
    setLocation(transport ? makeTransportRoute() : makeCreateOrderRoute())
  }, [transport])

  const onChangePalletDimension = useCallback((v: string) => {
    setPalletDimension(v)
  }, [setPalletDimension])

  const onSearchClick = useCallback(() => {
    setOrders(ordersData)
  }, [setOrders])

  const onProductTypeChange = useCallback((v: string) => {
    setMaterialType(v)
  }, [setMaterialType])

  const onChangeWeight = useCallback((v: string) => {
    setWeight(v)
  }, [setWeight])
  const onChangeHeight = useCallback((v: string) => {
    setHeight(v)
  }, [setHeight])
  const onChangeDeliver = useCallback((v: string) => {
    setDeliver(v)
  }, [setDeliver])

  return (
    <>
      <Switch
        checked={transport}
        onChange={() => setTransport(!transport)}
        innerLabel="Spedizione"
        innerLabelChecked="Trasporto"
      />
      <div className={main}>
        {transport ? (
          <div>
            <h2 className={Classes.HEADING + ' ' + title}>FLASH trasporti</h2>
            <h3>Cosa desideri trasportare?</h3>
            <br/>
            <FormGroup className={formGroup} inline
              label={<Translated message="Tipo trasporto" />}>
              <MaterialSelect
                value={kindValue !== undefined ? kindValue : 'Scegli il tipo di trasporto'}
                selectOptions={['Scegli il tipo di trasporto', ...transportKindsValue]}
                onChange={(e) =>
                  onChangeType(e.target.value as TransportKind | undefined)}
              />
            </FormGroup>
            <FormGroup className={formGroup} inline
              label={<Translated message="Da dove si parte" />}>
              <MaterialSelect
                value={fromValue !== undefined
                  ? fromValue.municipality : 'Scegli il punto di partenza'}
                selectOptions={
                  ['Scegli il punto di partenza', ...transportFromValue]
                }
                onChange={(e) =>
                  onChangeFrom(e.target.value as TransportKind | undefined)} />
            </FormGroup>
            <FormGroup className={formGroup} inline
              label={<Translated message="Dove si va" />}>
              <MaterialSelect
                value={toValue !== undefined ? toValue.municipality : 'Scegli il punto di arrivo'}
                selectOptions={
                  ['Scegli il punto di arrivo', ...transportToValue]
                }
                onChange={(e) =>
                  onChangeTo(e.target.value as TransportKind | undefined)} />
            </FormGroup>
            <RadioGroup className={formGroup} inline
              selectedValue={stopsStatusValue ? 'yes' : 'no'}
              label={<Translated message="Altre fermate" />} onChange={(event) =>
                onChangeStopsStatus(event.currentTarget.value)}>
              <Radio label={translate('si')} value="yes" />
              <Radio label={translate('no')} value="no" />
            </RadioGroup>
            {stopsStatusValue ? <Stops form={form}/> : <></>}
            <RadioGroup className={formGroup} inline
              selectedValue={palletToReturnValue ? 'yes' : 'no'}
              label={<b><Translated message="Bancale EPAL a rendere" /></b>} onChange={(event) =>
                onChangePalletToReturn(event.currentTarget.value)}>
              <Radio label={translate('si')} value="yes" />
              <Radio label={translate('no')} value="no" />
            </RadioGroup>

            <div className={buttonContainer}>
              <Button className={submitButton}
                onClick={() => goToNextPage()} text="VAI" />
            </div>
          </div>
        ) : (
          <div>
            <h2 className={Classes.HEADING}>La Mia Ceramica cosa desideri spedire:</h2>

            <FormGroup className={formGroup} inline
              label={<Translated message="Tipo di materiale:"/>}>
              <MaterialSelect
                value={materialTypeValue !== undefined ? materialTypeValue
                  : 'Scegli il tipo di materiale'}
                selectOptions={
                  ['Scegli il tipo di materiale', ...materialTypesValue]
                }
                onChange={(e) =>
                  onProductTypeChange(e.target.value)} />
            </FormGroup>
            <FormGroup className={formGroup} inline
              label={<Translated message="Da dove si parte" />}>
              <MaterialSelect
                value={fromValue !== undefined
                  ? fromValue.municipality : 'Scegli il punto di partenza'}
                selectOptions={
                  ['Scegli il punto di partenza', ...transportFromValue]
                }
                onChange={(e) =>
                  onChangeFrom(e.target.value as TransportKind | undefined)} />
            </FormGroup>
            <FormGroup className={formGroup} inline
              label={<Translated message="Dove si va" />}>
              <MaterialSelect
                value={toValue !== undefined ? toValue.municipality : 'Scegli il punto di arrivo'}
                selectOptions={
                  ['Scegli il punto di arrivo', ...transportToValue]
                }
                onChange={(e) =>
                  onChangeTo(e.target.value as TransportKind | undefined)} />
            </FormGroup>
            <RadioGroup className={formGroup} inline
              selectedValue={palletToReturnValue ? 'yes' : 'no'}
              label={<Translated message="EPAL a rendere" />} onChange={(event) =>
                onChangePalletToReturn(event.currentTarget.value)}>
              <Radio label={translate('si')} value="yes" />
              <Radio label={translate('no')} value="no" />
            </RadioGroup>
            <RadioGroup className={formGroup} inline
              selectedValue={palletDimensionValue}
              label={<Translated message="Dimensione bancale" />} onChange={(event) =>
                onChangePalletDimension(event.currentTarget.value)}>
              {palletDimensionsValues.map(pv =>
                <Radio label={translate(pv)} value={pv} />)}
            </RadioGroup>
            <FormGroup className={formGroup} inline
              label={<Translated message="Peso" />}>
              <InputGroup type="number" defaultValue={weightValue}
                onChange={(e) => { onChangeWeight(e.target.value) }} />
            </FormGroup>
            <FormGroup className={formGroup} inline
              label={<Translated message="Altezza" />}>
              <InputGroup type="number" defaultValue={heightValue}
                onChange={(e) => { onChangeHeight(e.target.value) }} />
            </FormGroup>
            <FormGroup className={formGroup} inline
              label={<Translated message="Giorno di consegna" />}>
              <InputGroup type="date" defaultValue={deliverValue}
                onChange={(e) => { onChangeDeliver(e.target.value) }} />
            </FormGroup>
            <div className={buttonContainer}>
              <Button className={submitButton}
                onClick={() => goToNextPage()} text="VAI" />
            </div>
          </div>
        )}
      </div>
    </>
  )
}

export default Home
