import React from 'react'
import Navbar from '../Navbar'
import Sidebar from '../Sidebar'
import '../../style/index.scss'

const Layout: React.FC = ({ children }) => {
  return (
    <>
      <Navbar />
      <Sidebar />
      {children}
    </>
  )
}

export default Layout
