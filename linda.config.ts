/*
 * Copyright (c) 2020. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck

import { Configuration } from '@linda/linda'

const config: Configuration = {
    baseDir: __dirname,
    debug: true,
    logging: {
        requests: true,
    },
    http: {
        port: 80,
    },
    db: {
        uri: 'mongodb://root:password@localhost:27017',
        database: 'linda',
    },
}

export default config
